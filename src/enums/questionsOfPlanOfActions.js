export default {
    "Strategic_Sphere": "0233c058-680d-4ebe-9948-0d789b6acb44",
    "Definite_Direction": "14f3028d-d6c8-4d15-b695-06ea3932aa0f",
    "Concrete_Commencement": "690be219-fc23-4e1e-b286-e7e3f41fd1a2",
    "Purposeful_Planning": "6512a508-d42b-41d3-a079-67b22bcc2f7d",
    "Growth_Frame": "014b2573-507f-4130-8d0a-665b17b2e38e",
    "Sales_Action_Plan": "da150611-4540-4898-892e-379fa57193f6",
    "Work_Action_Plan": "42c13edf-92fd-403a-96c2-9a6b40117d38",
    "Corrective_Action_Plan": "df91273e-7a51-47ef-98b8-7f8e24eb1a8c"
}