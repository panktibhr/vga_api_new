export default {
    TYPE1 : "Exceptional",
    TYPE2 : "Marginal",
    TYPE3 : "Satisfactory",
    TYPE4 : "Unsatisfactory"
}