export default {
    OrganizationDiagnosis : "dbf80e54-7c58-4396-a750-c4208362c0db" ,
    CompetencyMapping: "c946b957-1314-4565-974a-2507d489fab6",
    PerformanceEvaluation: "292e86d2-8fb3-4319-a9ab-54c205b25a3f",
    ValuegrowthMatrix: "3009ac0b-04b0-4293-ae60-a9e40c960651",
    PlanOfActionAndRecommendations: "85d8fe9b-5be2-40bb-b069-eadfd386a908"
}