export default {
    CREATEDSTATUS :'Created',
    AUDITINGSTATUS :'Auditing',
    SUBMITTEDSTATUS :'Submitted',
    REVIEWINGSTATUS :'Reviewing',
    REVIEWEDSTATUS :'Reviewed',
    SUBMITEDTOCLIENTSTATUS :'Submited to Client',
}