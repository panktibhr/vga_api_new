const { Model } = require('objection');
const Knex = require('knex');

const knex = Knex({
    client: 'pg',
    useNullAsDefault: true,
    searchPath: ['lookup', 'entity', 'public', 'hint_plan'],
    connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME,
    },
    pool: {
        max: 7,
        min: 3,
        acquireTimeout: 60 * 1000
    },
    debug: process.env.DB_DEBUG === 'true',
});
knex.on('query', function(queryData){
    console.log(queryData);
})
Model.knex(knex);
export default Model;
