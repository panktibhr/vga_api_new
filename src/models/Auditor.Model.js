const Model = require('./model').default;

export default class Auditor extends Model {
    static get tableName() {
        return 'auditor';
    }

    static get idColumn() {
        return 'id';
    }
}