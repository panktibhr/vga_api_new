const Model = require('../models/model').default;
const auditor = require('./Auditor.Model').default;
const consultant = require('./consultant.Model').default;
const Account = require("./../models/Account.Model").default;
export default class Clients extends Model {

    static get tableName() {
        return 'clients';
    }
    static get idColumn() {
        return 'id';
    }
    static get relationMappings() {
        return {
            consultants: {
                relation: Model.BelongsToOneRelation,
                modelClass: consultant,
                join: {
                    from: 'consultant.id',
                    to: 'clients.consultant'
                }
            },
            parentAccount: {
                relation: Model.BelongsToOneRelation,
                modelClass: Account,
                join: {
                    from: 'accounts.id',
                    to: 'clients.parent_account_id',
                }
            },
            auditors: {
                relation: Model.BelongsToOneRelation,
                modelClass: auditor,
                join: {
                    from: 'auditor.id',
                    to: 'clients.auditor'
                }
            }
        }
    }

}