const Model = require('./model').default;
const Consultant = require('../models/consultant.Model').default;
const Client = require('../models/client.Model').default;
export default class Audit extends Model {
  static get tableName() {
    return 'vga_audits';
  }

  static get idColumn() {
    return 'id';
  }

  static get relationMappings() {
    return {
      consultant: {
        relation: Model.BelongsToOneRelation,
        modelClass: Consultant,
        join: {
          from: 'vga_audits.consultant_id',
          to: 'consultant.id',
        },
      },
      client: {
        relation: Model.BelongsToOneRelation,
        modelClass: Client,
        join: {
          from: 'vga_audits.client_id',
          to: 'clients.id',
        },
      },
    };
  }
}
