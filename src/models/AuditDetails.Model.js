const Model = require('./model').default;
const audit = require('../models/Audit.Model').default;
const Client = require('../models/client.Model').default;
const section = require('../models/Section.Model').default;

export default class Audit extends Model {
  static get tableName() {
    return 'vga_audit_details';
  }

  static get idColumn() {
    return 'id';
  }

  static get relationMappings() {
    return {
      audit: {
        relation: Model.BelongsToOneRelation,
        modelClass: audit,
        join: {
          from: 'vga_audit_details.vga_audit_id',
          to: 'vga_audits.id',
        }
      },
      client: {
        relation: Model.BelongsToOneRelation,
        modelClass: Client,
        join: {
          from: 'vga_audits.client_id',
          to: 'clients.id',
        },
      },
        sections:{
          relation: Model.BelongsToOneRelation,
          modelClass: section,
          join: {
            from:'vga_audit_details.section_id',
            to:'section_master.id'
          }
       },
    } 
 }
}