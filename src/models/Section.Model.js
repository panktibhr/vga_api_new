const Model = require('./model').default;

export default class Section extends Model {
    static get tableName() {
        return 'section_master';
    }

    static get idColumn() {
        return 'id';
    }
}