const Model = require('./model').default;
const Section = require('../models/Section.Model').default;
const SubSection = require('../models/SubSection.Model').default;
const reportAnswerValues = require('../models/ReportAnswerValue.Model').default;
export default class Question extends Model {
    static get tableName() {
        return 'question_master';
    }

    static get idColumn() {
        return 'id';
    }

    static get relationMappings() {
        return {
            section: {
                relation: Model.BelongsToOneRelation,
                modelClass: Section,
                join: {
                    from: 'section_master.id',
                    to: 'question_master.section_id',
                }
            },
            sub_section: {
                relation: Model.BelongsToOneRelation,
                modelClass: SubSection,
                join: {
                    from: 'sub_section_master.id',
                    to: 'question_master.sub_section_id'
                }
            },
            reportAnswers: {
                relation: Model.BelongsToOneRelation,
                modelClass: reportAnswerValues,
                join: {
                    from: 'report_answer_values.question_id',
                    to: 'question_master.id'
                }
            }
        }
    }
}