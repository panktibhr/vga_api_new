const Model = require('./model').default;
const client = require('./client.Model').default;
const designations = require('./designation.Model').default;
const departments = require('./department.Model').default;

export default class clientsEmployee extends Model {

    static get tableName() {
        return 'client_employees';
    }
    static get idColumn() {
        return 'id';
    }
    static get relationMappings() {
        return {
            clients: {
                relation: Model.BelongsToOneRelation,
                modelClass: client,
                join: {
                    from: 'client.id',
                    to: 'client_employees.client_id'
                }
            },
            Departments: {
                relation: Model.BelongsToOneRelation,
                modelClass: departments,
                join: {
                    from: 'departments.id',
                    to: 'client_employees.department'
                }
            },
            Designations: {
                relation: Model.BelongsToOneRelation,
                modelClass: designations,
                join: {
                    from: 'designation.id',
                    to: 'client_employees.designation'
                }
            }
        }
    }

}