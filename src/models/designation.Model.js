const Model = require('./model').default;

export default class Designation extends Model {
    static get tableName() {
        return 'designation';
    }

    static get idColumn() {
        return 'id';
    }
}