const Model = require('./model').default;
const clientsEmployee = require('../models/clientsEmployee.Model').default;
const employeePerformance = require('../models/EmployeePerformance.Model').default;

export default class employeePerformanceUsersDetails extends Model {
    static get tableName() {
        return 'employee_performance_users_details';
    }

    static get idColumn() {
        return 'id';
    }

    static get relationMappings() {
        return {
            clientEmployee: {
                relation: Model.BelongsToOneRelation,
                modelClass: clientsEmployee,
                join: {
                    from: 'employee_performance_users_details.employee_id',
                    to: 'client_employees.id',
                }
            },
            employeePerformance : {
                relation: Model.BelongsToOneRelation,
                modelClass: employeePerformance,
                join:{
                    from:'employee_performance_users_details.employee_performance_questions_id',
                    to: 'employee_performance_questions.id',
                }
            }
        }
    }
}