const Model = require('./model').default;
const Section = require('../models/Section.Model').default;
export default class SubSection extends Model {
    static get tableName() {
        return 'sub_section_master';
    }

    static get idColumn() {
        return 'id';
    }
    static get relationMappings() {
        return {
            section: {
                relation: Model.BelongsToOneRelation,
                modelClass: Section,
                join: {
                    from: 'section_master.id',
                    to: 'sub_section_master.section_id',
                }
            }

        }
    }
}