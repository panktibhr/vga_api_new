const Model = require('./model').default;

export default class State extends Model {
    static get tableName() {
        return 'states';
    }

    static get idColumn() {
        return 'id';
    }
}