const Model = require("./model").default;

export default class Account extends Model {
  static get tableName() {
    return "accounts";
  }

  static get idColumn() {
    return "id";
  }

  static get relationMappings() {
    return {
      parent: {
        relation: Model.BelongsToOneRelation,
        modelClass: Account,
        join: {
          from: 'accounts.parent_account_id',
          to: 'accounts.id',
        }
      },
    }
  }
}
