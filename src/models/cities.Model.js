const Model = require('./model').default;

export default class Cities extends Model {
    static get tableName() {
        return 'cities';
    }

    static get idColumn() {
        return 'id';
    }
}