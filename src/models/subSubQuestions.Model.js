const Model = require("./model").default;

export default class SubSubQuestions extends Model {
  static get tableName() {
    return "sub_sub_questions";
  }

  static get idColumn() {
    return "id";
  }
}