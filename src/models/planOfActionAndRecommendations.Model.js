const Model = require('./model').default;

export default class PlanOfActionAndRecommendations extends Model {
    static get tableName(){
        return 'plan_of_action_and_recommendations';
    }

    static get idColumn(){
        return 'id';
    }
}