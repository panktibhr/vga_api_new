const Model = require('./model').default;
const designations = require('./designation.Model').default;
const departments = require('./department.Model').default;
export default class Employee extends Model {
    static get tableName() {
        return 'employee';
    }

    static get idColumn() {
        return 'id';
    }
    static get relationMappings() {
        return {
            Departments: {
                relation: Model.BelongsToOneRelation,
                modelClass: departments,
                join: {
                    from: 'departments.id',
                    to: 'employee.department'
                }
            },
            Designations: {
                relation: Model.BelongsToOneRelation,
                modelClass: designations,
                join: {
                    from: 'designation.id',
                    to: 'employee.designation'
                }
            }
        }
    }
}