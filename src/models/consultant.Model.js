const Model = require('../models/model').default;


export default class Consultant extends Model {

    static get tableName() {
        return 'consultant';
    }
    static get idColumn() {
        return 'id';
    }

}