const Model = require('./model').default;

export default class UserType extends Model {
    static get tableName() {
        return 'role';
    }

    static get idColumn() {
        return 'id';
    }
}