const Model = require('./model').default;
const clientEmployee = require('../models/clientsEmployee.Model').default;
const employeePerformance = require('../models/EmployeePerformance.Model').default;

export default class AnswerVlaues extends Model {
    static get tableName() {
        return 'report_answer_values';
    }

    static get idColumn() {
        return 'id';
    }

    static get relationMappings() {
        return {
            clientEmployee: {
                relation: Model.BelongsToOneRelation,
                modelClass: clientEmployee,
                join: {
                    from: 'report_answer_values.employee_id',
                    to: 'client_employees.id',
                }
            },
            employeePerformance : {
                relation: Model.BelongsToOneRelation,
                modelClass: employeePerformance,
                join:{
                    from:'report_answer_values.employee_performance_questions_id',
                    to: 'employee_performance_questions.id',
                }
            }
        }
    }
};