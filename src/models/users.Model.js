const Model = require('../models/model').default;
const UserType = require('./usersType.Model').default;
const Account = require("./../models/Account.Model").default;

export default class Users extends Model {
    static get tableName() {
        return 'users';
    }
    static get idColumn() {
        return 'id';
    }

    static get relationMappings() {
        return {
            parentAccount: {
                relation: Model.BelongsToOneRelation,
                modelClass: Account,
                join: {
                    from: 'accounts.id',
                    to: 'users.parent_account_id',
                }
            },
            role: {
                relation: Model.BelongsToOneRelation,
                modelClass: UserType,
                join: {
                    from: 'role.id',
                    to: 'users.role_id'
                }
            }
        }
    }
}

