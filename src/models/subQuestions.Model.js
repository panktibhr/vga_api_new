const Model = require("./model").default;

export default class SubQuestions extends Model {
  static get tableName() {
    return "sub_questions";
  }

  static get idColumn() {
    return "id";
  }
}