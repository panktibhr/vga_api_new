const Model = require('./model').default;

export default class Countries extends Model {
    static get tableName() {
        return 'countries';
    }

    static get idColumn() {
        return 'id';
    }
}