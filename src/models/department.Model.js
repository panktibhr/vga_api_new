const Model = require('./model').default;

export default class Departments extends Model {
    static get tableName() {
        return 'departments';
    }

    static get idColumn() {
        return 'id';
    }
}