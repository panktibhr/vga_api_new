const Model = require("./model").default;

export default class  UserRoleMapping extends Model {

    static get tableName() {
        return 'users_role_details'
    }

    static get idColumn() {
        return 'id'
    }
}