const Model = require('./model').default;

export default class EmployeePerformance extends Model {
    static get tableName() {
        return 'employee_performance_questions';
    }

    static get idColumn() {
        return 'id';
    }

}