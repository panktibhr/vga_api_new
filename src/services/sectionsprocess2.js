import {getSubSectionBySectionId} from '../controllers/QuestionController'
import {updateAuditReportStatus} from '../controllers/QuestionController'
import {checkcolorsbyvalues} from '../controllers/QuestionController'
import {getQuestionsBySectionId} from '../controllers/QuestionController'
import {getReportAnswers} from '../controllers/QuestionController'
import {getSubQuestions} from '../controllers/QuestionController'
import {getQuestionsBySubSectionIdAndSectionId} from '../controllers/QuestionController'
import {getPlanOfActionAnswers} from '../controllers/QuestionController'
import {getReportAnswersOfCompetencyMapping} from '../controllers/QuestionController'
import sectionEnum from '../enums/sectionEnum';
const section = require('../models/Section.Model').default;
const clientsEmployee = require('../models/clientsEmployee.Model').default;

export const sectionProcessing2 = async (sectionId, auditNo, clientId) =>{
    let response = {};
    let successcount;
    let trueArray = [];
    let falseArray = [];
  try{
    const Section = await section.query().where("id", sectionId).andWhere("is_deleted", false).orderBy('sequence_number');
    response.sections = Section;
    for (let i = 0; i < response.sections.length; i++) {
        let _subsections = await getSubSectionBySectionId(response.sections[i].id);
        if (_subsections && _subsections.length > 0) {  
            response.sections[i].subSection = _subsections;
            
            trueArray = [];
            falseArray = [];
            for (let j = 0; j < response.sections[i].subSection.length; j++) {
                if(response.sections[i].subSection[j].sequence_number === 1){
                    let _questions = await getQuestionsBySubSectionIdAndSectionId(response.sections[i].id, response.sections[i].subSection[j].id);
                    trueArray = [];
                    falseArray = [];
                    if (_questions && _questions.length > 0) {
                        response.sections[i].subSection[j].questions = _questions;

                        for(let k = 0; k < response.sections[i].subSection[j].questions.length; k++){
                            let _answers = await getReportAnswers(response.sections[i].subSection[j].questions[k].id, auditNo);
                            console.log(_answers);
                            if (_answers && _answers.length > 0) {
                                response.sections[i].subSection[j].questions[k].reportanswers = _answers;
                                successcount = true;
                                trueArray.push(successcount);
                            } else {
                                response.sections[i].subSection[j].questions[k].reportanswers = [];
                                successcount = false;
                                falseArray.push(successcount)
                            }
                            response.sections[i].subSection[j].questions[k].successcount = successcount;
                        }
                    }

                } else if(response.sections[i].subSection[j].sequence_number === 2){

                    let _questions = await getQuestionsBySubSectionIdAndSectionId(response.sections[i].id, response.sections[i].subSection[j].id);
                    trueArray = [];
                    falseArray = [];
                    if (_questions && _questions.length > 0) {
                        response.sections[i].subSection[j].questions = _questions;
    
                        let topLevelQuestions = [];
                        let managerailLevelQuestions = [];
                        let executiveLevelQuestions = [];

                        for (let k = 0; k < response.sections[i].subSection[j].questions.length; k++) {
                            if(response.sections[i].subSection[j].questions[k].question_type === 6){
                                topLevelQuestions.push(response.sections[i].subSection[j].questions[k]);
                            } else if(response.sections[i].subSection[j].questions[k].question_type === 7){
                                managerailLevelQuestions.push(response.sections[i].subSection[j].questions[k]);
                            } else if(response.sections[i].subSection[j].questions[k].question_type === 8){
                                executiveLevelQuestions.push(response.sections[i].subSection[j].questions[k]);
                            }
                        }
                        
                        const clientEmployees = await clientsEmployee.query().where('is_deleted', false).andWhere('client_id', clientId);
                        let employeesOfTopLevel = [];
                        let employeesOfManagerialLevel = [];
                        let employeesOfExecutiveLevel = [];
                        for(let c = 0; c < clientEmployees.length; c++){
                            if(clientEmployees[c].employee_level === 'Top Level'){
                                employeesOfTopLevel.push(clientEmployees[c]);
                                response.sections[i].topLevel = employeesOfTopLevel;
                            } else if(clientEmployees[c].employee_level === 'Managerial Level'){
                                employeesOfManagerialLevel.push(clientEmployees[c])
                                response.sections[i].managerialLevel = employeesOfManagerialLevel;
                            } else if(clientEmployees[c].employee_level === 'Executive Level'){
                                employeesOfExecutiveLevel.push(clientEmployees[c]);
                                response.sections[i].executiveLevel = employeesOfExecutiveLevel;
                            }
                        }  

                        // let _answers;
                        for(let t = 0; t < response.sections[i].topLevel.length; t++){
                            response.sections[i].topLevel[t].questions = topLevelQuestions;
                            // for(let q = 0; q < topLevelQuestions.length; q++){
                            //     _answers = await getReportAnswersOfCompetencyMapping(response.sections[i].topLevel[t].questions[q].id, auditNo, response.sections[i].topLevel[t].id);
                            //     console.log(_answers);

                            //     if (_answers && _answers.length > 0) {
                            //         _answers.map((ans)=>{
                            //             if(ans.client_employee_id === response.sections[i].topLevel[t].id){
                            //                 response.sections[i].topLevel[t].questions[q].reportanswers = _answers;
                            //                 successcount = true;
                            //                 trueArray.push(successcount);
                            //             } else{
                            //                 response.sections[i].topLevel[t].questions[q].reportanswers = [];
                            //                 successcount = false;
                            //                 falseArray.push(successcount)
                            //             }
                            //         })
                            //     } 
                            //     else {
                            //         response.sections[i].topLevel[t].questions[q].reportanswers = [];
                            //         successcount = false;
                            //         falseArray.push(successcount)
                            //     }
                                
                            //     response.sections[i].topLevel[t].questions[q].successcount = successcount;
                            // }
                        } 

                        // response.sections[i].managerialLevel.map((_, index) => {
                        //     response.sections[i].managerialLevel[index].questions = managerailLevelQuestions;
                        //     response.sections[i].managerialLevel[index].questions.map(async(_, index2) => {
                        //         let dddd = await getReportAnswersOfCompetencyMapping(response.sections[i].managerialLevel[index].questions[index2].id, auditNo, response.sections[i].managerialLevel[index].id);
                        //         response.sections[i].managerialLevel[index].questions[index2].reportanswers = dddd;
                        //     })
                        // })

                        for(let m = 0; m < response.sections[i].managerialLevel.length; m++){
                            response.sections[i].managerialLevel[m].questions = managerailLevelQuestions;
                            
                            // for (let q = 0; q < response.sections[i].managerialLevel[m].questions.length; q++) {
                            //     let answers = await AnswerValues.query()
                            //     .where('question_id', response.sections[i].managerialLevel[m].questions[q].id)
                            //     .andWhere('vga_audit_number', auditNo)
                            //     .andWhere('client_employee_id', response.sections[i].managerialLevel[m].id)
                            //     .andWhere('is_deleted', false);
                            //     // await getReportAnswersOfCompetencyMapping(response.sections[i].managerialLevel[m].questions[q].id, auditNo, response.sections[i].managerialLevel[m].id);
                            //     response.sections[i].managerialLevel[m].questions[q].answers = [];
                            //     // response.sections[i].managerialLevel[m].questions[q].answers = await getReportAnswersOfCompetencyMapping(response.sections[i].managerialLevel[m].questions[q].id, auditNo, response.sections[i].managerialLevel[m].id);
                            //     for (let a = 0; a < answers.length; a++) {
                            //         response.sections[i].managerialLevel[0].questions[q].answers.push(answers[a]);
                            //     }
                            //     // response.sections[i].managerialLevel[m].questions[q].answers = await getReportAnswersOfCompetencyMapping(response.sections[i].managerialLevel[m].questions[q].id, auditNo, response.sections[i].managerialLevel[m].id);
                            // }
                            // for(let q = 0; q < response.sections[i].managerialLevel[m].questions.length; q++){
                            //     let ans = await getReportAnswersOfCompetencyMapping(response.sections[i].managerialLevel[m].questions[q].id, auditNo, response.sections[i].managerialLevel[m].id);
                            //     if (ans && ans.length > 0) {
                            //         response.sections[i].managerialLevel[0].questions[q].reportanswers = ans;
                            //         successcount = true;
                            //         trueArray.push(successcount);
                            //     } else {
                            //         // response.sections[i].managerialLevel[m].questions[q].reportanswers = [];
                            //         successcount = false;
                            //         falseArray.push(successcount)
                            //     }
                            //     response.sections[i].managerialLevel[m].questions[q].successcount = successcount;
                            // }
                        }

                        for(let e = 0; e < response.sections[i].executiveLevel.length; e++){
                            response.sections[i].executiveLevel[e].questions = executiveLevelQuestions;

                            // for(let q = 0; q < response.sections[i].executiveLevel[e].questions.length; q++){
                            //     let _answers = await getReportAnswersOfCompetencyMapping(response.sections[i].executiveLevel[e].questions[q].id, auditNo, response.sections[i].executiveLevel[e].id);
                            //     if (_answers && _answers.length > 0) {
                            //         response.sections[i].executiveLevel[e].questions[q].reportanswers = _answers;
                            //         successcount = true;
                            //         trueArray.push(successcount);
                            //     } else {
                            //         response.sections[i].executiveLevel[e].questions[q].reportanswers = [];
                            //         successcount = false;
                            //         falseArray.push(successcount)
                            //     }
                            //     response.sections[i].executiveLevel[e].questions[q].successcount = successcount;
                            // }
                        }
                        
                    } else {
                        response.sections[i].subSection[0].questions = [];
                    }
                    response.sections[i].subSection[0].subsectionColor = await checkcolorsbyvalues(trueArray.length, falseArray.length, response.sections[i].subSection[0].questions.length);
                    trueArray = [];
                    falseArray = [];
                }
            }
            for (let j = 0; j < response.sections[i].subSection.length; j++) {
                if (response.sections[i].subSection[j].subsectionColor === 'green') {
                    successcount = true;
                    trueArray.push(successcount);
                } else {
                    successcount = false;
                    falseArray.push(successcount);
                }
            }
            response.sections[i].sectionColor = await checkcolorsbyvalues(trueArray.length, falseArray.length, response.sections[i].subSection.length);
            if (response.sections[i].sectionColor === 'green') {
                response.sections[i].status = await updateAuditReportStatus(auditNo, response.sections[i]);
            }else{
                response.sections[i].status = [];
            }
        } 
    }
    return response;
  }catch(error){
      console.log(error);
      return error
  }
}