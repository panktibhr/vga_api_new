import {getSubSectionBySectionId} from '../controllers/QuestionController'
import {updateAuditReportStatus} from '../controllers/QuestionController'
import {checkcolorsbyvalues} from '../controllers/QuestionController'
import {getQuestionsBySectionId} from '../controllers/QuestionController'
import {getReportAnswers} from '../controllers/QuestionController'
import {getSubQuestions} from '../controllers/QuestionController'
import {getQuestionsBySubSectionIdAndSectionId} from '../controllers/QuestionController'
import {getPlanOfActionAnswers} from '../controllers/QuestionController'
import {getReportAnswersOfCompetencyMapping} from '../controllers/QuestionController'
import sectionEnum from '../enums/sectionEnum';
const section = require('../models/Section.Model').default;


export const sectionProcessing = async (sectionId,auditNo, clientEmployeeId) =>{
    let response = {};
    let successcount;
    let trueArray = [];
    let falseArray = [];
  try{
    const Section = await section.query().where("id", sectionId).andWhere("is_deleted", false).orderBy('sequence_number');
    response.sections = Section;
    for (let i = 0; i < response.sections.length; i++) {
        let _subsections = await getSubSectionBySectionId(response.sections[i].id);
        if (_subsections && _subsections.length > 0) {
            response.sections[i].subSection = _subsections;
            trueArray = [];
            falseArray = [];
            for (let j = 0; j < response.sections[i].subSection.length; j++) {
                let _questions = await getQuestionsBySubSectionIdAndSectionId(response.sections[i].id, response.sections[i].subSection[j].id);
                trueArray = [];
                falseArray = [];
                if (_questions && _questions.length > 0) {
                    response.sections[i].subSection[j].questions = _questions;

                    for (let k = 0; k < response.sections[i].subSection[j].questions.length; k++) {
                        let _subquestions = await getSubQuestions(response.sections[i].subSection[j].questions[k].id);
                        if (_subquestions && _subquestions.length > 0) {
                            response.sections[i].subSection[j].questions[k].subquestions = _subquestions;
                        } else {
                            response.sections[i].subSection[j].questions[k].subquestions = [];
                        }

                        if(response.sections[i].id === sectionEnum.PlanOfActionAndRecommendations){
                            let _planofactionanswers = await getPlanOfActionAnswers(response.sections[i].subSection[j].questions[k].id, auditNo);
                            if(_planofactionanswers && _planofactionanswers.length > 0){
                                response.sections[i].subSection[j].questions[k].planofactionanswers = _planofactionanswers;
                                successcount = true;
                                trueArray.push(successcount);
                            } else {
                                response.sections[i].subSection[j].questions[k].planofactionanswers = _planofactionanswers;
                                successcount = false;
                                falseArray.push(successcount);
                            }
                        } else if(response.sections[i].id === sectionEnum.CompetencyMapping){
                            if(response.sections[i].subSection[j].sequence_number === 1){
                                let _answers = await getReportAnswers(response.sections[i].subSection[j].questions[k].id, auditNo);
                                if (_answers && _answers.length > 0) {
                                    response.sections[i].subSection[j].questions[k].reportanswers = _answers;
                                    successcount = true;
                                    trueArray.push(successcount);
                                } else {
                                    response.sections[i].subSection[j].questions[k].reportanswers = [];
                                    successcount = false;
                                    falseArray.push(successcount)
                                }
                            } else{
                                let _answers = await getReportAnswersOfCompetencyMapping(response.sections[i].subSection[j].questions[k].id, auditNo, clientEmployeeId);
                                if (_answers && _answers.length > 0) {
                                    response.sections[i].subSection[j].questions[k].reportanswers = _answers;
                                    successcount = true;
                                    trueArray.push(successcount);
                                } else {
                                    response.sections[i].subSection[j].questions[k].reportanswers = [];
                                    successcount = false;
                                    falseArray.push(successcount)
                                }
                            }
                        } else{
                            let _answers = await getReportAnswers(response.sections[i].subSection[j].questions[k].id, auditNo);

                            if (_answers && _answers.length > 0) {
                                response.sections[i].subSection[j].questions[k].reportanswers = _answers;
                                successcount = true;
                                trueArray.push(successcount);
                            } else {
                                response.sections[i].subSection[j].questions[k].reportanswers = [];
                                successcount = false;
                                falseArray.push(successcount)
                            }
                        }
                        
                        response.sections[i].subSection[j].questions[k].successcount = successcount;
                    }
                } else {
                    response.sections[i].subSection[j].questions = [];
                }
                response.sections[i].subSection[j].subsectionColor = await checkcolorsbyvalues(trueArray.length, falseArray.length, response.sections[i].subSection[j].questions.length);
                trueArray = [];
                falseArray = [];
            }
            for (let j = 0; j < response.sections[i].subSection.length; j++) {
                if (response.sections[i].subSection[j].subsectionColor === 'green') {
                    successcount = true;
                    trueArray.push(successcount);
                } else {
                    successcount = false;
                    falseArray.push(successcount);
                }
            }
            response.sections[i].sectionColor = await checkcolorsbyvalues(trueArray.length, falseArray.length, response.sections[i].subSection.length);
            if (response.sections[i].sectionColor === 'green') {
                response.sections[i].status = await updateAuditReportStatus(auditNo, response.sections[i]);
            }else{
                response.sections[i].status = [];
            }
        } else {
            response.sections[i].subSection = [];
            trueArray = [];
            falseArray = [];
            let _questions = await getQuestionsBySectionId(response.sections[i].id);

            if (_questions && _questions.length > 0) {
                response.sections[i].questions = _questions;
                for (let k = 0; k < response.sections[i].questions.length; k++) {
                    let _subquestions = await getSubQuestions(response.sections[i].questions[k].id);
                    if (_subquestions && _subquestions.length > 0) {
                        response.sections[i].questions[k].subquestions = _subquestions;
                    } else {
                        response.sections[i].questions[k].subquestions = [];
                    }
                    let _answers = await getReportAnswers(response.sections[i].questions[k].id, auditNo);

                    if (_answers && _answers.length > 0) {
                        response.sections[i].questions[k].reportanswers = _answers;
                        successcount = true;
                        trueArray.push(successcount);
                    } else {
                        response.sections[i].questions[k].reportanswers = [];
                        successcount = false;
                        falseArray.push(successcount)
                    }
                    response.sections[i].questions[k].successcount = successcount;
                }
            } else {
                response.sections[i].questions = [];
            }
            response.sections[i].sectionColor = await checkcolorsbyvalues(trueArray.length, falseArray.length, response.sections[i].questions.length);
            if (response.sections[i].sectionColor === 'green') {
                response.sections[i].status = await updateAuditReportStatus(auditNo, response.sections[i]);
            }else{
                response.sections[i].status = [];
            }
        }
    }
    return response;
  }catch(error){
      return error
  }
}