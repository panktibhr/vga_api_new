const express = require("express");
const AnswerValuesRoute = express.Router();
const tokenMiddleware = require('./../middleware').default;
const AnswersValues = require('../models/ReportAnswerValue.Model').default;
const users = require("./../models/users.Model").default;
import roleEnum from './../enums/roleEnum';
import statusReportEnum from '../enums/statusReportEnum'
import sectionEnum from '../enums/sectionEnum'


AnswerValuesRoute.get("/:id/:audit", tokenMiddleware, async (req, res, next) => {
    let questionId = req.params.id;
    let auditNo = req.params.audit;
    try {
        if (questionId) {
            const answerValuesobj = await AnswersValues.query()
                .where('is_deleted', false)
                .where('vga_audit_number', auditNo)
                .andWhere('question_id', questionId);

            if (answerValuesobj) {
                res.status(200).send({
                    success: true,
                    message: "get records successfully.",
                    res: answerValuesobj
                })
            }

        } else {
            res.status(500).send({
                error: error.message,
                message: "Id not Found."
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});

AnswerValuesRoute.post("/add-answers", tokenMiddleware, async (req, res, next) => {
    let _questionArray = req.body.Answer;
    let response = {};
    let answerValuesDetailsObj;
    let answerValuesList = [];

    try {
        let usersData = await users.query().where('is_deleted', false).andWhere('id', _questionArray.UserId).first();

        if (roleEnum.CONSULTANTROLE !== usersData.role_id && _questionArray.AnswerDetails) {
            let answersByQuestionId;
            if(sectionEnum.CompetencyMapping){
                answersByQuestionId = await AnswersValues.query().where('question_id', _questionArray.QuestionId).andWhere('vga_audit_number', _questionArray.vgaAuditNumber).andWhere('client_employee_id', _questionArray.clientEmployeeId).andWhere('is_deleted', false).first();
            } else{
                answersByQuestionId = await AnswersValues.query().where('question_id', _questionArray.QuestionId).andWhere('vga_audit_number', _questionArray.vgaAuditNumber).andWhere('is_deleted', false).first();
            }
            if (!answersByQuestionId) {

                for (let i = 0; i < _questionArray.AnswerDetails.length; i++) {
                    answerValuesDetailsObj = {
                        answer_value: _questionArray.AnswerDetails[i].AnswerValue,
                        answer_value1: _questionArray.AnswerDetails[i].AnswerValue1,
                        answer_value2: _questionArray.AnswerDetails[i].AnswerValue2,
                        sub_questions_id: _questionArray.AnswerDetails[i].SubQuestionId,
                        title: _questionArray.AnswerDetails[i].Title
                    }
                    answerValuesDetailsObj.user_id = _questionArray.UserId;
                    answerValuesDetailsObj.client_employee_id = _questionArray?.clientEmployeeId;
                    answerValuesDetailsObj.remarks = _questionArray.remarks;
                    answerValuesDetailsObj.question_id = _questionArray.QuestionId;
                    answerValuesDetailsObj.vga_audit_number = _questionArray.vgaAuditNumber;
                    answerValuesDetailsObj.submiited_answer = true;
                    const answerValuesObj = await AnswersValues.query().insertGraphAndFetch(answerValuesDetailsObj);
                    answerValuesList.push(answerValuesObj);
                }
                response.answerValues = answerValuesList;

                if (response) {
                    res.status(200).send({
                        success: true,
                        message: "Answers Added successfully",
                        res: response
                    })
                }
            } else {
                res.status(500).send({
                    success: false,
                    message: 'Answers of this Questions are already Submitted.',
                    res: answersByQuestionId
                })
            }
        } else {
            let updatedstatusList = [];
            let answersDetailsObj;
            if(sectionEnum.CompetencyMapping == _questionArray.SectionId){
                answersDetailsObj = await AnswersValues.query().where('client_employee_id', _questionArray.clientEmployeeId).first();
            } else{
                answersDetailsObj = await AnswersValues.query().where('question_id', _questionArray.QuestionId).first();
            }

            if (answersDetailsObj) {

                let review;
                if(sectionEnum.CompetencyMapping == _questionArray.SectionId){
                    review = await AnswersValues.query().where('client_employee_id', _questionArray.clientEmployeeId).andWhere('vga_audit_number', _questionArray.vgaAuditNumber).andWhere('is_deleted', false);
                } else {
                    review = await AnswersValues.query().where('question_id', _questionArray.QuestionId).andWhere('vga_audit_number', _questionArray.vgaAuditNumber).andWhere('is_deleted', false);
                }
                if (review) {
                    const consulatantobj = {
                        consultant_review: _questionArray.consultantReview,
                    }
                    let status = _questionArray.status;
                    if (status === 'Reviewing') {
                        consulatantobj.question_status = statusReportEnum.REVIEWINGSTATUS
                    } else {
                        consulatantobj.question_status = statusReportEnum.REVIEWEDSTATUS
                    }
                    for (let i = 0; i < review.length; i++) {
                        let updatedStatus;
                        if(sectionEnum.CompetencyMapping == _questionArray.SectionId){
                            updatedStatus = await AnswersValues.query().where('client_employee_id', _questionArray.clientEmployeeId).patchAndFetchById(review[i].id, consulatantobj);
                        } else{
                            updatedStatus = await AnswersValues.query().where('question_id', _questionArray.QuestionId).patchAndFetchById(review[i].id, consulatantobj);
                        }
                        updatedstatusList.push(updatedStatus)
                    }
                    response.consultantStatus = updatedstatusList;
                } else {
                    return null
                }

                if (response) {
                    res.status(200).send({
                        success: true,
                        message: 'consultant review submitted.',
                        res: response
                    })
                }
            } else {
                res.status(500).send({
                    success: false,
                    message: 'please, submit the answers first!'
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});

export default AnswerValuesRoute;