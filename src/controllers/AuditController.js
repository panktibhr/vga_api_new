const AuditRouter = require('express').Router();
const Audit = require('../models/Audit.Model').default;
const AuditDetails = require('../models/AuditDetails.Model').default;
const Clients = require('../models/client.Model').default;
const tokenMiddleware = require('./../middleware').default;
const { validationResult } = require('express-validator');
import { auditbodyvalidate } from '../common/validator';
import statusReportEnum from '../enums/statusReportEnum';
import Consultant from '../models/consultant.Model';
import {getaccountliangeBytoken} from './CommonController';

AuditRouter.post('/', tokenMiddleware, async (req, res, next) => {
  const auditNo = req.body.vgaAuditNo;
  const fromDate = req.body.fromDate;
  const toDate = req.body.toDate;
  const userId = req.body.userId;
  const clientId = req.body.clientId;

  try {
    const auditObj = await Audit.query()
      .where('vga_audits.is_deleted', false)
      .andWhere(function () {
        if (fromDate !== '') {
          this.whereRaw('??::date >= ?', ['vga_audits.audit_from_date', fromDate]);
        }
        if (toDate !== '') {
          this.whereRaw('??::date <= ?', ['vga_audits.audit_to_date', toDate]);
        }
        if (auditNo !== '') {
          this.where('vga_audits.vga_audit_number', 'like', '%' + auditNo + '%');
        }
      }).withGraphFetched('[client]');

    if (auditObj) {
      res.status(200).send({
        success: true,
        meassge: 'Get all vga audits',
        res: auditObj
      })
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stack
    })
  }
});

AuditRouter.post('/getVgaAuditReportsOfClientsByUserId', tokenMiddleware, async (req, res, next) => {
  const auditNo = req.body.vgaAuditNo;
  const fromDate = req.body.fromDate;
  const toDate = req.body.toDate;
  let auditObj; 
  let vgaAuditReports = [];

  try {
    const result = await getaccountliangeBytoken(req.headers.authorization);
    let clients = await Clients.query().where("clients.is_deleted", false).andWhere("clients.parent_account_id", "in", result.accounts.map((x) => x.id));

    for(let i = 0; i < clients.length; i++){
      auditObj = await Audit.query().innerJoin('clients', 'clients.id', 'vga_audits.client_id').select('clients.*', 'vga_audits.*').where('vga_audits.is_deleted', false).andWhere('vga_audits.client_id', clients[i].id).andWhere(function () {
        if (fromDate !== '') {
          this.whereRaw('??::date >= ?', ['vga_audits.audit_from_date', fromDate]);
        }
        if (toDate !== '') {
          this.whereRaw('??::date <= ?', ['vga_audits.audit_to_date', toDate]);
        }
        if (auditNo !== '') {
          this.where('vga_audits.vga_audit_number', 'like', '%' + auditNo + '%');
        }
      });
      vgaAuditReports.push(...auditObj);   
    }

    if (vgaAuditReports) {
      res.status(200).send({
        success: true,
        meassge: 'Get all vga audits',
        res: vgaAuditReports
      })
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stack
    })
  }
});

AuditRouter.post('/add-new-vgaudit', auditbodyvalidate, tokenMiddleware, async (req, res, next) => {
  let response = {};
  let _sections = req.body.sectionsList;
  let auditsDetailsArray = [];
  let splitvalues = [];
  let max = [];
  let vgaAuditNumber;
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() })
  }

  try {
    let auditcount = await Audit.query().where('is_deleted', false);
    if (auditcount && auditcount.length > 0) {
      for (let i = 0; i < auditcount.length; i++) {
        splitvalues.push(+(auditcount[i].vga_audit_number).slice(7, auditcount[i].vga_audit_number.length));
      }
      max = splitvalues[0];
      for (let j = 1; j < splitvalues.length; j++) {
        if (splitvalues[j] > max) {
          max = splitvalues[j];
        }
      }
      let maximumcount = max;
      if (maximumcount == undefined) {
        maximumcount++;
        vgaAuditNumber = 'VGARPT_' + maximumcount;
      } else {
        maximumcount++;
        vgaAuditNumber = 'VGARPT_' + maximumcount;
      }
    } else {
      vgaAuditNumber = 'VGARPT_' + 1;
    }
    const auditObj = {
      client_id: req.body.clientId,
      user_id: req.body.userId,
      vga_audit_number: vgaAuditNumber,
      audit_from_date: req.body.fromDate,
      audit_to_date: req.body.toDate,
      status: statusReportEnum.CREATEDSTATUS
    }
    const auditsData = await Audit.query().insertGraphAndFetch(auditObj);
    response.audits = auditsData;
    if (response.audits) {
      for (let i = 0; i < _sections.length; i++) {
        const auditDetailsObj = {
          section_id: _sections[i].sectionId
        }
        auditDetailsObj.client_id = auditObj.client_id;
        auditDetailsObj.user_id = auditObj.user_id;
        auditDetailsObj.vga_audit_id = auditsData.id;
        auditDetailsObj.vga_audit_number = vgaAuditNumber;
        auditDetailsObj.status = statusReportEnum.CREATEDSTATUS;

        const auditDetailsData = await AuditDetails.query().insertGraphAndFetch(auditDetailsObj);
        auditsDetailsArray.push(auditDetailsData);
      }
      response.auditsDetails = auditsDetailsArray;
      if (response.auditsDetails) {
        res.status(201).send({
          success: true,
          meassge: "vgAudit Report created successfully.",
          res: response
        })
      } else {
        res.status(500).send({
          success: false,
          meassge: "something wrong with insertion in sub table"
        })
      }
    }
    else {
      res.status(500).send({
        success: false,
        meassge: "something wrong with insertion"
      })
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stack
    })
  }
});

AuditRouter.post('/getStatusReportsOfClient', tokenMiddleware, async (req, res, next) => {
  let clientId = req.body.clientId;
  const submittedStatus = 'Submitted';
  const auditingStatus = 'Auditing';
  let statusArray = [];

  try {
    let auditObj = await Audit.query().where('is_deleted', false).andWhere('client_id', clientId);
    let submitStatus = await Audit.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('status', submittedStatus);
    let auditStatus = await Audit.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('status', auditingStatus);

    let statusObj = {
      'totalReports': auditObj.length,
      'submittedReports': submitStatus.length,
      'auditingReports': auditStatus.length
    }

    statusArray.push(statusObj)
    
    if (statusObj) {
      res.status(200).send({
        success: true,
        meassge: 'Get status reports of client!',
        res: statusObj
      })
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stack
    })
  }
});

AuditRouter.post('/:clientId', tokenMiddleware, async (req, res, next) => {
  const auditNo = req.body.vgaAuditNo;
  const fromDate = req.body.fromDate;
  const toDate = req.body.toDate;
  const userId = req.params.userId;
  const clientId = req.params.clientId;
  let auditObj;

  try {
    let clientIdFromClientTable = await Clients.query().where('is_deleted', false).andWhere('id', clientId);
    if(clientIdFromClientTable[0]?.id === clientId){
      auditObj = await Audit.query()
        .where('is_deleted', false)
        .andWhere('client_id', clientId)
        .andWhere(function () {
          if (fromDate !== '') {
            this.whereRaw('??::date >= ?', ['vga_audits.audit_from_date', fromDate]);
          }
          if (toDate !== '') {
            this.whereRaw('??::date <= ?', ['vga_audits.audit_to_date', toDate]);
          }
          if (auditNo !== '') {
            this.where('vga_audits.vga_audit_number', 'like', '%' + auditNo + '%');
          }
        }).withGraphFetched('[client]');
    } else if(clientIdFromClientTable.length === 0){
      auditObj = await Audit.query()
        .where('is_deleted', false)
        .andWhere('user_id', clientId)
        .andWhere(function () {
          if (fromDate !== '') {
            this.whereRaw('??::date >= ?', ['vga_audits.audit_from_date', fromDate]);
          }
          if (toDate !== '') {
            this.whereRaw('??::date <= ?', ['vga_audits.audit_to_date', toDate]);
          }
          if (auditNo !== '') {
            this.where('vga_audits.vga_audit_number', 'like', '%' + auditNo + '%');
          }
        }).withGraphFetched('[client]');
    }

    if (auditObj) {
      res.status(200).send({
        success: true,
        meassge: 'Get all vga audits',
        res: auditObj
      })
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stack
    })
  }
});

AuditRouter.get('/getVgaAuditReportsOfClient/:consultantId', tokenMiddleware, async (req, res, next) => {
  const consultantId = req.params.consultantId;
  let auditObj; 
  let vgaAuditReports = [];

  try {
    let clients = await Clients.query().innerJoin('consultant', 'consultant.id', 'clients.consultant').select('clients.id','clients.name as clientName').where('clients.is_deleted', false).andWhere('consultant', consultantId);

    for(let i = 0; i < clients.length; i++){
      auditObj = await Audit.query().innerJoin('clients', 'clients.id', 'vga_audits.client_id').select('clients.*', 'vga_audits.*').where('vga_audits.is_deleted', false).andWhere('vga_audits.client_id', clients[i].id)
      vgaAuditReports.push(...auditObj);   
    }

    if (vgaAuditReports) {
      res.status(200).send({
        success: true,
        meassge: 'Get all vga audits',
        res: vgaAuditReports
      })
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stack
    })
  }
});

AuditRouter.get("/:auditNo", tokenMiddleware, async (req, res, next) => {
  let auditNo = req.params.auditNo;
  let response = {};
  try {
    if (auditNo) {
      const auditsByAuditNo = await Audit.query().where("vga_audit_number", auditNo).andWhere("is_deleted", false).first();
      response.auditsByNo = auditsByAuditNo;

      const auditDetails = await AuditDetails.query()
      .innerJoin('section_master', 'section_master.id', 'vga_audit_details.section_id')
      .select(
        'section_master.id as section_id',
        'section_master.section_name',
        'vga_audit_details.id',
        'vga_audit_details.status',
        'vga_audit_details.client_id',
        'vga_audit_details.vga_audit_id',
        'vga_audit_details.user_id'
      )
      .where('vga_audit_details.vga_audit_id', response.auditsByNo.id)
      .andWhere('vga_audit_details.is_deleted', false)
      .andWhere("section_master.is_deleted", false)
      .orderBy('section_master.sequence_number');
      response.auditDetailsData = auditDetails;

      if (response) {
        res.status(200).send({
          success: true,
          message: "Audits get Successfully",
          res: response
        })
      }
    } else {
      res.status(400).send({
        error: error.meassge,
        stack: error.stck,
        message: "Audit Id Not Found."
      });
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stck
    });
  }
});

AuditRouter.put("/update-vgaudit/:auditNo", tokenMiddleware, async (req, res, next) => {
  let AuditNo = req.params.auditNo;
  let response = {};

  const auditDetailsObj = {
    audit_from_date: req.body.fromDate,
    audit_to_date: req.body.toDate,
    status: statusReportEnum.AUDITINGSTATUS
  }
  try {
    const auditsByNo = await Audit.query().where('vga_audit_number', AuditNo).andWhere('is_deleted', false).first();
    if (auditsByNo) {
      const updateAudit = await Audit.query().patchAndFetchById(auditsByNo.id, auditDetailsObj).withGraphFetched('[client]');
      response.audits = updateAudit;
      const updateAuidtsDetails = await AuditDetails.query()
        .innerJoin('section_master', 'section_master.id', 'vga_audit_details.section_id')
        .select(
          'section_master.id as section_id',
          'section_master.section_name',
          'vga_audit_details.id',
          'vga_audit_details.status',
          'vga_audit_details.client_id',
          'vga_audit_details.vga_audit_id',
          'vga_audit_details.user_id'
        )
        .where('vga_audit_details.vga_audit_id', response.audits.id)
        .andWhere("section_master.is_deleted", false)
        .andWhere("vga_audit_details.is_deleted", false)
        .orderBy('section_master.sequence_number');

      response.auditsDetails = updateAuidtsDetails;
      let statusArray = [];
      let auditStatus;

      for(let i = 0; i < response.auditsDetails.length; i++){
        if(response.auditsDetails[i].status === statusReportEnum.SUBMITTEDSTATUS){
          statusArray.push(response.auditsDetails[i]);
        } else{
          statusArray.push('Report is not submitted till now!');
        }
      }
      statusArray.filter(async(res) =>{
        if(res.status === 'Submitted'){
          auditStatus = await Audit.query().where('is_deleted',false).patchAndFetchById(res.vga_audit_id,{status : statusReportEnum.SUBMITTEDSTATUS});
          response.audits = auditStatus;
        } else {
          response.audits = updateAudit
        }
      })
 
      if (response) {
        res.status(200).send({
          success: true,
          meassge: 'Audits Updated Successfully.',
          res: response
        })
      } else {
        res.status(500).send({
          success: false,
          meassge: 'something wrong with data you want to update.'
        })
      }
    } else {
      res.status(400).send({
        success: false,
        meassge: 'AuditNo not found.'
      })
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stack
    })
  }
});

export default AuditRouter;
