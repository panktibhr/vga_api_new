const express = require('express');
const PlanOfActionRoute = express.Router();
const tokenMiddleware = require('./../middleware').default;
const PlanOfActionAndRecommendations = require('../models/planOfActionAndRecommendations.Model').default;
const users = require("./../models/users.Model").default;
import sectionEnum from '../enums/sectionEnum';
import roleEnum from '../enums/roleEnum';

PlanOfActionRoute.get("/", tokenMiddleware, async (req, res) => {
    try{
        const planOfActionValue = await PlanOfActionAndRecommendations.query().where('is_deleted', false);
        if(planOfActionValue){
            res.status(200).send({
                success : true,
                message : "Plan of Action and Recommendation values get Successfully",
                res : planOfActionValue
            })
        } else{
            res.status(500).send({
                success : false,
                message : "Something went wrong"
            })
        }
    }catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message : "Somethibg went wrong!"
        })
    }
})

PlanOfActionRoute.get("/:id/:vgaAuditNumber", tokenMiddleware, async (req, res) => {
    let QuestionId = req.params.id;
    let vgaAuditNumber = req.params.vgaAuditNumber
    try{
        if(QuestionId && vgaAuditNumber){
            let planOfActionByQuestionId = await PlanOfActionAndRecommendations.query().where('is_deleted', false).andWhere('question_id', QuestionId).andWhere('vga_audit_number', vgaAuditNumber);

            if(planOfActionByQuestionId){
                res.status(200).send({
                    success : true,
                    message : "Plan of Action and Recommendation values By ID get Successfully",
                    res : planOfActionByQuestionId
                })
            } else {
                res.status(500).send({
                    success : false,
                    message : "Something went wrong"
                })
            }
        }else {
            res.status(404).send({
                message: "Id Not Found.Please Check Again!"
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message: "Something went wrong!"
        })
    }  
});

PlanOfActionRoute.post("/add-answers", tokenMiddleware, async (req, res) => {
    const answer = req.body.planOfActionDetails;
    let answerdetails;
    let PlanofActionRecommendationDetails;

    let usersData = await users.query().where('is_deleted', false).andWhere('id', req.body.user_id).first();
    try{
        if(roleEnum.CONSULTANTROLE !== usersData.role_id && req.body.user_id){
            if(req.body.section_id === sectionEnum.PlanOfActionAndRecommendations){
                let answersByQuestionId = await PlanOfActionAndRecommendations.query().where('question_id', req.body.question_id)
                .andWhere('is_deleted', false)
                .andWhere('client_id', req.body.client_id)
                .andWhere('vga_audit_number', req.body.vga_audit_number).first();
                if(!answersByQuestionId){
                    answerdetails = {
                        answer_value : answer,
                        section_id : req.body.section_id,
                        sub_section_id : req.body.sub_section_id,
                        question_id : req.body.question_id,
                        vga_audit_number : req.body.vga_audit_number,
                        client_id : req.body.client_id,
                        user_id : req.body.user_id,
                        submitted_answer : true
                    }
                    if(answerdetails){
                        PlanofActionRecommendationDetails = await PlanOfActionAndRecommendations.query().insertGraphAndFetch(answerdetails);
                        if(PlanofActionRecommendationDetails){
                            res.status(200).send({
                            success: true,
                            message: 'Answers of Plan of Action and Recommendation submitted successfully!',
                            res: PlanofActionRecommendationDetails
                            })
                        }else{
                            res.status(500).send({
                                success : false,
                                message : "Something went wrong",
                            })
                        }
                    }
                } else{
                    answerdetails = {
                        answer_value : answer,
                        section_id : req.body.section_id,
                        sub_section_id : req.body.sub_section_id,
                        question_id : req.body.question_id,
                        vga_audit_number : req.body.vga_audit_number,
                        client_id : req.body.client_id,
                        user_id : req.body.user_id,
                        submitted_answer : true
                    }
                    if(answerdetails){
                        PlanofActionRecommendationDetails = await PlanOfActionAndRecommendations.query().patchAndFetchById(answersByQuestionId.id , answerdetails);
                        if(PlanofActionRecommendationDetails){
                            res.status(200).send({
                            success: true,
                            message: 'Answers of Plan of Action and Recommendation updated successfully!',
                            res: PlanofActionRecommendationDetails
                            })
                        }else{
                            res.status(500).send({
                                success : false,
                                message : "Something went wrong",
                            })
                        }
                    }
                } 
            } else{
                res.status(500).send({
                    success : false,
                    message : "Section Id does not match!"
                })
            } 
        }else{
            let updatedstatusList = [];
            let response = {};
            const answersDetailsObj = await PlanOfActionAndRecommendations.query()
            .where('question_id', req.body.QuestionId)
            .andWhere('client_id', req.body.ClientId)
            .andWhere('vga_audit_number', req.body.vgaAuditNumber).first();

            if (answersDetailsObj) {

                const review = await PlanOfActionAndRecommendations.query().where('question_id', req.body.QuestionId)
                .andWhere('vga_audit_number', req.body.vgaAuditNumber)
                .andWhere('client_id', req.body.ClientId)
                .andWhere('is_deleted', false);
                if (review) {
                    const consulatantobj = {
                        consultant_review : req.body.consultantReview,
                    }
                    for (let i = 0; i < review.length; i++) {
                        const updatedStatus = await PlanOfActionAndRecommendations.query().where('question_id', req.body.QuestionId).patchAndFetchById(review[i].id, consulatantobj);
                        updatedstatusList.push(updatedStatus)
                    }
                    response.consultantStatus = updatedstatusList;
                } else {
                    return null
                }

                if (response) {
                    res.status(200).send({
                        success: true,
                        message: 'consultant review submitted.',
                        res: response
                    })
                }
            } else {
                res.status(500).send({
                    success: false,
                    message: 'please, submit the answers first!'
                })
            }
        }
    } catch (error){
            res.status(500).send({
            error: error.message,
            stack: error.stack,
            message : "Something went wrong!"
        })
    }
});
export default PlanOfActionRoute;