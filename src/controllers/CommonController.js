const express = require("express");
const CommonRoute = express.Router();
const users = require("./../models/users.Model").default;
const account = require('../models/Account.Model').default;
const tokenMiddleware = require('./../middleware').default;
let jwt = require('jsonwebtoken');

CommonRoute.use(async (req, res, next) => {
  next();
});

CommonRoute.get('/user-profile', tokenMiddleware, async (req, res, next) => {
  const id = req.body.id;
  try {
    if (id) {
      const userProfileObj = await users.query().where('id', id);
      if (userProfileObj) {
        res.status(200).send({
          success: true,
          message: 'User profile data retrieved successfully',
          user: userProfileObj
        })
      }
    } else {
      res.status(400).send({
        success: false,
        message: 'Something went wrong! Please check request'
      })
    }
  } catch (error) {
    res.status(500).send({
      error: error.message,
      stack: error.stack
    })
  }
});


CommonRoute.post("/update-lastlogin-date", tokenMiddleware, async (req, res, next) => {
  const id = req.body.id;
  try {
    if (id) {
      let updateDateObj = await users.query().patchAndFetchById(id, { 'last_login_on': new Date });
      if (updateDateObj) {
        res.status(200).send({
          success: true,
          message: 'Lastlogin date updated successfully',
          res: updateDateObj
        })
      } else {
        res.status(500).send({
          success: false,
          message: 'user not found.'
        })
      }
    } else {
      res.status(400).send({
        success: false,
        message: 'Something went wrong! Please check request'
      })
    }
  } catch (error) {
    res.status(500).send({
      error: error.message,
      stack: error.stack
    })
  }
});

export const getUserByEmail = async (email) => {
  let userResponse = null;
  const userByEmail = await users.query().where('email', email).first();
  if (userByEmail) {
    userResponse = userByEmail;
  }
  return userResponse;
}

export const getaccountliangeBytoken = async (authorization) => {
  let response = {};
  const token = authorization.split(' ')[1];
  const decodedToken = jwt.decode(token);
  const u = await users.query().where('email', decodedToken.email).withGraphFetched("[parentAccount]").first();
  const accountIds = await account.query().select('id', 'name').where('account_lineage', '<@', u.parentAccount.account_lineage);
  response.users = u;
  response.accounts = accountIds;
  return response;
}

export default CommonRoute;