const express = require("express");
const index = express.Router();
const Users = require("./UsersController").default;
const Client = require("./ClientController").default;
const ClientEmployee = require("./ClientEmployeeController").default;
const Consultant = require("./ConsultantController").default;
const Auth = require("./AuthController").default;
const Role = require('./RoleController').default;
const Account = require("./AccountController").default;
const Common = require("./CommonController").default;
const Employee = require("./EmployeeController").default;
const Auditor = require("./AuditorController").default;
const Section = require("./SectionController").default;
const SubSection = require("./SubSectionController").default;
const Question = require("./QuestionController").default;
const AnswerValues = require("./ReportAnswerValuesController").default;
const ResetPassword = require("./ResetPasswordController").default;
const Audit = require("./AuditController").default;
const Country = require("./CountryController").default;
const State = require("./StatesController").default;
const Cities = require("./CityController").default;
const Pdf = require("./ReportPdfController").default;
const EmployeePerformance = require("./EmployeePerformanceController").default;
const PlanOfActionAndRecommndation = require("./PlanOfActionController").default;
index.use(async (req, res, next) => {
  next();
});

index.get("/", async (req, res, next) => {
  res.status(200).send("Welcome to VGA (Value Growth Audit)");
});

const router = (app) => {
  app.use("/", index);
  app.use("/user", Users);
  app.use("/auth", Auth);
  app.use("/role", Role);
  app.use("/account", Account);
  app.use("/reset", ResetPassword);
  app.use("/common", Common);
  app.use("/client", Client);
  app.use("/clientsEmployee",ClientEmployee);
  app.use("/consultant", Consultant);
  app.use("/auditor", Auditor);
  app.use("/employee", Employee);
  app.use("/section", Section);
  app.use("/subsection", SubSection);
  app.use("/question", Question);
  app.use("/answervalues", AnswerValues);
  app.use("/audit", Audit);
  app.use("/countries", Country);
  app.use("/states", State);
  app.use("/cities", Cities);
  app.use("/pdf",Pdf);
  app.use("/employeePerformance", EmployeePerformance);
  app.use("/planofActionAndRecommendation" , PlanOfActionAndRecommndation)
};

export default router;
