const express = require("express");
const StatesRoute = express.Router();
const states = require("./../models/state.Model").default;
const { validationResult } = require('express-validator');
import {statebodyvalidate} from '../common/validator';

StatesRoute.post("/", async (req, res, next) => {

    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 10);
    const sortColumn = req.body.SortColumn;
    const sortOrder = req.body.SortOrder;
    const SearchText = req.body.SearchText;

    try {

        let stateObj = await states.query()
            .where("is_deleted", false)
            .andWhere('states.states_name', 'like', '%' + SearchText + '%')
            .orderBy(sortColumn || 'states.states_name', sortOrder || 'ASC')
            .page(pageNo, PageSiZe);

        if (stateObj) {
            res.status(200).send({
                success: true,
                meassge: "Get all state Successfully.",
                res: stateObj
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

StatesRoute.get("/getAll-state", async (req, res, next) => {
    try {
        let state = await states.query()
            .where("is_deleted", false).orderBy("states_name");

        if (state) {
            res.status(200).send({
                success: true,
                res: state
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            error: error.stack
        })
    }
});

StatesRoute.get("/:id", async (req, res, next) => {
    let country_id = req.params.id;
    try {
        if (country_id) {
            let state = await states.query().where("countries_id", country_id)
                .andWhere("is_deleted", false).orderBy("states_name");

            if (state) {
                res.status(200).send({
                    success: true,
                    res: state
                })
            }
        } else {
            res.status(400).send({
                success: false,
                message: 'country id does not existes.'
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            error: error.stack
        })
    }
});

StatesRoute.post("/add-state", statebodyvalidate,async (req, res, next) => {
   
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
   
    const statesDetailsobj = {
        states_name: req.body.stateName,
        countries_id: req.body.countryId
    }

    try {
        let state = await states.query().insertGraphAndFetch(statesDetailsobj);

        if (state) {
            res.status(200).send({
                status: true,
                message: 'State added Successfully.',
                res: state
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});

StatesRoute.delete("/delete-state/:id", async (req, res, next) => {
    let states_id = req.params.id;

    let deletedRecord = await states.query().where("id", states_id).first();

    try {
        if (deletedRecord) {
            const stateById = await states.query().where("id", states_id).patch({ is_deleted: true, deleted_on: new Date() });

            if (stateById) {
                res.status(200).send({
                    success: true,
                    meassge: "state Deleted Successfully"
                })
            }
        } else {
            res.status(500).send({
                success: false,
                meassge: "Id Not Found."
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

StatesRoute.put("/update-state/:id", async (req, res, next) => {
    let states_id = req.params.id;
    const statesDetailsobj = {
        states_name: req.body.stateName,
        countries_id: req.body.countryId
    }
    try {
        if (states_id) {
            let state = await states.query().patchAndFetchById(states_id, statesDetailsobj);

            if (state) {
                res.status(200).send({
                    success: true,
                    message: "update state successfully.",
                    res: state
                })
            }
        } else {
            res.status(500).send({
                success: false,
                meassge: "Id Not Found."
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

export default StatesRoute;