const express = require("express");
const UsersRoute = express.Router();
const tokenMiddleware = require('./../middleware').default;
const users = require("./../models/users.Model").default;
const userRoleDetails = require('./../models/UsersRoleMapping.Model').default;
const auditor = require('./../models/Auditor.Model').default;
const consultant = require('./../models/consultant.Model').default;
const clients = require('./../models/client.Model').default;
const Employee = require('./../models/Employee.Model').default;
const CryptoJS = require("crypto-js");
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
import roleEnum from './../enums/roleEnum';
let jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
import {userbodyvalidate} from '../common/validator';
import { updateuservalidate } from '../common/validator';
import { getaccountliangeBytoken } from './CommonController';

UsersRoute.post("/", tokenMiddleware, async (req, res, next) => {
  try {
    let response;
    const result = await getaccountliangeBytoken(req.headers.authorization);

    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 5);
    const sortColumn = req.body.SortColumn;
    const sortOrder = req.body.SortOrder;
    const SearchText = req.body.SearchText;
    const SearchText2 = req.body.SearchText2;

    if (result.users.role_id === roleEnum.SUPERADMINROLE) {
      response = await users.query()
        .where("is_deleted", false)
        .andWhere("users.parent_account_id", "in", result.accounts.map((x) => x.id))
        .andWhere('users.name', 'like', '%' + SearchText + '%')
        .andWhere('users.email', 'like', '%' + SearchText2 + '%')
        .orderBy(sortColumn || 'users.name', sortOrder || 'ASC')
        .page(pageNo, PageSiZe)
        .withGraphFetched("[role]");
    } else {
      response = await users.query()
        .where("is_deleted", false)
        .andWhere("users.parent_account_id", result.users.parent_account_id)
        .andWhere('users.name', 'like', '%' + SearchText + '%')
        .andWhere('users.email', 'like', '%' + SearchText2 + '%')
        .orderBy(sortColumn || 'users.name', sortOrder || 'ASC')
        .page(pageNo, PageSiZe)
        .withGraphFetched("[role]");
    }

    if (response) {
      res.status(200).send({
        success: true,
        meassge: "Get all Users Successfully.",
        res: response
      });
    }
  } catch (error) {
    res.status(500).send({
      error: error.message,
      error: error.stack,
    });
  }
});

UsersRoute.post("/add-user", userbodyvalidate, tokenMiddleware, async (req, res, next) => {
  const pass = CryptoJS.AES.encrypt(req.body.password, process.env.SECRET).toString();
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() })
  }
  const userDetailsobj = {
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    role_id: req.body.role,
    parent_account_id: req.body.parent_account_id,
    password: pass
  }

  try {
    let roleObj, userMasterObj, userObj, consultantobj, clientobj, employeeobj, auditorobj;
    if (roleEnum.SUPERADMINROLE != userDetailsobj.role_id) {
      userMasterObj = await users.query().insertGraphAndFetch(userDetailsobj);
    }
    if (req.body.role === roleEnum.CLIENTROLE) {
      delete userDetailsobj.role_id;
      clientobj = await clients.query().insertGraphAndFetch(userDetailsobj);
    } else if (req.body.role === roleEnum.CONSULTANTROLE) {
      delete userDetailsobj.role_id;
      consultantobj = await consultant.query().insertGraphAndFetch(userDetailsobj);
    } else if (req.body.role === roleEnum.EMPLOYEEROLE) {
      delete userDetailsobj.role_id;
      employeeobj = await Employee.query().insertGraphAndFetch(userDetailsobj);
    } else if (req.body.role === roleEnum.AUDITORROLE) {
      delete userDetailsobj.role_id;
      auditorobj = await auditor.query().insertGraphAndFetch(userDetailsobj);
    } else {
      userObj = await users.query().insertGraphAndFetch(userDetailsobj);
    }
    if (userMasterObj || userObj) {
      const roleDetailsObj = {
        user_id: userMasterObj ? userMasterObj.id : userObj.id,
        role_id: req.body.role
      }
      roleObj = await userRoleDetails.query().insertGraphAndFetch(roleDetailsObj);
    }
    if (userMasterObj && roleObj || userObj && roleObj) {

      const passDecrepted = CryptoJS.AES.decrypt(userMasterObj.password || userObj.password, process.env.SECRET).toString(CryptoJS.enc.Utf8);
      let smtpTransport = nodemailer.createTransport({
        type: 'OAuth2',
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        service: 'gmail', // true for 465, false for other ports
        auth: {
          user: process.env.AUTH_EMAIL,
          pass: process.env.AUTH_PASSWORD,
        },
        tls: true,
      });
      let info = await smtpTransport.sendMail({
        from: process.env.AUTH_EMAIL, // sender address
        to: userDetailsobj.email, // list of receivers
        subject: "VGA - Temporary credentials", // Subject line
        text: "Temporary Password With Login.",
        html: generateHTMLInquiryEmail(userDetailsobj.email, passDecrepted)
      });

      res.status(201).send({
        success: true,
        message: "User created successfully & Please! check your mail.",
        user: userMasterObj || userObj && roleObj,
      });
    } else {
      res.status(500).send({
        error: error.message,
        stack: error.stack
      })
    }
  } catch (error) {
    res.status(404).send({
      error: error.message,
      stack: error.stack
    })
  }
});

UsersRoute.delete("/delete-user/:id", tokenMiddleware, async (req, res, next) => {
  let user_id = req.params.id;
  let userObj = await users.query().where('id', user_id).first();
  
  try {
    if (userObj) {
      let enable = await users.query().where('id', user_id).patch({ is_deleted: true, deleted_on: new Date() });
      
      res.status(200).send({
        success: true,
        message: "deleted Successfully."
      });

    } else {
      res.status(500).send({
        success: false,
        message: "User Not Found."
      });
    }
  } catch (error) {
    res.send({
      message: error.message,
      stack: error.stack
    });
  }
});

UsersRoute.put("/update-user/:id",updateuservalidate, tokenMiddleware, async (req, res, next) => {
  const is_user_id = req.params.id;

  const userDetailsobj = {
    name: req.body.name,
    // email: req.body.email,
    username: req.body.username,
    // role_id: req.body.role,
    parent_account_id: req.body.parent_account_id
  };

  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() })
  }

  try {
    const userDetailsById = await users.query().where("id", is_user_id).first();
    if (userDetailsById) {
      const userObj = await users.query().patchAndFetchById(is_user_id, userDetailsobj);

      if (userObj) {
        res.status(200).send({
          success: true,
          message: "User Updated Successfully!",
          section: userObj
        })
      } else {
        res.status(500).send({
          success: false,
          message: "User Not Found!"
        });
      }
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stack
    })
  }
  // try {
  //   let user_record = await users.query().where('id', is_user_id).first();
  //   let consultantobjByEmail, clientobjByEmail, employeeobjByEmail, auditorobjByEmail;
  //   // if (user_record.role_id === roleEnum.CONSULTANTROLE) {
  //   //   consultantobjByEmail = await consultant.query().where('email', user_record.email).first();
  //   // } else if (user_record.role_id === roleEnum.CLIENTROLE) {
  //   //   clientobjByEmail = await clients.query().where('email', user_record.email).first();
  //   // } else if (user_record.role_id === roleEnum.EMPLOYEEROLE) {
  //   //   employeeobjByEmail = await Employee.query().where('email', user_record.email).first();
  //   // } else if (user_record.role_id === roleEnum.AUDITORROLE) {
  //   //   auditorobjByEmail = await auditor.query().where('email', user_record.email).first();
  //   // } else {
  //   //   usersobjByEmail = await users.query().where('email', user_record.email).first();
  //   // }

  //   if (user_record && consultantobjByEmail || user_record && clientobjByEmail ||
  //     user_record && employeeobjByEmail || user_record && auditorobjByEmail || user_record) {


  //     let roleObj, userObj, consultantobj, clientobj, employeeobj, auditorobj;
  //     userObj = await users.query().patchAndFetchById(is_user_id, userDetailsobj);
  //     if (user_record.role_id === roleEnum.CLIENTROLE) {
  //       delete userDetailsobj.role_id;
  //       clientobj = await clients.query().patchAndFetchById(clientobjByEmail.id, userDetailsobj);
  //     } else if (user_record.role_id === roleEnum.CONSULTANTROLE) {
  //       delete userDetailsobj.role_id;
  //       consultantobj = await consultant.query().patchAndFetchById(consultantobjByEmail.id, userDetailsobj);
  //     } else if (user_record.role_id === roleEnum.EMPLOYEEROLE) {
  //       delete userDetailsobj.role_id;
  //       employeeobj = await Employee.query().patchAndFetchById(userDetailsobj);
  //     } else if (user_record.role_id === roleEnum.AUDITORROLE) {
  //       delete userDetailsobj.role_id;
  //       auditorobj = await auditor.query().patchAndFetchById(auditorobjByEmail.id, userDetailsobj);
  //     } else if (user_record.role_id === roleEnum.SUPERADMINROLE) {
  //       userObj = await users.query().patchAndFetchById(is_user_id, userDetailsobj);
  //     }

  //     let roleDetails = await userRoleDetails.query().where('user_id', is_user_id);
  //     let role = {
  //       user_id: is_user_id,
  //       role_id: req.body.role_id
  //     }
  //     if (roleDetails) {
  //       roleObj = await userRoleDetails.query().patchAndFetchById(roleDetails[0].id, role);
  //     }

  //     if (userObj && roleObj) {
  //       res.status(200).send({
  //         success: true,
  //         message: "User Updated successfully.!",
  //         res: userObj,
  //       });
  //     }

  //   } else {
  //     res.status(500).send({
  //       success: false,
  //       message: "User Not Found."
  //     })
  //   }

  // } catch (error) {
  //   res.send({
  //     success: false,
  //     message: error.message,
  //     stack: error.stack,
  //   });
  // }
});

UsersRoute.get("/users-hierarchy", tokenMiddleware, async (req, res, next) => {
  try {
    let response = {};
    const result = await getaccountliangeBytoken(req.headers.authorization);

    response.parent = result.accounts;
    for (let i = 0; i < response.parent.length; i++) {
      response.parent[i].child = await getChildDetails(response.parent[i].id)
    }

    if (response) {
      res.status(200).send({
        success: true,
        res: response
      })
    }

  } catch (error) {
    res.status(500).send({
      success: false,
      error: error.meassge,
      stack:error.stack
    });
  }
});

const getChildDetails = async (Id) => {
  try {
    const Users = await users.query().where("parent_account_id", Id)
      .andWhere("is_deleted", false)
      .andWhere('role_id', 'in', [roleEnum.EMPLOYEEROLE]);
    return Users;
  } catch (error) {
    return error;
  }
};

function generateHTMLInquiryEmail(email, password) {
  return ` 
  <!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<title>Welcome to VGA</title>
<style type="text/css"></style>
</head>

<body bgcolor="#FFF" style="margin: 0;min-width: 100%;height: 100%;padding:0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
<table align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
<tr>
  <td bgcolor="#FFF" align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if gte mso 9]>
  <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
  <tr>
  <td align="center" valign="top">
  <![endif]-->
    
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="max-width: 600px;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
      <tr>
        <td align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
            <tr>
              <td id="pre-header" valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:20px;padding-bottom:20px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                  <tr>
                    <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <tr>
                          <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; line-height: 25%;">
              <!-- Partner Branding: Start -->
                <!-- Partner Branding: End -->
              </td>
                        </tr>
                      </table></td>
                    <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <tr>
                          <td valign="top" style="padding-right:15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: right;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <!-- Message: Start -->
              
                <!-- Message: End -->
              </td>
                        </tr>
                      </table></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
            <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
            <table style="width:100%;">
                   <tr>
                     <td style="width:15%;">
                      
                    </td>
                     <td style="width:1%;"><img src="https://image.freepik.com/free-vector/welcome-word-flat-cartoon-people-characters_81522-4207.jpg" alt="Welcome" style="width:200px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"></td>
                     <td style="width:15%;"></td>
                   </tr>
                 </table>
            </td>
           </tr>
      <tr>
        <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
              <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <tr>
                          <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom:15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <!-- Main Headline: Start -->
              
              <!-- <h1 style="font-family: Helvetica, Arial, sans-serif;font-size:36px;line-height: 40px; font-weight: normal; text-align: center; color:#2F5597; margin:0;">Welcome!</h1> -->
              
                <!-- Main Headline: End -->
              </td>
                        </tr>
                      </table></td>
                  </tr>
                </table></td>
            </tr>
        <tr>
              <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <tr>
                          <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:15px;padding-bottom:30px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <!-- Content Area: Start -->
              
              <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;">
                Welcome to VGA, Please use these temporary credentials to sign in and update your password.
                <br/>
                <br/>
                <table>
                  <tr>
                    <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Email Id:</td>
                    <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${email}</td>
                  </tr>
                  <tr>
                    <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Password:</td>
                    <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${password}</td>
                  </tr>
                </table>
              </p>
              
      <br/>                 
                <!-- Content Area: End -->
              </td>
                        </tr>
                      </table></td>
                  </tr>
                </table></td>
            </tr>
        
<!-- 					<tr>
              <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top" style="padding-right: 0;padding-left: 0; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                         <tr>
                    <td align="center" valign="top" style="padding-top: 0;padding-bottom: 0;padding-right: 0px;padding-left: 0px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
             <img src="https://dev.localbox.net/static/email/footer-ilocalbox-gradient-bar.jpg" alt="" width="600" style="max-width:600px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0">
          
          </td>
                  </tr>
                      </table></td>
                  </tr>
                </table></td>
            </tr> -->
          </table></td>
      </tr>
     <tr>
        <td id="footer" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
              <td align="center" valign="top" width="100%" style="padding-top:15px;padding-right: 15px;padding-bottom: 30px;padding-left: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="left" valign="top" width="350" style="max-width: 350px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <tr>
                          <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <!-- Footer iLocal Box Copyright: Start -->
              <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0; margin-bottom: 18px;">&copy; iLocal Box, All Rights Reserved</p> -->
              <!-- Footer iLocal Box Copyright: End -->
              <!-- Footer Partner Info: Start -->
                            
              <!-- Footer Partner Info: End -->
                        </tr>
                      </table></td>
                    <td align="left" valign="top" width="160" style="max-width: 160px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <tr>
                          <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top: 0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <!-- Footer Powered by iLocal Box: Start -->
              <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0;margin-bottom: 5px;"><em>Powered by:</em></p>
                            <img src="https://dev.localbox.net/static/email/logo-ilocalbox.png" alt="iLocal Box" width="110" height="19" style="max-width: 110px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"> -->
              <!-- Footer Powered by iLocal Box: End -->
              </td>
                        </tr>
                      </table></td>
                  </tr>
                </table></td>
            </tr>
          </table></td>
      </tr>  
          </table></td>
      </tr>
    </table>
    
    <!--[if gte mso 9]>
  </td>
  </tr>
  </table>
  <![endif]--></td>
</tr>
</table>
</body>
</html>
   `;
}

export default UsersRoute;