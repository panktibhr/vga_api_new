const express = require("express");
const RoleRoute = express.Router();
const tokenMiddleware = require('./../middleware').default;
const UserType = require("./../models/usersType.Model").default;

RoleRoute.use(async (req, res, next) => {
  next();
});

RoleRoute.get("/roles", tokenMiddleware, async(req,res,next) => {
    const roles = await UserType.query().orderBy("name");
    try {
        if (roles && roles.length > 0) {
             res.status(200).send({
                 success: true,
                 count: roles.length,
                 role: roles
             })
         }
    }catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
     }
});

export default RoleRoute;