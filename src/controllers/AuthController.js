const express = require('express');
const AuthRoute = express.Router();
let jwt = require('jsonwebtoken');
const users = require('./../models/users.Model').default;
const CryptoJS = require("crypto-js");

AuthRoute.use(async (req, res, next) => {
    next();
})

AuthRoute.post('/login', async (req, res, next) => {
    let email = req.body.email;
    let password = req.body.password;
    try {
        let roleWiseUser = await users.query()
            .innerJoin('role', 'users.role_id', 'role.id')
            .innerJoin('accounts', 'users.parent_account_id', 'accounts.id')
            .select(
                'users.id',
                'users.name',
                'users.parent_account_id',
                'users.username',
                'users.picture_url',
                'users.role_id',
                'users.email',
                'users.mobile',
                'users.last_login_on',
                'users.password',
                'role.name as roleName',
                'accounts.name as parentAccountName'
            ).where("users.email", email)
            .andWhere('users.is_deleted', false)
            .andWhere('users.is_enabled', true)
            .andWhere('accounts.is_deleted', false)
            .andWhere('accounts.is_enabled', true)
            .first();

        const pass = CryptoJS.AES.decrypt(roleWiseUser.password, process.env.SECRET).toString(CryptoJS.enc.Utf8);
        console.log('*******' , pass)
        if (roleWiseUser) {
            if (pass === password) {
                if (email && password) {
                    if (email === roleWiseUser.email) {
                        let token = jwt.sign(
                            {
                                username: roleWiseUser.username,
                                created: roleWiseUser.created_on,
                                email: roleWiseUser.email
                            },
                            process.env.SECRET,
                            {
                                expiresIn: '24h'
                            }
                        );
                        delete roleWiseUser.password;
                        res.status(200).send({
                            success: true,
                            message: 'Authentication successful!',
                            token: token,
                            user: roleWiseUser
                        });
                    } else {
                        res.status(404).send({
                            success: false,
                            message: 'Invalid username or password!'
                        })
                    }

                } else {
                    res.status(404).send({
                        success: false,
                        message: 'Invalid username or password!'
                    })
                }
            } else {
                res.status(400).send({
                    success: false,
                    message: 'Authentication failed! Please check the request'
                });
            }

        } else {
            res.status(400).send({
                success: false,
                message: 'Password Did Not Match!'
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
        });
    }
})

AuthRoute.put('/change-password', async (req, res) => {
    let password = req.body.password;
    let id = req.body.id;
    const pass = CryptoJS.AES.encrypt(password, process.env.SECRET).toString();
    try {
        if (pass && id) {
            let updatePassObj = await users.query().patchAndFetchById(id, { 'password': pass });
            if (updatePassObj) {
                res.status(200).send({
                    success: true,
                    message: 'Password Updated successfully.',
                    user: updatePassObj
                })
            }
        } else {
            res.status(400).send({
                success: false,
                message: 'Something went wrong! Please check request.'
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});

AuthRoute.post('/validate-password/:id', async (req, res, next) => {
    let password = req.body.password;
    let id = req.params.id;

    try {
        if (id) {
            let checkuser = await users.query().where("id", id).first();

            if (checkuser) {
                const pass = CryptoJS.AES.decrypt(checkuser.password, process.env.SECRET).toString(CryptoJS.enc.Utf8);
                if (pass === password) {
                    res.status(200).send({
                        success: true,
                        message: 'old password match successfully.',
                    });
                } else {
                    res.status(500).send({
                        success: false,
                        message: 'old password did not match.',
                    })
                }
            } else {
                res.status(400).send({
                    success: false,
                    message: 'User Not Found.'
                })
            }

        } else {
            res.status(404).send({
                success: false,
                message: 'user id not found.'
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});

export default AuthRoute;