const express = require("express");
const ClientEmployeeRoute = express.Router();
const tokenMiddleware = require('./../middleware').default;
const clientsEmployee = require('../models/clientsEmployee.Model').default;
const Departments = require('../models/department.Model').default;
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const Designations = require('../models/designation.Model').default;
const users = require("./../models/users.Model").default;
const CryptoJS = require("crypto-js");
const { validationResult } = require('express-validator');
import {clientEmployeebodyvalidate, updateclientvalidate} from '../common/validator';
import { updateclientemployeevalidate } from '../common/validator';
import { getUserByEmail } from './CommonController';
import roleEnum from './../enums/roleEnum';
import {getaccountliangeBytoken} from './CommonController';
import employeePerformanceQuestionsEnum from '../enums/employeePerformanceQuestionsEnum';
const EmployeePerformanceUsers = require("./../models/employeePerformanceUsersDetails.Model").default;

ClientEmployeeRoute.get("/employee-department", tokenMiddleware, async (req, res, next) => {

    try {
        const department = await Departments.query();
        if (department) {
            res.status(200).send({
                success: true,
                message: "Departments Got Successfully",
                res: department
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

ClientEmployeeRoute.get("/employee-designation", tokenMiddleware, async (req, res, next) => {

    try {
        const designation = await Designations.query();
        if (designation) {
            res.status(200).send({
                success: true,
                message: "Designation Got Successfully",
                res: designation
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

ClientEmployeeRoute.get("/getClientsEmployees", tokenMiddleware, async (req,res) =>{
    let clients;
    
    try{
        clients = await clientsEmployee.query().where('is_deleted', false);
        
        if(clients){
           res.status(200).send({
                success:true,
                meassge:'get all clientsEmployees Successfully.',
                res:clients
            })
        } else{
            res.status(500).send({
                success: false,
                error: 'Something went wrong!'
            })
        }
    } catch(error){
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
})

ClientEmployeeRoute.get("/get-all-clients-employee", tokenMiddleware, async (req,res) =>{
    // let questionId = req.body.questionId;
    // let vgaAuditNo = req.body.vgaAuditNo;
    let clients;
    
    try{
        const employeePerformanceUsers = await EmployeePerformanceUsers.query().where('is_deleted',false);
        clients = await clientsEmployee.query().where('is_deleted', false);
        // for(let i=0; i<employeePerformanceUsers.length; i++){
        //     if(employeePerformanceUsers[i].vga_audit_number === vgaAuditNo){
        //         console.log(employeePerformanceUsers[i]);
        //     }
        // }
        // if(questionId === employeePerformanceQuestionsEnum.SELF){
        //     clients = await clientsEmployee.query().where('is_deleted', false).andWhere('is_emp_status',true).orderBy('name');
        // } else if(questionId === employeePerformanceQuestionsEnum.MANAGER){
        //     clients = await clientsEmployee.query().where('is_deleted', false).andWhere('is_manage_status', false);
        // } else {
        //     clients = await clientsEmployee.query().where('is_deleted', false);
        // }
        
        if(clients){
           res.status(200).send({
                success:true,
                meassge:'get all clientsEmployees Successfully.',
                res:clients
            })
        } else{
            res.status(500).send({
                success: false,
                error: 'Something went wrong!'
            })
        }
    } catch(error){
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
})

ClientEmployeeRoute.post("/", tokenMiddleware, async (req, res) => {
    try {
        let response;
        const result = await getaccountliangeBytoken(req.headers.authorization);

        const pageNo = parseInt(req.body.PageNo || 0);
        const PageSiZe = parseInt(req.body.PageSize || 5);
        const sortColumn = req.body.SortColumn;
        const sortOrder = req.body.SortOrder;
        const SearchText = req.body.SearchText;
        const SearchText2 = req.body.SearchText2;

        if (result.users.role_id === roleEnum.SUPERADMINROLE) {
            response = await clientsEmployee.query()
                .where("is_deleted", false)
                .andWhere("client_employees.parent_account_id", "in", result.accounts.map((x) => x.id))
                .andWhere('client_employees.name ', 'like', '%' + SearchText + '%')
                .orderBy(sortColumn || 'client_employees.name ', sortOrder || 'ASC')
                .page(pageNo, PageSiZe)
                .withGraphFetched("[Departments,Designations]");
        } else {

            response = await clientsEmployee.query()
                .where("is_deleted", false)
                .andWhere("client_employees.parent_account_id", result.users.parent_account_id)
                .andWhere('client_employees.name ', 'like', '%' + SearchText + '%')
                .orderBy(sortColumn || 'client_employees.name ', sortOrder || 'ASC')
                .page(pageNo, PageSiZe)
                .withGraphFetched("[Departments,Designations]");
        }

        if (response) {
            res.status(200).send({
                success: true,
                meassge: "Get all Successfully.",
                res: response
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

ClientEmployeeRoute.get("/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;
    try {
        if (is_id) {
            const clientEmployeeById = await clientsEmployee.query().where("id", is_id).andWhere("is_deleted", false).first();

            if (clientEmployeeById) {
                res.status(200).send({
                    success: true,
                    message: "ClientsEmployee By Id get Successfully",
                    res: clientEmployeeById
                })
            }
        } else {
            res.status(400).send({
                error: error.meassge,
                stack: error.stck,
                message: "Id Not Found."
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        });
    }
});

ClientEmployeeRoute.post("/add-new-clientEmployee", clientEmployeebodyvalidate,tokenMiddleware, async (req, res, next) => {
    let email = req.body.email;
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    try {
        const pass = CryptoJS.AES.encrypt(req.body.password, process.env.SECRET).toString();
        const clientEmployeeDetailsobj = {
            name: req.body.clientEmpName,
            employee_title: req.body.clientEmpTitle,
            employee_level: req.body.clientEmpLevel,
            designation: req.body.designation,
            department: req.body.deptdiv,
            email: req.body.email,
            username: req.body.username,
            parent_account_id: req.body.parent_account_id,
            password: pass,
            mobile: req.body.mobileNumber,
            address: req.body.address,
            countries_id: req.body.country,
            states_id: req.body.state,
            city_id: req.body.city,
            zip: req.body.zipCode,
            client_id: req.body.client_id,
            company_name: req.body.companyName,
        }

        const userdata = {
            password: pass,
            role_id: roleEnum.EMPLOYEEROLE,
            username: clientEmployeeDetailsobj.username,
            email: clientEmployeeDetailsobj.email,
            parent_account_id: clientEmployeeDetailsobj.parent_account_id
        }

        const employeeObj = await clientsEmployee.query().insertGraphAndFetch(clientEmployeeDetailsobj);
        const userobjEntry = await users.query().insertGraphAndFetch(userdata);

        if (employeeObj && userobjEntry) {

            const passDecrepted = CryptoJS.AES.decrypt(userobjEntry.password, process.env.SECRET).toString(CryptoJS.enc.Utf8);
            let smtpTransport = nodemailer.createTransport({
                type: 'OAuth2',
                host: "smtp.gmail.com",
                port: 587,
                secure: false,
                service: 'gmail', // true for 465, false for other ports
                auth: {
                    user: process.env.AUTH_EMAIL,
                    pass: process.env.AUTH_PASSWORD,
                },
                tls: true,
            });
            let info = await smtpTransport.sendMail({
                from: process.env.AUTH_EMAIL,
                to: userobjEntry.email, // list of receivers
                subject: "VGA - Temporary credentials", // Subject line
                text: "Temporary Password With Login.",
                html: generateHTMLInquiryEmail(userobjEntry.email, passDecrepted)
            });
            res.status(200).send({
                success: true,
                message: "Employee Created successfully & Please! check your mail.",
                res: employeeObj
            })
        } else {
            res.status(500).send({
                success: false,
                message: "Something MissMatch With Database Fields"
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});

ClientEmployeeRoute.delete("/delete-clientEmployee/:id", tokenMiddleware, async (req, res, next) => {
    const is_employee_id = req.params.id;
    try {
        let deleted_record = await clientsEmployee.query().where("id", is_employee_id).first();
        if (deleted_record) {
            let _clientsemp = await clientsEmployee.query().where('id', is_employee_id).patch({ is_deleted: true, deleted_on: new Date() });
            const _getUserByEmail = await getUserByEmail(deleted_record.email);
            if (_getUserByEmail !== null) {
                let _user = await users.query().where('id', _getUserByEmail.id).patch({ is_deleted: true, deleted_on: new Date() });
            }
            if (_clientsemp) {
                res.status(200).send({
                    success: true,
                    message: "deleted Successfully."
                });
            }
        } else {
            res.status(500).send({
                success: false,
                message: "clientEmployee Not Found."
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        });
    }
});

ClientEmployeeRoute.put("/update-clientEmployee/:id", updateclientemployeevalidate,tokenMiddleware, async (req, res, next) => {
    const is_employee_id = req.params.id;

    const clientEmployeeDetailsobj = {
        name: req.body.clientEmpName,
        employee_title: req.body.clientEmpTitle,
        employee_level: req.body.clientEmpLevel,
        designation: req.body.designation,
        department: req.body.deptdiv,
        email: req.body.email,
        username: req.body.username,
        parent_account_id: req.body.parent_account_id,
        mobile: req.body.mobileNumber,
        address: req.body.address,
        countries_id: req.body.country,
        states_id: req.body.state,
        city_id: req.body.city,
        zip: req.body.zipCode,
        company_name: req.body.companyName,
        client_id: req.body.clientName
    }

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }

    try {
        const employeeById = await clientsEmployee.query().where("id", is_employee_id).first();
        if (employeeById) {
            const employeeobj = await clientsEmployee.query().patchAndFetchById(is_employee_id, clientEmployeeDetailsobj);
            const _getUserByEmail = await getUserByEmail(clientEmployeeDetailsobj.email);
            if (_getUserByEmail !== null) {
                let _user = {
                    role_id: roleEnum.EMPLOYEEROLE,
                    username: clientEmployeeDetailsobj.username,
                    email: clientEmployeeDetailsobj.email,
                    parent_account_id: clientEmployeeDetailsobj.parent_account_id,
                    name: clientEmployeeDetailsobj.clientEmpName
                }
                await users.query().patchAndFetchById(_getUserByEmail.id, _user);
            }
            if (employeeobj) {
                res.status(200).send({
                    success: true,
                    message: "Update Employee Successfully",
                    res: employeeobj
                })
            }
        }

    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

ClientEmployeeRoute.post("/add-department", tokenMiddleware, async (req, res, next) => {

    const departmentsObj = {
        department_name: req.body.department
    }
    try {
        const departmentDetailsObj = await Departments.query().insertGraphAndFetch(departmentsObj);

        if (departmentDetailsObj) {
            res.status(200).send({
                success: true,
                message: "department Created Successfully",
                res: departmentDetailsObj
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

ClientEmployeeRoute.post("/add-designation", tokenMiddleware, async (req, res, next) => {

    const designationObj = {
        designation_name: req.body.designation
    }
    try {
        const designationDetailsObj = await Designations.query().insertGraphAndFetch(designationObj);

        if (designationDetailsObj) {
            res.status(200).send({
                success: true,
                message: "designation Created Successfully",
                res: designationDetailsObj
            })
        }

    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

ClientEmployeeRoute.post("/:clientId", tokenMiddleware, async (req, res) => {
    let clientId = req.params.clientId;
    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 5);
    const sortColumn = req.body.SortItem;
    const sortOrder = req.body.SortOrder;
    const SearchText = req.body.SearchText;
    
    try{
        const clientsEmployeesByClientId = await clientsEmployee.query().where('is_deleted', false).andWhere('client_id', clientId)
        .page(pageNo, PageSiZe).withGraphFetched("[Departments,Designations]");

        if(clientsEmployeesByClientId){
            res.status(200).send({
                success: true,
                message: "Get Client Employees By Clients!",
                res: clientsEmployeesByClientId
            })
        } 
    } catch (error) {
        res.status(500).send({
            success: false,
            message: error.message,
            stack: error.stack
        })
    }
})

ClientEmployeeRoute.post("/:clientId/:empLevel", tokenMiddleware, async (req, res) => {
    let clientId = req.params.clientId;
    let empLevel = req.params.empLevel;
    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 5);
    const sortColumn = req.body.SortItem;
    const sortOrder = req.body.SortOrder;
    const SearchText = req.body.SearchText;
    let employeeLevel
    
    try{
        if(empLevel === '1'){
            employeeLevel = 'Top Level'
        } else if(empLevel === '2'){
            employeeLevel = 'Managerial Level'
        } else if(empLevel === '3'){
            employeeLevel = 'Executive Level'
        }
        const clientsEmployeesByClientId = await clientsEmployee.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('employee_level', employeeLevel)
        .page(pageNo, PageSiZe).withGraphFetched("[Departments,Designations]");
5
        if(clientsEmployeesByClientId){
            res.status(200).send({
                success: true,
                message: "Get Client Employees By Clients!",
                res: clientsEmployeesByClientId
            })
        } 
    } catch (error) {
        res.status(500).send({
            success: false,
            message: error.message,
            stack: error.stack
        })
    }
})

function generateHTMLInquiryEmail(email, password) {
    return ` 
    <!doctype html>
  <html>
  <head>
  <meta charset="utf-8">
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
  <title>Welcome to VGA</title>
  <style type="text/css"></style>
  </head>
  
  <body bgcolor="#FFF" style="margin: 0;min-width: 100%;height: 100%;padding:0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
  <table align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
  <tr>
    <td bgcolor="#FFF" align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if gte mso 9]>
    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
    <tr>
    <td align="center" valign="top">
    <![endif]-->
      
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="max-width: 600px;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tr>
          <td align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <tr>
                <td id="pre-header" valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:20px;padding-bottom:20px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tr>
                      <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; line-height: 25%;">
                <!-- Partner Branding: Start -->
                  <!-- Partner Branding: End -->
                </td>
                          </tr>
                        </table></td>
                      <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right:15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: right;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Message: Start -->
                
                  <!-- Message: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
              <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <table style="width:100%;">
                     <tr>
                       <td style="width:15%;">
                        
                      </td>
                       <td style="width:1%;"><img src="https://image.freepik.com/free-vector/welcome-word-flat-cartoon-people-characters_81522-4207.jpg" alt="Welcome" style="width:200px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"></td>
                       <td style="width:15%;"></td>
                     </tr>
                   </table>
              </td>
             </tr>
        <tr>
          <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
      <table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom:15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Main Headline: Start -->
                
                <!-- <h1 style="font-family: Helvetica, Arial, sans-serif;font-size:36px;line-height: 40px; font-weight: normal; text-align: center; color:#2F5597; margin:0;">Welcome!</h1> -->
                
                  <!-- Main Headline: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
          <tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:15px;padding-bottom:30px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Content Area: Start -->
                
                <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;">
                  Welcome to VGA, Please use these temporary credentials to sign in and update your password.
                  <br/>
                  <br/>
                  <table>
                    <tr>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Email Id:</td>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${email}</td>
                    </tr>
                    <tr>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Password:</td>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${password}</td>
                    </tr>
                  </table>
                </p>
                
        <br/>                 
                  <!-- Content Area: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
          
  <!-- 					<tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 0;padding-left: 0; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                           <tr>
                      <td align="center" valign="top" style="padding-top: 0;padding-bottom: 0;padding-right: 0px;padding-left: 0px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <img src="https://dev.localbox.net/static/email/footer-ilocalbox-gradient-bar.jpg" alt="" width="600" style="max-width:600px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0">
            
            </td>
                    </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr> -->
            </table></td>
        </tr>
       <tr>
          <td id="footer" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td align="center" valign="top" width="100%" style="padding-top:15px;padding-right: 15px;padding-bottom: 30px;padding-left: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="left" valign="top" width="350" style="max-width: 350px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Footer iLocal Box Copyright: Start -->
                <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0; margin-bottom: 18px;">&copy; iLocal Box, All Rights Reserved</p> -->
                <!-- Footer iLocal Box Copyright: End -->
                <!-- Footer Partner Info: Start -->
                              
                <!-- Footer Partner Info: End -->
                          </tr>
                        </table></td>
                      <td align="left" valign="top" width="160" style="max-width: 160px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top: 0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Footer Powered by iLocal Box: Start -->
                <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0;margin-bottom: 5px;"><em>Powered by:</em></p>
                              <img src="https://dev.localbox.net/static/email/logo-ilocalbox.png" alt="iLocal Box" width="110" height="19" style="max-width: 110px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"> -->
                <!-- Footer Powered by iLocal Box: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>  
            </table></td>
        </tr>
      </table>
      
      <!--[if gte mso 9]>
    </td>
    </tr>
    </table>
    <![endif]--></td>
  </tr>
  </table>
  </body>
  </html>
     `;
};

export default ClientEmployeeRoute;