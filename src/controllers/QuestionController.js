const express = require("express");
const questionRoute = express.Router();
const section = require('../models/Section.Model').default;
const subSection = require('../models/SubSection.Model').default;
const Question = require('../models/question.Model').default;
const AnswerValues = require('../models/ReportAnswerValue.Model').default;
const AuditDetails = require('../models/AuditDetails.Model').default;
const Audit = require('../models/Audit.Model').default;
const Clients = require('../models/client.Model').default;
const tokenMiddleware = require('./../middleware').default;
const subQuestions = require('../models/subQuestions.Model').default;
const subSubQuestions = require('../models/subSubQuestions.Model').default;
const employeePerformance = require('../models/EmployeePerformance.Model').default;
const empPerformnceUsersDetails =require('../models/employeePerformanceUsersDetails.Model').default;
const PlanOfActionAndRecommendations = require('../models/planOfActionAndRecommendations.Model').default;
const jwt = require('jsonwebtoken');
import Users from '../models/users.Model';
import statusReportEnum from '../enums/statusReportEnum';
import ReportPdfController from "./ReportPdfController";
import { sectionProcessing } from '../services/sectionsprocess';
import { sectionProcessing2 } from '../services/sectionsprocess2';
import sectionEnum from '../enums/sectionEnum';
import performanceEnum from '../enums/performanceEnum';
import competencyMappingEnum from '../enums/CompetencyMappingEnum';
import employeePerformanceUsersDetails from '../models/employeePerformanceUsersDetails.Model';
const nodemailer = require('nodemailer');
let sectionDetails;
let commonhtml = ``;

export const getSubSectionBySectionId = async (sectionId) => {
    try {
        const _subSectionBySectionId = await subSection.query().where('section_id', sectionId).andWhere('is_deleted', false);
        return _subSectionBySectionId;
    } catch (error) {
        return (`Error while getting sub section by section Id : ${error}`)
    }
}

export const updateAuditReportStatus = async (auditno, sections) => {
    try {
        const auditStatus = await Audit.query().where('vga_audit_number', auditno).andWhere('is_deleted', false).first();
        const clients = await Clients.query().where('is_deleted',false).andWhere('id', auditStatus.client_id).first();
        sectionDetails = clients.section_details;

        if (auditStatus) {
            const patchStatus = await AuditDetails.query()
                .where('section_id', sections.id)
                .andWhere('vga_audit_id', auditStatus.id)
                .andWhere('is_deleted', false)
                .patch({ status: statusReportEnum.SUBMITTEDSTATUS });

                let smtpTransport = nodemailer.createTransport({
                    type: 'OAuth2',
                    host: "smtp.gmail.com",
                    port: 587,
                    secure: false,
                    service: 'gmail', // true for 465, false for other ports
                    auth: {
                        user: process.env.AUTH_EMAIL,
                        pass: process.env.AUTH_PASSWORD,
                    },
                    tls: true,
                });
                  
                let info = await smtpTransport.sendMail({
                    from: process.env.AUTH_EMAIL, // sender address
                    to: clients.email, // list of receivers
                    subject: "VGA - Report Status", // Subject line
                    text: "Report Status",
                    html: generateHTMLInquiryEmail(auditStatus.vga_audit_number, auditStatus.status, sectionDetails, auditStatus.
                        audit_from_date, auditStatus.audit_to_date)
                });

            const updatedStatue = await AuditDetails.query().where('section_id', sections.id).andWhere('vga_audit_id', auditStatus.id).andWhere('is_deleted', false);
            return updatedStatue
        } else {
            throw new error('audit report not found');
        }
    } catch (error) {
        return error.message    //fix bug by Nikesh Thakkar 
        // res.status(500).send({
        //     error: error.message,
        //     stack: error.stack,
        //     message : "Something went wrong!"
        // })
    }
}

export const checkAuditReportStatus = async (auditno, sectionId) => {
    try {
        const auditStatus = await Audit.query().where('vga_audit_number', auditno).andWhere('is_deleted', false).first();
        if (auditStatus) {
            const sectionsStatue = await AuditDetails.query()
                .where('section_id', sectionId)
                .andWhere('vga_audit_id', auditStatus.id)
                .andWhere('is_deleted', false);

            if (sectionsStatue[0].status === statusReportEnum.SUBMITTEDSTATUS || sectionsStatue[0].status === statusReportEnum.REVIEWINGSTATUS || sectionsStatue[0].status === statusReportEnum.REVIEWEDSTATUS || sectionsStatue[0].status === statusReportEnum.SUBMITEDTOCLIENTSTATUS) {
                return sectionsStatue
            } else {
                return {success:false,
                  meassge:'report status is not submitted!'}
            }

        } else {
            return {success:false,
                meassge:'report not found!'}
        }

    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message : "Something went wrong!"
        })
    }
}

questionRoute.post('/average', tokenMiddleware, async (req, res, next) => {

    let section_id = req.body.sectionId;
    let auditno = req.body.vgAuditNo;
    
    let response = {};
    try {
        if (section_id === sectionEnum.OrganizationDiagnosis && auditno) {
            const checkstatus = await checkAuditReportStatus(auditno, section_id);
            if (checkstatus.length !== 0 && checkstatus.success !== false) {
                const sub = await getSubSectionBySectionId(section_id);
                if (sub) {
                    for (let i = 0; i < sub.length; i++) {
                        const allQuestions = await getQuestionsBySubSectionIdAndSectionId(section_id, sub[i].id);
                        const _responseObject = {
                            sub_section_id: "",
                            externalMacroQuestions: [],
                            externalMicroQuestions: [],
                            internalFactorsQuestions: []
                        }
                        if (allQuestions) {
                            const _externalMacroBySubtopicAndQuestionType = allQuestions.filter((item) => item.question_type === 1 && item.sub_section_id === sub[i].id);
                            const _externamMicroBySubTopicAndQuestionType = allQuestions.filter((item) => item.question_type === 2 && item.sub_section_id === sub[i].id);
                            const _internalFactorsBySubtopicAndQuestionType = allQuestions.filter((item) => item.question_type === 3 && item.sub_section_id === sub[i].id);

                            let avgOfExternalMacroQuestion = 0,
                                avgOfExternalMicroQuestion = 0,
                                avgOfinternalFactorQuestion = 0,
                                totalExternamMacroCount = 0,
                                totalExternamMicroCount = 0,
                                totalInternalFactorsCount = 0,
                                totalExternamMacroAnswersCount = 0,
                                totalExternamMicroAnswersCount = 0,
                                totalInternalFactorsAnswersCount = 0,
                                externalMacroAnswerCounter = 0,
                                externalMicroAnswerCounter = 0,
                                internalFactorsAnswerCounter = 0,
                                MacroCounter = 0,
                                MicroCounter = 0,
                                InternalfactorCounter = 0;
                            for (let j = 0; j < _externalMacroBySubtopicAndQuestionType.length; j++) {
                                let externalMacroSum = 0;
                                _responseObject.sub_section_id = sub[i].id;
                                const getAnswersByQuestionId = await getReportAnswers(_externalMacroBySubtopicAndQuestionType[j].id, auditno);
                                for (let k = 0; k < getAnswersByQuestionId.length; k++) {
                                    externalMacroSum += +getAnswersByQuestionId[k].answer_value;
                                    externalMacroAnswerCounter = getAnswersByQuestionId.length;
                                    avgOfExternalMacroQuestion = Math.floor(externalMacroSum / externalMacroAnswerCounter);
                                }
                                _responseObject.externalMacroQuestions.push({ "question_id": _externalMacroBySubtopicAndQuestionType[j].id, "question_name": _externalMacroBySubtopicAndQuestionType[j].question, "questionType": _externalMacroBySubtopicAndQuestionType[j].question_type, "externalMacroSum": externalMacroSum, "totalExternamMacroCount": externalMacroAnswerCounter, "externalMacroAvgOfQuestions": avgOfExternalMacroQuestion });
                                const avg = await AnswerValues.query().where('question_id', _externalMacroBySubtopicAndQuestionType[j].id).andWhere('is_deleted', false).patch({ question_average: avgOfExternalMacroQuestion });
                                totalExternamMacroAnswersCount += externalMacroSum;
                                _responseObject.totalExternamMacroAnswersCount = totalExternamMacroAnswersCount;
                                totalExternamMacroCount += externalMacroAnswerCounter;
                                _responseObject.totalExternamMacroCount = totalExternamMacroCount;
                                _responseObject.totalRatioOfMacroQuestions = Math.floor(totalExternamMacroAnswersCount / totalExternamMacroCount)
                                MacroCounter++;
                                if (MacroCounter === _externalMacroBySubtopicAndQuestionType.length) {
                                    for (let i = 0; i < _externalMacroBySubtopicAndQuestionType.length; i++) {
                                        const avg = await AnswerValues.query().where('question_id', _externalMacroBySubtopicAndQuestionType[i].id).andWhere('is_deleted', false).patch({ total_average: _responseObject.totalRatioOfMacroQuestions });
                                        if (avg) {
                                            _responseObject.meassge1 = 'External Macro averaege is stored in answersReports table.';
                                        } else {
                                            _responseObject.meassge1 = 'something went Wrong.';
                                        }
                                    }
                                }
                            }

                            //externalMicro
                            for (let j = 0; j < _externamMicroBySubTopicAndQuestionType.length; j++) {
                                let externalMicroSum = 0;
                                _responseObject.sub_section_id = sub[i].id;
                                const getAnswersByQuestionId = await getReportAnswers(_externamMicroBySubTopicAndQuestionType[j].id, auditno);
                                for (let k = 0; k < getAnswersByQuestionId.length; k++) {
                                    externalMicroSum += +getAnswersByQuestionId[k].answer_value;
                                    externalMicroAnswerCounter = getAnswersByQuestionId.length;
                                    avgOfExternalMicroQuestion = Math.floor(externalMicroSum / externalMicroAnswerCounter);
                                }
                                _responseObject.externalMicroQuestions.push({ "question_id": _externamMicroBySubTopicAndQuestionType[j].id, "question_name": _externamMicroBySubTopicAndQuestionType[j].question, "questionType": _externamMicroBySubTopicAndQuestionType[j].question_type, "externalMicroSum": externalMicroSum, "totalExternamMicroCount": externalMicroAnswerCounter, "externalMicroAvgOfQuestions": avgOfExternalMicroQuestion });
                                const avg = await AnswerValues.query().where('question_id', _externamMicroBySubTopicAndQuestionType[j].id).andWhere('is_deleted', false).patch({ question_average: avgOfExternalMicroQuestion });
                                totalExternamMicroAnswersCount += externalMicroSum
                                _responseObject.totalExternamMicroAnswersCount = totalExternamMicroAnswersCount;
                                totalExternamMicroCount += externalMicroAnswerCounter;
                                _responseObject.totalExternamMicroCount = totalExternamMicroCount;
                                _responseObject.totalRatioOfMicroQuestions = Math.floor(totalExternamMicroAnswersCount / totalExternamMicroCount)
                                MicroCounter++;
                                if (MicroCounter === _externamMicroBySubTopicAndQuestionType.length) {
                                    for (let i = 0; i < _externamMicroBySubTopicAndQuestionType.length; i++) {

                                        const avg = await AnswerValues.query().where('question_id', _externamMicroBySubTopicAndQuestionType[i].id).andWhere('is_deleted', false).patch({ total_average: _responseObject.totalRatioOfMicroQuestions });
                                        if (avg) {
                                            _responseObject.meassge2 = 'External Micro averaege is stored in answersReports table.';
                                        } else {
                                            _responseObject.meassge2 = 'something went Wrong.';
                                        }
                                    }
                                }
                            }

                            //internalfactors
                            for (let j = 0; j < _internalFactorsBySubtopicAndQuestionType.length; j++) {
                                let internalFactorSum = 0;
                                _responseObject.sub_section_id = sub[i].id;
                                const getAnswersByQuestionId = await getReportAnswers(_internalFactorsBySubtopicAndQuestionType[j].id, auditno);
                                for (let k = 0; k < getAnswersByQuestionId.length; k++) {
                                    internalFactorSum += +getAnswersByQuestionId[k].answer_value;
                                    internalFactorsAnswerCounter = getAnswersByQuestionId.length;
                                    avgOfinternalFactorQuestion = Math.floor(internalFactorSum / internalFactorsAnswerCounter)
                                }
                                _responseObject.internalFactorsQuestions.push({ "question_id": _internalFactorsBySubtopicAndQuestionType[j].id, "question_name": _internalFactorsBySubtopicAndQuestionType[j].question, "questionType": _internalFactorsBySubtopicAndQuestionType[j].question_type, "internalFactorSum": internalFactorSum, "internalFactorsCount": internalFactorsAnswerCounter, "internalFactorsAvgOfQuestions": avgOfinternalFactorQuestion });
                                const avg = await AnswerValues.query().where('question_id', _internalFactorsBySubtopicAndQuestionType[j].id).andWhere('is_deleted', false).patch({ question_average: avgOfinternalFactorQuestion });
                                totalInternalFactorsAnswersCount += internalFactorSum;
                                _responseObject.totalInternalFactorsAnswersCount = totalInternalFactorsAnswersCount;
                                totalInternalFactorsCount += internalFactorsAnswerCounter;
                                _responseObject.totalInternalFactorsCount = totalInternalFactorsCount;
                                _responseObject.totalRatioOfInternalfactorsQuestions = Math.floor(totalInternalFactorsAnswersCount / totalInternalFactorsCount)
                                InternalfactorCounter++;
                                if (InternalfactorCounter === _internalFactorsBySubtopicAndQuestionType.length) {
                                    for (let i = 0; i < _internalFactorsBySubtopicAndQuestionType.length; i++) {

                                        const avg = await AnswerValues.query().where('question_id', _internalFactorsBySubtopicAndQuestionType[i].id).andWhere('is_deleted', false).patch({ total_average: _responseObject.totalRatioOfInternalfactorsQuestions });
                                        if (avg) {
                                            _responseObject.meassge3 = ' InternalFactors averaege is stored in answersReports table.';
                                        } else {
                                            _responseObject.meassge3 = 'something went Wrong.';
                                        }
                                    }
                                }
                            }
                        } else {
                            res.status(500).send({
                                success:false,
                                meassge:'questions not found!'
                            })
                        }
                        response[sub[i].id] = _responseObject;
                    }
                } else {
                    res.status(500).send({
                        success:false,
                        meassge:'sectionId not found!'
                    })
                }
            } else {
                res.status(500).send({
                    success: false,
                    meassge: 'Please, first fill all questions answers!'
                })
            }
            return res.status(200).send({
                success: true,
                res: response
            });
        }

        else if(section_id === sectionEnum.CompetencyMapping && auditno){
            const checkstatus = await checkAuditReportStatus(auditno, section_id);
            if (checkstatus.length !== 0 && checkstatus.success !== false) {
                const sub = await getSubSectionBySectionId(section_id);
                if (sub) {
                    for (let i = 0; i < sub.length; i++) {
                        const allQuestions = await getQuestionsBySubSectionIdAndSectionId(section_id, sub[i].id);
                        const _responseObject = {
                            sub_section_id: "",
                            performanceOfTheCompetencies: [],
                            importanceOfTopCompetencies: [],
                            importanceOfManagerialCompetencies: [],
                            importanceOfExecutiveCompetencies: []
                        }
                        if (allQuestions) {
                            // const _performanceOfTheCompetenciesQuestionType = allQuestions.filter((item) => item.sequence_number === 2 && item.section_id === section_id);
                            const _importanceOfTheTopCompetenciesQuestionType = allQuestions.filter((item) => item.question_type === 6 && item.section_id === section_id);
                            const _importanceOfTheManagerialCompetenciesQuestionType = allQuestions.filter((item) => item.question_type === 7 && item.section_id === section_id);
                            const _importanceOfTheExecutiveCompetenciesQuestionType = allQuestions.filter((item) => item.question_type === 8 && item.section_id === section_id);
        
                            let avgOfImportanceOfTopCompetenciesQuestion = 0,
                                importanceOfTopCompetenciesAnswerCounter = 0,
                                avgOfImportanceOfManagerialCompetenciesQuestion = 0,
                                importanceOfManagerialCompetenciesAnswerCounter = 0,
                                avgOfImportanceOfExecutiveCompetenciesQuestion = 0,
                                importanceOfExecutiveCompetenciesAnswerCounter = 0;
        
                            // Importance Of the Top Competencies
                            for (let j = 0; j < _importanceOfTheTopCompetenciesQuestionType.length; j++) {
                                let importanceTopCompetenciesSum = 0;
                                const getAnswersByQuestionId = await getReportAnswers(_importanceOfTheTopCompetenciesQuestionType[j].id, auditno);
                                for (let k = 0; k < getAnswersByQuestionId.length; k++) {
                                    if(competencyMappingEnum.TYPE1 === getAnswersByQuestionId[k].answer_value){
                                        importanceTopCompetenciesSum += 1;
                                    } else if(competencyMappingEnum.TYPE2 === getAnswersByQuestionId[k].answer_value){
                                        importanceTopCompetenciesSum += 2;
                                    } else if(competencyMappingEnum.TYPE3 === getAnswersByQuestionId[k].answer_value){
                                        importanceTopCompetenciesSum += 3;
                                    } else if(competencyMappingEnum.TYPE4 === getAnswersByQuestionId[k].answer_value){
                                        importanceTopCompetenciesSum += 4;
                                    } else if(competencyMappingEnum.TYPE5 === getAnswersByQuestionId[k].answer_value){
                                        importanceTopCompetenciesSum += 5;
                                    }
                                    importanceOfTopCompetenciesAnswerCounter = getAnswersByQuestionId.length;
                                    avgOfImportanceOfTopCompetenciesQuestion = Math.floor(importanceTopCompetenciesSum / importanceOfTopCompetenciesAnswerCounter);
                                }
                                console.log(avgOfImportanceOfTopCompetenciesQuestion)
                                _responseObject.importanceOfTopCompetencies.push({ "question_id": _importanceOfTheTopCompetenciesQuestionType[j].id, "question_name": _importanceOfTheTopCompetenciesQuestionType[j].question, "questionType": _importanceOfTheTopCompetenciesQuestionType[j].question_type, "importanceTopCompetenciesSum": importanceTopCompetenciesSum, "importanceOfTopCompetenciesAnswerCounter": importanceOfTopCompetenciesAnswerCounter, "avgOfImportanceOfTopCompetenciesQuestion": avgOfImportanceOfTopCompetenciesQuestion });
                                const avg = await AnswerValues.query().where('question_id', _importanceOfTheTopCompetenciesQuestionType[j].id).andWhere('is_deleted', false).patch({ question_average: avgOfImportanceOfTopCompetenciesQuestion });
                                console.log(avg)
                                if (avg) {
                                    _responseObject.meassge1 = 'Importance of the Competencies averaege is stored in answersReport table!';
                                } else {
                                    _responseObject.meassge1 = 'something went Wrong!';
                                }
                            }

                            // Importance Of the Managerial Competencies
                            for (let j = 0; j < _importanceOfTheManagerialCompetenciesQuestionType.length; j++) {
                                let importanceManagerialCompetenciesSum = 0;
                                const getAnswersByQuestionId = await getReportAnswers(_importanceOfTheManagerialCompetenciesQuestionType[j].id, auditno);
                                for (let k = 0; k < getAnswersByQuestionId.length; k++) {
                                    if(competencyMappingEnum.TYPE1 === getAnswersByQuestionId[k].answer_value){
                                        importanceManagerialCompetenciesSum += 1;
                                    } else if(competencyMappingEnum.TYPE2 === getAnswersByQuestionId[k].answer_value){
                                        importanceManagerialCompetenciesSum += 2;
                                    } else if(competencyMappingEnum.TYPE3 === getAnswersByQuestionId[k].answer_value){
                                        importanceManagerialCompetenciesSum += 3;
                                    } else if(competencyMappingEnum.TYPE4 === getAnswersByQuestionId[k].answer_value){
                                        importanceManagerialCompetenciesSum += 4;
                                    } else if(competencyMappingEnum.TYPE5 === getAnswersByQuestionId[k].answer_value){
                                        importanceManagerialCompetenciesSum += 5;
                                    }

                                    importanceOfManagerialCompetenciesAnswerCounter = getAnswersByQuestionId.length;
                                    avgOfImportanceOfManagerialCompetenciesQuestion = Math.floor(importanceManagerialCompetenciesSum / importanceOfManagerialCompetenciesAnswerCounter);
                                }
                                _responseObject.importanceOfManagerialCompetencies.push({ "question_id": _importanceOfTheManagerialCompetenciesQuestionType[j].id, "question_name": _importanceOfTheManagerialCompetenciesQuestionType[j].question, "questionType": _importanceOfTheManagerialCompetenciesQuestionType[j].question_type, "importanceTopCompetenciesSum": importanceManagerialCompetenciesSum, "importanceOfTopCompetenciesAnswerCounter": importanceOfManagerialCompetenciesAnswerCounter, "avgOfImportanceOfTopCompetenciesQuestion": avgOfImportanceOfManagerialCompetenciesQuestion });

                                const avg = await AnswerValues.query().where('question_id', _importanceOfTheManagerialCompetenciesQuestionType[j].id).andWhere('is_deleted', false).patch({ question_average: avgOfImportanceOfManagerialCompetenciesQuestion });
                                console.log(avg)
                                if (avg) {
                                    _responseObject.meassge1 = 'Importance of the Competencies averaege is stored in answersReport table!';
                                } else {
                                    _responseObject.meassge1 = 'something went Wrong!';
                                }
                            }

                            // Importance Of the Executive Competencies
                            for (let j = 0; j < _importanceOfTheExecutiveCompetenciesQuestionType.length; j++) {
                                let importanceOfExecutiveCompetencySum = 0;
                                const getAnswersByQuestionId = await getReportAnswers(_importanceOfTheExecutiveCompetenciesQuestionType[j].id, auditno);
                                for (let k = 0; k < getAnswersByQuestionId.length; k++) {
                                    if(competencyMappingEnum.TYPE1 === getAnswersByQuestionId[k].answer_value){
                                        importanceOfExecutiveCompetencySum += 1;
                                    } else if(competencyMappingEnum.TYPE2 === getAnswersByQuestionId[k].answer_value){
                                        importanceOfExecutiveCompetencySum += 2;
                                    } else if(competencyMappingEnum.TYPE3 === getAnswersByQuestionId[k].answer_value){
                                        importanceOfExecutiveCompetencySum += 3;
                                    } else if(competencyMappingEnum.TYPE4 === getAnswersByQuestionId[k].answer_value){
                                        importanceOfExecutiveCompetencySum += 4;
                                    } else if(competencyMappingEnum.TYPE5 === getAnswersByQuestionId[k].answer_value){
                                        importanceOfExecutiveCompetencySum += 5;
                                    }

                                    importanceOfExecutiveCompetenciesAnswerCounter = getAnswersByQuestionId.length;
                                    avgOfImportanceOfExecutiveCompetenciesQuestion = Math.floor(importanceOfExecutiveCompetencySum / importanceOfExecutiveCompetenciesAnswerCounter);
                                }
                                _responseObject.importanceOfExecutiveCompetencies.push({ "question_id": _importanceOfTheExecutiveCompetenciesQuestionType[j].id, "question_name": _importanceOfTheExecutiveCompetenciesQuestionType[j].question, "questionType": _importanceOfTheExecutiveCompetenciesQuestionType[j].question_type, "importanceOfExecutiveCompetencySum": importanceOfExecutiveCompetencySum, "importanceOfExecutiveCompetenciesAnswerCounter": importanceOfExecutiveCompetenciesAnswerCounter, "avgOfImportanceOfExecutiveCompetenciesQuestion": avgOfImportanceOfExecutiveCompetenciesQuestion });

                                const avg = await AnswerValues.query().where('question_id', _importanceOfTheExecutiveCompetenciesQuestionType[j].id).andWhere('is_deleted', false).patch({ question_average: avgOfImportanceOfExecutiveCompetenciesQuestion });
                                console.log(avg)
                                if (avg) {
                                    _responseObject.meassge1 = 'Importance of the Competencies averaege is stored in answersReport table!';
                                } else {
                                    _responseObject.meassge1 = 'something went Wrong!';
                                }
                            }

                        } else {
                            res.status(500).send({
                                success:false,
                                meassge:'Questions not found!'
                            })
                        }
                        response['section'] = _responseObject;
                    }
                }
                else {
                    res.status(500).send({
                        success:false,
                        meassge:'sectionId not found!'
                    })
                }
            } else {
                res.status(500).send({
                    success: false,
                    meassge: 'Please, first fill all questions answers!'
                })
            }
            return res.status(200).send({
                success: true,
                res: response
            });
        }

        else if(section_id === sectionEnum.PerformanceEvaluation && auditno){
            const checkstatus = await checkAuditReportStatus(auditno, section_id);
            if (checkstatus.length !== 0 && checkstatus.success !== false) {
                const allQuestions = await getQuestionsBySectionId(section_id);
                const _responseObject = {
                    sub_section_id: "",
                    individualPerformanceEvaluation: [],
                    organizationalPerformanceEvaluation: []
                }
                if (allQuestions) {
                    const _individualPerformanceQuestionType = allQuestions.filter((item) => item.question_type === 4 && item.section_id === section_id);
                    const _organizationalPerformanceQuestionType = allQuestions.filter((item) => item.question_type === 5 && item.section_id === section_id);

                    let avgOfIndividualPerformanceQuestion = 0,
                        avgOfOrganizationalPerformanceQuestion = 0,
                        totalOrganizationalPerformanceCount = 0,
                        totalOrganizationalPerformanceAnswersCount = 0,
                        individualPerformanceAnswerCounter = 0,
                        organizationalPerformanceAnswerCounter = 0,
                        OrganizationalCounter = 0;

                    // Organizational Performance
                    for (let j = 0; j < _organizationalPerformanceQuestionType.length; j++) {
                        let organizationalPerformanceSum = 0;
                        const getAnswersByQuestionId = await getReportAnswers(_organizationalPerformanceQuestionType[j].id, auditno);
                        for (let k = 0; k < getAnswersByQuestionId.length; k++) {
                            if(performanceEnum.TYPE1 === getAnswersByQuestionId[k].answer_value){
                                organizationalPerformanceSum += 1;
                            } else if(performanceEnum.TYPE2 === getAnswersByQuestionId[k].answer_value){
                                organizationalPerformanceSum += 2;
                            } else if(performanceEnum.TYPE3 === getAnswersByQuestionId[k].answer_value){
                                organizationalPerformanceSum += 3;
                            } else if(performanceEnum.TYPE4 === getAnswersByQuestionId[k].answer_value){
                                organizationalPerformanceSum += 4;
                            }
                            organizationalPerformanceAnswerCounter = getAnswersByQuestionId.length;
                            avgOfOrganizationalPerformanceQuestion = Math.floor(organizationalPerformanceSum / organizationalPerformanceAnswerCounter);
                        }
                        _responseObject.organizationalPerformanceEvaluation.push({ "question_id": _organizationalPerformanceQuestionType[j].id, "question_name": _organizationalPerformanceQuestionType[j].question, "questionType": _organizationalPerformanceQuestionType[j].question_type, "organizationalPerformanceSum": organizationalPerformanceSum, "totalOrganizationalPerformanceCount": organizationalPerformanceAnswerCounter, "avgOfOrganizationalPerformanceQuestion": avgOfOrganizationalPerformanceQuestion });
                        const avg = await AnswerValues.query().where('question_id', _organizationalPerformanceQuestionType[j].id).andWhere('is_deleted', false).patch({ question_average: avgOfOrganizationalPerformanceQuestion });
                        totalOrganizationalPerformanceAnswersCount += organizationalPerformanceSum;
                        _responseObject.totalOrganizationalPerformanceAnswersCount = totalOrganizationalPerformanceAnswersCount;
                        totalOrganizationalPerformanceCount += organizationalPerformanceAnswerCounter;
                        _responseObject.totalOrganizationalPerformanceCount = totalOrganizationalPerformanceCount;
                        _responseObject.totalRatioOfOrganizationalPerformanceQuestions = Math.floor(totalOrganizationalPerformanceAnswersCount / totalOrganizationalPerformanceCount)
                        OrganizationalCounter++;
                        if (OrganizationalCounter === _organizationalPerformanceQuestionType.length) {
                            for (let i = 0; i < _organizationalPerformanceQuestionType.length; i++) {
                                const avg = await AnswerValues.query().where('question_id', _organizationalPerformanceQuestionType[i].id).andWhere('is_deleted', false).patch({ total_average: _responseObject.totalRatioOfOrganizationalPerformanceQuestions });
                                if (avg) {
                                    _responseObject.meassge1 = 'Organizations Performance averaege is stored in answersReport table!';
                                } else {
                                    _responseObject.meassge1 = 'something went Wrong!';
                                }
                            }
                        }
                    }

                    // Individual Performance
                    for (let i = 0; i < _individualPerformanceQuestionType.length; i++) {
                        const getEmployeePerformanceQuestions = await getQuestionsByEmployeePerformanceQuestionId(section_id, _individualPerformanceQuestionType[i].id)
                        for(let j = 0; j < getEmployeePerformanceQuestions.length; j++){
                            let individualPerformanceSum = 0;
                            const getEmployeePerformanceUsers = await getEmployeesOfEmployeePerformance(_individualPerformanceQuestionType[i].id, getEmployeePerformanceQuestions[j].id, auditno);
                            for(let a = 0; a < getEmployeePerformanceUsers.length; a++){
                                individualPerformanceSum = 0;
                                const getAnswersByQuestionId = await getReportAnswersForEmployeePerformance(_individualPerformanceQuestionType[i].id, getEmployeePerformanceQuestions[j].id, auditno, getEmployeePerformanceUsers[a].employee_id);
                                for (let k = 0; k < getAnswersByQuestionId.length; k++) {
                                    if(getEmployeePerformanceQuestions[j].id === getAnswersByQuestionId[k].employee_performance_questions_id && getAnswersByQuestionId[k].employee_id === getEmployeePerformanceUsers[a].employee_id){
                                        if(performanceEnum.TYPE1 === getAnswersByQuestionId[k].answer_value){
                                            individualPerformanceSum += 1;
                                        } else if(performanceEnum.TYPE2 === getAnswersByQuestionId[k].answer_value){
                                            individualPerformanceSum += 2;
                                        } else if(performanceEnum.TYPE3 === getAnswersByQuestionId[k].answer_value){
                                            individualPerformanceSum += 3;
                                        } else if(performanceEnum.TYPE4 === getAnswersByQuestionId[k].answer_value){
                                            individualPerformanceSum += 4;
                                        }
                                    }
                                }
                                individualPerformanceAnswerCounter = getAnswersByQuestionId.length;
                                avgOfIndividualPerformanceQuestion = Math.floor(individualPerformanceSum / individualPerformanceAnswerCounter);
                                _responseObject.individualPerformanceEvaluation.push({ "question_id": _individualPerformanceQuestionType[i].id, "question_name": _individualPerformanceQuestionType[i].question, "questionType": _individualPerformanceQuestionType[i].question_type,
                                "employeePerformanceQuestionId": getEmployeePerformanceQuestions[j].id,"employeePerformanceQuestionName": getEmployeePerformanceQuestions[j].name, "employeeId": getEmployeePerformanceUsers[a].employee_id,
                                "individualPerformanceSum": individualPerformanceSum, "individualPerformanceAnswerCounter": individualPerformanceAnswerCounter, "avgOfIndividualPerformanceQuestion": avgOfIndividualPerformanceQuestion });
                                const avg = await AnswerValues.query().where('question_id', _individualPerformanceQuestionType[i].id)
                                .andWhere('employee_performance_questions_id', getEmployeePerformanceQuestions[j].id).andWhere('employee_id', getEmployeePerformanceUsers[a].employee_id).andWhere('is_deleted', false).patch({ question_average: avgOfIndividualPerformanceQuestion });
                                const employeePerformanceUsersAverage = await empPerformnceUsersDetails.query().where('question_id', _individualPerformanceQuestionType[i].id)
                                .andWhere('employee_performance_questions_id', getEmployeePerformanceQuestions[j].id).andWhere('employee_id', getEmployeePerformanceUsers[a].employee_id).andWhere('is_deleted', false).patch({ question_average: avgOfIndividualPerformanceQuestion });
                                if (avg && employeePerformanceUsersAverage) {
                                    _responseObject.meassge1 = 'Individual performance averaege is stored in answersReports table.';
                                } else {
                                    _responseObject.meassge1 = 'something went Wrong!';
                                }  
                            }
                        }
                    }
                } else {
                    res.status(500).send({
                        success:false,
                        meassge:'Questions not found!'
                    })
                }
                response['section'] = _responseObject;
            } else {
                res.status(500).send({
                    success: false,
                    meassge: 'Please, first fill all questions answers!'
                })
            }
            return res.status(200).send({
                success: true,
                res: response
            });
        }

        else if(section_id === sectionEnum.ValuegrowthMatrix && auditno){
            res.status(200).send({
                success: true,
                meassge: 'There is no average for this particular section!'
            })
        }

        else if(section_id === sectionEnum.PlanOfActionAndRecommendations && auditno){
            
        }
        
        else {
            res.status(500).send({
                success: false,
                meassge: 'please check the passed parameters!'
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message : "Something went wrong!"
        })
    }
})

const getSubSection = async (subSectionId) => {
    try {
        const SubSection = await subSection.query().where("id", subSectionId).andWhere("is_deleted", false);
        return SubSection;
    } catch (Error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
        })
    }
};

export const checkcolorsbyvalues = async (truevalues, falsevalues, conditions) => {
    try {
        if (conditions > 0) {
            if (truevalues === conditions) {
                return 'green';
            } else if (falsevalues === conditions) {
                return 'red';
            } else {
                return 'orange';
            }
        } else {
            return 'red';
        }
    } catch (error) {
        return error
    }
}

export const getQuestionsBySubSectionIdAndSectionId = async (sectionId, SubSectionId) => {
    try {
        const _subSectionBySectionId = await Question.query()
            .where('is_deleted', false)
            .andWhere('section_id', sectionId).andWhere('sub_section_id', SubSectionId)
            .orderBy('sequence_number');
        return _subSectionBySectionId;
    } catch (error) {
        return (`Error while getting sub section by section Id : ${error}`)
    }
}

export const getPlanOfActionAnswers = async (questionId, auditNumber) => {
    try{
        const _planofactionanswers = await PlanOfActionAndRecommendations.query()
        .where('is_deleted', false)
        .andWhere('question_id', questionId).andWhere('vga_audit_number', auditNumber);
        return _planofactionanswers;
    } catch(error) {
        return error;
    }
}

export const getQuestionsBySectionId = async (sectionId) => {
    try {
        const _questions = await Question.query()
            .where('is_deleted', false)
            .andWhere('section_id', sectionId).orderBy('sequence_number');
        return _questions;
    } catch (error) {
        return (`Error while getting questiond by section Id : ${error}`)
    }
};

export const getQuestionsByEmployeePerformanceQuestionId = async (sectionId, questionId) => {
    try {
        const _employeePerformanceQuestionsByQuestionId = await employeePerformance.query()
            .where('is_deleted', false)
            .andWhere('section_id', sectionId).andWhere('question_id', questionId)
            .orderBy('sequence_number');
        return _employeePerformanceQuestionsByQuestionId;
    } catch (error) {
        return (`Error while getting employee performance questions by question Id : ${error}`)
    }
}

export const getEmployeesOfEmployeePerformance = async (questionId, employeePerformanceId, auditNumber) => {
    try {
        const _employeesByEmployeePerformanceId = await empPerformnceUsersDetails.query().where('is_deleted', false).andWhere('question_id', questionId).andWhere('employee_performance_questions_id', employeePerformanceId).andWhere('vga_audit_number', auditNumber);
        // .distinct('employee_id');
        return _employeesByEmployeePerformanceId;
    } catch (error) {
        return (`Errror while getting employees of employee performance by employee performance Id : ${error}`)
    }
} 

export const getReportAnswers = async (questionId, auditNumber) => {
    try {
        const answers = await AnswerValues.query()
            .where('question_id', questionId)
            .andWhere('vga_audit_number', auditNumber)
            .andWhere('is_deleted', false);
        return answers;
    } catch (error) {
        return error;
    }
}

export const getReportAnswersOfCompetencyMapping = async (questionId, auditNumber, clientemployeeId) => {
    try {
        const answers = await AnswerValues.query()
            .where('question_id', questionId)
            .andWhere('vga_audit_number', auditNumber)
            .andWhere('client_employee_id', clientemployeeId)
            .andWhere('is_deleted', false);
        return answers;
    } catch (error) {
        return error;
    }
}

export const getReportAnswersOfSection2 = async (clientemployeeId) => {
    try {
        const answers = await AnswerValues.query()
            .where('client_employee_id', clientemployeeId)
            .andWhere('is_deleted', false);
        return answers;
    } catch (error) {
        return error;
    }
}

export const getReportAnswersForEmployeePerformance = async (questionId, employeePerformanceQuestionId, auditNumber, employeeId) => {
    try {
        const answers = await AnswerValues.query()
            .where('question_id', questionId)
            .andWhere('employee_id', employeeId)
            .andWhere('employee_performance_questions_id', employeePerformanceQuestionId)
            .andWhere('vga_audit_number', auditNumber)
            .andWhere('is_deleted', false);
        return answers;
    } catch (error) {
        return error;
    }
}

const getSection = async (sectionId) => {
    try {
        const sections = await section.query()
            .where('id', sectionId)
            .andWhere('is_deleted', false);
        return sections;
    } catch (error) {
        return error
    }
}

export const getSubQuestions = async (questionId) => {
    try {
        const Type = await subQuestions.query().where("question_id", questionId).andWhere("is_deleted", false);
        return Type;
    } catch (error) {
        return error;
    }
}

export const getSubSubQuestions = async (subQuestionId) => {
    try {
        const Type = await subSubQuestions.query().where("sub_question_id", subQuestionId).andWhere("is_deleted", false);
        return Type;
    } catch (error) {
        return error;
    }
}

questionRoute.post('/getReportAnswers', tokenMiddleware, async (req, res, next) => {
    let questionId = req.body.questionId;
    let auditNo = req.body.vgAuditNo;
    let clientEmployeeId = req.body.clientEmployeeId;

    try {
        let reportAnswers;
        // if(clientEmployeeId === undefined) {
        //     reportAnswers = await AnswerValues.query().where('question_id', questionId).andWhere('vga_audit_number', auditNo).andWhere('is_deleted', false);
        // }else {
            if(clientEmployeeId !== null ){
                reportAnswers = await getReportAnswersOfCompetencyMapping(questionId, auditNo, clientEmployeeId);
            } else{
                reportAnswers = await AnswerValues.query().where('question_id', questionId).andWhere('vga_audit_number', auditNo).andWhere('is_deleted', false);
            }
        // }
       
        if (reportAnswers) {
            res.status(200).send({
                success: true,
                meassge: 'Get Report Answers.',
                res: reportAnswers
            })
        } else {
            res.status(500).send({
                success: false,
                meassge: 'something went wrong'
            })
        }

    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message : "Something went wrong!"
        })
    }
})

questionRoute.post('/sectionsProcess/:secId', tokenMiddleware, async (req, res) => {

    let sectionId = req.params.secId;
    let auditNo = req.body.vgAuditNo;
    let clientId = req.body.clientId;
    let clientEmployeeId = req.body.clientEmployeeId;

    try {

        let sectionsProcess = await sectionProcessing(sectionId, auditNo, clientId, clientEmployeeId);
        // if(sectionId === sectionEnum.CompetencyMapping){
        //     sectionsProcess = await sectionProcessing2(sectionId, auditNo, clientId);
        // } else{
        //     sectionsProcess = await sectionProcessing(sectionId, auditNo, clientId);
        // }

        if (sectionsProcess) {
            res.status(200).send({
                success: true,
                meassge: 'sections Processing is completed.',
                res: sectionsProcess
            })
        } else {
            res.status(500).send({
                success: false,
                meassge: 'something went wrong'
            })
        }

    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message : "Something went wrong!"
        })
    }
});

questionRoute.post('/sectionsProcessForCompetency/:secId', tokenMiddleware, async (req, res) => {

    let sectionId = req.params.secId;
    let auditNo = req.body.vgAuditNo;
    let clientId = req.body.clientId;

    try {

        let sectionsProcess = await sectionProcessing2(sectionId, auditNo, clientId);

        if (sectionsProcess) {
            res.status(200).send({
                success: true,
                meassge: 'sections Processing is completed.',
                res: sectionsProcess
            })
        } else {
            res.status(500).send({
                success: false,
                meassge: 'something went wrong'
            })
        }

    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message : "Something went wrong!"
        })
    }
});

questionRoute.get('/left-nav', tokenMiddleware, async (req, res) => {
    try {
        let response = {}
        const sections = await section.query().where('is_deleted', false).orderBy('sequence_number', 'ASC')
        response.sections = sections;
        for (let i = 0; i < response.sections.length; i++) {
            let _subSection = await getSubSectionBySectionId(response.sections[i].id);
            if (_subSection && _subSection.length > 0) {
                response.sections[i].subSection = _subSection;
            } else {
                response.sections[i].subSection = [];

                let _questions = await getQuestionsBySectionId(response.sections[i].id);

                if (_questions && _questions.length > 0) {
                    response.sections[i].questions = _questions;
                } else {
                    response.sections[i].questions = [];
                }
            }
            if (response.sections[i].subSection.length > 0) {
                for (let j = 0; j < response.sections[i].subSection.length; j++) {
                    let _questions = await getQuestionsBySubSectionIdAndSectionId(response.sections[i].id, response.sections[i].subSection[j].id);
                    if (_questions && _questions.length > 0) {
                        response.sections[i].subSection[j].questions = _questions;
                    } else {
                        response.sections[i].subSection[j].questions = [];
                    }
                }
            }
        }
        res.status(200).send({
            response
        })
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
        })
    }
});

questionRoute.post("/", tokenMiddleware, async (req, res, next) => {

    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 5);
    const sortColumn = req.body.SortColumn;
    const sortOrder = req.body.SortOrder;
    const SearchText = req.body.SearchText;
    let response = {}

    try {
        let questionObj = await Question.query()
            .where("is_deleted", false)
            .andWhere('question_master.question', 'like', '%' + SearchText + '%')
            .orderBy(sortColumn || 'question_master.question', sortOrder || 'ASC')
            .page(pageNo, PageSiZe)
            .withGraphFetched('[section,sub_section,reportAnswers]');

        response.questions = questionObj.results;
        for (let i = 0; i < response.questions.length; i++) {
            response.questions[i].sub_questions = await getSubQuestions(response.questions[i].id);
            response.questions[i].section = await getSection(response.questions[i].section_id);
            response.questions[i].subSection = await getSubSection(response.questions[i].sub_section_id)
        }
        if (questionObj) {
            res.status(200).send({
                success: true,
                meassge: "Get all questions Successfully.",
                res: questionObj
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

questionRoute.get("/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;
    let response = {};

    try {
        if (is_id) {
            const questionById = await Question.query().where("id", is_id).andWhere("is_deleted", false).first();
            response = questionById;
            response.sub_questions = await getSubQuestions(is_id);
            response.section = await getSection(questionById.section_id);
            response.subSection = await getSubSection(questionById.sub_section_id);

            if (response) {
                res.status(200).send({
                    success: true,
                    message: "questionById get Successfully",
                    res: response
                })
            }
        } else {
            res.status(400).send({
                error: error.meassge,
                stack: error.stck,
                message: "Id Not Found."
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stck
        });
    }
});

questionRoute.post("/add-question", tokenMiddleware, async (req, res, next) => {
    let subQuestionList = [];
    let response = {};
    const questionDetailsObj = {
        question: req.body.questionName,
        answer_type: req.body.answerControlType,
        answer_value: req.body.answerValueType,
        section_id: req.body.sectionId,
        by_default_value: req.body.deafaultAnswerValue,
        sequence_number: req.body.sequenceNumber
    }
    if (req.body.subSectionId) {
        questionDetailsObj.sub_section_id = req.body.subSectionId
    } else {
        questionDetailsObj.sub_section_id = null
    }

    try {
        response.questionObj = await Question.query().insertGraphAndFetch(questionDetailsObj);
        let _subQuestionObj = req.body.subQuestionsList;
        for (let i = 0; i < _subQuestionObj.length; i++) {
            const subQuestionsDetailsObj = {
                title: _subQuestionObj[i].subQuestion,
                question_id: response.questionObj.id
            }
            const subQuestionObj = await subQuestions.query().insertGraphAndFetch(subQuestionsDetailsObj);
            subQuestionList.push(subQuestionObj);
        }
        response.subQuestionObj = subQuestionList;
        if (response) {
            res.status(200).send({
                success: true,
                meassge: "question Created Successfully",
                res: response
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

questionRoute.delete("/delete-question/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;

    let deletedRecord = await Question.query().where("id", is_id).andWhere("is_deleted", false).first();
    const subQuestionObjById = await getSubQuestions(is_id);
    try {
        if (deletedRecord) {
            const questionById = await Question.query().where("id", is_id).patch({ is_deleted: true, deleted_on: new Date() });
            const subQuestionObj = await subQuestions.query().where("question_id", is_id).patch({ is_deleted: true, deleted_on: new Date() });
            if (questionById) {
                res.status(200).send({
                    success: true,
                    meassge: "Question Deleted Successfully"
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

questionRoute.put("/update-question/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;
    let subQuestionList = [];
    let response = {};
    const questionDetailsObj = {
        question: req.body.questionName,
        answer_type: req.body.answerControlType,
        answer_value: req.body.answerValueType,
        section_id: req.body.sectionId,
        by_default_value: req.body.deafaultAnswerValue,
        sequence_number: req.body.sequenceNumber
    }
    if (req.body.subSectionId) {
        questionDetailsObj.sub_section_id = req.body.subSectionId
    } else {
        questionDetailsObj.sub_section_id = null
    }
    try {
        const questionObjById = await Question.query().where("id", is_id).andWhere('is_deleted', false).first();

        if (questionObjById) {

            const subQuestionObjById = await getSubQuestions(is_id);
            for (let i = 0; i < subQuestionObjById.length; i++) {
                const delete_subquestion = await subQuestions.query().where('question_id', is_id).andWhere('is_deleted', false).patch({ is_deleted: true, deleted_on: new Date() });
            }
            response.questionObj = await Question.query().patchAndFetchById(is_id, questionDetailsObj);
            let _subQuestionObj = req.body.subQuestionsList;
            for (let i = 0; i < _subQuestionObj.length; i++) {
                const subQuestionsDetailsObj = {
                    title: _subQuestionObj[i].subQuestion,
                    question_id: is_id
                }
                const subQuestionObj = await subQuestions.query().insertGraphAndFetch(subQuestionsDetailsObj);
                subQuestionList.push(subQuestionObj);
            }
            response.subQuestionObj = subQuestionList;
            if (response) {
                res.status(200).send({
                    success: true,
                    meassge: "Questions and subsection Updated successfully",
                    questions: response
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

questionRoute.get("/secId/:id", tokenMiddleware, async (req, res) => {
    let sectionId = req.params.id;
    try {
        if (sectionId) {
            const _questionsBySectionId = await Question.query().where('section_id', sectionId).andWhere('is_deleted', false)
                .orderBy('sequence_number')
                .withGraphFetched('[reportAnswers]');
            if (_questionsBySectionId) {
                res.status(200).send({
                    success: true,
                    message: "get Questions by sectionId.",
                    res: _questionsBySectionId
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            message: error.meassge,
            stack: error.stack
        })
    }
});

questionRoute.get("/subSectionId/:subId", tokenMiddleware, async (req, res) => {

    let subSectionId = req.params.subId;
    try {
        if (subSectionId) {
            let response = {};
            const token = req.headers.authorization.split(' ')[1];
            const decodedToken = jwt.decode(token);
            const users = await Users.query().where('email', decodedToken.email).first();
            const _questionsBySubSectionId = await Question.query()
                .where('question_master.is_deleted', false)
                .andWhere('question_master.sub_section_id', subSectionId)
                .orderBy('sequence_number')
                .withGraphFetched('[reportAnswers]');

            response.question = _questionsBySubSectionId;
            if (response) {
                res.status(200).send({
                    success: true,
                    message: "get questions by subsectionId.",
                    res: _questionsBySubSectionId
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            message: error.meassge,
            stack: error.stack
        });
    }
});

questionRoute.get("/sectionId/:id", tokenMiddleware, async (req, res) => {
    let sectionId = req.params.id;
    try {
        if (sectionId) {
            let response = {};
            const _subSectionBySectionId = await subSection.query()
                .where('section_id', sectionId)
                .andWhere('is_deleted', false);
            response.subsection = _subSectionBySectionId;
            if (response) {
                res.status(200).send({
                    success: true,
                    message: "get sub sections by sectionId.",
                    res: response
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            message: error.meassge,
            stack: error.stack
        })
    }
});

// questionRoute.get("/level/:questionType", tokenMiddleware, async (req, res) => {
//     // let questionId = req.params.id;
//     let level = req.params.questionType;
//     let clientEmployeeId = req.body.clientEmployeeId;

//     try {
//         // if (questionId) {
//             let questionType;
//             if(level === '1'){
//                 questionType = '6'
//             } else if(level === '2'){
//                 questionType = '7'
//             } else if(level === '3'){
//                 questionType = '8'
//             }
//             const _questionsByQuestionType = await Question.query().where('question_type', questionType).andWhere('is_deleted', false)
//                 .andWhere('question_type', questionType)
//                 .orderBy('sequence_number')
//                 .withGraphFetched('[reportAnswers]');
//             console.log(_questionsByQuestionType);

//             for(let i = 0; i < _questionsByQuestionType.length; i++){
//                 const answers = await AnswerValues.query().where('is_deleted', false).andWhere('question_id', _questionsByQuestionType[i].id).andWhere('client_employee_id', clientEmployeeId);
//                 _questionsByQuestionType.reportanswers = answers;
//             }

//             if (_questionsByQuestionType) {
//                 res.status(200).send({
//                     success: true,
//                     message: "get Questions by question type.",
//                     res: _questionsByQuestionType
//                 })
//             }
//         // }
//     } catch (error) {
//         res.status(500).send({
//             message: error.meassge,
//             stack: error.stack
//         })
//     }
// });

questionRoute.post('/genrate-pdf', tokenMiddleware, async (req, res, next) => {
    let auditNo = req.body.vgAuditNo;
    let section_id = req.body.sectionId;
    let client_id = req.body.clientId;
    try {
        if(auditNo && section_id && client_id){
            let storePdf = await ReportPdfController(section_id, auditNo, client_id);
            if (storePdf) {
                res.status(200).send({
                    success: true,
                    res: storePdf
                });
            }
        } else{
            res.status(500).send({
                success:false,
                meassge:'please check the passed arguments'
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});

function generateHTMLInquiryEmail(vgAuditNo, status, sectionDetails, fromDate, toDate) {
    commonhtml +=  ` 
    <!doctype html>
  <html>
  <head>
  <meta charset="utf-8">
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
  <title>Welcome to VGA</title>
  <style type="text/css"></style>
  </head>
  
  <body bgcolor="#FFF" style="margin: 0;min-width: 100%;height: 100%;padding:0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
  <table align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
  <tr>
    <td bgcolor="#FFF" align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if gte mso 9]>
    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
    <tr>
    <td align="center" valign="top">
    <![endif]-->
      
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="max-width: 600px;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tr>
          <td align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <tr>
                <td id="pre-header" valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:20px;padding-bottom:20px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tr>
                      <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; line-height: 25%;">
                <!-- Partner Branding: Start -->
                  <!-- Partner Branding: End -->
                </td>
                          </tr>
                        </table></td>
                      <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right:15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: right;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Message: Start -->
                
                  <!-- Message: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
              <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <table style="width:100%;">
                     <tr>
                       <td style="width:15%;">
                        
                      </td>
                       <td style="width:1%;"><img src="/src/uploads/branding/vga-logo.png" alt="VGA" style="width:200px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"></td>
                       <td style="width:15%;"></td>
                     </tr>
                   </table>
              </td>
             </tr>
        <tr>
          <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
      <table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom:15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Main Headline: Start -->
                
                <!-- <h1 style="font-family: Helvetica, Arial, sans-serif;font-size:36px;line-height: 40px; font-weight: normal; text-align: center; color:#2F5597; margin:0;">Welcome!</h1> -->
                
                  <!-- Main Headline: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
          <tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:15px;padding-bottom:30px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Content Area: Start -->
                
                <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;">
                  Welcome to VGA, Your Report is submitted! So now you can download your Report!
                  <br/>
                  <br/><table>
                    <tr>
                    <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Report Number:</td>
                    <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${vgAuditNo}</td>
                    </tr>`
                    for(let i = 0; i< sectionDetails.length; i++){
                        let sectionName = JSON.parse(sectionDetails[i]);
                        commonhtml += `
                        <tr>
                            <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Report :</td>
                            <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${sectionName.sectionName}</td>
                        </tr>`
                    }
                commonhtml +=`
                <tr>
                <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Report Status :</td>
                <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${status}</td>
                </tr>
                <tr>
                <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Report Duration : </td>
                <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${fromDate} TO ${toDate}</td>
                </tr>
                </table></p>
        <br/>                 
                  <!-- Content Area: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
          
  <!-- 					<tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 0;padding-left: 0; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                           <tr>
                      <td align="center" valign="top" style="padding-top: 0;padding-bottom: 0;padding-right: 0px;padding-left: 0px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <img src="https://dev.localbox.net/static/email/footer-ilocalbox-gradient-bar.jpg" alt="" width="600" style="max-width:600px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0">
            
            </td>
                    </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr> -->
            </table></td>
        </tr>
       <tr>
          <td id="footer" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td align="center" valign="top" width="100%" style="padding-top:15px;padding-right: 15px;padding-bottom: 30px;padding-left: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="left" valign="top" width="350" style="max-width: 350px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Footer iLocal Box Copyright: Start -->
                <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0; margin-bottom: 18px;">&copy; iLocal Box, All Rights Reserved</p> -->
                <!-- Footer iLocal Box Copyright: End -->
                <!-- Footer Partner Info: Start -->
                              
                <!-- Footer Partner Info: End -->
                          </tr>
                        </table></td>
                      <td align="left" valign="top" width="160" style="max-width: 160px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top: 0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Footer Powered by iLocal Box: Start -->
                <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0;margin-bottom: 5px;"><em>Powered by:</em></p>
                              <img src="https://dev.localbox.net/static/email/logo-ilocalbox.png" alt="iLocal Box" width="110" height="19" style="max-width: 110px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"> -->
                <!-- Footer Powered by iLocal Box: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>  
            </table></td>
        </tr>
      </table>
      
      <!--[if gte mso 9]>
    </td>
    </tr>
    </table>
    <![endif]--></td>
  </tr>
  </table>
  </body>
  </html>
     `;
    return commonhtml;
}

export default questionRoute;