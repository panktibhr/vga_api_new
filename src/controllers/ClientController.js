const express = require("express");
const ClientsRoute = express.Router();
const moment = require('moment');
const tokenMiddleware = require('./../middleware').default;
const users = require("./../models/users.Model").default;
const clients = require('./../models/client.Model').default;
const consultant = require('./../models/consultant.Model').default;
const account = require('../models/Account.Model').default;
const CryptoJS = require("crypto-js");
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
let jwt = require('jsonwebtoken');
import roleEnum from './../enums/roleEnum';
import { getUserByEmail } from './CommonController';
import {getaccountliangeBytoken} from './CommonController';
const { validationResult } = require('express-validator');
import {clientbodyvalidate} from '../common/validator';
import {updateclientvalidate} from '../common/validator';
const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer');
 
ClientsRoute.get("/get-All-clients", tokenMiddleware, async (req, res) => {
    try {
        let clientsDetailsObj = await clients.query().where("is_deleted", false).withGraphFetched("[consultants,auditors]");

        if (clientsDetailsObj) {
            res.status(200).send({
                success: true,
                meassge: "Get all clients Successfully!",
                res: clientsDetailsObj
            })
        }
    } catch (error) {
        res.status(500).send({
            success: false,
            error: error.meassge,
            stack: error.stack,
            message: 'Something Went Wrong.Please Try Again!'
        })
    }
})

ClientsRoute.post("/", tokenMiddleware, async (req, res, next) => {
    try {
        let response;
        const result = await getaccountliangeBytoken(req.headers.authorization);
        const pageNo = parseInt(req.body.PageNo || 0);
        const PageSiZe = parseInt(req.body.PageSize || 5);
        const sortColumn = req.body.SortColumn;
        const sortOrder = req.body.SortOrder;
        const SearchText = req.body.SearchText;
        const SearchConsultant = req.body.SearchConsultant;
        const SearchRenewalStartDate = req.body.SearchRenewalStartDate;
        const SearchRenewalEndDate = req.body.SearchRenewalEndDate;
        let consul;
  console.log(SearchRenewalStartDate,SearchRenewalEndDate);
        if(SearchConsultant){
            consul = await consultant.query().where('name', 'like', '%' + SearchConsultant + '%' ).andWhere('is_deleted',false);
            if (result.users.role_id === roleEnum.SUPERADMINROLE) {
                response = await clients.query()
                    .where("clients.is_deleted", false)
                    .andWhere("clients.parent_account_id", "in", result.accounts.map((x) => x.id))
                    .andWhere('clients.name', 'like', '%' + SearchText + '%')
                    .andWhere('clients.consultant',  consul[0].id)
                    .andWhere(function () {
                        if (SearchRenewalStartDate !== '') {
                          this.whereRaw('??::date >= ?', ['clients.renewal_date', SearchRenewalStartDate]);
                        }
                        if (SearchRenewalEndDate !== '') {
                          this.whereRaw('??::date <= ?', ['clients.renewal_date', SearchRenewalEndDate]);
                        }
                     })
                    .orderBy(sortColumn || 'clients.name', sortOrder || 'ASC')
                    .page(pageNo, PageSiZe)
                    .withGraphFetched("[consultants,auditors]");
            } else {
                response = await clients.query()
                    .where("clients.is_deleted", false)
                    .andWhere("clients.parent_account_id",result.users.parent_account_id)
                    .andWhere('clients.name', 'like', '%' + SearchText + '%' )
                    .andWhere('clients.consultant',  consul[0].id)
                    .orderBy(sortColumn || 'clients.name', sortOrder || 'ASC')
                    .page(pageNo, PageSiZe)
                    .withGraphFetched("[consultants,auditors]");
            }
        } else{
            if (result.users.role_id === roleEnum.SUPERADMINROLE) {
                response = await clients.query()
                    .where("clients.is_deleted", false)
                    .andWhere("clients.parent_account_id", "in", result.accounts.map((x) => x.id))
                    .andWhere('clients.name', 'like', '%' + SearchText + '%')
                    .andWhere(function () {
                        if (SearchRenewalStartDate !== '') {
                          this.whereRaw('??::date >= ?', ['clients.renewal_date', SearchRenewalStartDate]);
                        }
                        if (SearchRenewalEndDate !== '') {
                          this.whereRaw('??::date <= ?', ['clients.renewal_date', SearchRenewalEndDate]);
                        }
                     })
                    .orderBy(sortColumn || 'clients.name', sortOrder || 'ASC')
                    .page(pageNo, PageSiZe)
                    .withGraphFetched("[consultants,auditors]");
            } else {
                response = await clients.query()
                    .where("clients.is_deleted", false)
                    .andWhere("clients.parent_account_id",result.users.parent_account_id)
                    .andWhere('clients.name', 'like', '%' + SearchText + '%' )
                    .orderBy(sortColumn || 'clients.name', sortOrder || 'ASC')
                    .page(pageNo, PageSiZe)
                    .withGraphFetched("[consultants,auditors]");
            }
        }
        if (response) {
            res.status(200).send({
                success: true,
                meassge: "Clients Get Successfully!",
                res: response
            })
        }
    } catch (error) {
        res.status(500).send({
            success: false,
            error: error.stack,
            error: error.message,
            message: 'Something Went Wrong. Please Try Again!'
        })
    }
});

ClientsRoute.post("/get-client", tokenMiddleware, async (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    const userId = req.body.user_id;
    console.log('userId - ', userId)
    
    const user = await users.query().where('is_deleted', false).andWhere('id', userId).first(); 

    try{
        if(roleEnum.SUPERADMINROLE === user.role_id){
            const clientId = req.body.client_id;
            console.log('clientId - ', clientId)
            const client = await clients.query().where('is_deleted', false).andWhere('id', clientId).first();
            if(client){
                res.status(200).send({
                    success : true,
                    message : "Client get Successfully",
                    res : client
                })
            } else {
                res.status(500).send({
                    success : false,
                    messge : "No Such Client is exist"
                })
            }
        } else{
            if (token) {
                try {
                    const clientByEmail = await clients.query().where('email', decodedToken.email).first();
        
                    if (clientByEmail) {
                        res.status(200).send({
                            success: true,
                            meassge: 'get client successfully.',
                            res: clientByEmail
                        });
        
                    } else {
                        res.status(404).send({
                            success: false,
                            meassge: 'client not found.',
                        })
                    }
                } catch (error) {
                    res.status(500).send({
                        success: false,
                        error: error.meassge,
                        stack: error.stack
                    })
                }
            } else {
                res.status(500).send({
                    success: false,
                    error: error.meassge,
                    meassge:'token invalid..'
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            success: false,
            error: error.meassge,
            stack: error.stack,
            message: 'Something Went Wrong. Please Try Again!'
        })
    }
});

ClientsRoute.get("/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;

    try{
        if (is_id) {
            try {
                let clientById = await clients.query().where("id", is_id).andWhere("is_deleted", false).first().withGraphFetched("[consultants,auditors]");
    
                if (clientById) {
                    res.status(200).send({
                        success: true,
                        message: "User By Id get Successfully",
                        res: { ...clientById }
                    })
                }
            } catch (error) {
                res.status(500).send({
                    success: false,
                    error: error.message,
                    stack: error.stack
                })
            }
        } else {
            res.status(404).send({
                message: "Id Not Found.Please Check Again!"
            })
        }
    } catch (error) {
        res.status(500).send({
            success: false,
            error: error.message,
            stack: error.stack,
            message: 'Something Went Wrong.Please Try Again!'
        })
    }

});

ClientsRoute.post("/add-client", clientbodyvalidate, tokenMiddleware, async (req, res, next) => {
    let email = req.body.email;
    let sectionArray = [];
    sectionArray = req.body.sections;
   let image = req.body.clientLogo.replace(/^data:image\/\w+;base64,/, '');
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    try {
        const pass = CryptoJS.AES.encrypt(req.body.password, process.env.SECRET).toString();
        const clientDetailsObj = {
            section_details : sectionArray,
            name: req.body.clientName,
            address: req.body.address,
            city: req.body.city,
            state: req.body.state,
            country: req.body.country,
            zip: req.body.zip,
            email: req.body.email,
            username: req.body.username,
            password: pass,
            business_nature: req.body.businessNature,
            business_scale: req.body.businessScale,
            chief_person: req.body.chiefPerson,
            chief_person_designation: req.body.chiefPersonDesignation,
            chief_person_email: req.body.chiefPersonEmail,
            chief_person_phone: req.body.chiefPersonPhone,
            client_creation_date: req.body.clientCreationDate,
            client_duration: req.body.clientDuration,
            logo: image,
            client_type: req.body.clientType,
            company_name: req.body.companyName,
            contact_person: req.body.contactPerson,
            contact_person_designation: req.body.contactPersonDesignation,
            contact_person_email: req.body.contactPersonEmail,
            contact_person_phone: req.body.contactPersonPhone,
            core_competence: req.body.coreCompetence,
            core_driving_force: req.body.coreDrivingForce,
            customer_type: req.body.customerType,
            industry_type: req.body.industryType,
            intensity: req.body.intensity,
            market_type: req.body.marketType,
            minor_competence: req.body.minorCompetence,
            minor_driving_force: req.body.minorDrivingForce,
            mobile_number: req.body.mobileNumber,
            moderate_competence: req.body.moderateCompetence,
            moderate_driving_force: req.body.moderateDrivingForce,
            ownership_type: req.body.ownershipType,
            process_type: req.body.processType,
            profitability_status: req.body.profitabilityStatus,
            renewal_date: req.body.renewalDate,
            website: req.body.website,
            parent_account_id: req.body.parent_account_id,
            employee: req.body.employees,
            auditor: req.body.vgaAuditor,
            consultant: req.body.consultant
        }

        const userdata = {
            password: pass,
            role_id: roleEnum.CLIENTROLE,
            username: clientDetailsObj.username,
            email: clientDetailsObj.email,
            name: clientDetailsObj.name,
            parent_account_id: clientDetailsObj.parent_account_id
        }

        const clientData = await clients.query().insertGraphAndFetch(clientDetailsObj);
        const userobjEntry = await users.query().insertGraphAndFetch(userdata);

        if (clientData && userobjEntry) {

            const passDecrepted = CryptoJS.AES.decrypt(userobjEntry.password, process.env.SECRET).toString(CryptoJS.enc.Utf8);
            let smtpTransport = nodemailer.createTransport({
                type: 'OAuth2',
                host: "smtp.gmail.com",
                port: 587,
                secure: false,
                service: 'gmail', // true for 465, false for other ports
                auth: {
                    user: process.env.AUTH_EMAIL,
                    pass: process.env.AUTH_PASSWORD,
                },
                tls: true,
            });
            let info = await smtpTransport.sendMail({
                from: process.env.AUTH_EMAIL, // sender address
                to: clientDetailsObj.email, // list of receivers
                subject: "VGA - Temporary credentials", // Subject line
                text: "Temporary Password With Login.",
                html: generateHTMLInquiryEmail(clientDetailsObj.email, passDecrepted)
            });

            res.status(200).send({
                success: true,
                message: "Client created successfully & Please! check your mail.",
                res: clientData
            })
        } else {
            res.status(500).send({
                success: false,
                message: "wrong data"
            })
        }
    } catch (error) {
        res.status(500).send({
            success: false,
            error: error.message,
            stack: error.stack,
            message: 'Something Went Wrong.Please Try Again!'
        })
    }
});

ClientsRoute.delete("/delete-client/:id", tokenMiddleware, async (req, res, next) => {
    let is_delete_id = req.params.id;

    let deleted_record = await clients.query().where("id", is_delete_id).first();
    try {
        if (deleted_record) {
            let _client = await clients.query().where("id", is_delete_id).patch({ is_deleted: true, deleted_on: new Date() });
            const _getUserByEmail = await getUserByEmail(deleted_record.email);
            
            if (_getUserByEmail !== null) {
                let _user = await users.query().where('id', _getUserByEmail.id).patch({ is_deleted: true, deleted_on: new Date() });
            }
            if (_client) {
                res.status(200).send({
                    success: true,
                    message: "deleted Successfully."
                });
            }
        } else {
            res.status(500).send({
                success: false,
                message: "Client Not Found."
            });
        }
    } catch (error) {
        res.status(500).send({
            success: false,
            message: error.message,
            stack: error.stack,
            message: 'Something Went Wrong.Please Try Again!'
        });
    }
});

ClientsRoute.put("/update-client/:id", updateclientvalidate ,tokenMiddleware, async (req, res, next) => {
    let updated_id = req.params.id;
    let image;
    let sectionDetail= [];
    let sectionArray = [];
    sectionArray = req.body.sections;
    
    for(let i = 0 ; i < sectionArray.length; i++){
        sectionDetail.push(sectionArray[i]) ;
    }
    image = req.body.clientLogo.replace(/^data:image\/\w+;base64,/, '');

    try {
        const clientDetailsObj = {
            section_details: sectionDetail,
            name: req.body.clientName,
            address: req.body.address,
            city: req.body.city,
            state: req.body.state,
            country: req.body.country,
            zip: req.body.zip,
            email: req.body.email,
            business_nature: req.body.businessNature,
            business_scale: req.body.businessScale,
            chief_person: req.body.chiefPerson,
            chief_person_designation: req.body.chiefPersonDesignation,
            chief_person_email: req.body.chiefPersonEmail,
            chief_person_phone: req.body.chiefPersonPhone,
            client_creation_date: req.body.clientCreationDate,
            client_duration: req.body.clientDuration,
            //logo: req.body.clientLogo,
            logo: image,
            client_type: req.body.clientType,
            company_name: req.body.companyName,
            contact_person: req.body.contactPerson,
            contact_person_designation: req.body.contactPersonDesignation,
            contact_person_email: req.body.contactPersonEmail,
            contact_person_phone: req.body.contactPersonPhone,
            core_competence: req.body.coreCompetence,
            core_driving_force: req.body.coreDrivingForce,
            customer_type: req.body.customerType,
            industry_type: req.body.industryType,
            intensity: req.body.intensity,
            market_type: req.body.marketType,
            minor_competence: req.body.minorCompetence,
            minor_driving_force: req.body.minorDrivingForce,
            mobile_number: req.body.mobileNumber,
            moderate_competence: req.body.moderateCompetence,
            moderate_driving_force: req.body.moderateDrivingForce,
            ownership_type: req.body.ownershipType,
            process_type: req.body.processType,
            profitability_status: req.body.profitabilityStatus,
            renewal_date: req.body.renewalDate,
            website: req.body.website,
            parent_account_id: req.body.parent_account_id,
            employee: req.body.employees,
            auditor: req.body.vgaAuditor,
            consultant: req.body.consultant
        }

        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() })
        }

        let clientById = await clients.query().where("id", updated_id).first();

        if (clientById) {
            const update_Client = await clients.query().patchAndFetchById(updated_id, clientDetailsObj);
            const _getUserByEmail = await getUserByEmail(clientDetailsObj.email);
            if (_getUserByEmail !== null) {
                let _user = {
                    role_id: roleEnum.CLIENTROLE,
                    username: clientDetailsObj.username,
                    email: clientDetailsObj.email,
                    parent_account_id: clientDetailsObj.parent_account_id,
                    name: clientDetailsObj.name
                }
                await users.query().patchAndFetchById(_getUserByEmail.id, _user);
            }
            if (update_Client) {
                res.status(200).send({
                    success: true,
                    message: "Update Client Successfully",
                    res: update_Client
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message: "somthing went wrong. please Check agian!"
        })
    }
});

ClientsRoute.get("/get-client-expiry-notification/:id", tokenMiddleware, async(req,res) =>{
    try {
        let clientId = req.params.id;
        let notificationMessage;
        if (clientId) {
            notificationMessage = await getNotificationsOfClientExpire(clientId);
        }
        if (notificationMessage) {
            res.status(200).send({
                success: true,
                message: "Client Notification Sent Successfully",
                res: notificationMessage
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message: "somthing went wrong. please Check agian!"
        })
    }
});

ClientsRoute.get("/get-client-organization-hierarchy/:id", tokenMiddleware, async(req,res) =>{
    try {
        let clientId = req.params.id;
        let view;
        if (clientId) {
            view = await getOrganizationOverviewByClient(clientId);
            if (view) {
                res.status(200).send({
                    success: true,
                    message: "Client hierarchy created successfully",
                    res: view
                });
            }
        }

    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message: "somthing went wrong. please Check agian!"
        })
    }
});

ClientsRoute.get("/get-client-organization-hierarchy/:id/:isDownload", tokenMiddleware, async(req,res) =>{
    try {
        let clientId = req.params.id;
        let isDownload = JSON.parse(req.params.isDownload);
        let response;
        if (clientId) {
            const view = await getOrganizationOverviewByClient(clientId,isDownload);
            response = await getFinalChart();
            if(response) {
                res.set('Content-Type', 'application/pdf');
                res.send(response);

            }
        }

        if(response) {
            res.status(200).send({
                success: true,
                message: "Client hierarchy chart download successfully",
                res: response
            });
        }

    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
            message: "somthing went wrong. please Check agian!"
        })
    }
});
const getNotificationsOfClientExpire = async (Id) => {
    try {
      const Client = await clients.query().select('renewal_date as renewalDate').where("id", Id)
        .andWhere("is_deleted", false).first();
      let currentDate = moment(new Date()).diff(Client.renewalDate,'days');
      console.log(currentDate, Client);
      if(currentDate <= 30) {
        return `Your subscription is going to expired after ${currentDate} days.`  
      } 
      return `Your subscription is going to expired after ${currentDate -1} days.`
    } catch (error) {
      return error;
    }
}

const getOrganizationOverviewByClient = async (Id,download) =>{
    try {
        let response = {};
        let html =``;
        let downloadChart = download;
        const client = await clients.query().where('id', Id).withGraphFetched("[parentAccount]").first();
        const accountIds = await account.query().select('id', 'name').where('account_lineage', '<@', client.parentAccount.account_lineage);
        response.client = client;
        response.parent = accountIds;
        let data;
        for (let i = 0; i < response.parent.length; i++) {
         data = await users.query()
            .innerJoin('clients', 'clients.email', 'users.email')
            .where("clients.parent_account_id", response.parent[i].id)
            .andWhere("clients.is_deleted", false)
            .andWhere('users.role_id', 'in', [roleEnum.EMPLOYEEROLE]);
            response.parent[i].child = data  
        }
        if(downloadChart) {
           html +=` 
           <html>
           <head>
             <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
             <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
           </head>
         
           <body>
           <div class="container" style="padding: 1%;">
            <ul class="tree vertical cascade-1">
                <li style="display: flex; justify-content: center; align-items: center;">
                    <div style="font-size: 30px; background-color: #03253f; color: white;  box-sizing: border-box;
                        width: 200px;
                        padding: 2%;
                        border: 1px solid #888888;
                        box-shadow: 5px 10px 20px #888888;
                        text-align: center;
                        overflow: hidden;
                        text-overflow: ellipsis;">${accountIds[0].name}
                    </div>

                    <ul>`
                    for(let i=1; i< accountIds.length; i++) {
                    html +=`
                    <li>
                        <div class="nodecontent parent"
                        
                            placement="top"
                        >${accountIds[i].name}</div>
                        <ul>`
                    }
                    for(let j=0; j<data.length; j++) {
                        html +=`
                        <li>
                        <div class="nodecontent child"
                            tooltip="${data[j].name}"
                            placement="top"
                            >${data.name}
                        </div>
                        </li> 
                    </ul> 
                    </li> </ul> 
                </li> </ul> </div>
            </body>
            </html>`
           }  
           fs.writeFileSync('src/uploads/printOrganizationOverview.html', html);
         return path.resolve('src/uploads/printOrganizationOverview.html');
        }
        if (response) {
           return response
        }  
    } catch (error) {
        return error;
    }
}

const getFinalChart = async () => {

    try {
        const browser = await puppeteer.launch({ headless: true });
        const page = await browser.newPage();
        const options ={
          path:`src/uploads/OrganizationOverview.pdf`,
          printBackground : true,
          format : 'A4',
          margin : { top: '180px', bottom: '120px', left: '10px', right: '10px' },
          displayHeaderFooter : false,
        }

        await page.goto(`file:///${path.resolve('src/uploads/printOrganizationOverview.html')}`, {  waitUntil: 'networkidle0'});   //update path varibale by Nikesh Thakkar

    await page.setCacheEnabled(false);
    const pdf = await page.pdf(options);
    await browser.close();
    
    return pdf;
    } catch (error) {
        return error;
    }
 }


function generateHTMLInquiryEmail(email, password) {
    return ` 
    <!doctype html>
  <html>
  <head>
  <meta charset="utf-8">
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
  <title>Welcome to VGA</title>
  <style type="text/css"></style>
  </head>
  
  <body bgcolor="#FFF" style="margin: 0;min-width: 100%;height: 100%;padding:0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
  <table align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
  <tr>
    <td bgcolor="#FFF" align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if gte mso 9]>
    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
    <tr>
    <td align="center" valign="top">
    <![endif]-->
      
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="max-width: 600px;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tr>
          <td align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <tr>
                <td id="pre-header" valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:20px;padding-bottom:20px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tr>
                      <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; line-height: 25%;">
                <!-- Partner Branding: Start -->
                  <!-- Partner Branding: End -->
                </td>
                          </tr>
                        </table></td>
                      <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right:15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: right;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Message: Start -->
                
                  <!-- Message: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
              <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <table style="width:100%;">
                     <tr>
                       <td style="width:15%;">
                        
                      </td>
                       <td style="width:1%;"><img src="https://image.freepik.com/free-vector/welcome-word-flat-cartoon-people-characters_81522-4207.jpg" alt="Welcome" style="width:200px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"></td>
                       <td style="width:15%;"></td>
                     </tr>
                   </table>
              </td>
             </tr>
        <tr>
          <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
      <table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom:15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Main Headline: Start -->
                
                <!-- <h1 style="font-family: Helvetica, Arial, sans-serif;font-size:36px;line-height: 40px; font-weight: normal; text-align: center; color:#2F5597; margin:0;">Welcome!</h1> -->
                
                  <!-- Main Headline: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
          <tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:15px;padding-bottom:30px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Content Area: Start -->
                
                <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;">
                  Welcome to VGA, Please use these temporary credentials to sign in and update your password.
                  <br/>
                  <br/>
                  <table>
                    <tr>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Email Id:</td>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${email}</td>
                    </tr>
                    <tr>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Password:</td>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${password}</td>
                    </tr>
                  </table>
                </p>
                
        <br/>                 
                  <!-- Content Area: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
          
  <!-- 					<tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 0;padding-left: 0; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                           <tr>
                      <td align="center" valign="top" style="padding-top: 0;padding-bottom: 0;padding-right: 0px;padding-left: 0px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <img src="https://dev.localbox.net/static/email/footer-ilocalbox-gradient-bar.jpg" alt="" width="600" style="max-width:600px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0">
            
            </td>
                    </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr> -->
            </table></td>
        </tr>
       <tr>
          <td id="footer" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td align="center" valign="top" width="100%" style="padding-top:15px;padding-right: 15px;padding-bottom: 30px;padding-left: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="left" valign="top" width="350" style="max-width: 350px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Footer iLocal Box Copyright: Start -->
                <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0; margin-bottom: 18px;">&copy; iLocal Box, All Rights Reserved</p> -->
                <!-- Footer iLocal Box Copyright: End -->
                <!-- Footer Partner Info: Start -->
                              
                <!-- Footer Partner Info: End -->
                          </tr>
                        </table></td>
                      <td align="left" valign="top" width="160" style="max-width: 160px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top: 0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Footer Powered by iLocal Box: Start -->
                <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0;margin-bottom: 5px;"><em>Powered by:</em></p>
                              <img src="https://dev.localbox.net/static/email/logo-ilocalbox.png" alt="iLocal Box" width="110" height="19" style="max-width: 110px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"> -->
                <!-- Footer Powered by iLocal Box: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>  
            </table></td>
        </tr>
      </table>
      
      <!--[if gte mso 9]>
    </td>
    </tr>
    </table>
    <![endif]--></td>
  </tr>
  </table>
  </body>
  </html>
     `;
}

export default ClientsRoute;