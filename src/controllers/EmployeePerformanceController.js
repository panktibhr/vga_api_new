const express = require("express");
const tokenMiddleware = require('../middleware').default;
const EmployeePerformanceRoute = express.Router();
const section = require('../models/Section.Model').default;
const clientsEmployee = require('../models/clientsEmployee.Model').default;
const subSection = require('../models/SubSection.Model').default;
const Question = require('../models/question.Model').default;
const subQuestions = require('../models/subQuestions.Model').default;
const employeePerformance = require('../models/EmployeePerformance.Model').default;
const Users = require("./../models/users.Model").default;
const AnswersValues = require('../models/ReportAnswerValue.Model').default;
const empPerformnceUsersDetails =require('../models/employeePerformanceUsersDetails.Model').default;
import roleEnum from './../enums/roleEnum';
import statusReportEnum from '../enums/statusReportEnum';
import employeePerformanceQuestionsEnum from '../enums/employeePerformanceQuestionsEnum';

EmployeePerformanceRoute.use(async (req, res, next) => {
    next();
})

export const getEmpPerformanceByQuestion = async (questionId) => {
    try {

    } catch (error) {
        return (`Error while getting employee performance questions : ${error}`)
    }
}

export const getEmployeePerformanceByQuestionId = async (questionId) => {
    try {
        const _employeePerformanceQuestionsByQuestionId = await subSection.query().where('section_id', sectionId).andWhere('is_deleted', false);
        return _employeePerformanceQuestionsByQuestionId;
    } catch (error) {
        return (`Error while getting sub section by section Id : ${error}`)
    }
}

EmployeePerformanceRoute.get("/getAllEmployeePerformanceUsers", tokenMiddleware, async (req,res,next) =>{

    try{
      const empListing = await empPerformnceUsersDetails.query().where('is_deleted',false).withGraphFetched('[employeePerformance,clientEmployee]').orderBy('employee_id');

      if(empListing){
          res.status(200).send({
              success:true,
              meassage:'Get all Employee Performance Users Successfully!',
              res: empListing
          })
      } else{
        res.status(500).send({
            success:false,
            meassage:'something went wrong.'
        })
      }
    } catch(error){
        res.status(500).send({
            success:false,
            meassage: error
        })
    }
});

EmployeePerformanceRoute.get("/getAllEmployeePerformanceUser/:id/:vgAuditNo", tokenMiddleware, async (req,res,next) =>{
    let questionId = req.params.id;
    let vgAuditNo = req.params.vgAuditNo;

    try{
      const empListing = await empPerformnceUsersDetails.query().where('is_deleted',false).andWhere('question_id', questionId).andWhere('vga_audit_number', vgAuditNo).withGraphFetched('[employeePerformance,clientEmployee]').orderBy('employee_id');

      if(empListing){
          res.status(200).send({
              success:true,
              meassage:'Get all Employee Performance Users Successfully!',
              res: empListing
          })
      } else{
        res.status(500).send({
            success:false,
            meassage:'something went wrong.'
        })
      }
    } catch(error){
        res.status(500).send({
            success:false,
            meassage: error
        })
    }
});

EmployeePerformanceRoute.post("/getEmployeePerformanceUsers/:queId/:vgAuditNo", tokenMiddleware, async (req,res,next) =>{
    let questionId = req.params.queId;
    let vgAuditNo = req.params.vgAuditNo;
    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 10);

    try{
      const empListing = await empPerformnceUsersDetails.query().where('is_deleted',false).andWhere('question_id', questionId).andWhere('vga_audit_number', vgAuditNo).withGraphFetched('[employeePerformance,clientEmployee]').orderBy('employee_id').page(pageNo, PageSiZe);

      if(empListing){
          res.status(200).send({
              success:true,
              meassage:'Get all Employee Performance Users Successfully!',
              res: empListing
          })
      }else{
        res.status(500).send({
            success:false,
            meassage:'something went wrong.'
        })
      }
    } catch(error){
        res.status(500).send({
            success:false,
            meassage: error
        })
    }
});

// For manager
EmployeePerformanceRoute.post("/getEmployeesListingOfManager", tokenMiddleware, async (req,res,next) =>{
    let questionId = employeePerformanceQuestionsEnum.SELF;
    let vgAuditNo = req.body.vgAuditNo;

    try{
        const empListingOfManager = await empPerformnceUsersDetails.query().where('is_deleted',false).distinct('employee_id').orderBy('employee_id').andWhere('question_id', questionId).andWhere('vga_audit_number', vgAuditNo).withGraphFetched('[clientEmployee]');

        if(empListingOfManager){
            res.status(200).send({
                success:true,
                meassage:'Get all Employees of Manager Successfully!',
                res: empListingOfManager
            })
        }else{
            res.status(500).send({
                success:false,
                meassage:'something went wrong.'
            })
        }
    }catch(error){
        res.status(500).send({
            success:false,
            meassage: error
        })
    }
});
// End Manager

EmployeePerformanceRoute.get("/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;
    
    try{
        if(is_id){
            const getEmployeePerformanceUsersById = await empPerformnceUsersDetails.query().where("id", is_id).andWhere('is_deleted', false).withGraphFetched('[clientEmployee]').first();
            if (getEmployeePerformanceUsersById) {
                res.status(200).send({
                  success: true,
                  message: "get employee performance users by id Successfully",
                  res: getEmployeePerformanceUsersById
                })
            }
        } else {
            res.status(500).send({
                success: false,
                message: "employee performance users Not Found."
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
        })
    }
})

export const getQuestionsBySectionId = async (sectionId) => {
    try {
        const _questions = await Question.query()
            .where('is_deleted', false)
            .andWhere('section_id', sectionId).orderBy('sequence_number');
        return _questions;
    } catch (error) {
        return (`Error while getting questiond by section Id : ${error}`)
    }
};

EmployeePerformanceRoute.post("/addEmployeePerformanceUserDetails", tokenMiddleware, async (req, res, next) => {
    let userId = req.body.userId;
    let empId = req.body.employeeId;
    let auditNo = req.body.vgAuditNo;
    let questionId = req.body.questionId;
    let sectionId = req.body.sectionId;
    let employeePerformanceQuestionId = req.body.employeePerformanceQuestionId;
    let employee;
    let response = {};
    let empList = [];
    let empDetailsList = [];
    let _individualPerformanceQuestionType;

    const allQuestions = await getQuestionsBySectionId(sectionId);
    if (allQuestions){
        _individualPerformanceQuestionType = allQuestions.filter((item) => item.question_type === 4 && item.section_id === sectionId);
    }
    try {
        if (empId && auditNo && questionId) {
            let usersData = await Users.query().where('is_deleted', false).andWhere('id', userId).first();
            
            employee = await AnswersValues.query()
                .where('is_deleted', false)
                .andWhere('employee_id', empId)
                .andWhere('vga_audit_number', auditNo)
                .andWhere('employee_performance_questions_id', employeePerformanceQuestionId)
                .withGraphFetched('[employeePerformance,clientEmployee]');
            if(employee && employee.length > 0){
                empList.push(employee[0]);
                const empDetails = {
                    employee_id: empId,
                    employee_performance_questions_id: empList[0].employee_performance_questions_id,
                    question_id: questionId,
                    vga_audit_number: auditNo
                }
                if (roleEnum.CONSULTANTROLE !== usersData.role_id){
                    const _empDetails = await empPerformnceUsersDetails.query().where('is_deleted',false).insertGraphAndFetch(empDetails);
                    empDetailsList.push(_empDetails);
                    empList.pop();
                }
            }
            response.empSections = empDetailsList;
            if(response.empSections && roleEnum.CONSULTANTROLE !== usersData.role_id) {
                if(questionId === _individualPerformanceQuestionType[0].id){
                    const clientEmp = await clientsEmployee.query().where('is_deleted', false).andWhere('id', empId).patch({ is_emp_status: false });
                } else if(questionId === _individualPerformanceQuestionType[1].id){
                    const clientEmp = await clientsEmployee.query().where('is_deleted', false).andWhere('id', empId).patch({ is_manage_status: true });
                }
            }
            if (response) {
                res.status(200).send({
                    success: true,
                    meassage: 'Add Employee Performance User Details Successfully!',
                    res: response
                })
            } else {
                return null
            }
            
        } else {
            res.status(500).send({
                success: false,
                meassage: 'Employee Id or Audit No or question Id is missing!'
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
        })
    }
})

EmployeePerformanceRoute.post("/getQuestionsOfEmployeePerformance", tokenMiddleware, async (req, res, next) => {
    let section_id = req.body.sectionId;
    let employee_id = req.body.employeeId;
    let response = {}

    try {
        const sectionObj = await section.query().where("is_deleted", false).andWhere("id", section_id).first();
        const questionObj = await Question.query().where("is_deleted", false).andWhere("section_id", sectionObj.id).orderBy('sequence_number');

        let arrayOfQuestions = [];
        for (let i = 0; i < questionObj.length; i++) {
            if (questionObj[i].sequence_number === 1) {
                arrayOfQuestions.push(questionObj[i]);
            } else if (questionObj[i].sequence_number === 2) {
                arrayOfQuestions.push(questionObj[i]);
            }
        }
        response = arrayOfQuestions;
        for (let i = 0; i < response.length; i++) {
            response[i].employeePerformance = await employeePerformance.query().where("is_deleted", false).andWhere('question_id', response[i].id).orderBy('sequence_number');
            for(let j = 0; j < response[i].employeePerformance.length; j++){
                response[i].employeePerformance[j].subQuestions =  await subQuestions.query().where("is_deleted", false).andWhere("employee_performance_questions_id", response[i].employeePerformance[j].id);
                let _answers = await AnswersValues.query().where("is_deleted", false).andWhere("employee_performance_questions_id", response[i].employeePerformance[j].id).andWhere("employee_id",  employee_id);
                if(_answers && _answers.length > 0){
                    response[i].employeePerformance[j].reportanswers = _answers;
                } else{
                    response[i].employeePerformance[j].reportanswers = [];
                }
            } 
        }
        res.status(200).send({
            success: true,
            message: 'get subQuestions of employee performance',
            res: response
        });
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
        })
    }
})

EmployeePerformanceRoute.post("/add-answers", tokenMiddleware, async (req, res, next) => {
    let _questionArray = req.body.Answer;
    let response = {};
    let answerValuesDetailsObj;
    let answerValuesList = [];

    try {
        let usersData = await Users.query().where('is_deleted', false).andWhere('id', _questionArray.UserId).first();

        if (roleEnum.CONSULTANTROLE !== usersData.role_id && _questionArray.AnswerDetails) {
            let answersByQuestionId = await AnswersValues.query()
                .where('question_id', _questionArray.QuestionId)
                .andWhere('vga_audit_number', _questionArray.vgaAuditNumber)
                .andWhere('is_deleted', false)
                .andWhere('employee_id', _questionArray.employeeId)
                .andWhere('employee_performance_questions_id', _questionArray.employeePerformanceQuestionId);

            if (answersByQuestionId.length === 0) {
                for (let i = 0; i < _questionArray.AnswerDetails.length; i++) {
                    answerValuesDetailsObj = {
                        answer_value: _questionArray.AnswerDetails[i].AnswerValue,
                        sub_questions_id: _questionArray.AnswerDetails[i].SubQuestionId,
                        title: _questionArray.AnswerDetails[i].Title
                    }
                    answerValuesDetailsObj.user_id = _questionArray.UserId;
                    answerValuesDetailsObj.remarks = _questionArray.remarks;
                    answerValuesDetailsObj.question_id = _questionArray.QuestionId;
                    answerValuesDetailsObj.vga_audit_number = _questionArray.vgaAuditNumber;
                    answerValuesDetailsObj.submiited_answer = true;
                    answerValuesDetailsObj.employee_id = _questionArray.employeeId;
                    answerValuesDetailsObj.employee_performance_questions_id = _questionArray.employeePerformanceQuestionId;
                    const answerValuesObj = await AnswersValues.query().insertGraphAndFetch(answerValuesDetailsObj);
                    answerValuesList.push(answerValuesObj);
                }
                response.answerValues = answerValuesList;

                if (response) {
                    res.status(200).send({
                        success: true,
                        message: "Answers Added successfully",
                        res: response
                    })
                }
            } else {
                res.status(500).send({
                    success: false,
                    message: 'Answers of this Questions are already Submitted.'
                })
            }
        } else {
            let updatedstatusList = [];
            const answersDetailsObj = await AnswersValues.query().where('question_id', _questionArray.QuestionId).first();

            if (answersDetailsObj) {
               
                let review
                if(_questionArray.employeeId === undefined && _questionArray.employeePerformanceQuestionId === undefined) {
                    review = await AnswersValues.query().where('question_id', _questionArray.QuestionId).andWhere('vga_audit_number', _questionArray.vgaAuditNumber).andWhere('is_deleted', false);
                }else  {
                    review = await AnswersValues.query().where('question_id', _questionArray.QuestionId).andWhere('vga_audit_number', _questionArray.vgaAuditNumber).andWhere('employee_id', _questionArray.employeeId).andWhere('employee_performance_questions_id', _questionArray.employeePerformanceQuestionId).andWhere('is_deleted', false);
                }
                
                 if (review) {
                    const consulatantobj = {
                        consultant_review: _questionArray.consultantReview,
                    }
                    let s ;
                    if(_questionArray.employeeId === undefined && _questionArray.employeePerformanceQuestionId === undefined) {
                        s = await empPerformnceUsersDetails.query().where('is_deleted', false).andWhere('question_id', _questionArray.QuestionId).andWhere('vga_audit_number', _questionArray.vgaAuditNumber).patch(consulatantobj);
                    }else {

                     s = await empPerformnceUsersDetails.query().where('is_deleted', false).andWhere('question_id', _questionArray.QuestionId).andWhere('employee_performance_questions_id', _questionArray.employeePerformanceQuestionId).andWhere('vga_audit_number', _questionArray.vgaAuditNumber).andWhere('employee_id', _questionArray.employeeId).patch(consulatantobj);
                    }
                    let status = _questionArray.status;
                    if (status === 'Reviewing') {
                        consulatantobj.question_status = statusReportEnum.REVIEWINGSTATUS
                    } else {
                        consulatantobj.question_status = statusReportEnum.REVIEWEDSTATUS
                    }
                    for (let i = 0; i < review.length; i++) {
                        const updatedStatus = await AnswersValues.query().where('question_id', _questionArray.QuestionId).patchAndFetchById(review[i].id, consulatantobj);
                        updatedstatusList.push(updatedStatus)
                    }
                    response.consultantStatus = updatedstatusList;
                } else {
                    return null
                }
                if (response) {
                    res.status(200).send({
                        success: true,
                        message: 'consultant review submitted.',
                        res: response
                    })
                }
            } else {
                res.status(500).send({
                    success: false,
                    message: 'please, submit the answers first!'
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});



export default EmployeePerformanceRoute;