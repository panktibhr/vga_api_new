const express = require("express");
const CountryRoute = express.Router();
const countries = require("./../models/Countries.Model").default;
const { validationResult } = require('express-validator');
import {countrybodyvalidate} from '../common/validator'

CountryRoute.post("/", async (req, res, next) => {

    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 10);
    const sortColumn = req.body.SortColumn;
    const sortOrder = req.body.SortOrder;
    const SearchText = req.body.SearchText;

    try {
        let countryObj = await countries.query()
            .where("is_deleted", false)
            .andWhere('countries.countries_name', 'like', '%' + SearchText + '%')
            .orderBy(sortColumn || 'countries.countries_name', sortOrder || 'ASC')
            .page(pageNo, PageSiZe);

        if (countryObj) {
            res.status(200).send({
                success: true,
                meassge: "Get all country Successfully.",
                res: countryObj
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

CountryRoute.get("/getAll-countries", async (req, res, next) => {
    try {
        let country = await countries.query()
            .where("is_deleted", false).orderBy("countries_name");

        if (country) {
            res.status(200).send({
                success: true,
                res: country
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            error: error.stack
        })
    }
});

CountryRoute.post("/add-countries",countrybodyvalidate, async (req, res, next) => {
    
    const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() })
        }
    
    const countriesDetailsobj = {
        countries_name: req.body.countryName
    }

    try {
        let country = await countries.query().insertGraphAndFetch(countriesDetailsobj);

        if (country) {
            res.status(200).send({
                status: true,
                message: 'country added Successfully.',
                res: country
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});

CountryRoute.delete("/delete-countries/:id", async (req, res, next) => {
    let country_id = req.params.id;

    let deletedRecord = await countries.query().where("id", country_id).first();

    try {
        if (deletedRecord) {
            const countryById = await countries.query().where("id", country_id).patch({ is_deleted: true, deleted_on: new Date() });

            if (countryById) {
                res.status(200).send({
                    success: true,
                    meassge: "Country Deleted Successfully"
                })
            }
        } else {
            res.status(500).send({
                success: false,
                meassge: "Id Not Found."
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

CountryRoute.put("/update-countries/:id", async (req, res, next) => {
    let country_id = req.params.id;
    const countriesDetailsobj = {
        countries_name: req.body.countryName
    }
    try {
        if (country_id) {
            let country = await countries.query().patchAndFetchById(country_id, countriesDetailsobj);

            if (country) {
                res.status(200).send({
                    success: true,
                    message: "update country successfully.",
                    res: country
                })
            }
        } else {
            res.status(500).send({
                success: false,
                meassge: "Id Not Found."
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

export default CountryRoute;