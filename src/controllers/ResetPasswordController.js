const express = require('express');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const ResetPasswordRoute = express.Router();
const users = require('../models/users.Model').default;


ResetPasswordRoute.use(async (req, res, next) => {
    next();
})

ResetPasswordRoute.post('/reset-password', async (req, res, next) => {
    let email = req.body.email;
    try {
        if (email) {
            let emailObj = await users.query().where('email', email).first();
            if (emailObj) {
                let smtpTransport = nodemailer.createTransport({
                    type: 'OAuth2',
                    host: "smtp.gmail.com",
                    port: 587,
                    secure: false,
                    service: 'gmail', // true for 465, false for other ports
                    auth: {
                        user: process.env.AUTH_EMAIL,
                        pass: process.env.AUTH_PASSWORD,
                    },
                    tls: true,
                });
                let info = await smtpTransport.sendMail({
                    from: process.env.AUTH_EMAIL, // sender address
                    to: email, // list of receivers
                    subject: "Hello ✔", // Subject line
                    text: "Hello, Don't Worry Here Is Your Reser Password Link...", // plain text body"
                    // html: `<h2>Reset Password link : <a href='http://localhost:4200/change-password?id=${user_id}'> click here </a></h2>`, // html body
                    html: `
                    <head>
                        <title>Rating Reminder</title>
                        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
                        <meta content="width=device-width" name="viewport">
                        <style type="text/css">
                                    @font-face {
                                      font-family: "&#x27;Postmates Std&#x27";
                                      font-weight: 600;
                                      font-style: normal;
                                     
                                    }
                        
                                    @font-face {
                                      font-family: "&#x27;Postmates Std&#x27";
                                      font-weight: 500;
                                      font-style: normal;
                                      
                                    }
                        
                                    @font-face {
                                      font-family: "&#x27;Postmates Std&#x27";
                                      font-weight: 400;
                                      font-style: normal;
                                      
                                    }
                                </style>
                        <style media="screen and (max-width: 680px)">
                                    @media screen and (max-width: 680px) {
                                        .page-center {
                                          padding-left: 0 !important;
                                          padding-right: 0 !important;
                                        }
                                        
                                        .footer-center {
                                          padding-left: 20px !important;
                                          padding-right: 20px !important;
                                        }
                                    }
                                </style>
                        </head>
                        <body style="background-color: #f4f4f5;">
                        <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%; background-color: #f4f4f5; text-align: center;">
                        <tbody><tr>
                        <td style="text-align: center;">
                        <table align="center" cellpadding="0" cellspacing="0" id="body" style="background-color: #fff; width: 100%; max-width: 680px; height: 100%;">
                        <tbody><tr>
                        <td>
                        <table align="center" cellpadding="0" cellspacing="0" class="page-center" style="text-align: left; padding-bottom: 88px; width: 100%; padding-left: 120px; padding-right: 120px;">
                        <tbody><tr>
                        <td style="padding-top: 24px;">
                        <img src="https://d1pgqke3goo8l6.cloudfront.net/wRMe5oiRRqYamUFBvXEw_logo.png" style="width: 56px;">
                        </td>
                        </tr>
                        <tr>
                        <td colspan="2" style="padding-top: 72px; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #000000; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 48px; font-smoothing: always; font-style: normal; font-weight: 600; letter-spacing: -2.6px; line-height: 52px; mso-line-height-rule: exactly; text-decoration: none;">Reset your password</td>
                        </tr>
                        <tr>
                        <td style="padding-top: 48px; padding-bottom: 48px;">
                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                        <tbody><tr>
                        <td style="width: 100%; height: 1px; max-height: 1px; background-color: #d9dbe0; opacity: 0.81"></td>
                        </tr>
                        </tbody></table>
                        </td>
                        </tr>
                        <tr>
                            <td style="-ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 16px; font-smoothing: always; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
                                                                  You're receiving this e-mail because you requested a password reset for your account.
                                                                </td>
                            </tr>
                            <tr>
                            <td style="padding-top: 24px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 16px; font-smoothing: always; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
                                                                  Please tap the button below to choose a new password.
                                                                </td>
                            </tr>
                            <tr>
                            <td>
                            <a data-click-track-id="37" href="http://localhost:4200/auth/reset-old-password?id=${emailObj.id}" style="margin-top: 36px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #ffffff; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 12px; font-smoothing: always; font-style: normal; font-weight: 600; letter-spacing: 0.7px; line-height: 48px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 220px; background-color: #00cc99; border-radius: 28px; display: block; text-align: center; text-transform: uppercase" target="_blank">
                                                                    Reset Password
                                                                  </a>
                            </td>
                            </tr>
                            </tbody></table>
                            </td>
                            </tr>
                            </tbody></table>
                            <table align="center" cellpadding="0" cellspacing="0" id="footer" style="background-color: #000; width: 100%; max-width: 680px; height: 100%;">
                            
                            
                            
                            </body>`
                });
                res.status(200).send({
                    success: true,
                    message: 'Email send successfully! Please check your E-mailBox.'
                })
            } else {
                res.status(402).send({
                    success: false,
                    message: 'We dont have find your email in our records.'
                })
            }
        } else {
            res.status(400).send({
                success: false,
                message: 'Something went wrong! Please check request.'
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack,
        })
    }
});
export default ResetPasswordRoute;