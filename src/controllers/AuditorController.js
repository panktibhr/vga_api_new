const express = require("express");
const AuditorRouter = express.Router();
const auditor = require('../models/Auditor.Model').default;
const account = require('../models/Account.Model').default;
const users = require("./../models/users.Model").default;
const tokenMiddleware = require('./../middleware').default;
let jwt = require('jsonwebtoken');
import roleEnum from './../enums/roleEnum';
import {getaccountliangeBytoken} from './CommonController';

AuditorRouter.post("/", tokenMiddleware, async (req, res, next) => {
    try {
        let response;
        const result = await getaccountliangeBytoken(req.headers.authorization);

        const pageNo = parseInt(req.body.PageNo || 0);
        const PageSiZe = parseInt(req.body.PageSize || 5);
        const sortColumn = req.body.SortColumn;
        const sortOrder = req.body.SortOrder;
        const SearchText = req.body.SearchText;

        if (u.role_id === roleEnum.SUPERADMINROLE) {
            response = await auditor.query()
                .where("is_deleted", false)
                .andWhere("auditor.parent_account_id", "in", result.accounts.map((x) => x.id))
                .andWhere('auditor.name', 'like', '%' + SearchText + '%')
                .orderBy(sortColumn || 'auditor.name', sortOrder || 'ASC')
                .page(pageNo, PageSiZe);
        } else {
            response = await auditor.query()
                .where("is_deleted", false)
                .andWhere("auditor.parent_account_id",result.users.parent_account_id)
                .andWhere('auditor.name', 'like', '%' + SearchText + '%')
                .orderBy(sortColumn || 'auditor.name', sortOrder || 'ASC')
                .page(pageNo, PageSiZe);
        }

        if (response) {
            res.status(200).send({
                success: true,
                meassge: "Get all Successfully.",
                res: response
            })
        }

    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }

});

AuditorRouter.get("/getAll-auditors", tokenMiddleware, async (req, res, next) => {
    try {
         const  response = await auditor.query()
                .where("is_deleted", false);
    
        if (response) {
            res.status(200).send({
                success: true,
                meassge: "Get all Successfully.",
                res: response
            })
        }

    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }

});

AuditorRouter.get(":/id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;
    try{
        if (is_id) {
            try {
                const auditorById = await auditor.query().where("id", is_id).andWhere("is_deleted", false).first();
    
                if (auditorById) {
                    res.status(200).send({
                        success: true,
                        message: "Auditor get Successfully",
                        res: auditorById
                    })
                }
            } catch (error) {
                res.status(500).send({
                    error: error.meassge,
                    stack: error.stack
                });
            }
        } else {
            res.status(400).send({
                error: error.meassge,
                stack: error.stack,
                message: "Auditor Id Not Found."
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack,
        });
    }
})

export default AuditorRouter;