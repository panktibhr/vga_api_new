const puppeteer = require('puppeteer');
const fs = require('fs');
const path = require('path');
const express = require("express");
const pdfRoute = express.Router();
const tokenMiddleware = require('./../middleware').default;
const Audit = require('../models/Audit.Model').default;
const Consultant = require('../models/consultant.Model').default;
const AuditDetails = require('../models/AuditDetails.Model').default;
import { sectionProcessing } from '../services/sectionsprocess';
import sectionEnum from './../enums/sectionEnum';
import employeePerformanceQuestionsEnum from './../enums/employeePerformanceQuestionsEnum';
import questionsOfPlanOfActions from './../enums/questionsOfPlanOfActions';
const empPerformanceQuestions =require('../models/EmployeePerformance.Model').default;
const empPerformanceUsersDetails =require('../models/employeePerformanceUsersDetails.Model').default;
const planOfActionDetails =require('../models/planOfActionAndRecommendations.Model').default;
const AnswersValues = require('../models/ReportAnswerValue.Model').default;
import { checkAuditReportStatus } from '../controllers/QuestionController';
import { sectionProcessing2 } from '../services/sectionsprocess2';
const pdfparse = require('pdf-parse');
const pdffile = fs.readFileSync('src/uploads/VGAuditReport_3_Performance Evaluation.pdf')
let pageCounterForPerformanceEvaluation;
pdfparse(pdffile).then(function (data){
  let pageNo = data.numpages;
  // console.log(pageNo);
  pageCounterForPerformanceEvaluation = pageNo - 9;
  // console.log(pageCounterForPerformanceEvaluation);
})

// import AWS from 'aws-sdk';
// import { v4 as uuidv4 } from 'uuid';

let commonhtml = ``;
const gencommonhtml = async (apiData) => {
  commonhtml += `
  <html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  </head>
  <body>
    <br/><br/><br/>
    <div class="container" style="padding: 1%;">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="line" style="border-right:6px solid green; height:800px;"></div>
        <div class="col-md-7 mt-5">
          <h2 class="pl-md-4 text-center" style="font-family: 'Roboto', sans-serif; color:#0C5A9E;"><strong> VALUEGROWTH AUDIT REPORT</strong> </h2>
          <h3 class="pl-md-4 text-center mt-5" style="font-family: 'Roboto', sans-serif; color:#0C5A9E;"><strong>
          Section : ${apiData[0].sequence_number}. ${apiData[0].section_name} </h3></strong>
          <br/><br/><br/>
          <h4 class="pl-md-4 text-center" style="font-family: 'Roboto', sans-serif;"><strong> Client Name : </strong></h4>
          <h3 class="pl-md-4 text-center" style="font-family: 'Roboto', sans-serif;"> 
          <strong>${apiData[0].client.name}</strong>
          </h3>
          <br/><br/><br/><br/><br/>
          <h5 class="pl-md-4 text-center" style="font-family: 'Roboto', sans-serif;"><strong><i>Prepared by : </i></strong></h5>
          <h5 class="pl-md-4 text-center" style="font-family: 'Roboto', sans-serif;"> 
          <strong> ${apiData[0].consultantName} </strong>
          </h5>
          <br/>
          <h5 class="pl-md-4 text-center" style="font-family: 'Roboto', sans-serif;"><strong><i> Report Duration : </i></strong></h5>
          <h5 class="pl-md-4 text-center" style="font-family: 'Roboto', sans-serif;"> 
            <strong>${apiData[0].audit_from_date.toISOString().slice(0, 10)}  TO ${apiData[0].audit_to_date.toISOString().slice(0, 10)}</strong>
          </h5>
        </div>
      </div>
    </div>
    <div style="page-break-before: always; margin-top: 5%"></div>
  </body>
  </html>`;
  fs.writeFileSync('src/uploads/print.html', commonhtml);
  return path.resolve('src/uploads/print.html')
};

const indexpageHtml = async (apiData) => {

  pageCounterForPerformanceEvaluation = pageCounterForPerformanceEvaluation;

  commonhtml += `
  <html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>
    <br/>
    <div class="container">
      <div class="row">
        <div class="col-md-5"></div>
          <div class="col-md-4" style="font-family: 'Roboto', sans-serif; font-size: 5x-large;">
            <strong>INDEX</strong>
          </div>
          <div class="col-md-3"></div>
      </div>
    <br/>`;
    commonhtml += `
      <table style="border: 1px;" width="100%">
        <thead>
          <tr class="border border-secondary">
            <th style="font-family: 'Roboto', sans-serif;font-size: x-large; padding: 10px;" width="15%"><b>Sr. No.</b></th>
            <th style="text-align: center; font-family: 'Roboto', sans-serif;font-size: x-large; padding: 10px; width:"100%;"><b>Particulars</b></th>
            <th style="font-family: 'Roboto', sans-serif;font-size: x-large; padding: 10px; width:"10%;"><b>Page No.</b></th>
          </tr>
        </thead>
    `;

  if(sectionEnum.PlanOfActionAndRecommendations === apiData.sectionsProcess.sections[0].id){
    commonhtml += `
    <tbody>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>1.</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;"><b>CounsultationCounsel</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;"></td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>1.1</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">Strategic Sphere</td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">3</td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>1.2</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">Definite Direction</td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">6</td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>1.3</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">Concrete Commencement</td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">9</td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>1.4</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">Purposeful Planning</td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">12</td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>2.</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;"><b>GrowthFrame</b></td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;"></td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>2.1</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">GrowthFrame</td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">16</td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>3.</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;"><b>Growth Timeline</b></td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;"></td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>3.1</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">Sales Action Plan</td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">17</td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>3.2</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">Work Action Plan</td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">18</td>
    </tr>
    <tr class="border border-secondary">
      <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>3.3</b></td>
      <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">Corrective Action Plan</td>
      <td class="border border-secondary text-center" style="font-family: 'Roboto';font-size: 20px; padding: 10px;">19</td>
    </tr>`
  }  
  if(apiData.sectionsProcess.sections[0].subSection  && apiData.sectionsProcess.sections[0].subSection.length > 0 && apiData.sectionsProcess.sections[0].id !== sectionEnum.PlanOfActionAndRecommendations && apiData.sectionsProcess.sections[0].id !== sectionEnum.CompetencyMapping){
    let pageCounter = 3;
    for (let j = 0; j < apiData.sectionsProcess.sections[0].subSection.length; j++) {
      commonhtml += `
      <tbody>
      <tr class="border border-secondary">
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>${j + 1}.</b></td>
        <td class="border border-secondary" style="font-family: 'Roboto';font-size: 20px; padding: 10px;"><b>${apiData.sectionsProcess.sections[0].subSection[j].sub_section_name}</b></td>
        <td class="border border-secondary"></td>
      </tr>`;
      for (let k = 0; k < apiData.sectionsProcess.sections[0].subSection[j].questions.length; k++) {
        pageCounter++;
        commonhtml += `
        <tr class="border border-secondary">
          <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;">${j + 1}.${k + 1}</td>
          <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;">${apiData.sectionsProcess.sections[0].subSection[j].questions[k].question}</td>
          <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px; text-align: center;">${pageCounter}</td>
        </tr>
        </tbody>
        `;
      }
    }
  } else if(apiData.sectionsProcess.sections[0].id !== sectionEnum.PlanOfActionAndRecommendations && apiData.sectionsProcess.sections[0].id !== sectionEnum.CompetencyMapping){
    let pageCounter = 2;
    for (let k = 0; k < apiData.sectionsProcess.sections[0].questions.length; k++) {
      if(apiData.sectionsProcess.sections[0].questions[k].question_type === 4 && apiData.sectionsProcess.sections[0].questions[k].sequence_number === 1){
        pageCounter++;
        commonhtml += `
        <tbody>
        <tr class="border border-secondary">
          <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>1.</b></td>
          <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>Individual Performance Evaluation By Self & Manager</b></td>
          <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px; text-align: center;">${pageCounter}</td>
        </tr>`
      }
      if(apiData.sectionsProcess.sections[0].questions[k].question_type === 5){
        pageCounterForPerformanceEvaluation++;
        commonhtml += `
        <tbody>
        <tr class="border border-secondary">
          <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>${(k + 1) - 1}.</b></td>
          <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>${apiData.sectionsProcess.sections[0].questions[k].question}</b></td>
          <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px; text-align: center;">${pageCounterForPerformanceEvaluation}</td>
        </tr>`
      }
      if(sectionEnum.ValuegrowthMatrix === apiData.sectionsProcess.sections[0].id){
        let pageCounter = 2;
        if(apiData.sectionsProcess.sections[0].questions[k].reportanswers.length === 0){
          for(let l=0; l < apiData.sectionsProcess.sections[0].questions[k].subquestions.length; l++){
            pageCounter++;
            commonhtml += `
            <tr class="border border-secondary">
              <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;">${k + 1}.${l + 1}</td>
              <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;">${apiData.sectionsProcess.sections[0].questions[k].subquestions[l].title}</td>
              <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px; text-align: center;">${pageCounter}</td>
            </tr>
            </tbody>
            `;
          }
        } else{
          for(let l=0; l < apiData.sectionsProcess.sections[0].questions[k].reportanswers.length; l++){
            pageCounter++;
            commonhtml += `
            <tr class="border border-secondary">
              <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;">${k + 1}.${l + 1}</td>
              <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;">${apiData.sectionsProcess.sections[0].questions[k].reportanswers[l].title}</td>
              <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px; text-align: center;">${pageCounter}</td>
            </tr>
            </tbody>
            `;
          }
        }
      }
    }
  } else if(apiData.sectionsProcess2.sections[0].id === sectionEnum.CompetencyMapping){
    commonhtml += `
    <tbody>
      <tr class="border border-secondary">
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>2.</b></td>
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>Competency Mapping</b></td>
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px; text-align: center;"></td>
      </tr>
      <tr class="border border-secondary">
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>2.1.</b></td>
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>Top Level</b></td>
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px; text-align: center;">3</td>
      </tr>
      <tr class="border border-secondary">
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>2.2.</b></td>
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>Managerial Level</b></td>
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px; text-align: center;"></td>
      </tr>
      <tr class="border border-secondary">
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>2.3.</b></td>
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px;"><b>Executive Level</b></td>
        <td class="border border-secondary" style="font-family: 'Roboto', sans-serif;font-size: large; padding: 10px; text-align: center;"> </td>
      </tr>
    </tbody>`
  }
  commonhtml += `</table><div style="page-break-before: always; margin-top: 5%"></div></div>
  </body>
  </html>`;

  fs.writeFileSync('src/uploads/print.html', commonhtml);
  return path.resolve('src/uploads/print.html');
}
const ThankYouHtml = async () =>{
  commonhtml +=`
  <html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  </head>
  <body>
    <h1 style="text-align: center; margin-top: 40%; font-size: 500%; font-family: 'Roboto', sans-serif; color:#0C5A9E; "><strong> THANK YOU </strong> </h1>
  </body>
  </html>`
  fs.writeFileSync('src/uploads/print.html', commonhtml);
  return path.resolve('src/uploads/print.html')
}
const commonHeaderFooterHtml = async (apiData) => {

  try{
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    const options ={
      path:`src/uploads/VGAuditReport_${apiData[0].sequence_number}_${apiData[0].section_name}.pdf`,
      printBackground : true,
      format : 'A4',
      margin : { top: '180px', bottom: '120px', left: '10px', right: '10px' },
      displayHeaderFooter : true,
      headerTemplate : `
      <div class="header" style="height:70%;font-size:20px;width:100%; display: flex;">
      <div style=" flex: 1; margin-right: 10px; margin-left: 5px; margin-top:-2%;">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAF9CAYAAADY7xbEAAAgAElEQVR42u3dB5gTZeLH8XeXXdouu7Al2V3s2JBTFBVFPXtB7CJ6nv3sgu3UU6woyiHq6Z2oSJUmVbCBNCkqlsPytxdsWE7pSJFO/u+bTDQOk+RNMslO3vnO83ye89jJJPPOvDO/mfedd0QoFBIAAABwD4UAAABAwAIAACBgAQAAELAAAABAwAIAACBgAQAAELAAAABAwAIAACBgAQAAELAAAABAwAIAACBgAQAAELAAAAAIWAAAACBgAQAAELAAAAAIWAAAACBgAQAAELAAAAAIWAAAACBgAQAAELAAAAAIWAAAACBgAQAAELAAAAAIWAAAAAQsAAAAELAAAAAIWAAAAAQsAAAAELAAAAAIWAAAAAQsAAAAELAAAAAIWAAAAAQsAAAAELAAAAAIWAAAAAQsAAAAAhYAAAAIWAAAAAQsAAAAAhYAAAAIWAAAAAQsAAAAAhYAAAAIWAAAAAQsAAAAAhYAAAAIWAAAAAQsAAAAAhYAAAABCwAAAAQsAAAAAhYAAAABCwAAAAQsAAAAAhYAAAABCwAAAAQsAAAAAhYAAAABCwAAgIAFAAAAAhYAAAABCwAAgIAFAAAAAhYAAAABCwAAgIAFAAAAAhYAAAABCwAAgIAFAAAAAhYAAAABCwAAgIAFAABAwAIAAAABCwAAgIAFAABAwAIAAAABCwAAgIAFAABAwAIAAAABCwAAgIAFAABAwAIAAAABCwAAgIAFAABAwAIAACBgAQAAgIAFAABAwAIAACBgAQAAgIAFAABAwAIAACBgAQAAgIAFAABAwAIAACBgxRMYvGRH6RBpT0O1l2okAeSBIqlC2lnaWzpcOlo6T7pIul7qKfWQ7pD+Iw2VBsNzhkjTpL18vD/vJh0lnS91l3rFlI3636ekgdK90i3SX6UjpVbZ+D21U0Ki2XkPijSn06XR0uAUjZE6Wssok/pKw6VRUio/5jhrWSOkgVJA5Nnkx4B1qBSSNhhKrdtX1kmLEzi8QAX+faXjpa5Sb+lpaar0rrW/rpA2W/sv8tcnUrmP9u3dpXOs4PSGtCrNclsuvSr1lzpLO7nx+2pGrxVVj3wqCgqL0skHKtiE0vSItYw6h7+10/z+2bbP7U7A8n7AUh7xwYHuOU7syLFKaX/rqvxBax98S1pE8PCFpT4JV9tKV0kvS5uyVJZrpJekC6TqtH/roCWidlJINDv/oXTywRXSGw6+jAk9n8WZ5xxrGUFphS0o3aPx3dXSxpjPbJZ2JmDlR8AqkD7wwQHvPE76yCLVJH2m9Lg0RfqRkOFrRxm+v+9t7esrclyuP1kXLDumHrAWi8DQ5aJummoq7ONWbugcE3yOTDJvjbTcFrA+0fiObrbPbCJg5U/AUtr44IC3nv5YcNEe1t2pEdI8AgVi9DR4v29j9aGq7zJWd7UellqmGrKCw1aIuukyZJ3rSsg6Jyb4dEohYL1pBSX13/sn+dwb1nzfWv+7hYCVXwFLWB1oTT/wzSAYIE2NrT6LD0mzCRGI4w1D9/8mVif1DR4r7+VWB/nUQ9Y0V0LWeTEB66QUAtat0kzrvxN1dt/WmudHqXfMdxGw8ixgCasd3fQDYFfCAjQVS8dIA6TPCQ9IYqWhd8lPlL7weNm/Z10ApdBc6MqdrHQD1lnSVdZ/L0jwmauteR6WTiNg5XfAqs3gyY98oTpi7kB4QAL7WP08PiE0IAWnGFgXHsizbXBTSiEr2lz41965DlgXWkMtRD97UJzPvGP9fV/pWAJWfgcsYT1ma/qB8E1CBGzKpEukmQQFpKGvYfVBXWzPydNtMcFq0tQOWS3nhETT47vlMmD93fq3L6z//4DD/Ltaf9vo8F0ErDwNWMooHxwQbyZUQNrOGuhwASEBGYx3VWBQndhP+iHPt8lngciAvVohq2b8ehEYsFAUNi3PVcC6y/q3BxI0E95i/W289f+vImCZEbCa+mC8ni3Wk2CEDH+KPma+loCADLsc7GxQvfiz9Ksh22ah1E5rvQcuDjcVlpx4fa4DVvuYz3ewzf+R9e8nW///GgKWGQFLOdonV56EDf8NrzCSYACXXGhQ3TjEGs7GpO2jhnM4KPm6LxbBsWtFdX91F6ssFwHrzph//8qhmbC19W9rpGIClnkByy+jvPcidPhCtTVuzkZCAVwy0qD6cbC0ztDtpO5S76p1F2taSJScclOuA1Zvh2bCe6x/GxzzbwQswwJWoTTfBwfKAwggRrtWWkIggIu+CURezm1C/WhlULNgohHgK5Pdxap9YYtocdfMXAesvWKWsZf1b9FX8BxBwDI3YCltfXCw/NIKk4QRs3S0xschEMBt+xhSRxpKn/pkmyV/enzIMhEcu0k03POoXAas2P5WXaVK67+XSAUELLMDltLdB5WvH4HEGA2kRwkByJJrDaorw3y27foke6KwbmpIlF3WL9cBq6f17y9ID8UZuoGAZWjAUl71QeU7gnCS945igFBk0YsG1ZXOPt2Ghycql+CYX0XVv78QheXVuQxYe9pe6ByynjAkYPkkYG1rcCfIqKXWEBUElfx0GwEAWX7sv8Sghz5+8el2/FlqFP8u1hJR+0JINGrXKZcBK7aZUPnU4e8ELIMDlnK+DyrfcIJKXo7CPokAgCw7zKA6M9Xn2/LpROVTOzkkyi55Qic3dIsJPX9NMu82MfM+6vD3XjF/7+nw9x4xf9+DgGVewFIm+qDynU5oyRt7BXgRM7LvDoPqzBVsz7Cz4jYTjv5VVD/xtSisaJksN+xu3VlSdkwyb1PpcmveA+Lc4Youq9bh73vH/L05AcvMgFVmNaWZXPHUC6/LCS+ed0zA/JeTo/69alCdUS+638Q2/W0Q0qBTOVUPWChqnw+J6icXiAbJQxYTAcu1gKUcR2dW1LOzOEEgB1ZKVQbVm3ls0z94OW5ZqZA1WYasx74VorgRCYmAlbOAJaxhDUyvfOcRZDypGycG5EhHg+pND7ano25xy2zgovA7Cpudcz8JiYCV04BVZA3QaXLFU09N1hFoPOVsTgjIkYcNqjf7sj3j2iztFrfsnlom6qaEROkZd5KSCFg5C1h+qbRzCDWecSwnA+TIOwbVm2Lpa7ZpQh8EEgw+Ghy+Mnwnq/T020lKBKycBSzlLh9UvmsIN/VuFx+MwwZvWC9tb1Dd6cc21dI7YcgasUrUzQiJ4p3bk5YIWDkLWMpbPqh8uxBy6k1j6XtOAKjvx/fzUEe2Z0oOTRSyasZtENV954vCFnUkJgJWzgKWevR3g+EV712CTr0ZxYEfOTLEoHpT7oMhddz2P6tJNf6ThZNCorr/d4QsAlbOApZysQ8qXw/CTs6dykEfOfKpVGBQ3ZnANk3LsITlGh2+od8CIQoakJwIWDkJWMoLPqh8bQk9OX0FznIO+MiRXQ2qO+ezPTNyasLyHShD1osh0fzGcaJo2z+RnghYOQlYFT44IX4hFRJ+cmIsB3rkyOUG1ZtaHgjJmHoRdoukIWtySNRMDInSzreJgoZNfte0GYmKgJWVyn0S4+PABedwkEeOjDas7sxhm7piatKyVh3fR28QNeNDorrv178JDPyfqH70Y9GgageSFQHLdYN9UPkOIwRljboTupoDPHLgB6mRQXXnRrapq67UCVmBp1aImjGbfhMctUHUTZXn2CE/icLK7UlXBCxXqVHevzG84v1krSeByH1TOLAjR9oZVG/2ZHu6bkvaQ/RYTxwGBv8gCps2J2ARsFzV3geVbxBhyHWXcVBHjtxkWN35lG3qsSF6ZMiqmxkSZZc9TsAiYLnuPh9UvhMIRa7ZxgfjqcEbJhlWdx5km2ZVr/S3zbJwc2HpX+8lYBGwXPeu4RVvsTWgHwGJzrnID8uk5gbVm8PZpjlxcFrbR71mZ9hKUftSSJRfM0g0CO5EwCJguWZnqx3b5Io3gXCUsRs4gCNHDjeo3qjXSP3INs2J79PudztoUfhOVss5IVH16HzR9JgrwkpOv0E0qNqRgEXAysgVPqh8ZxGS0rabtJkDOHLgbsPqzgi2aU4Nzmh7DVgogqM3iropobCWr8vz8NCfRGGzKgIWASsjUw2veL9KNYSltPwfB27kwCuG1ZvT2ab14iTXtqHqBD87JMq7DSZgEbAY2yiJKYSllPXggI0cXQAFDao3apTxFWzXeuvD18K9bblM1E1TneB7EbAIWBk5wweVryuhSdveHKyXrJXekMZLj0sDpdnSe9LbyJh6yOYd6UTD6s406k69esm1bTlokQgOWxV+t2Fl73micYczCVgErLQNN7ziqWEGdiI8JaXe5zjfx51l+0odA5H3xrE/gD6t+ecyN0NWYMhSUftcSNQ+L4NWn3dF1SOfSu+KBoFWBCwClrZiHzz18hYngaQe8OEB+TXpPKmU7Y80qYu3TYQbT1DbYVdXt68KWk8tC7/bUKmbHhLB4YuNCFkErNw51AeVrzsng7gO8dmBWDVVdWa7wwVvE2w8ZV5Wt7d63c6LKmQtkiFrZwIWAUvb/T6ofHtyQthKQ+k7nxx8VSfkq9jmcMndBBpP6pH1kKXeaTj0Z1G0XRsCFgFL2yeGV7yPOSls5XEfdYJtxfaGS9oRZDytfbZDVs3EkKgZ/Ytodv6DovHBf8k7BKzca+2Dinc/J4ffHOuTg+0NbGu43G/1S0KMp30VSHeUd+2QtUgER66LdISflH8IWPXj7z6ofB04SSxpJC00fDurps8j2NZw2RMEmLzQn301PgJW/ZlpeMX70roK9XMFG2b4Np4bMGsgS3jDMQSXvNKJfZaA5bWAVReIjLRscsV70seVq7Ph23Y0B1BkQTMf3PU1zdKAq6O8E7AIWO441weV7ygfVqzm0hKDt2k/Dp7IkrEElrz0HPsuAcuLG2Gc4RXv54D/BpmcYPD2fIADJ7LkHMOPhRsNX78L2YcJWF68Jb7I8Io31EeV6jzCFZCyWsO7TKjhaw6Ulhu8juulHdmXCVg8yp97Z/igQgWkXwhXQMpeNvz4F33S9gLD1/M19mUClhf9x/CKt0yqMLxCTTN02w3jYIksus7wY9+/bev7rOHrezv7NAHLawqk+YZXvBcMrkyXG7rNnuVAiSzaw/Bj3jdSA9s6VxreVKjsw75NwPKa/XzQVGhiR8gdpLUGbqu3AtkeqTl1h1nj7iB3yrK4Pd83/Hh3QJz1Ptnw9f7UIVgSsAhY9e4OwyveKmk7wyrSawZupx+sPmVeKucreBzeqHfO3Wt4uf0z4O+BiPsSsAhYXvSW4RVvlkGVyMTXHm2S2nqsnI8m6BjV3HOQ4WWm89L7htL/DC+HowlYBCyv2dE6yZlc8a4zpP/IFgO3TRePlfOu0mqCjjEBSzUdfW14mf1JsyyOMrwcfgz4bxxEAhadpj1h5zyvQO8ZuE36eKyMS6TPCTlGBawnDS+vHjxB/gejCFgELC96yfCK91YeV55bDdweMz1YzlMIOEYFrI6Gl9U7ad7R+9LwcjmbgEXA8poqaaXhFe/OPKw4+xi4HdQLdpt7rJz7Em6MCljqaUTT31qxa5pl087wclkjtSRgEbC85lQfHMDb5tk2+djAbXCYx8r4WoKNcQFrguHldEOG5fNPHm4iYBGwcu8pnrjxjF4Glv99Hivj4wg1xgWscw0vozku7fvvE0IJWASs3FKP835veMXrkwfboQP94LJOPX21jlBjVMDaJhB5EbCp5bMx4N7Yfnv6YH9qTcAiYHFyz72DPb4NTOuI+qvH+kWoPjrfEGiMC1hzDC+fy1yuBzcYXl4fErAIWF70gOEV79uAd1+vYOKj1Gd6rIxnE2aMC1imh4XJWaoL/zW83HoTsAhYXvSh4RXvCQ+W+REGlvNgj5XxQIKMcQGrtQ+ejKvNUn3Y3mp6pMWCgEXAyqHWPjigH+uh8m4UiLyXz6Ty/Uoq9FAZ30iIMTJgfWB4uZylVQ4DFong6I2i9vmQCD69Nvz/Ncuvq+Hl96N1fCVgEbB4/10O/Sw180hZD+WEmVWnEGCM3F96G14mE/TC1UJRNzkkKvu8K5ocdr6o7veDqH0ulErImmp4OQ4lYBGwvGiW4RVvtAfK+AQDy/XvHtqH9zL86TK/BizTX+S8RCpPWg4DF4qaiVvk/y4QDSpaCjUV77SvCD69QtSMWycCg7RCVrW01vDy7EzAImB5Ta0PHmc/vR7LV91BM23U6Zc9tP9W+WDoET8GrGIDm9TtTkxaDjI81YzZLGqfDYnC5nUidipudYComxoSweFr5HyLdcr0LB/0ZasiYBGwvOZsH1S8ynoqW9NGnV5Vj2Xp5HWCi5EBa5DhZTFCJ1wFh68WLWeFRMnJNwunqeyivqLlbHk+emq5bsgyfRT8KQQsApYXjTe84r1QD2XaxcByPNlD++xIQouRAcv0/nTqzlzT5OWwXNRNCYnSLneIRFP55f1E3SwVslbohKxSq2nS5PLtSsAiYHlNSSDyol6TK96lOSzPSuvOmUnl199D++ttBBYjA5YaJNb0F9MfodOpveWckCi7or/Qmcqv6CdqX1TnpaV+7RNqHxF/JwIWActrjjS84qmO0DU5KsuXDBySocgj+2kXwoqxAesFw8vgcZ1wVTtZnWO+E4XNKoXuFBgsPzcpFP68RjmbPl7cPAIWAcuLHjO84r2SgzK8yMBy28sj+6c6SW8hrBgZsC4xfP2/lgqSPzEozy9DfhSFLbYVqUzFrdqL4JhV1vANC3UeIvif4eV9NwGLgOU1hdbdCpMr3jVZLD/1QtoNhpXXjR7ZN4PSUoKKkQGrZcD8EccPTPrE4NhN4SEZGlSkFq6iU9EO7URw5FIZtNbp9Mc6yAf73P4ELAKW1+xteKVTd0B2yVLZvWJYWc2KE8LrY7+cR0gxNmC9afi635/0icFhq8L9rkpOuUVkMjU55kqxzZvqLphWp/dHDC/3BQFvvW2CgEXA8kUn4veyUGbdDCujXx2GZDg6UD+Dt44joBgbsLobvt4f64SrupkhUXpWD5HxVFAQ7hwfHr5BL2R9Znj5DyBgEbC8yPSrSjfb6Hc0sHycRkb+0fpbLq8KexNOjA1Ye/q+79mQZaL2BRmu/nKPcHMqv7K/7pOFe/lgG5xIwCJgeY3qT7TJ8IrXluYrR8Mc1vH+mL93yNE+eC7BxNigocZk+sLwde6e9InBl0Ki6oF3RDamwKCfRc2zoXDneZ+3WKyy9jcCFgHLUy42vOJ97kIZ3WJYmai7VI1t63iYbZ5eOdj3DiCUGB2wrjZ8fd9O3DS4WATHbhTB8etFg9pddTPTHtKbUjOtJwt37SDqZIALjFir01T4ruHb43kCFgHLiyYZXvEeyaBs9jCwPNrb1rGR9HNg63GxsrnP1Um/EEqMDli3G7yuyQe7VE2Dk1Pu1D5Dkmc5MUz3A2UXP2aNq5W0qXBnH+yDFxOwCFheo15YvMLwindImmXzkWHlcK/DOo6KM++fs7S/FVkdgwklZgesmwxe125JmwZl6Kns83Yq4aqrFa6iTtIfhPSnyPhYyZsKrzF8H9xoXbwRsAhYnnKi4RVPDbpXnGKZ9DKsDOal2AcqW08TPksYIWDlsTlJmwbHbBQ14zaIorrddTPSNtIWW8D6RSrR+XDD3Q4OB7qgXlPhHMP3w/8SsAhYXtTf8Io3KIWy2NewdVcPM2wX2Hpgz3WBxOOJuf3qoT4EEQJWHlPvH61N3DS4NPxKm5JTuqdy9+oNW7iKelZ3Ac0uflzU6TUVBgKR14qZvC/eRsAiYHnRD4ZXvBM0yqAgYN5o95c5rOfrGp/7l4vNgn0JIQSsPHeBVtPg/fNSCVe3xAlXUX/VXVC1flPh+b4fPoOARcCqB6Y/2aX6mjUN+Ot9jS84rOMdKfRpcOMulnpq8R6ph9X5GYktIGB5znPJmwY3iJqx60RRy9a6mah1knClrFPdrPSaCg+xmgq1XqXzvOHH+m8Cyd4NScAiYNWDnoZXvAkJ1v1gw9Z1pVQeyOxVSaN9Xh/qw3QClufGWWqWtGlQPTV4akpPDX6iEbCUl7WbCi/pJ1rOVHexkjYVlvvg4abHCFgELC96z/CKd2acuyymvYH+RIemunTujhxIncipuQQsTzkjWdNg3YyQqOydUtNgT81wFdVNd8Et7pgsWsrfo35Xkv3sZB80FR5NwCJgeY16NcxmgyuduiKttq3zUMPWcYjDdh2S5rIWUCcIWD4NWKOSNQ3WjFkvAv2/FUXb7KGbgdqlGK6ittNZeINgK/mblongKK2nCocbHrCWSCUELAKW11xpeMWbHrOuHQ1bt++lhi5frT5GnSBg+SxgLUneZ3OpaDkrJBrudaz265qlBWkGLO137jQ58lKxzWtaTYVOAw2b5hkCFgGLviC5d7K1nssMW68DbNuxwrprl+lyu1AnCFg+ClgddZoGyy7rl0rT4CNphquo7ro5rsXtk0XddK2mwsN90FR4HgGLgOU1LaTVBle6nw0MkU7vEZzm4nhau1MvCFg+CFhPJn9qcL0MLwtEQUlz3XB1RIbhKqqtVlNhQDUVLtdtKvy34QFrQyAy9h8Bi4DlKWcwxk/e+L9A9l+P8bN1R4y6QcAyNWD96NDEbntqcEmqTYNF0k8uBazP9ZsKLxPbzFVNhVr73XzDj4+vE7AIWF40lPCSF3azbbc2WfqejxyGfwABy5SAdUjSpsHpKTcNDnEpXEX1yUJT4T4+OEbeTMAiYHlNQwOHMDDNdQ7b7bMsft8HVhMy9YOAZVLAejD5U4PrRHX/70RhU+2mwU4uh6uoA/WaCncStc9vEjXjNumM8t7DB8fKNgQsApbXHEqI8azZDtvrXzn4XhXgWlE3CFiGBKxPAslG/x6yVNTNDIlGex6j3UonLctSwPpODeCu8yOKdzlI1E1So7z/qtMf613Dj5efEbAIWF70AGHGc9YGtn4B7bE5/P5l1lNI1A8CVr4HrLYJt8vAhaJuaspNg+OyFK6iBuj+kLJLn7ReCJ10/9vFB8fNfxGwCFhe9BGhxlPOtW0fNaje0nr4HVdTNwhYeRyweiTrd1XzrDw3PLUklabBM7IcrqKO0/1BgYE/itrntV4IfZ0Pjp2HE7AIWF6zK6HG0y9ynlCPv2d8wJ2XQxOwCFi59HbyfldrRc3oX0VR3e66WaZa+iVHAWu51EjrhdB7HC5qX9BuKnzV8OOneiK6EQGLgOU11xNu6p1qmiuzbZdLPXLQ+gt1hICVR79952TbpHZSyi9ynpqjcBU1Rrup8LL+4RdTa+yHLaV1hh9HxxCwCFheNIuQU69OC2z9/sgtHvp9z0l7U08IWB53Q9IhGaaERMW9c1MJVxfnOFxFnal9e23gD7pNhRf74Fh6FgGLgOU1tT64usmnFzn/16O/9Ukr/FFnCFheM0enaTD49BpRtP1eutlle2lTPQWslVKVVlNhmyPDASs4Yo1c16RNhc8bfjz9tT67NhCwEM85hJ16GWXa/gLaXnnwpOPD0p+oMwQsj1gvbZtsW6ihDUpO657K3atX6ylcRU3Xbiq8vL9oOVtrlHc13t1Kw4+rswhYBCwvGk3oyanDbOXfIY9++xarI3wn6g0Bq55dotc0+Hoq4eraeg5XUX/T/cEtbp8kaqdojfJ+ug+OrVcTsAhYXlMqLST45MR/bGWvnoD5Lk/XRQ3qeJd0MHVoK68RsLLqpWRNg8Exv4rgyNWiaIe9dbPK7h4JVyGriXI77RdCj1sp13WNzn450gfH2N0JWAQsrzmS8JN1n0uFtnIfYci6fRiIDGLb2Xpyye/1aQ4BK6vN1cGkTYOTU35qcJ6HAlbIaqrUG2r+qMtFy1dVU+FinYvpHww/zn5IwCJgedF/CEFZta+tvM80dD1XSPOkF6UpPqPurEyWlhOw6umJMdU0OC0kKnqm1DR4j8fCVdRNWr++sCjSVPiSVlPhMT441v6TgEXA8qLPCEJZ0dNhfJrVlAsIWCmZkOwYFhytBuD8SRTvuI9uuNrLo+EqajetpsLgziI4bpUIjljNxXTEwQQsApbX7MmJxnXvBvw3wjIIWG5bIjVPePx6aqloOSMkGuq/yLmB9InHA9ZHKTUVvqLVVKheiP2V4fuz6ttaTMAiYHlNd042rmpjK9+bKRMQsFJ2YsLjlgwVqt9V2WVPptI0+JDHw1XUPXpxsVi0uH1yeNR6jQFI2/tgnx5KwCJgeRF3WNzR3Vau7SgTELBSNkyraXDAz6KgSZluuOqQJ+Eqan+9psJdwgOQ1ozdqBOyevpgvz6ZgEXA8prtpY2cdDLypq1Mi6QvKBcQsFLyvVSSuGlwmaibHhIN99JuGiyWvsqzgPWFVKCzco3adhQ1E7eI4PBVOsf6dw3fr9VDN1UELAKW1/yNk05GA3K2spXnY5QLCFgpOypZ06BqEiu7fEAqTYOP51m4inpUdwXLrxgYaSoclLQ/1u4+2LcnE7AIWF70LCceV15AeyJlAgJWyh7TaRqsHrhQFDTVbho8Ok/DVdRhOitZ0KBYVD/5rajReyG0H/qFXk7AImB5jbq1uoiTT0rmOlwhLqFcQMBKyfzEx6bFkabBaSHRqO2xuuGqmbQwzwPWT+qBQa2mwn06iZqJm0Vw+EpTX++UaqvCzgQsApbXdOLko22d1X8ttvzuoVxAwEpZh6RNgy+m3DT4VJ6Hq6gh2k2FVw4StZO1mgpV+Fhv+D4+j4BFwPKifpyAtFzpUHbVPrg6BAHLTb2TNg2OUgOKLhKFTZrpZo0uhoSrqJO0mgqLG4tAv29EzXOqqXBRsuP8ZT7Yz3sQsAhYXtPQumXPiSi+mQnKr7kPntYBAcsNHwWSvMg5MHR5+KnBRm2P0w1XldISwwLWUqm5VlNhuxNFzYTNIqDXVDjFB/t6ewIWActrDuAklFLToF2NtICyAgErob0TB6wl4Y7bZZf1T6VpcLxh4Spqgm4BlF05WNTN0rqLpe64/2L4vq6GyykkYBGwvKYnJyJHXTXLb7eA+fkuA2oAACAASURBVG+zBwErXbcmrD8DFonaqSFRed/cVMLVBYaGq6hztZoKGzUVLe6cGh6EVCNkneOD/b0vAYuA5UVvczL6g1kplt9OVjMIZQcC1u/eS/bUYHDUahF8eqUobrW/brgKSisND1irpTqtUd5rdxU14zeK4PBfdI5To3ywz3ckYBGwvEbdhWGU99+bBndIowwrrBMKZQgCVuQR+tbJmgbrpoZE6el3pHL3arrh4Spqum6BND3uKt2mQnWM+tnwfX5RINkLxAlYBKx60JUTUth5GZShqtjTKEMQsJZcn7RpcEpIVPScK0RhkW6WuMIn4Sqqq1ZTYVFD0eKOaaJW76nCjj7Y7ycQsAhYXjTd5yekIS6UoXo/4XBO7vBxwJqbtGlwtNU0uLN202Arab3PAtYma72TTkW1u4iaZzaL4DCtpsL+Ptj3LyRgEbC8Rj0Vt9ynJ6MPpQYulmUvTvDwYcBSA1tul7RpcHpIlJx+eypNg2/4LFxFvandVNixq2g5U2sAUnWc+8bwfX+9xlPgBCwCVs518eGJaE0gO69cuEjazIkePgpYlyVrGlT9rirunSsKGmg3Dd7g03AV1V27qfCuGbpNhYf6YP9/lYBFwPKioT47EXXKYll28MHVIghYIWtAy+RPDY5KqWlwD6upLORze2g/VTghpPtU4f0+qAP/IGARsLw4yvv3PjkJXZeD8gxIz3DCh8EBa7VUmShcBQYvFS1fS/mpwfcJV2Ef6jcVdrOaCpPexVIDc37ig3rQhoBFwPKaw31Q8f6d4zK9jZM+DA1Y5yXrd6UGxCzvNizclKU53UGw+oP7tEqtqFi06DFTt6nwTz6oBx9LBQQsApbXmNxRe1Q9lWkHq0M9AQCmBKxnAkneNVgzZm34CTc1+rjmtB+BytHBeiGroaju/71uyOrhg7rwMAGLgOVFUw2sbDPquUybSI8TAGBAwFohlSUKV8GhKyIvct6nUypNg58Qphx9qbpaab0Quv1pVn+sFTrHpHk+qA9HErAIWF7TVPrAoEo22+pj5oWyPTrAK3aQ3wHr5KRNgy+ERHnX4amEq0cIUgn10y3I8q5DRd20UPjpzSTHol0CkdH3Ta4PahT7ZgQsApbXBKWvDahg6h2DxR4rW3U3qw9hAHkYsIYmbRocu04ERqwSBQ21mwb/TIDScoxOYRaWthDVjy/QfSG0H97mMZqARcDyopbSV3lcsca7PJCo29pKEwkFyJOAtURqlLBp8KlfRK1qGmx3gm64UgNjfU140rJIaqZTqI33P1XUTgyJ4DCtpsLZPqgX5xKwCFheHen9rTysUE/kURmrJpd3CQfweMA6WqtpsNuIVJoGBxKcUjJKv6lwmG5TYa015IbJ9WKdNXQOAYuA5TmqSWtcHlWm2/K0nK9igFJ4NGA9kbRpcNw6ERy5WhQUN9bNAMcRmNJyll5TYUUqTYXn+6BuzCBgEbC8zOuP9qqrsM4GhNlrCVrwUMBakLAfY/ipwZWRpwb1mwbLpYWEpbSslprrPlVYN0n7qUI/DIx8HQGLgOVlh0lfeLDivBLIzrsF6zNoXSG9R2hAPQes9gn31YFLRN2UkCi/dlQqTYOjCUoZeVF7lPfjuomWL4fCTbhJjjll1hAcJtcP9dTk7gQsApaXlVpNBl6pNPdlbV0HLBQ140Ki7qWt1UyU++jQX3Ruv2fqNOklwgPqIWA9GEj2IudpIVHZ+41UwtUZBCRXXKRV2gWFoqLHnMjxatDiZMeaE3xQRz4gYBGw8sHB9Xzin5306joTAxeGr8wr7p0rmhx58Vaa/+NZUftiSNROylnQ2sca3uFHggRyELC+SNbvKvj0ankBsloUt9J+kXOF9CvhyBVrpe20Xggd2FHUPBcSwRFaL4Qe5oN68gABiwCTL06S5uSwcqiXlV6Y1XWSYaluakhUPfR/orAkfneHRu06ioq7pkSC1guh8B2vHJR3kXSGNFxaTKhAlgJWm2RNgy3nhERJai9ynkQwctWr2k2Fx18t6vSaChtLP/igrhxGwEI+OUIaIv2SpQqh3ud3SdbXQ4WrKSpcvS8KGmsNOyOD1vGisvfcyGPRAxfnsszVKMXnSAN8clBEbgLW3VpNg73eEAXFjXTP8RcSiLLiBr2mwgJRcfcruk2Fh/qgrvwvEHlrCQELeaVK+ps1gu6iDCvB0kDkJc2nZRqagsNWhftTJRIcs0XUzUgtXP1+H76hPOHMjVwlDl2ZiyZDuwKr2fZG6XkXyh7+DFjvaDUNjl+TStNgjbSRMJQ1u+k1Fe4Q01SYNGT9ywf1ZbhvA5ZqokHuha9wnlrh1p0YNfLzn6VrpL7SNOlL6+SvBn/bIK2XNkrLreEJVL+ue6QugUQvldUKVnId1OjSk+V6jd0iqh75NKHqAQtE5b2vpB6uYkJWxZ3Twk2GwdG/1kfIst/q31e6zOq7NcO6y7WWIGIse5/Eu9NYxi4J96shS8MPeZSc0l23VpRK8wlBWbVUqtVqKuzYTdTK7Vc9UOsY8rUP6owaFqfKdwGraceuqActbn0pHEhUSHAxaNmply9vL+0k7Si1ksrdW/7icDNGtBN6xd1TRcO2x8oAVJRQQcNGwo2pyRHnh7+3Zuz6+g5ZdoXSdlYTwDnW3a5e0iBpgjTFGhriXetOBvLD+9Y2281hAMnPNLan2uYfS5cn24eCI9eG61dhsyrd6nCIpB4znIiseV3qpP2kwX1viZpx63Vf5zVP+j9D681Hgcj7as/1XcBiqr+p8QGniIp7Z+UiaGXB4vBvDfcR+ecrotF+neqnDA/+q3XnLEnIUv0hhiyT820WwRGr86ic4SvhfjuLRe1zkYuWwrLqVN43yJT9qVB7lNcrB4qWc9W7CldyvIlc7DclYDHVQ9A6VVSqoKWavEat8drdGOe+VsNXi23kwaOy5+xUjjlZC1mq2TU8lIN6J1g0TI2RYWrUxvBrLFSfL/X3qkfny39bL1rOkv//WVneT6+PObFxgkc9Bqvwq3BC4YuW6n4/iJJTbxEFjUo4QObpVHbxY/JYs1TUjM/HC+jsIGAx1dvU9JhLw+32Nc+EvHvCl+GqZuyG8EGj2bn3hQfY88LUqO3xorLP6+ErxrqZkQNadd8vZdD6UV5JDhIlp3UXTY++NPw0VsPWh4rSs7qL5jeMl0FxUSRoDbPuakmqeSbcRMNVJ3IVrMaGwq/BqX7yB1Eq99WCJmUcEPN9alAcfmdko/1OEpX3zQ4f28PvLMzNMDMELAIW01b9ig67UARHrwmf9D13grfClWqSa3zQWd4rvIIiUdr5FlHa5VbR5KhLREHDxqKgpDTx/f4W1aL538eGA5kaNkLd2Qo8tVwEhq4I/3f4zph6IujpDSI4ZhPNi3D1DQbBp9f9FqzURUAhwcrclooDTxOVD7wVPqb49RhCwGKq/wufFi3lFc/r4QNvyhUx3Dy2VIa0jWFxm7/C/ybnG7lO1Dwb83Sj6qM0an14GX/4nApX46xwdWAX84LtkReIFt3Hi2Zn3y6Ktm0jirbfM/zfJSfdIJpfN1oEh/0sqh/7JnxCjAavcPiSV6TBket/K+/fyn3IMpodje0jtSR8B1c95Rccviq9Jn31eqgJofC+UnJ6d1HQlGDlh6mgqJGovP+t9I7tBCwCFpNLFbFhY1HZ+03R8rVoJ8lFSYdKUPOF2/tlCKp+/FtR/cSC8F0ZVZl/GyJiijqoq7sxm8O3rIMjl4jmN04QJSffJEpOvVlUPfJZ+PU1ahmq31KkU/j6yHLk5xsf0MWX26OwWXm4P0zD1n8Wzc65M1JeUvm1I2W5L4yUdwzV30uVXzSEhUd2ziRwqc8OXREJcE+vj9xxy3SoEHmCDwxZIU/2atmrIn3VXByKJHznLxw+N2wd2JM9RSfXUX1W/ca0vvvFSP+6cNiV5faH77YuLsIhKW65LP+t+S4cqMZuifz95Uh9qvznf0XpmXeHA3fNuI2phSwrXKm3EjTc82gOdn4MWfLYvs0bkf52rtS1Z626NipBXVOBTtb32Avq2AtrdYxSXSPUeSFbwxMRsJg8VBEbi2Z/+5eofSZOvyyrM7e6CxV+MfK4SLBqesJ1oqBxabgfR8nJN4rSM+4IhyeltPPtorrfd/KziyJNEhXBP35ncWNRWNIi/MqH0jN7hMetCo5ZJCp7zRIN2x7PRnEKX2XNI+Udo+lxV8nyuytc5uXdhoq6WdY2TLVDvRpwcviacFhRB8bqJyLbruzivr9t03S1uH1q+GCqQnzNs1vk7xyW8TJjNb9+TLiPm9rffn9SNs4TVdbBX50oIiP1LxSBYYtEizumpfXdqnwCgxeFyyt6QgnfbZInkdqJkf9f2fu/zuVy54xIf5nnInVKBaqqhz6I1B+5TZt2ujo8Yne42adDl8jFx0tWWE12VyIarmSYbfgnwpWfj+2lZ90pSk+71Z26duMzVl37Ibwvhl8p9tQvv/UrDdetSZGL7+Y3P7/V56se/kS0nKk+s0yUnn2fq8eBWAQsJu81Xx1+obzCWBvpl6WeklNNgCPWRcafmqxOHItFxe0TRcM2R8iK2zB55ZbBq6CJ3iCf4cBV0YKNkOFUcuLV4U736mStFbRUsBqxJhKcx6yVJ/h3RJM/nxPeboWl7m2Pxgd3FmWX3y33nUOyEz7LW4iCpuXh0N9CvbhbBgsVXn4LWuGD//LIviz/vUKGm9LTbxMNKreVwTWz9VTlpMpLlVt5t4Hhp0rVHaiqRz4UTY6+9LeQ5Fguh5wl53tP/rZF8mKjm6xXxXHnbdS2o7wAmR0JkZPiBC3rblh4/cPh6igqBZPrda2wtCLcraHFLS/89u7W8D4n982Ke6eLxu1PitNi0kR+7nrRoHbXrP5GAhaTJ6cGFdvKCjJXbPNfK1SNWCKa3zRRXk1fKwqbV1JAebERi+T2ukZUPTY/ftCyglWtasp9eq0ov7y/KNr+T8YUQaO9jxIVdzwXOei/GLlLpEJk5T/nyLB3Rla/u3jXDjLoXZ3anYbGpfph9cBTROV9L0fufsUGLbVNVRPj9Ei9bdiGcMWUg7q277Gi8sE35IXCO6LxASd54jcRsJg8O6khBppd2FuUnPh3GaqqKJC8DVrF8kSvgtaX4eawSPPvkvDYYuFgNfJXUaaC1bZtzD34732kqOz9iqh69MOsB6tcT407nCoqe80UdZMjfR6VwICfwk31BY3pzM7k34mAxcTElMOgda2o7js/3HQVHL0pEqy2aUPZGBK0Knq9JJpd8LAobFpOgTARsAhYTExMuZwKZNAqOfUmUbxzewqDiYmJgEXAYmJiYmJiYmIiYBGwvDPtLfWQ/h7jXJe/Yx/b8m+SnIZf7yDdIl2UwXcda/uuti6tg3qM7NqY5aoeys0zXGatRrkfbJsnE+dmaR9qIqlH//6W5u+6UmpsW2Zrl9b5EmufaO7Suu4mna353ZdKR2p+t2q3u9X2ebW/Ob04Wf3bdQ7fV+PC+rVzqKudHOqwG66NKZsdbX87LoN1sNeZg3J4PD3KOoadmeFydrWtwyku/LbDbMt0esKhsVUf09me6rh9gHU8SHc9T7b9/aAs7Hfh/YuAxZSLaQ8V5h0c4dLyG0hLHJZvf4TqVtvfX5KK0/i+ybbl3O7SerRxWIfWGS7zCNvyljvM82Sc7ZOOFS7vO+rg119a6MJvC9iWfZWL6x0t2+elE9NYT/UUx13S59LmNL97nHR0kpD6g8Nnb3OYt9CqH/Z5x7mwTZ22pSqzni5vj6g9rO890/bvUzJYhwG2ZT2Ro2Pp/bbvHZnBsi6xLesdF37fSNsyn3WYp9qFbbrYKvM901jPt21/75el/W4KAYspV9OpDjvgFy4t+58Oy55om+e0OJXg6TS+b6BtGVe5tB67SOtjlrtaapXhMg/UOIi6eWJ716WyUIM2Pe7i71IHZPv4Hudk6cCqvGDdkdSZznUpQEaNlipSCPG/Sk7jM6h/W+kwfyZ3bLs6LG9GnPDghnXSztbyO9n+NjiD9bjPtqy7c3AMPT/OOt6f5vLO1AhDqU4PaQRPtW8ucnEb35fietrPDfdk6RgwmIDFlMtpusNOeJMLt7nty1xvO8E0sE6w8SrCnwlYnrqDpZ7tfzMLB7xADgOW8pFGyLorS989X9omzncOdZj/kTjzXuMw78w0t2sja/+wL2936+9jsnwHK58DlhopeU2CdUxnxExTAlb0gibdgPUEd7AIWCZMOzvshBtEZv06Zjss80rbPA8kqQifEbD+MM8sqW+abnGhHObG2U7rre39RBq/S13llyQJWO+muc5DpI/j/ObnEqzn3xPsk59Iw5J8r7rDN1VaFWcZ30pNHb63zqp3sfNuTFAPv3NY9qFpbNd7na7yY/7eWaOsv7d9/vkk8z9iNUnle8B6KskxbEYeB6wNVh1Ktu1VV4Y3EpTBuDQD1oka3/212Lp5OdlnziVgMeV6crodOz7NZf3FYVnvaIS6kEYo83PAOtlj+4fSR9rO5e+yB6xMwqHqs3S4iPShsv/2/RzmbxtnPdXdoaOs5elOKkDcbNt3oobF+cz9Kcx7lsO876VYPuo3bhJbN9+lOoLw87ZlpDKIWr4GrHaax7BUX57qlYD1S4rf0cbadk5lcFEaAUtnsvctO1jnQwQsplxPqrnuG5F5h3fV3LDUYTl72eab5TCPU0ffNdZteAKWEJfV075R53ASVv2DjsnS99kD1gMuLFO9TXyFxnKd9sv7M/zufYVz04vTu4dU/6rlDvPuFmfZ7znM2zmF3zbI4fPd01jHl23L6OiDgPV/msew7/M0YK2y6n6q03kOZbBMapiFgPWcbRlaT3ASsJjqY+rkUDG+THEZTu3m/7HNc7rDPD+KSN+UeQ5/e5yAVa8B6x/C+emybE3ZCFhO++YE299bO6zncJe+Ww2JYm/+6xtn3uuEfl+WwxzmXaD5m5z6Sf4s0nuC128B6wqHsvtQamkdM3WeCDU1YKmpm0MZnEvAImD5fXreoWLcqPlZp+aVhbYDdgPh/GTWX6y/HyESd4j1c8C6op72iTm23/FMlr8vWwGra5KAZQ826o5XiYvr9bDYuj9XgcN8BXHuJu8bZ7kzHObV2VcmO3wu3TGcvBKw/pGDgNXCuoNrL7sTrL+fIZz7Kdb6KGAJh4vlYQQsApbfp1qHK211cNDp8P6Ow4HF/vr0Pg7zvGqbx+mppTkELHFhTAC5KAWZ9N0qs+4uxv6OTnkasHomOXHZx1Aa6PJ6qbGBYsfS+sW646FzknWqJ9FpN+E8/lZRgt/SweEz72ewbtkIWKpPz2nWsnSoscZG5SBgjXQou8m2eWYL52E6/BSw7E2Fqkm1kIBFwPL7dLNIfSDDyx0+86JtntZx7k7tYpuvyiHkKclGNDY9YHWJHh9E6mNNpTu1En98Gk6te7xhBtSI5OdbQTBVe+cgYNnvCtmbnqfY/n6xy/VKBZ75tu/YP8H8Tn184g1Y+i+HeRMNtPuWw/wHeCRgDUoQZFLldsBq7/AdGx3uTsV7iOdAHwUse/BXfdGKCVgELKbIEAn2g8PhceZtbp14kz1e/prQ70Cs+mltcaigDXwcsKKvGFqe4kkmk5Ggd7TutMT27ylPcIcm3RNh3ywGLBXYx4rkT3e9bfv7UVmoV/Zmk0SvcnFqLv8wzrxOT3mqZiynB0ROFKmNV1RfAWuwBwPWh0L/oYCrHeb9wEcBS3Vqj32w5CvbXVUCFgHLt9PhwrnzrNNj6sMd5r3BNo/TI+U/x5zET7JOaIp6h5t6v91PDp+5ox4ClroS2+RywGqfwztYSzL4nTuJP44a/oOI/3693R1CcTonwngBq8oKRcfE7CtOjrTCSRfrs05P8DmNbD8jg5CgO9mb0dsnmd/ptTidHe4ybohTrv92WOZ8h/m28WDAGu+xgOU02n30rReqOfOEmH1Q7X/HCecR9y9PMWBNdOG32wPWgBwErFLbhfcCAhYBi+n3aYxGs8OBDvN8bJunkXB+J+Eh1t/jvY5kQ5zb8XU5Dlg7iD82k6n/3t7lgPWBRsCKPoWjDo4jU3BfBr+zwrZ91icIl3u4dCKMF7DOEO6M5Lxa/D5Keexkv2Nytcv1qYn444Cca6x9K9HkVKbf2OYZnWBdVZ+vbWPmvVQzhHkhYKmLNNX3cqamSSIyiGs2AlaVw1366Hhf6qJzXZzy3+jwb2oYm7IE3/VX4f5DJfbx1f6Vg4DVzuG8UEDAImAxRaag2PppmQ22q12dARwfcphnUoK/JTM2xwGrufjj+DYb45ygU5nsQ1W8oRGw6uspQvvrcc5PcMWqruJPtu5IxnOEQ3jXCVinuxCuVMCJ19fI3vfwBZfL0X5XWN1J0hkSwampLDoA714a6xx9r6fTy9dXCOf3HdZnwMrkKcI7sxSwRjmU61DrbxPT2A8fTfBd9vezvubC7+9vW+Y/chCwbkhy3CZgEbB8PzndFo8+3u401om9bX/3OAcY1Y/n0AxOlAfmMGCpE5N9QMcLMlym/YXJIzUCVn2Ng2UfW+gjF5Z5cxoB6+w095X11l2fPiJ+86awLgzsn23tYjnam/uGaH7O6RU6KigWia37lm2Ic6elzNpn7f9+jUvr5pWAdU8WAtYhcfYpVf5/yeAYtrvmnZ811sVuJtMXGhdJbgYsdVdvgUj8Vg4CFgGLyTqh2g8O1zlUIDVar/0da6+I+M2M6jb1POsKLR514H5f6D1Snq2AJayTYeyyP81gWduJrV8Qe4OHA5bTE1GZdjy/L42ApV7+/Xqc/UTtZ9OF8/hEaj/VHYPIHqRV023DLF2oHJbC53s5fF6t71qxddPP4Q7zzhVbP0U538V9xOSBRp3u0kfvJo+27vAmOoapvn2fCP2Xc6tjqH1olIcz+P1/E85Nm9kMWPZma9VUXU3AImAxbT0dpHlFdrbtc10c5vlO/N5RviiFO0hOTzVekiRgufmY/bEO368OCKk2r+xonbTty9peI2BdUI/7wBCH36xO5o3TXF6PNAKWzqSa/5w6Fo8WiZ9AjU4d44STXVy8W5fo5BpvamZdwCSrg9ET4jyNeY93cf8wNWBdK5xf+B17p0Znsve/izo9zvwDHeZN5wJLPTFq7wf2dpx57QFrZZI7vk6Tmt/poSenp8UJWAQsJmtK9si0/YShTrwLXTyoO70SZJkt4NgPStfG/JZUlMS5a/Guw2/4VkSebFQHsr2soKSaP4utAFklfn/C6D9xTv5PxVlne8DqmsH6NMpw+1cL56fx1NW9aipWzRpl1non+z1CbN3p1q2Apab94pTzNM1QP0I4D3mgfofqP1Zj7R9O61Zs7ZN7WncO3hDOHc93S2MbXCP0+/UclmTeOS4fH0wMWNUOdwiTDa2RaDotzgWnU/CPN2bgy9adefWk4q7WndnGVv1ubO2bamw19UDMhDjLOFMzYK2yLggbJKjLRVb4Vxc2t8QJkV/FuRAjYBGwmKypRZyTlrLFqoix04Mi+cCjqU7jHJb5cIKApQ4W6smVL1OkmlKc+kTtLZyfCoo9ca6wDpqfW/6X5DP/S3CV2NNhfT5MY32+FpFXIGU6dYhzwoltivtc4/d85HA3xs2AJayTzKo4ISvZnawGcYJR7HZQfVrmO6zb59b+sznB509Ks/wLrW0Z78lI+340I8Fv+BMBK+nkNNDpSJfLKdHQMzclCckq9C+29rvovrgoyWcShRh7wNps7cufJajLn4utn9y075fxXnNGwCJgMcVMl8epRPahG3aLEz4yHWtnmzhhZSfNu2ypeC/Obzg6QdBM1TfWXS+hGbAy8a1L+8ChVigMucztgKWm9nFC1lSNkFUqnN/LmYlfrbsYmUxnxln29Q7ztokz77AsHBtMC1iHxgkLFRmWUyvhPJRDvFcmPeji/jc5yR3cCo2AlooF1p1t3X2ZgEXA8v30ukg85pXTPCHr9rEb0+1xbpurqb+LB4fXE/wG1el7dIYnWtVcWJ5kXXu7uD6fuLgPqKaTQS6Hj14xyz/f9rdHMvit8ULWHKHXf06N0r3UhfWbmiRMpzK95dAEE28aaptXDZgbzMJxwf6WhhNS+OxJts+OyOB39EmwX+lOTcTWD/C4+dCMU71+LsH8ncXWD1+kGnRu1vhdlXHqSjoe1wijZ4vE73PUmeyvtzqbgEXAyudJDVoYHRNKjamzq+3vZ8UcyDcluRuUzqQGqnvf4TsOjAlfmzIUG9oSTXtaB64pVoBZbN2p22wtRzWdqsfmf7LKQPWJuNIKaDrTPS6uz9tZ2Bf2tq6w1cl1oW3dU/19d8UsNzrQYnRU+D4Z/s4DxO9vBYj9zpc0Qm70zun11j6h7gSuS7Jd1O9eZpW5GhT2GJfLXe0/0dcXrRWJR4NX/e9inwL+S5aOC9Nt2+y4FD57fEx5ZvqS7Z62Zd2VxjJucNhX5rpYVqr/3qcO33Fsks+pu4KqS4Tq7/qltQ9ssS1DXbx9Z10gDrPu6Oh2VK9wqCfJRL//Z+s4cH8KFxJdbPtMOgOqToxpIYl92wUBi4CVt9MO0o3CeYwgFbgOssLHntbBv8Ll76+wlhv9jgOt36Q6lO8f8+/pUsvYKcXf1NgKn3tZ1HLaikgzTa344wjGupO609DBhfXZL4VQl+5UZ1v3VLS33VVRJ4R9rfI7QOgPsZBoqrXtGyocHq0ZsOz7Xmvr8/HWp61IPkJ7ppNqhv+H2HpQ33i/Wd2JOzmLv2dHq3zVuu8jnN+BGG9qZq1HtO5l0pUgGLOd26d5t24Pa7+LPb6Uulxe1bb9sUOKx5wya/62tn16N+s4mM7UwNq390mh7ka/P506Wm5t97bWtkrn7RjbW+WojjvtdOuz7wIWAACAn1AIAAAABCwAAAACFgAAAAELAAAABCwAAAACFgAAAAELAAAABCwAAAACFgAAAAELAAAABCwAAAACFgAAAAELAACAgAUAAAACFgAAAAELAACA5vQlBgAAAZFJREFUgAUAAAACFgAAAAELAACAgAUAAAACFgAAAAELAACAgAUAAAACFgAAAAELAACAgAUAAEDAAgAAAAELAACAgAUAAEDAAgAAAAELAACAgAUAAEDAAgAAAAELAACAgAUAAEDAAgAAAAELAACAgAUAAEDAAgAAIGABAACAgAUAAEDAAgAAIGABAACAgAUAAEDAAgAAIGABAACAgAUAAEDAAgAAIGABAACAgAUAAEDAAgAAIGABAAAQsAAAAEDAAgAAIGABAAAQsAAAAEDAAgAAIGABAAAQsAAAAEDAAgAAIGABAAAQsAAAAAhYAAAAIGABAAAQsAAAAAhYAAAAIGABAAAQsAAAAAhYAAAAIGABAAAQsAAAAAhYAAAAIGABAAAQsAAAAAhYAAAABCwAAAAQsAAAAAhYAAAABCwAAAAQsAAAAAhYAAAABCwAAAAQsAAAAAhYAAAABCwAAAAQsAAAAAhYAAAABCwAAAACFgAAAAhYAAAABCwAAAACFgAAAAhYAAAABCwAAAACFgAAAOL4f2T2EOAT8BWAAAAAAElFTkSuQmCC"
        alt="logo" height="50" width="100" style="border-radius: 50%">  
      </div>
      <div style=" flex: 1; ">
        <p style="-webkit-print-color-adjust: exact; margin-left: -40%; margin-top: -6%;
        background-color: #0C5A9E; color: white; border-bottom-right-radius: 25px; width: 100%; padding:3%; font-size:70%;">Value Growth Audit Report <br>
        <span style="font-size:80%;color: white;"> ${apiData[0].section_name} <span></p>
      </div>
    </div>`,
    footerTemplate : `
      <div class="footer" onChange=onChange(this) style="height:50%; width: 100%; font-size: 9px; color: #bbb; position: relative;">
      <img src=${`data:image/png;base64,${apiData[0].client.logo}`}
        alt="logo" height="40" width="70" style="border-radius: 50%; position: absolute; margin-top: 0%; margin-left: 2%;">
        <p style="-webkit-print-color-adjust: exact; margin-left: 20%; margin-top: 0%; background-color: #0C5A9E; color: white; border-top-right-radius: 25px; width: 50%; padding:3%; font-size: 14px;">Value Growth Audit</p>
        <p style="-webkit-print-color-adjust: exact; margin-top: 0%; background-color: #0C5A9E; color: white; padding:3%; border-top-left-radius: 25px; position: absolute; right: 5px; top: 5px; font-size: 14px;">Page <span class="pageNumber" style="font-weight: bold;"></span> of <span class="totalPages" style="font-weight: bold;"></span></p>
      </div>`,
    }
    
    // await page.goto(`file:///Users/apple/Documents/VGA_/vga_api_new/src/uploads/print.html`, {
    //   waitUntil: 'networkidle0'
    // });

    await page.goto(`file:///${path.resolve('src/uploads/print.html')}`, {  waitUntil: 'networkidle0'});   //update path varibale by Nikesh Thakkar

    await page.setCacheEnabled(false);
    const pdf = await page.pdf(options);
    await browser.close();
    
    return pdf;
  }catch(err){
    console.log("Failed to convert URL to PDF ("+err.name+" - "+err.message+")");
    console.log(err);
  }
};

const contentOfOrganizationDiagnosisHtml = async (apiData) => {
  let consultantReview;
  commonhtml += `
  <html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  </head>
  <body>`;
  for (let i = 0; i < apiData.sectionsProcess.sections[0].subSection.length; i++) {
    for (let j = 0; j < apiData.sectionsProcess.sections[0].subSection[i].questions.length; j++) {
      commonhtml += `
      <p class="mt-3 ml-5" style="color: #0C5A9E; font-size: 30px;"><b><i>${apiData.sectionsProcess.sections[0].subSection[i].sequence_number}. ${apiData.sectionsProcess.sections[0].subSection[i].sub_section_name}</i></b></p>
      <p class="ml-5" style="color: #0C5A9E; font-size: 20px;"><b><i>${i+1}.${j+1} ${apiData.sectionsProcess.sections[0].subSection[i].questions[j].question}</i></b></p>
      <div class="container mt-5" style="height: 120%;">
        <div class="row">
          <div class="col-md-1">
            <h5 style="color:#0C5A9E; width:150px; margin-top:200px; margin-left:-10px; transform: rotate(-90deg);"><b><i>${apiData.sectionsProcess.sections[0].subSection[i].questions[j].question}</i></b></h5>
          </div>`
          commonhtml += `
          <div class="col-md-11">
            <table style="text-align: center;margin-left: 5%;">
              <tr> 
                <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                  <b>Relevant Factors</b>
                </td>`
                const averageOfQuestion = apiData.sectionsProcess.sections[0].subSection[i].questions[j].reportanswers[0].question_average;
                const totalAverage = apiData.sectionsProcess.sections[0].subSection[i].questions[j].reportanswers[0].total_average;
                if (averageOfQuestion === 1 && totalAverage === 3) {
                  commonhtml += `
                  <td class="border border-white" style="background-color: #A9A9A9; padding:71; width:25%">
                    <b>${averageOfQuestion}  /  ${totalAverage} </b>
                  </td>`
                } else{
                  commonhtml +=`
                  <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                }
                if (averageOfQuestion === 1 && totalAverage === 2) {
                  commonhtml += `
                  <td class="border border-white" style="background-color: #A9A9A9; padding:71; width:25%">
                    <b>${averageOfQuestion}  /  ${totalAverage} </b>
                  </td>`
                } else{
                  commonhtml +=`
                  <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                }
                if (averageOfQuestion === 1 && totalAverage === 1) {
                  commonhtml += `
                  <td class="border border-white" style="background-color: #A9A9A9; padding:71; width:25%">
                    <b>${averageOfQuestion}  /  ${totalAverage} </b>
                  </td>`
                } else{
                  commonhtml +=`
                  <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                }
                commonhtml += `
              </tr>
              <tr> 
                <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                  <b>Somewhat Relevant Factors</b>
                </td>`
                if (averageOfQuestion === 2 && totalAverage === 3) {
                  commonhtml += `
                  <td class="border border-white" style="background-color: #A9A9A9; padding:71; width:25%">
                    <b>${averageOfQuestion}  /  ${totalAverage} </b>
                  </td>`
                } else{
                  commonhtml +=`
                  <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                }
                if (averageOfQuestion === 2 && totalAverage === 2) {
                  commonhtml += `
                  <td class="border border-white" style="background-color: #A9A9A9; padding:71; width:25%">
                    <b>${averageOfQuestion}  /  ${totalAverage} </b>
                  </td>`
                } else{
                  commonhtml +=`
                  <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                }
                if (averageOfQuestion === 2 && totalAverage === 1) {
                  commonhtml += `
                  <td class="border border-white" style="background-color: #A9A9A9; padding:71; width:25%">
                    <b>${averageOfQuestion}  /  ${totalAverage} </b>
                  </td>`
                } else{
                  commonhtml +=`
                  <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                }
                commonhtml += `
              </tr>
              <tr> 
                <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                  <b>Irrelevant Factors</b>
                </td>`
                if (averageOfQuestion === 3 && totalAverage === 3) {
                  commonhtml += `
                  <td class="border border-white" style="background-color: #A9A9A9; padding:71; width:25%">
                    <b>${averageOfQuestion}  /  ${totalAverage} </b>
                  </td>`
                } else{
                  commonhtml +=`
                  <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                }
                if (averageOfQuestion === 3 && totalAverage === 2) {
                  commonhtml += `
                  <td class="border border-white" style="background-color: #A9A9A9; padding:71; width:25%">
                    <b>${averageOfQuestion}  /  ${totalAverage} </b>
                  </td>`
                } else{
                  commonhtml +=`
                  <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                }
                if (averageOfQuestion === 3 && totalAverage === 1) {
                  commonhtml += `
                  <td class="border border-white" style="background-color: #A9A9A9; padding:71; width:25%">
                    <b>${averageOfQuestion}  /  ${totalAverage} </b>
                  </td>`
                } else{
                  commonhtml +=`
                  <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                }
                commonhtml += `
              </tr>
              <tr> 
                <td class="border border-white" style="color: white; background-color: #0C5A9E;padding:71; width:25%"></td>
                <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                  <b>Irrelevant Factors</b>
                </td>
                <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                  <b>Somewhat Relevant Factors</b>
                </td>
                <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                  <b>Relevant Factors</b>
                </td>
              </tr>
            </table>
          </div>
        </div>`
        if (apiData.sectionsProcess.sections[0].subSection[i].questions[j].question_type === 1) {
          commonhtml += `<h4 style="color:#0C5A9E; text-align: center; margin-left: 30%;"><b><i>EXTERNAL - MACRO FACTORS</i></b></h4>`
        } else if (apiData.sectionsProcess.sections[0].subSection[i].questions[j].question_type === 2) {
          commonhtml += `<h4 style="color:#0C5A9E; text-align: center; margin-left: 30%;"><b><i>EXTERNAL - MICRO FACTORS</i></b></h4>`
        } else if (apiData.sectionsProcess.sections[0].subSection[i].questions[j].question_type === 3) {
          commonhtml += `<h4 style="color:#0C5A9E; text-align: center; margin-left: 30%;"><b><i>INTERNAL FACTORS</i></b></h4>`
        }

        if(apiData.sectionsProcess.sections[0].subSection[i].questions[j].reportanswers.length > 0) {
          if(consultantReview === null){
            consultantReview = ''
          } else{
            consultantReview = apiData.sectionsProcess.sections[0].subSection[i].questions[j].reportanswers[0]?.consultant_review === null ? '' : apiData.sectionsProcess.sections[0].subSection[i].questions[j].reportanswers[0]?.consultant_review;
          }
        }
        commonhtml += `<hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="10" cols="110" style="resize:none;border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;"> ${consultantReview}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>`
    }
  }
  commonhtml += `
  </body>
  </html>`;
  fs.writeFileSync('src/uploads/print.html', commonhtml);
  return path.resolve('src/uploads/print.html');
}

const contentOfCompetencyMappingHtml = async (apiData, auditNo) => {
  commonhtml += `
  <html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  </head>
  <body>`;
  let topLevel = [];
  for (let t = 0; t < apiData.sectionsProcess2.sections[0].topLevel.length; t++) {
    let reportanswers;
    for (let q = 0; q < apiData.sectionsProcess2.sections[0].topLevel[t].questions.length; q++) {
      reportanswers = await AnswersValues.query().where('question_id', apiData.sectionsProcess2.sections[0].topLevel[t].questions[q].id).andWhere('vga_audit_number', auditNo).andWhere('client_employee_id', apiData.sectionsProcess2.sections[0].topLevel[t].id).andWhere('is_deleted', false);
      if(reportanswers && reportanswers.length > 0){
        topLevel.push(apiData.sectionsProcess2.sections[0].topLevel[t])
      } else {}
    }
  }
  apiData.sectionsProcess2.sections[0].topLevel = [...new Set(topLevel.map(item => item.id))].map(store => { return topLevel.find(items => items.id === store)})
  for (let t = 0; t < apiData.sectionsProcess2.sections[0].topLevel.length; t++) {
    commonhtml += `
    <p class="mt-3 ml-5" style="color: #0C5A9E; font-size: 30px;"><b><i>2.1. Top Level</i></b></p>
    <p class="mt-3 ml-5" style="color: #0C5A9E; font-size: 25px;"><b><i>${t+1}. ${apiData.sectionsProcess2.sections[0].topLevel[t].name}</i></b></p>`
    let topLevelQuestionsOfImportance = [];
    let managerialLevelQuestionsOfImportance = [];
    let executiveLevelQuestionsOfImportance = [];
    let subSection1, subSection2;
    let performanceHighImportanceLow = [];
    let performanceLowImportanceLow = [];
    let performanceHighImportanceHigh = [];
    let performanceLowImportanceHigh = [];
    let consultantReview;

    for(let subSec = 0; subSec < apiData.sectionsProcess2.sections[0].subSection.length; subSec++){
      if(apiData.sectionsProcess2.sections[0].subSection[subSec].sequence_number === 1){
        subSection1 = apiData.sectionsProcess2.sections[0].subSection[subSec].sub_section_name;
        for(let que = 0; que < apiData.sectionsProcess2.sections[0].subSection[subSec].questions.length; que++){
          if(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que].question_type === 6){
            topLevelQuestionsOfImportance.push(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que]);
          } else if(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que].question_type === 7){
            managerialLevelQuestionsOfImportance.push(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que]);
          } else if(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que].question_type === 8){
            executiveLevelQuestionsOfImportance.push(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que]);
          }
        }
      } else{
        subSection2 = apiData.sectionsProcess2.sections[0].subSection[subSec].sub_section_name;
      }
    }
    commonhtml += `
    <div class="row mt-5">
      <div class="col-md-2 mr-2">
        <h4 class="mr-5" style="transform: rotate(-90deg); color: #0C5A9E; margin-top: 100px; width: 170%;"><b><i>${subSection1}</i></b></h4>
      </div>`
      performanceHighImportanceLow = [];
      performanceLowImportanceLow = [];
      performanceHighImportanceHigh = [];
      performanceLowImportanceHigh = [];
    for (let q = 0; q < apiData.sectionsProcess2.sections[0].topLevel[t].questions.length; q++) {
      for(let tl = 0; tl < topLevelQuestionsOfImportance.length; tl++){
        let reportanswers = await AnswersValues.query().where('question_id', apiData.sectionsProcess2.sections[0].topLevel[t].questions[q].id).andWhere('vga_audit_number', auditNo).andWhere('client_employee_id', apiData.sectionsProcess2.sections[0].topLevel[t].id).andWhere('is_deleted', false);
        let answerSum = 0;
        let average;
        if(reportanswers && reportanswers.length > 0){
          if(consultantReview === null){
            consultantReview = ''
          } else{
            consultantReview = reportanswers[0]?.consultant_review === null ? '' : reportanswers[0]?.consultant_review;
          }
          for(let ans = 0; ans < reportanswers.length; ans++){
            answerSum = answerSum + parseInt(reportanswers[ans].answer_value);
            average = answerSum / 4;
          }
          // console.log(answerSum)
          // console.log(average)
          if(topLevelQuestionsOfImportance[tl].question === apiData.sectionsProcess2.sections[0].topLevel[t].questions[q].question){
            if(average >= 3 && topLevelQuestionsOfImportance[tl].reportanswers[0].question_average < 3){
              performanceHighImportanceLow.push(apiData.sectionsProcess2.sections[0].topLevel[t].questions[q].question);
              // console.log('1 --- ', apiData.sectionsProcess2.sections[0].topLevel[t].questions[q].question)
            } else if(average < 3 && topLevelQuestionsOfImportance[tl].reportanswers[0].question_average < 3){
              performanceLowImportanceLow.push(apiData.sectionsProcess2.sections[0].topLevel[t].questions[q].question);
              // console.log('2 --- ', apiData.sectionsProcess2.sections[0].topLevel[t].questions[q].question)
            } else if(average >= 3 && topLevelQuestionsOfImportance[tl].reportanswers[0].question_average >= 3){
              performanceHighImportanceHigh.push(topLevelQuestionsOfImportance[tl].question);
              // console.log('3 --- ', apiData.sectionsProcess2.sections[0].topLevel[t].questions[q].question)
            } else if(average < 3 && topLevelQuestionsOfImportance[tl].reportanswers[0].question_average >= 3){
              performanceLowImportanceHigh.push(topLevelQuestionsOfImportance[tl].question);
              // console.log('4 --- ', apiData.sectionsProcess2.sections[0].topLevel[t].questions[q].question)
            }
          }
        } else {
          // average = 0;
        }
      } 
    }
    commonhtml += `
      <div class="col-md-8">
        <table>
          <tr>
            <td><p>High</p></td>
            <td style="background-color: #eee!important; color: #000!important; border-top: 2px solid #1f91c0!important; border-left: 2px solid #1f91c0!important; height: 200px; width: 50%;">
              <div class="text-center">`
                for(let i = 0; i < performanceLowImportanceHigh.length; i++){
                  commonhtml +=`<p>${performanceLowImportanceHigh[i]}</p>`
                }
              commonhtml += `</div>
            </td>
            <td style="background-color: #e0e0e0!important; color: #000!important; height: 200px; border-top: 2px solid #1f91c0!important; border-right: 2px solid #1f91c0!important; width: 50%;">
              <div class="text-center">`
              for(let i = 0; i < performanceHighImportanceHigh.length; i++){
                commonhtml +=`<p>${performanceHighImportanceHigh[i]}</p>`
              }
              commonhtml +=`</div>
            </td>
          </tr>
          <tr>
            <td><p>Low</p></td>
            <td style="background-color: #bdbdbd!important; color: #000; height: 200px; border-bottom: 2px solid #1f91c0!important; border-left: 2px solid #1f91c0!important; width: 50%;">
              <div class="text-center">`
              for(let i = 0; i < performanceLowImportanceLow.length; i++){
                commonhtml +=`<p>${performanceLowImportanceLow[i]}</p>`
              }
              commonhtml +=`</div>  
            </td>
            <td style=" background-color: #9e9e9e!important; color: #000!important; height: 200px; border-bottom: 2px solid #1f91c0!important; border-right: 2px solid #1f91c0!important; width: 50%;">
              <div class="text-center">`
              for(let i = 0; i < performanceHighImportanceLow.length; i++){
                commonhtml +=`<p>${performanceHighImportanceLow[i]}</p>`
              }
              commonhtml +=`</div>
            </td>
          </tr>
          <tr>
            <td></td>
            <td class="text-center">Low</td>
            <td class="text-center">High</td>
          </tr>
        </table>
        <h4 style="color: #0C5A9E;" class="text-center mt-3"><b><i>${subSection2}</i></b></h4>
      </div>  
      </div>
      <hr class="mt-5" style="width: 100%"/>
      <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
      <textarea class="mt-3 mx-5" rows="7" cols="115" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${consultantReview}</textarea>
      <div style="page-break-before: always; margin-top: 20%"></div>
    `
  }
  for (let m = 0; m < apiData.sectionsProcess2.sections[0].managerialLevel.length; m++) {
    commonhtml += `
    <p class="mt-3 ml-5" style="color: #0C5A9E; font-size: 30px;"><b><i>2.2. Managerial Level</i></b></p>
    <p class="mt-3 ml-5" style="color: #0C5A9E; font-size: 25px;"><b><i>${m+1}. ${apiData.sectionsProcess2.sections[0].managerialLevel[m].name}</i></b></p>`
    let topLevelQuestionsOfImportance = [];
    let managerialLevelQuestionsOfImportance = [];
    let executiveLevelQuestionsOfImportance = [];
    let subSection1, subSection2;
    let performanceHighImportanceLow = [];
    let performanceLowImportanceLow = [];
    let performanceHighImportanceHigh = [];
    let performanceLowImportanceHigh = [];
    let consultantReview;

    for(let subSec = 0; subSec < apiData.sectionsProcess2.sections[0].subSection.length; subSec++){
      if(apiData.sectionsProcess2.sections[0].subSection[subSec].sequence_number === 1){
        subSection1 = apiData.sectionsProcess2.sections[0].subSection[subSec].sub_section_name;
        for(let que = 0; que < apiData.sectionsProcess2.sections[0].subSection[subSec].questions.length; que++){
          if(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que].question_type === 6){
            topLevelQuestionsOfImportance.push(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que]);
          } else if(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que].question_type === 7){
            managerialLevelQuestionsOfImportance.push(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que]);
          } else if(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que].question_type === 8){
            executiveLevelQuestionsOfImportance.push(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que]);
          }
        }
      } else{
        subSection2 = apiData.sectionsProcess2.sections[0].subSection[subSec].sub_section_name;
      }
    }
    commonhtml += `
    <div class="row mt-5">
      <div class="col-md-2 mr-2">
        <h4 class="mr-5" style="transform: rotate(-90deg); color: #0C5A9E; margin-top: 100px; width: 170%;"><b><i>${subSection1}</i></b></h4>
      </div>`
      performanceHighImportanceLow = [];
      performanceLowImportanceLow = [];
      performanceHighImportanceHigh = [];
      performanceLowImportanceHigh = [];
    for (let q = 0; q < apiData.sectionsProcess2.sections[0].managerialLevel[m].questions.length; q++) {
      for(let ml = 0; ml < managerialLevelQuestionsOfImportance.length; ml++){
        let reportanswers = await AnswersValues.query().where('question_id', apiData.sectionsProcess2.sections[0].managerialLevel[m].questions[q].id).andWhere('vga_audit_number', auditNo).andWhere('client_employee_id', apiData.sectionsProcess2.sections[0].managerialLevel[m].id).andWhere('is_deleted', false);
        if(consultantReview === null){
          consultantReview = ''
        } else{
          consultantReview = reportanswers[0]?.consultant_review === null ? '' : reportanswers[0]?.consultant_review;
        }
        let answerSum = 0;
        let average;
        if(reportanswers && reportanswers.length > 0){
          for(let ans = 0; ans < reportanswers.length; ans++){
            answerSum = answerSum + parseInt(reportanswers[ans].answer_value);
            average = answerSum / 4;
          }
          if(managerialLevelQuestionsOfImportance[ml].question === apiData.sectionsProcess2.sections[0].managerialLevel[m].questions[q].question){
            if(average >= 3 && managerialLevelQuestionsOfImportance[ml].reportanswers[0].question_average < 3){
              performanceHighImportanceLow.push(apiData.sectionsProcess2.sections[0].managerialLevel[m].questions[q].question);
            } else if(average < 3 && managerialLevelQuestionsOfImportance[ml].reportanswers[0].question_average < 3){
              performanceLowImportanceLow.push(apiData.sectionsProcess2.sections[0].managerialLevel[m].questions[q].question);
            } else if(average >= 3 && managerialLevelQuestionsOfImportance[ml].reportanswers[0].question_average >= 3){
              performanceHighImportanceHigh.push(managerialLevelQuestionsOfImportance[ml].question);
            } else if(average < 3 && managerialLevelQuestionsOfImportance[ml].reportanswers[0].question_average >= 3){
              performanceLowImportanceHigh.push(managerialLevelQuestionsOfImportance[ml].question);
            }
          }
        } else {
          // average = 0;
        }
      } 
    }
    commonhtml += `
      <div class="col-md-8">
        <table>
          <tr>
            <td><p>High</p></td>
            <td style="background-color: #eee!important; color: #000!important; border-top: 2px solid #1f91c0!important; border-left: 2px solid #1f91c0!important; height: 200px; width: 50%;">
              <div class="text-center">`
                for(let i = 0; i < performanceLowImportanceHigh.length; i++){
                  commonhtml +=`<p>${performanceLowImportanceHigh[i]}</p>`
                }
              commonhtml += `</div>
            </td>
            <td style="background-color: #e0e0e0!important; color: #000!important; height: 200px; border-top: 2px solid #1f91c0!important; border-right: 2px solid #1f91c0!important; width: 50%;">
              <div class="text-center">`
              for(let i = 0; i < performanceHighImportanceHigh.length; i++){
                commonhtml +=`<p>${performanceHighImportanceHigh[i]}</p>`
              }
              commonhtml +=`</div>
            </td>
          </tr>
          <tr>
            <td><p>Low</p></td>
            <td style="background-color: #bdbdbd!important; color: #000; height: 200px; border-bottom: 2px solid #1f91c0!important; border-left: 2px solid #1f91c0!important; width: 50%;">
              <div class="text-center">`
              for(let i = 0; i < performanceLowImportanceLow.length; i++){
                commonhtml +=`<p>${performanceLowImportanceLow[i]}</p>`
              }
              commonhtml +=`</div>  
            </td>
            <td style=" background-color: #9e9e9e!important; color: #000!important; height: 200px; border-bottom: 2px solid #1f91c0!important; border-right: 2px solid #1f91c0!important; width: 50%;">
              <div class="text-center">`
              for(let i = 0; i < performanceHighImportanceLow.length; i++){
                commonhtml +=`<p>${performanceHighImportanceLow[i]}</p>`
              }
              commonhtml +=`</div>
            </td>
          </tr>
          <tr>
            <td></td>
            <td class="text-center">Low</td>
            <td class="text-center">High</td>
          </tr>
        </table>
        <h4 style="color: #0C5A9E;" class="text-center mt-3"><b><i>${subSection2}</i></b></h4>
      </div>  
      </div>
      <hr class="mt-5" style="width: 100%"/>
      <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
      <textarea class="mt-3 mx-5" rows="7" cols="115" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${consultantReview}</textarea>
      <div style="page-break-before: always; margin-top: 20%"></div>
    `
  }
  for (let e = 0; e < apiData.sectionsProcess2.sections[0].executiveLevel.length; e++) {
    commonhtml += `
    <p class="mt-3 ml-5" style="color: #0C5A9E; font-size: 30px;"><b><i>2.3. Executive Level</i></b></p>
    <p class="mt-3 ml-5" style="color: #0C5A9E; font-size: 25px;"><b><i>${e+1}. ${apiData.sectionsProcess2.sections[0].executiveLevel[e].name}</i></b></p>`
    let topLevelQuestionsOfImportance = [];
    let managerialLevelQuestionsOfImportance = [];
    let executiveLevelQuestionsOfImportance = [];
    let subSection1, subSection2;
    let performanceHighImportanceLow = [];
    let performanceLowImportanceLow = [];
    let performanceHighImportanceHigh = [];
    let performanceLowImportanceHigh = [];
    let consultantReview;

    for(let subSec = 0; subSec < apiData.sectionsProcess2.sections[0].subSection.length; subSec++){
      if(apiData.sectionsProcess2.sections[0].subSection[subSec].sequence_number === 1){
        subSection1 = apiData.sectionsProcess2.sections[0].subSection[subSec].sub_section_name;
        for(let que = 0; que < apiData.sectionsProcess2.sections[0].subSection[subSec].questions.length; que++){
          if(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que].question_type === 6){
            topLevelQuestionsOfImportance.push(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que]);
          } else if(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que].question_type === 7){
            managerialLevelQuestionsOfImportance.push(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que]);
          } else if(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que].question_type === 8){
            executiveLevelQuestionsOfImportance.push(apiData.sectionsProcess2.sections[0].subSection[subSec].questions[que]);
          }
        }
      } else{
        subSection2 = apiData.sectionsProcess2.sections[0].subSection[subSec].sub_section_name;
      }
    }
    commonhtml += `
    <div class="row mt-5">
      <div class="col-md-2 mr-2">
        <h4 class="mr-5" style="transform: rotate(-90deg); color: #0C5A9E; margin-top: 100px; width: 170%;"><b><i>${subSection1}</i></b></h4>
      </div>`
      performanceHighImportanceLow = [];
      performanceLowImportanceLow = [];
      performanceHighImportanceHigh = [];
      performanceLowImportanceHigh = [];
    for (let q = 0; q < apiData.sectionsProcess2.sections[0].executiveLevel[e].questions.length; q++) {
      for(let el = 0; el < executiveLevelQuestionsOfImportance.length; el++){
        let reportanswers = await AnswersValues.query().where('question_id', apiData.sectionsProcess2.sections[0].executiveLevel[e].questions[q].id).andWhere('vga_audit_number', auditNo).andWhere('client_employee_id', apiData.sectionsProcess2.sections[0].executiveLevel[e].id).andWhere('is_deleted', false);
        if(consultantReview === null){
          consultantReview = ''
        } else{
          consultantReview = reportanswers[0]?.consultant_review === null ? '' : reportanswers[0]?.consultant_review;
        }
        let answerSum = 0;
        let average;
        if(reportanswers && reportanswers.length > 0){
          for(let ans = 0; ans < reportanswers.length; ans++){
            answerSum = answerSum + parseInt(reportanswers[ans].answer_value);
            average = answerSum / 4;
          }
          if(executiveLevelQuestionsOfImportance[el].question === apiData.sectionsProcess2.sections[0].executiveLevel[e].questions[q].question){
            if(average >= 3 && executiveLevelQuestionsOfImportance[el].reportanswers[0].question_average < 3){
              performanceHighImportanceLow.push(apiData.sectionsProcess2.sections[0].executiveLevel[e].questions[q].question);
            } else if(average < 3 && executiveLevelQuestionsOfImportance[el].reportanswers[0].question_average < 3){
              performanceLowImportanceLow.push(apiData.sectionsProcess2.sections[0].executiveLevel[e].questions[q].question);
            } else if(average >= 3 && executiveLevelQuestionsOfImportance[el].reportanswers[0].question_average >= 3){
              performanceHighImportanceHigh.push(executiveLevelQuestionsOfImportance[el].question);
            } else if(average < 3 && executiveLevelQuestionsOfImportance[el].reportanswers[0].question_average >= 3){
              performanceLowImportanceHigh.push(executiveLevelQuestionsOfImportance[el].question);
            }
          }
        } else {
          // average = 0;
        }
      } 
    }
    commonhtml += `
      <div class="col-md-8">
        <table>
          <tr>
            <td><p>High</p></td>
            <td style="background-color: #eee!important; color: #000!important; border-top: 2px solid #1f91c0!important; border-left: 2px solid #1f91c0!important; height: 200px; width: 50%;">
              <div class="text-center">`
                for(let i = 0; i < performanceLowImportanceHigh.length; i++){
                  commonhtml +=`<p>${performanceLowImportanceHigh[i]}</p>`
                }
              commonhtml += `</div>
            </td>
            <td style="background-color: #e0e0e0!important; color: #000!important; height: 200px; border-top: 2px solid #1f91c0!important; border-right: 2px solid #1f91c0!important; width: 50%;">
              <div class="text-center">`
              for(let i = 0; i < performanceHighImportanceHigh.length; i++){
                commonhtml +=`<p>${performanceHighImportanceHigh[i]}</p>`
              }
              commonhtml +=`</div>
            </td>
          </tr>
          <tr>
            <td><p>Low</p></td>
            <td style="background-color: #bdbdbd!important; color: #000; height: 200px; border-bottom: 2px solid #1f91c0!important; border-left: 2px solid #1f91c0!important; width: 50%;">
              <div class="text-center">`
              for(let i = 0; i < performanceLowImportanceLow.length; i++){
                commonhtml +=`<p>${performanceLowImportanceLow[i]}</p>`
              }
              commonhtml +=`</div>  
            </td>
            <td style=" background-color: #9e9e9e!important; color: #000!important; height: 200px; border-bottom: 2px solid #1f91c0!important; border-right: 2px solid #1f91c0!important; width: 50%;">
              <div class="text-center">`
              for(let i = 0; i < performanceHighImportanceLow.length; i++){
                commonhtml +=`<p>${performanceHighImportanceLow[i]}</p>`
              }
              commonhtml +=`</div>
            </td>
          </tr>
          <tr>
            <td></td>
            <td class="text-center">Low</td>
            <td class="text-center">High</td>
          </tr>
        </table>
        <h4 style="color: #0C5A9E;" class="text-center mt-3"><b><i>${subSection2}</i></b></h4>
      </div>  
      </div>
      <hr class="mt-5" style="width: 100%"/>
      <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
      <textarea class="mt-3 mx-5" rows="7" cols="115" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${consultantReview}</textarea>
      <div style="page-break-before: always; margin-top: 20%"></div>
    `
  }
  commonhtml += `
  </body>
  </html>`;
  fs.writeFileSync('src/uploads/print.html', commonhtml);
  return path.resolve('src/uploads/print.html');
}

const contentOfValueGrowthMatrixHtml = async (apiData) => {
  let consultantReview;
  commonhtml += `
  <html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  </head>
  <body>`;
  for (let i = 0; i < apiData.sectionsProcess.sections[0].questions.length; i++) {
    let no = 1;
    if(apiData.sectionsProcess.sections[0].questions[i].reportanswers.length === 0){
      for (let j = 0; j < apiData.sectionsProcess.sections[0].questions[i].subquestions.length; j++) {
        commonhtml += `
        <p style="color: #0C5A9E; text-align: center; font-size: 20px;">${no}. ${apiData.sectionsProcess.sections[0].questions[i].subquestions[j].title}</p>
        <div class="container mt-5">
          <table>
            <tbody>
              <tr style="height: 70px; border: 1px solid #0C5A9E;">
                <th class="text-center" colspan=2 style="background-color: #0C5A9E; color: white;">
                  <b>PROFIT / GROWTH MAXIMIZATION</b>
                </th>
              </tr>
              <tr style="height: 30vh;">
                <td class="text-center" width="25%" style="border: 1px solid #0C5A9E;">
                  <div class="rotate-div">
                    <h5 style="color:#0C5A9E; width:200px; margin-top:50px; margin-left:50px; transform: rotate(-90deg);"><b><i>VALUE / WEALTH MAXIMIZATION</i></b></h5>
                  </div>
                </td>
                <td width="75%" style="border: 1px solid #0C5A9E;">
                  <table class="table table-responsive">
                    <tbody>
                      <tr>
                        <td class="text-center" colspan="4">
                          <b>${apiData.sectionsProcess.sections[0].questions[i].subquestions[j].title}</b>
                        </td>
                      </tr> 
                      <tr>
                        <td rowspan="2" style="width: 60px;"><b>${apiData.sectionsProcess.sections[0].questions[i].subquestions[j].title}</b></td>
                        <td class="text-center" style="padding: 50px; border: 1px solid #0C5A9E;"><b>Status</b></td>
                        <td class="text-center" style="padding: 50px; border: 1px solid #0C5A9E;"><b>Score</b></td>
                        <td class="text-center" style="padding: 50px; border: 1px solid #0C5A9E;"><b>Synergy</b></td>
                      </tr>
                      <tr style="height: 10vh;" style="border: 1px solid #0C5A9E;">
                        <td style="border: 1px solid #0C5A9E;"></td>
                        <td style="border: 1px solid #0C5A9E;"></td>
                        <td style="border: 1px solid #0C5A9E;"></td>
                      </tr>
                      </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>`
            if(apiData.sectionsProcess.sections[0].questions[i].reportanswers.length > 0) {
              if(consultantReview === null){
                consultantReview = ''
              } else{
                consultantReview = apiData.sectionsProcess.sections[0].questions[i].reportanswers[0]?.consultant_review === null ? '' : apiData.sectionsProcess.sections[0].questions[i].reportanswers[0]?.consultant_review;
              }
            }
            commonhtml += `<hr class="mt-5" style="width: 100%"/>
          <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
          <textarea class="mt-3" rows="10" cols="105" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%; margin-left: 9%;"> ${consultantReview} </textarea>
        <div style="page-break-before: always; margin-top: 20%"></div>
      </div>`
      no++;          
      }
    } else {
      for (let k = 0; k < apiData.sectionsProcess.sections[0].questions[i].reportanswers.length; k++) {
        commonhtml += `
        <p style="color: #0C5A9E; text-align: center; font-size: 25px;"><b><i>${no}. ${apiData.sectionsProcess.sections[0].questions[i].reportanswers[k].title}</i></b></p>
        <div class="container mt-5" style="margin-left: 9%;">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <table>
                  <tbody>
                    <tr style="height: 70px;" style="border: 1px solid #0C5A9E;">
                      <th class="text-center" colspan="2" style="background-color: #0C5A9E; color: white;">
                        <b>PROFIT / GROWTH MAXIMIZATION</b>
                      </th>
                    </tr>
                    <tr style="height: 30vh;">
                      <td class="text-center" width="25%" style="border: 1px solid #0C5A9E;">
                        <div class="rotate-div">
                          <h5 style="color:#0C5A9E; width:200px; margin-top:50px; margin-left:50px; transform: rotate(-90deg);"><b><i>VALUE / WEALTH MAXIMIZATION</i></b></h5>
                        </div>
                      </td>
                      <td width="75%" style="border: 1px solid #0C5A9E;">
                        <table class="table table-responsive">
                          <tbody>
                            <tr>
                              <td class="text-center" colspan="4">
                                <b>${apiData.sectionsProcess.sections[0].questions[i].reportanswers[k].title}</b>
                              </td>
                            </tr> 
                            <tr>
                              <td class="" rowspan="2" style="width: 150px; border: 1px solid #0C5A9E; padding: 30px;
                              overflow: auto;"><br/><br/><br/><br/><b>${apiData.sectionsProcess.sections[0].questions[i].reportanswers[k].title}</b></td>
                              <td class="text-center" style="padding: 50px; border: 1px solid #0C5A9E;"><b>Status</b></td>
                              <td class="text-center" style="padding: 50px; border: 1px solid #0C5A9E;"><b>Score</b></td>
                              <td class="text-center" style="padding: 50px; border: 1px solid #0C5A9E;"><b>Synergy</b></td>
                            </tr>
                            <tr style="height: 10vh;">
                              <td class="text-center" style="padding: 50px; border: 1px solid #0C5A9E;">${apiData.sectionsProcess.sections[0].questions[i].reportanswers[k].answer_value}</td>
                              <td class="text-center" style="padding: 50px; border: 1px solid #0C5A9E;">${apiData.sectionsProcess.sections[0].questions[i].reportanswers[k].answer_value1}</td>
                              <td class="text-center" style="padding: 50px; border: 1px solid #0C5A9E;">${apiData.sectionsProcess.sections[0].questions[i].reportanswers[k].answer_value2}</td>
                            </tr>
                            </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>`
            if(apiData.sectionsProcess.sections[0].questions[i].reportanswers.length > 0) {
              if(consultantReview === null){
                consultantReview = ''
              } else{
                consultantReview = apiData.sectionsProcess.sections[0].questions[i].reportanswers[k].consultant_review === null ? '' : apiData.sectionsProcess.sections[0].questions[i].reportanswers[k].consultant_review;
              }
            }
            commonhtml += `<hr class="mt-5" style="width: 100%"/>
          <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
          <textarea class="mt-3" rows="10" cols="105" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;margin-left: 9%;"> ${consultantReview}</textarea>
        <div style="page-break-before: always; margin-top: 20%"></div>
      </div>`    
      no++;      
      }
    }                                     
  }
  commonhtml += `
  </body>
  </html>`;
  fs.writeFileSync('src/uploads/print.html', commonhtml);
  return path.resolve('src/uploads/print.html');
}

const removeDuplications = (data) => {
  console.log(data)
  return data.filter((value, index) => data.indexOf(value) === index);
}

const contentOfPerformanceEvaluationHtml = async (apiData, auditNo) => {
  let questionId = employeePerformanceQuestionsEnum.SELF;
  const employeePerformanceUsers = await empPerformanceUsersDetails.query().whereNotNull('question_average').where('is_deleted',false).andWhere('question_id', employeePerformanceQuestionsEnum.MANAGER).andWhere('vga_audit_number', auditNo).withGraphFetched('[employeePerformance,clientEmployee]').orderBy('employee_id');
  const employeePerformanceQuestions = await empPerformanceQuestions.query().where('is_deleted',false).andWhere('question_id', questionId).orderBy('sequence_number');

  commonhtml += `
  <html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  </head>
  <body>`;
    if(apiData.sectionsProcess.sections[0].questions[0].question_type === 4 && apiData.sectionsProcess.sections[0].questions[0].sequence_number === 1){
      commonhtml += `
      <p class="mt-3 ml-5" style="color: #0C5A9E; font-size: 30px;"><b><i>${apiData.sectionsProcess.sections[0].questions[0].sequence_number}. ${apiData.sectionsProcess.sections[0].questions[0].question}</i></b></p>
      <div class="container mt-5">`
        const individualPerformanceSelf = employeePerformanceQuestionsEnum.SELF;
        const individualPerformanceMAnager = employeePerformanceQuestionsEnum.MANAGER;
        let individualPerformanceBySelf;
        let individualPerformanceByManager;    
        let clientEmployees = [];
        let clientEmployee;
        let employeeCounter = 0;
        let employeePerformanceCounter = 0;

        for(let i = 0; i < employeePerformanceUsers.length; i++){
          individualPerformanceBySelf = await empPerformanceUsersDetails.query().where('is_deleted', false).withGraphFetched('[employeePerformance,clientEmployee]').andWhere('question_id', individualPerformanceSelf).andWhere('vga_audit_number', auditNo);
          individualPerformanceByManager = await empPerformanceUsersDetails.query().where('is_deleted', false).withGraphFetched('[employeePerformance,clientEmployee]').andWhere('question_id', individualPerformanceMAnager).andWhere('vga_audit_number', auditNo);
          if(employeePerformanceUsers[i].question_average !== null){
            clientEmployees.push(employeePerformanceUsers[i].clientEmployee);
            clientEmployee = await(removeDuplications(clientEmployees));
          }
        }  
        for(let i = 0; i < clientEmployee.length; i++){
          employeeCounter++;
          employeePerformanceCounter = 0;
          commonhtml += `
          <div class="row" style="margin-top: 20px;">
            <h5 style="color: #0C5A9E; font-size: 30px;">${employeeCounter}. ${clientEmployee[i]?.name}</h5>
          </div>`
          for(let j = 0; j < employeePerformanceQuestions.length; j++){
            employeePerformanceCounter++;
            commonhtml +=`<div class="row">
              <h5 style="color: #0C5A9E; font-size: 30px;">${employeeCounter}.${employeePerformanceCounter}. ${employeePerformanceQuestions[j].name}</h5>
            </div>
            <div class="row">
              <div class="col-md-1">
                <h5 style="color: #0C5A9E; width: 300px; margin-top: 350px; margin-left: -50px; transform: rotate(-90deg);"><b><i>Individual Performance By Self</i></b></h5>
              </div>
              <div class="col-md-11">
              <table class="mt-3" style="text-align: center;margin-left: 5%;">
              <tr> 
                <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                  <b>Exceptional</b>
                </td>`
              for(let l = 0; l < individualPerformanceByManager.length; l++){
                if(clientEmployee[i]?.id === individualPerformanceByManager[l].employee_id && employeePerformanceQuestions[j]?.name === individualPerformanceByManager[l]?.employeePerformance.name){
                  const averageOfManager = individualPerformanceByManager[l].question_average;
                  for(let k = 0; k < individualPerformanceBySelf.length; k++){
                    if(individualPerformanceBySelf[k]?.clientEmployee.id === clientEmployee[i]?.id && individualPerformanceBySelf[k]?.employeePerformance.name === employeePerformanceQuestions[j]?.name){
                      const averageOfQuestion = individualPerformanceBySelf[k].question_average;
                      if (averageOfQuestion === 1 && averageOfManager === 4) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 1 && averageOfManager === 3) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 1 && averageOfManager === 2) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${totalAverage} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 1 && averageOfManager === 1) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      commonhtml += `
                    </tr>
                    <tr> 
                      <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                        <b>Satisfactory</b>
                      </td>`
                      if (averageOfQuestion === 2 && averageOfManager === 4) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 2 && averageOfManager === 3) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 2 && averageOfManager === 2) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 2 && averageOfManager === 1) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      commonhtml += `
                    </tr>
                    <tr> 
                      <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                        <b>Marginal</b>
                      </td>`
                      if (averageOfQuestion === 3 && averageOfManager === 4) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 3 && averageOfManager === 3) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 3 && averageOfManager === 2) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 3 && averageOfManager === 1) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      commonhtml += `
                    </tr>
                    <tr> 
                      <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                        <b>Unsatisfactory</b>
                      </td>`
                      if (averageOfQuestion === 4 && averageOfManager === 4) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 4 && averageOfManager === 3) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 4 && averageOfManager === 2) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      if (averageOfQuestion === 4 && averageOfManager === 1) {
                        commonhtml += `
                        <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                          <b>${averageOfQuestion}  /  ${averageOfManager} </b>
                        </td>`
                      } else{
                        commonhtml +=`
                        <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                      }
                      commonhtml += `
                    </tr>
                    <tr> 
                      <td class="border border-white" style="color: white; background-color: #0C5A9E;padding:71; width:25%"></td>
                      <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                        <b>Unsatisfactory</b>
                      </td>
                      <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                        <b>Marginal</b>
                      </td>
                      <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                        <b>Satisfactory</b>
                      </td>
                      <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                        <b>Exceptional</b>
                      </td>
                  </tr>
                </table>
              </div>
            </div>
            <h4 style="color:#0C5A9E; text-align: center; margin-left: 15%;"><b><i>Individual Performance By Manager</i></b></h4>`
            commonhtml += `<hr class="mt-5" style="width: 100%"/>
            <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
            <textarea class="mt-3" rows="5" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${individualPerformanceByManager[l].consultant_review === null ? '' : individualPerformanceByManager[l].consultant_review}</textarea>
            <div style="page-break-before: always; margin-top: 5%"></div>`
                  }
                }
              }
            }
          }
        }
      commonhtml += `</div>`
    }
    for (let i = 0; i < apiData.sectionsProcess.sections[0].questions.length; i++) {
      let consultantReview;
      if(apiData.sectionsProcess.sections[0].questions[i].question_type === 5 && apiData.sectionsProcess.sections[0].questions[i].reportanswers[0].
        vga_audit_number === auditNo){
        commonhtml += `
        <p class="mt-3 ml-5" style="color: #0C5A9E; font-size: 30px;"><b><i>${apiData.sectionsProcess.sections[0].questions[i].sequence_number}. ${apiData.sectionsProcess.sections[0].questions[i].question}</i></b></p>
        <div class="container mt-5" style="height: 120%;">
          <div class="row">
            <div class="col-md-1">
              <h5 style="color:#0C5A9E; width:150px; margin-top:200px; margin-left:-10px; transform: rotate(-90deg);"><b><i>${apiData.sectionsProcess.sections[0].questions[i].question}</i></b></h5>
            </div>`
            commonhtml += `
            <div class="col-md-11">
              <table style="text-align: center;margin-left: 5%;">
                <tr> 
                  <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                    <b>Exceptional</b>
                  </td>`
                  const averageOfQuestion = apiData.sectionsProcess.sections[0].questions[i].reportanswers[0].question_average;
                  const totalAverage = apiData.sectionsProcess.sections[0].questions[i].reportanswers[0].total_average;
                  if (averageOfQuestion === 1 && totalAverage === 4) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 1 && totalAverage === 3) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 1 && totalAverage === 2) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 1 && totalAverage === 1) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  commonhtml += `
                </tr>
                <tr> 
                  <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                    <b>Satisfactory</b>
                  </td>`
                  if (averageOfQuestion === 2 && totalAverage === 4) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 2 && totalAverage === 3) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 2 && totalAverage === 2) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 2 && totalAverage === 1) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  commonhtml += `
                </tr>
                <tr> 
                  <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                    <b>Marginal</b>
                  </td>`
                  if (averageOfQuestion === 3 && totalAverage === 4) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 3 && totalAverage === 3) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 3 && totalAverage === 2) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 3 && totalAverage === 1) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  commonhtml += `
                </tr>
                <tr> 
                  <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                    <b>Unsatisfactory</b>
                  </td>`
                  if (averageOfQuestion === 4 && totalAverage === 4) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 4 && totalAverage === 3) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 4 && totalAverage === 2) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  if (averageOfQuestion === 4 && totalAverage === 1) {
                    commonhtml += `
                    <td class="border border-white" style="background-color: #A9A9A9; padding:50; width:25%">
                      <b>${averageOfQuestion}  /  ${totalAverage} </b>
                    </td>`
                  } else{
                    commonhtml +=`
                    <td class="border border-white" style="background-color: #D2D6DE; padding:71; width:25%"></td>`
                  }
                  commonhtml += `
                </tr>
                <tr> 
                  <td class="border border-white" style="color: white; background-color: #0C5A9E;padding:71; width:25%"></td>
                  <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                    <b>Unsatisfactory</b>
                  </td>
                  <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                    <b>Marginal</b>
                  </td>
                  <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                    <b>Satisfactory</b>
                  </td>
                  <td class="border border-white" style="color: white; background-color: #0C5A9E; width:25%">
                    <b>Exceptional</b>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <h4 style="color:#0C5A9E; text-align: center; margin-left: 30%;"><b><i>ORGANIZATION'S OVERALL PERFORMANCE</i></b></h4>`
          if(apiData.sectionsProcess.sections[0].questions[i].reportanswers.length > 0) {
            if(consultantReview === null){
              consultantReview = ''
            } else{
              consultantReview = apiData.sectionsProcess.sections[0].questions[i].reportanswers[0].consultant_review === null ? '' : apiData.sectionsProcess.sections[0].questions[i].reportanswers[0].consultant_review;
            }
          }
          commonhtml += `<hr class="mt-5" style="width: 100%"/>
          <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
          <textarea class="mt-3" rows="10" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;"> ${consultantReview}</textarea>
        </div>
        <div style="page-break-before: always; margin-top: -120%"></div>`
      }
    }
  // }
  commonhtml += `
  </body>
  </html>`
  fs.writeFileSync('src/uploads/print.html', commonhtml);
  return path.resolve('src/uploads/print.html');
}

const contentOfPlanOfActionsAndRecommendations = async (apiData, auditNo, clientId) => {

  commonhtml += `
  <html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  </head>
  <body>`;

    let answersOfPlanOfAction = await planOfActionDetails.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('vga_audit_number', auditNo).andWhere('question_id', questionsOfPlanOfActions.Strategic_Sphere).first();
    if(answersOfPlanOfAction.question_id === questionsOfPlanOfActions.Strategic_Sphere && answersOfPlanOfAction.sub_section_id === apiData.sectionsProcess.sections[0].subSection[0].id){
      commonhtml += `
      <div class="container mt-5" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1. ${apiData.sectionsProcess.sections[0].subSection[0].sub_section_name}</b></p>
        <p style="color: #0C5A9E; font-size: 28px;"><b>1.1. Strategic Sphere</b></p>
        <p class="text-center mt-5" style="font-size: 25px"><b>A. Strategic Sphere</b></p>
        <div class="row mx-5">
          <table style="border: 1px solid #0C5A9E; width:100%;">
            <thead>
              <tr style="background-color: #0C5A9E; color: white;">
                <th class="p-3" style="width: 30%; border: 1px solid white;">Zone of the Sphere</th>
                <th class="p-3" style="border: 1px solid white;">Strategic Intent</th>
              </tr>
              <tr style="background-color:#727375; color: white;">
                <td class="p-3" style="border: 1px solid white;">Core</td>
                <td class="p-3" style="border: 1px solid white;">${answersOfPlanOfAction.answer_value.strategicSphere1.core}</td>
              </tr>
              <tr style="background-color:#888888; color: white;">
                <td class="p-3" style="border: 1px solid white;">Vital Interests</td>
                <td class="p-3" style="border: 1px solid white;">${answersOfPlanOfAction.answer_value.strategicSphere1.vitalInterests}</td>
              </tr>
              <tr style="background-color:#727375; color: white;">
                <td class="p-3" style="border: 1px solid white;">Buffer Zones</td>
                <td class="p-3" style="border: 1px solid white;">${answersOfPlanOfAction.answer_value.strategicSphere1.bufferZones}</td>
              </tr>
              <tr style="background-color:#888888; color: white;">
                <td class="p-3" style="border: 1px solid white;">Pivotal Zones</td>
                <td class="p-3" style="border: 1px solid white;">${answersOfPlanOfAction.answer_value.strategicSphere1.pivotalZones}</td>
              </tr>
              <tr style="background-color:#727375; color: white;">
                <td class="p-3" style="border: 1px solid white;">Forward Positions</td>
                <td class="p-3" style="border: 1px solid white;">${answersOfPlanOfAction.answer_value.strategicSphere1.forwardPositions}</td>
              </tr>
              <tr style="background-color:#888888; color: white;">
                <td class="p-3" style="border: 1px solid white;">Power Vacuums</td>
                <td class="p-3" style="border: 1px solid white;">${answersOfPlanOfAction.answer_value.strategicSphere1.powerVaccums}</td>
              </tr>
            </thead>
          </table>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfPlanOfAction.consultant_review !== null ? answersOfPlanOfAction.consultant_review.A : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>

      <div class="container mt-5" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1.1. Strategic Sphere</b></p>
        <p class="text-center mt-5" style="font-size: 25px"><b>B. Strategic Sphere</b></p>
        <img alt="img" src=${path.resolve('src/uploads/images/StrategicSphere1.png')}
          style="align-content: center; margin-left: 150px; width:812px; height: 512px;">
        <div style="position: absolute; margin-left: 351px; margin-top: -425px; border: none;">
          <textarea rows="2" style="width:120px; color: black;background: #fff; border: none; text-align: center;">${answersOfPlanOfAction.answer_value.strategicSphere2.textbox1}</textarea>
        </div>
        <div style="position: absolute; margin-left: 211px; margin-top: -279px;">
          <textarea rows="2" style="width:116px; color: black;background: #919191; border: none; text-align: center;">${answersOfPlanOfAction.answer_value.strategicSphere2.textbox2}</textarea>
        </div>
        <div style="position: absolute; margin-left: 500px; margin-top: -279px;">
          <textarea rows="2" style="width:115px; color: black;background: #919191; border: none; text-align: center;">${answersOfPlanOfAction.answer_value.strategicSphere2.textbox3}</textarea>
        </div>
        <div style="position: absolute; margin-left: 354px; margin-top: -138px;">
          <textarea rows="2" style="width:120px; color: black;background: #fff; border: none; text-align: center;">${answersOfPlanOfAction.answer_value.strategicSphere2.textbox4}</textarea>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfPlanOfAction.consultant_review !== null ? answersOfPlanOfAction.consultant_review.B : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>

      <div class="container mt-5" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1.1. Strategic Sphere</b></p>
        <p class="text-center mt-5" style="font-size: 25px"><b>C. Strategic Sphere</b></p>
        <div class="row mt-3">
          <div class="col-md-3">
            <p style="font-size: 20px;">Initiatives</p>
          </div>
          <div class="col-md-9">
            <p class="text-center" style="font-size: 20px;">Operating priorities</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <p style="background-color: #0C5A9E; color: white; border-radius: 10px; padding: 15%;">${answersOfPlanOfAction.answer_value.strategicSphere3.initiative1.txtIntiative1}</p>
          </div>
          <div class="col-md-9">
            <p style="background-color: #D2D6DE; border-radius: 10px; padding:1%;">${answersOfPlanOfAction.answer_value.strategicSphere3.initiative1.txtOperatingProperties1}</p>
            <p class="mt-n5" style="background-color: #D2D6DE; border-radius: 10px; padding:1%;">${answersOfPlanOfAction.answer_value.strategicSphere3.initiative1.txtOperatingProperties2}</p>
          </div>
        </div>
        <div class="row" mt-1>
          <div class="col-md-3">
            <p style="background-color: #0C5A9E; color: white; border-radius: 10px; padding: 15%;">${answersOfPlanOfAction.answer_value.strategicSphere3.initiative2.txtIntiative2}</p>
          </div>
          <div class="col-md-9">
            <p style="background-color: #D2D6DE; border-radius: 10px; padding:1%;">${answersOfPlanOfAction.answer_value.strategicSphere3.initiative2.txtOperatingProperties3}</p>
            <p class="mt-n5" style="background-color: #D2D6DE; border-radius: 10px; padding:1%;">${answersOfPlanOfAction.answer_value.strategicSphere3.initiative2.txtOperatingProperties4}</p>
          </div>
        </div>
        <div class="row mt-1">
          <div class="col-md-3">
            <p style="background-color: #0C5A9E; color: white; border-radius: 10px; padding: 15%;">${answersOfPlanOfAction.answer_value.strategicSphere3.initiative3.txtIntiative3}</p>
          </div>
          <div class="col-md-9">
            <p style="background-color: #D2D6DE; border-radius: 10px; padding:1%;">${answersOfPlanOfAction.answer_value.strategicSphere3.initiative3.txtOperatingProperties5}</p>
            <p class="mt-n5" style="background-color: #D2D6DE; border-radius: 10px; padding:1%;">${answersOfPlanOfAction.answer_value.strategicSphere3.initiative3.txtOperatingProperties6}</p>
          </div>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfPlanOfAction.consultant_review !== null ? answersOfPlanOfAction.consultant_review.C : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>`
    }

    // Definite Direction
    let answersOfDefiniteDirection = await planOfActionDetails.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('vga_audit_number', auditNo).andWhere('question_id', questionsOfPlanOfActions.Definite_Direction).first();
    if(answersOfDefiniteDirection.question_id === questionsOfPlanOfActions.Definite_Direction && answersOfDefiniteDirection.sub_section_id === apiData.sectionsProcess.sections[0].subSection[0].id){
      commonhtml += `
      <div class="container mt-5" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1.2. Definite Direction</b></p>
        <p class="text-center mt-5" style="font-size: 25px"><b>A. Definite Direction</b></p>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="row mt-1">
              <div class="col-md-3">
                <p style="background-color: #0C5A9E; color: white; border-radius: 10px; padding:7%; height: 100px;">Policies and Governance</p>
              </div>
              <div class="col-md-3">
                <p style="background-color: #D2D6DE; border-radius: 10px; padding: 7%; height: 100px;">${answersOfDefiniteDirection.answer_value.definiteDirection1.policies.policies1}</p>
              </div>
              <div class="col-md-3">
                <p style="background-color: #D2D6DE; border-radius: 10px; padding: 7%; height: 100px;">${answersOfDefiniteDirection.answer_value.definiteDirection1.policies.policies2}</p>
              </div>
              <div class="col-md-3">
                <p style="background-color: #D2D6DE; border-radius: 10px; padding: 7%; height: 100px;">${answersOfDefiniteDirection.answer_value.definiteDirection1.policies.policies3}</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="row mt-1">
              <div class="col-md-3">
                <p style="background-color: #0C5A9E; color: white; border-radius: 10px; padding:7%; height: 100px;">Demand Msanagement</p>
              </div>
              <div class="col-md-3">
                <p style="background-color: #D2D6DE; border-radius: 10px; padding: 7%; height: 100px;">${answersOfDefiniteDirection.answer_value.definiteDirection1.demands.demand1}</p>
              </div>
              <div class="col-md-3">
                <p style="background-color: #D2D6DE; border-radius: 10px; padding: 7%; height: 100px;">${answersOfDefiniteDirection.answer_value.definiteDirection1.demands.demand2}</p>
              </div>
              <div class="col-md-3">
                <p style="background-color: #D2D6DE; border-radius: 10px; padding: 7%; height: 100px;">${answersOfDefiniteDirection.answer_value.definiteDirection1.demands.demand3}</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="row mt-1">
              <div class="col-md-3">
                <p style="background-color: #0C5A9E; color: white; border-radius: 10px; padding:7%; height: 100px;">Supply Management</p>
              </div>
              <div class="col-md-3">
                <p style="background-color: #D2D6DE; border-radius: 10px; padding: 7%; height: 100px;">${answersOfDefiniteDirection.answer_value.definiteDirection1.supply.supply1}</p>
              </div>
              <div class="col-md-3">
                <p style="background-color: #D2D6DE; border-radius: 10px; padding: 7%; height: 100px;">${answersOfDefiniteDirection.answer_value.definiteDirection1.supply.supply2}</p>
              </div>
              <div class="col-md-3">
                <p style="background-color: #D2D6DE; border-radius: 10px; padding: 7%; height: 100px;">${answersOfDefiniteDirection.answer_value.definiteDirection1.supply.supply3}</p>
              </div>
          </div>
          </div>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfDefiniteDirection.consultant_review !== null ? answersOfDefiniteDirection.consultant_review.A : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>
      
      <div class="container mt-5" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1.2. Definite Direction</b></p>
        <p class="text-center" style="font-size: 25px;"><b>B. Definite Direction</b></p>
        <div class="row">
          <div class="col-md-12">
            <p class="ml-5 mt-3" style="font-size: 25px;">Strategic Decisions</p>
            <table>
              <tr>
                <td style="padding: 45px; background-color: #848585; color: white; border-radius: 15px; border: none; text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection2.strategicDesicion1}</td>
                <td><span style="font-size:60px;color: #848585;">→</span></td>
                <td rowspan="10" style=" border: none;"><img src=${path.resolve('src/uploads/images/download-img-arrow.png')} style="height:425px; width: 100%;"/></td>
              </tr>
              <br>
              <tr>
                <td rowspan="2" style="padding: 30px; background-color: #848585; color: white; border-radius: 15px; border: none; text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection2.strategicDesicion2}</td>
                <td><span style="font-size:60px;color: #848585;">→</span></td>
              </tr>
              <tr>
                <td><span style="font-size:60px;color: #848585;">←</span></td>
              </tr>
              <br>
              <tr>
                <td rowspan="2" style="padding: 30px; background-color: #848585;color: white; border-radius: 15px; border: none; text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection2.strategicDesicion3}</td>
                <td><span style="font-size:60px;color: #848585;">→</span></td>
              </tr>
              <tr>
                <td><span style="font-size:60px;color: #848585;">←</span></td>
              </tr>
              <br>
              <tr>
                <td rowspan="2" style="padding: 30px; background-color: #848585;color: white; border-radius: 15px; border: none; text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection2.strategicDesicion4}</td>
                <td><span style="font-size:60px;color: #848585;">→</span></td>
              </tr>
              <tr>
                <td><span style="font-size:60px;color: #848585;">←</span></td>
              </tr>
            </table>
            
          </div>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfDefiniteDirection.consultant_review !== null ? answersOfDefiniteDirection.consultant_review.B : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>
      
      <div class="container mt-5" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1.3. Definite Direction</b></p>
        <p class="text-center mt-5" style="font-size: 25px;"><b>C. Definite Direction</b></p>
        <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12">
          <div class="row mx-5 mt-3 p-2">
            <div class="col-lg-3 col-md-3 p-3" style="height: 100%; background-color: #d1cfcf; border-radius: 20px;">
              <span>We define our values through the acronym
              </span>
            </div>
            <div class="col-lg-1 col-md-1 p-3" style="height: 100%; background:#007ec2; border-radius: 20px; text-align: center;">
              <p style="margin: 24px 0px 0px 0px; color: white; text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection3.acronym1}</p>
            </div>
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-1 col-md-1 p-3" style="height: 100%; background:#007ec2; border-radius: 20px; text-align: center;">
              <p style="margin: 24px 0px 0px 0px; color: white; text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection3.acronym2}</p>
            </div>
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-1 col-md-1 p-3" style="height: 100%; background:#007ec2; border-radius: 20px; text-align: center;">
              <p style="margin: 24px 0px 0px 0px; color: white; text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection3.acronym3}</p>
            </div>
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-1 col-md-1 p-3" style="height: 100%; background:#007ec2; border-radius: 20px; text-align: center;">
              <p style="margin: 24px 0px 0px 0px; color: white; text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection3.acronym4}</p>
            </div>
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-1 col-md-1 p-3" style="height: 100%; background:#007ec2; border-radius: 20px; text-align: center;">
              <p style="margin: 24px 0px 0px 0px; color: white; text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection3.acronym5}</p>
            </div>
          </div>
          <div style="padding: 2px;"></div>
          <div class="row mx-5 p-2">
            <div class="col-lg-6 col-md-6 p-3" style="background-color: #7c7c7d; border-radius: 18px; text-align: center;">
              <h6 style="color: white;">OUR COMMITMENT TO STAKEHOLDERS</h6>
              <span style="color: white;">${answersOfDefiniteDirection.answer_value.definiteDirection3.commitmentTostackholders}</span>
            </div>
            <div class="col-lg-6 col-md-6 p-3" style="background-color: #7c7c7d; border-radius: 18px; text-align: center;">
              <h6 style="color: white;">OUR PLEDGE TO OUR CUSTOMERS</h6>
              <span style="color: white;">${answersOfDefiniteDirection.answer_value.definiteDirection3.pledgeToCustomer}</span>
            </div>
          </div>
          <div style="padding: 2px;"></div>
          <div class="row mx-5 p-2" style="background-color: #d1cfcf; padding: 20px; border-radius: 10px;">
            <div class="col-lg-4 col-md-4 p-3" style="background:#007ec2;border-radius: 18px;">
              <p style="color: white; text-align: center;">OUR MISSION</p>
            </div>
            <div class="col-lg-8 col-md-8 p-3">
              <p style="text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection3.ourMission}</p>
            </div>
          </div>
          <div style="padding: 2px;"></div>
          <div class="row mx-5 p-2" style="background-color: #d1cfcf; padding: 20px; border-radius: 10px;">
            <div class="col-lg-4 col-md-4 p-3" style="background:#007ec2;border-radius: 18px;">
              <p style="color: white; text-align: center;">OUR VISION</p>
            </div>
            <div class="col-lg-8 col-md-8 p-3">
              <p style="text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection3.ourVision}</p>
            </div>
          </div>
          <div style="padding: 2px;"></div>
          <div class="row mx-5 p-2" style="background-color: #d1cfcf; padding: 20px; border-radius: 10px;">
            <div class="col-lg-12 col-md-12" style="text-align: center; color: #007ec2; border-radius: 15px">
              VISION
              <p style="color: #007ec2">${answersOfDefiniteDirection.answer_value.definiteDirection3.year}</p>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12" style="color: #7c7c7d; border-radius: 15px">
                <p style="text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection3.vision}</p>
              </div>
            </div>
          </div>
          <div style="padding: 2px;"></div>
          <div class="row mx-5 p-2" style="background-color: #d1cfcf; padding: 20px; border-radius: 10px;">
            <div class="col-lg-4 col-md-4 p-3" style="background:#007ec2;border-radius: 18px;">
              <p style="color: white; text-align: center;">OUR STRATEGIC PROPOSITION</p>
            </div>
            <div class="col-lg-8 col-md-8 p-3">
              <p style="text-align: center;">${answersOfDefiniteDirection.answer_value.definiteDirection3.strategicProposition}</p>
            </div>
          </div>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfDefiniteDirection.consultant_review !== null ? answersOfDefiniteDirection.consultant_review.C : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>`
    }

    // Concrete Commencement
    let answersOfConcreteCommencement = await planOfActionDetails.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('vga_audit_number', auditNo).andWhere('question_id', questionsOfPlanOfActions.Concrete_Commencement).first();
    if(answersOfConcreteCommencement.question_id === questionsOfPlanOfActions.Concrete_Commencement && answersOfConcreteCommencement.sub_section_id === apiData.sectionsProcess.sections[0].subSection[0].id){
      commonhtml += `
      <div class="container mt-5" style="height: 120%;">
      <p style="color: #0C5A9E; font-size: 30px;"><b>1.3. Concrete Commencement</b></p>
      <p class="text-center mt-5" style="font-size: 25px"><b>A. Concrete Commencement</b></p>
      <div class="d-grid justify-content-center align-items-center" style="margin-left: 25%; width: 50%;">
        <div class="col-md-3"></div>
        <div class="col-md-8 justify-content-center" style="display: contents;">
          <div class="row mt-3">
            <div class="col-md-4">
              <div style="background-color: #d2d2d2; height: 135px; width: 140px; padding: 5px; color: white; border-radius: 15px;">
                <div style="border-radius: 10px; height: 125px; width: 130px; background-color: #125DA4; position: relative;">
                  <div style="border-radius: 10px; height: 80px; width: 110px; margin-top: 4px; margin-left: 1px; background-color: #C6C7C9; position: absolute; transform: translate(8%, 3%);"><p style="color:black; padding: 4px;">${answersOfConcreteCommencement.answer_value.concreteCommecement1.txtfocus1}</p></div>
                </div>
              </div>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-4">
              <div style="background-color: #d2d2d2; height: 135px; width: 140px; padding: 5px; color: white; border-radius: 15px;">
                <div style="border-radius: 10px; height: 125px; width: 130px; background-color: #125DA4; position: relative;">
                  <div style="border-radius: 10px; height: 80px; width: 110px; margin-top: 4px; margin-left: 1px; background-color: #C6C7C9; position: absolute; transform: translate(8%, 3%);"><p style="color:black; padding: 4px;">${answersOfConcreteCommencement.answer_value.concreteCommecement1.txtfocus4}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row" style="margin-left: 25%;">
          <div class="col-md-7 d-flex justify-content-center">
            <div style="border-radius: 50%; display: inline-block; width: 130px; height: 130px; background-color: #C6C7C9;"><p style="padding: 5px; margin-top:30px; margin-left: 15px;">Vision and Strategy</p></div>
          </div>
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-8 justify-content-center" style="display: contents;">
          <div class="row">
            <div class="col-md-4">
              <div style="background-color: #d2d2d2; height: 135px; width: 140px; padding: 5px; color: white; border-radius: 15px;">
                <div style="border-radius: 10px; height: 125px; width: 130px; background-color: #125DA4; position: relative;">
                  <div style="border-radius: 10px; height: 80px; width: 110px; margin-top: 4px; margin-left: 1px; background-color: #C6C7C9; position: absolute; transform: translate(8%, 3%);"><p style="color:black; padding: 4px;">${answersOfConcreteCommencement.answer_value.concreteCommecement1.txtfocus3}</p></div>
                </div>
              </div>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-4">
              <div style="background-color: #d2d2d2; height: 135px; width: 140px; padding: 5px; color: white; border-radius: 15px;">
                <div style="border-radius: 10px; height: 125px; width: 130px; background-color: #125DA4; position: relative;">
                  <div style="border-radius: 10px; height: 80px; width: 110px; margin-top: 4px; margin-left: 1px; background-color: #C6C7C9; position: absolute; transform: translate(8%, 3%);"><p style="color:black; padding: 4px;">${answersOfConcreteCommencement.answer_value.concreteCommecement1.txtfocus2}</p></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div><br><br>
        <div class="row" style="justify-content: space-between;">
          <div class="col-md-2"></div>
          <div class="col-md-2">
            <p style="border-radius: 10px; height: 35px; width: 120px; padding: 5px; background-color: #125DA4; color:white;">Area of Focus 1</p>
          </div>
          <div class="col-md-2">
            <p style="border-radius: 10px; height: 35px; width: 120px; padding: 5px; background-color: #125DA4; color:white;">Area of Focus 2</p>
          </div>
          <div class="col-md-2">
            <p style="border-radius: 10px; height: 35px; width: 120px; padding: 5px; background-color: #125DA4; color:white;">Area of Focus 3</p>
          </div>
          <div class="col-md-2">
            <p style="border-radius: 10px; height: 35px; width: 120px; padding: 5px; background-color: #125DA4; color:white;">Area of Focus 4</p>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row" style="justify-content: space-between;">
          <div class="col-md-2"></div>
          <div class="col-md-2">
            <p style="border-radius: 10px; background-color: #C6C7C9; padding: 5px; color: black; width: 120px; height: 100px;">${answersOfConcreteCommencement.answer_value.concreteCommecement1.txtareafocus1}</p>
          </div>
          <div class="col-md-2">
            <p style="border-radius: 10px; background-color: #C6C7C9; padding: 5px; color: black; width: 120px; height: 100px;">${answersOfConcreteCommencement.answer_value.concreteCommecement1.txtareafocus2}</p>
          </div>
          <div class="col-md-2">
            <p style="border-radius: 10px; background-color: #C6C7C9; padding: 5px; color: black; width: 120px; height: 100px;">${answersOfConcreteCommencement.answer_value.concreteCommecement1.txtareafocus3}</p>
          </div>
          <div class="col-md-2">
            <p style="border-radius: 10px; background-color: #C6C7C9; padding: 5px; color: black; width: 120px; height: 100px;">${answersOfConcreteCommencement.answer_value.concreteCommecement1.txtareafocus4}</p>
          </div>
          <div class="col-md-2"></div>
        </div>  
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="4" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfConcreteCommencement.consultant_review !== null ? answersOfConcreteCommencement.consultant_review.A : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>
      
      <div class="container mt-3" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1.3. Concrete Commencement</b></p>
        <p class="text-center mt-5" style="font-size: 25px"><b>B. Concrete Commencement</b></p>
        <img alt="img" src=${path.resolve('src/uploads/images/ConcreteCommencement2.png')} style="position: relative; height: 600px; margin-left: 100px; align-items: center;">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="row">
              <div class="Col-md-3 pl-0">
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; font-size: 20px; border-radius: 20px;">Scope</p>
                <p class="text-center p-3" style="background-color:#D2D6DE; font-size: 20px; border-radius: 20px;">${answersOfConcreteCommencement.answer_value.concreteCommecement2.txtscope}</p>
              </div>
              <div class="Col-md-3 pl-0">
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; font-size: 20px; border-radius: 20px;">Initiate</p>
                <p class="text-center p-3" style="background-color:#D2D6DE; font-size: 20px; border-radius: 20px;">${answersOfConcreteCommencement.answer_value.concreteCommecement2.txtinitiate}</p>
              </div>
              <div class="Col-md-3 pl-0">
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; font-size: 20px; border-radius: 20px;">Execute</p>
                <p class="text-center p-3" style="background-color:#D2D6DE; font-size: 20px; border-radius: 20px;">${answersOfConcreteCommencement.answer_value.concreteCommecement2.txtexecute}</p>
              </div>
              <div class="Col-md-3 pl-0">
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; font-size: 20px; border-radius: 20px;">Close</p>
                <p class="text-center p-3" style="background-color:#D2D6DE; font-size: 20px; border-radius: 20px;">${answersOfConcreteCommencement.answer_value.concreteCommecement2.txtclose}</p>
              </div>
            </div>
          </div>
          <div class="col-md-2"></div>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="4" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfConcreteCommencement.consultant_review !== null ? answersOfConcreteCommencement.consultant_review.B : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>
      
      <div class="container mt-5" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1.3. Concrete Commencement</b></p>
        <p class="text-center mt-5" style="font-size: 25px"><b>C. Concrete Commencement</b></p>
        <p class="text-center mt-3" style="font-size: 20px;">SIX STRATEGIC INITIATIVES</p>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="row">
              <div class="col-md-3">
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; border-radius: 10px;">S1</p>
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; border-radius: 10px;">S2</p>
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; border-radius: 10px;">S3</p>
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; border-radius: 10px;">S4</p>
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; border-radius: 10px;">S5</p>
                <p class="text-center p-3" style="background-color: #0C5A9E; color: white; border-radius: 10px;">S6</p>
              </div>
              <div class="col-md-9">
                <p class="text-center p-3" style="background-color: #D2D6DE; border-radius: 10px;">${answersOfConcreteCommencement.answer_value.concreteCommecement3.strategicInitiative1}</p>
                <p class="text-center p-3" style="background-color: #D2D6DE; border-radius: 10px;">${answersOfConcreteCommencement.answer_value.concreteCommecement3.strategicInitiative2}</p>
                <p class="text-center p-3" style="background-color: #D2D6DE; border-radius: 10px;">${answersOfConcreteCommencement.answer_value.concreteCommecement3.strategicInitiative3}</p>
                <p class="text-center p-3" style="background-color: #D2D6DE; border-radius: 10px;">${answersOfConcreteCommencement.answer_value.concreteCommecement3.strategicInitiative4}</p>
                <p class="text-center p-3" style="background-color: #D2D6DE; border-radius: 10px;">${answersOfConcreteCommencement.answer_value.concreteCommecement3.strategicInitiative5}</p>
                <p class="text-center p-3" style="background-color: #D2D6DE; border-radius: 10px;">${answersOfConcreteCommencement.answer_value.concreteCommecement3.strategicInitiative6}</p>
              </div>
            </div>
          </div>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfConcreteCommencement.consultant_review !== null ? answersOfConcreteCommencement.consultant_review.C : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>`
    }

     // Purposeful Planning
     let answersOfPurposefulPlanning = await planOfActionDetails.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('vga_audit_number', auditNo).andWhere('question_id', questionsOfPlanOfActions.Purposeful_Planning).first();
     if(answersOfPurposefulPlanning.question_id === questionsOfPlanOfActions.Purposeful_Planning && answersOfPurposefulPlanning.sub_section_id === apiData.sectionsProcess.sections[0].subSection[0].id){
      commonhtml += `
      <div class="container mt-3" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 25px;"><b>1.4. Purposeful Planning</b></p>
        <p class="text-center mt-2" style="font-size: 20px"><b>A. Our Strategy Management Process</b></p>
        <p class="ml-5" style="color: #0C5A9E;">Strategy Management Process</p>
        <p class="ml-5" style="color: #0C5A9E;">Getting Started: > Planning team > Create Schedule > Gather Documents</p>
        <div class="row">
          <div class="col-md-3">
            <img height="50px" src=${path.resolve('src/uploads/images/PurposefulPlanningA1.png')} width="150px">
          </div>
          <div class="col-md-3">
            <img height="50px" src=${path.resolve('src/uploads/images/PurposefulPlanningA2.png')} width="150px">
          </div>
          <div class="col-md-3">
            <img height="50px" src=${path.resolve('src/uploads/images/PurposefulPlanningA3.png')} width="150px">
          </div>
          <div class="col-md-3">
            <img height="50px" src=${path.resolve('src/uploads/images/PurposefulPlanningA4.png')} width="150px">
          </div>
        </div>
        <div class="row" style="border-top: 1px solid #f4f4f4;">
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Strategic issues :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.strategicIssue}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Mission :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.mission}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Use SWOT :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.useSwot}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Rollout :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.rollOut}</textarea>
          </div>
        </div>
        <div class="row" style="border-top: 1px solid #f4f4f4;">
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Industry & MarketData :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.industryMarketData}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Values :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.values}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Organizational Goal :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.organizationalGoal}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Set Calender :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.calander}</textarea>
          </div>
        </div>
        <div class="row" style="border-top: 1px solid #f4f4f4;">
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Customer Insights :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.customerInsights}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Vision :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.vision}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">KPIs :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.kpi}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Leverage Tool :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.leverageTool}</textarea>
          </div>
        </div>
        <div class="row" style="border-top: 1px solid #f4f4f4;">
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Employee Input :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.customerInsights}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Competitive Advantages :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.vision}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Department Goals :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.kpi}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Adapt Quarterly :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.leverageTool}</textarea>
          </div>
        </div>
        <div class="row" style="border-top: 1px solid #f4f4f4;">
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">SWOT :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.swot}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Org Wide Strategies :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.orgWideStrategies}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Team Member Goals :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.teamMemberGoals}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Update Annually :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.updateAnnually}</textarea>
          </div>
        </div>
        <div class="row" style="border-top: 1px solid #f4f4f4;">
          <div class="col-md-3"></div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Long-Term Objectives :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.longTermObjectives}</textarea>
          </div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 83px; left: 15px; color: #fff;">Budget :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.budget}</textarea>
          </div>
          <div class="col-md-3"></div>
        </div>
        <div class="row" style="border-top: 1px solid #f4f4f4;">
          <div class="col-md-3"></div>
          <div class="col-md-3">
            <img height="90px" src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} width="200px">
            <p style="position: relative; bottom: 90px; left: 15px; color: #fff;">Forecast :</p>
            <textarea style="position: relative; bottom: 85px; width: 199px; height: 30px;">${answersOfPurposefulPlanning.answer_value.strategyManagementProcess.forecast}</textarea>
          </div>
          <div class="col-md-3"></div>
          <div class="col-md-3"></div>
        </div>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>

      <div class="container mt-3" style="height: 120%;">
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfPurposefulPlanning.consultant_review !== null ? answersOfPurposefulPlanning.consultant_review.A : ''}</textarea>
      </div>
      
      <div class="container" style="height: 120%; margin-top: -280px;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1.4. Purposeful Planning</b></p>
        <p class="text-center mt-5" style="font-size: 25px"><b>B. Balanced Scorecard</b></p>
        <div class="row mt-5">
          <div class="col-md-4"></div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #0C5A9E; color: white; border-radius: 20px;">OBJECTIVES</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #0C5A9E; color: white; border-radius: 20px;">MATRICS</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <p class="p-3" style="background-color: #757575; color: white; border-radius: 20px;">Governance & Leadership</p>            
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.governanceLeadership1}</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.governanceLeadership2}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <p class="p-3" style="background-color: #757575; color: white; border-radius: 20px;">Programs & Services</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.programServices1}</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.programServices2}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <p class="p-3" style="background-color: #757575; color: white; border-radius: 20px;">Sustainable Infrastructure</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.sustainableInfrastructure1}</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.sustainableInfrastructure2}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <p class="p-3" style="background-color: #757575; color: white; border-radius: 20px;">Community Development</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.communityDevelopment1}</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.communityDevelopment2}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <p class="p-3" style="background-color: #757575; color: white; border-radius: 20px;">Marketing & Sales</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.marketingSales1}</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.marketingSales2}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <p class="p-3" style="background-color: #757575; color: white; border-radius: 20px;">Quality & Communication</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.qualityCommunication1}</p>
          </div>
          <div class="col-md-4">
            <p class="p-3" style="background-color: #D2D6DE; border-radius: 20px;">${answersOfPurposefulPlanning.answer_value.balanceScoreCard.qualityCommunication2}</p>
          </div>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfDefiniteDirection.consultant_review !== null ? answersOfDefiniteDirection.consultant_review.B : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>
      
      <div class="container mt-3" style="height: 120%;">
        <p style="color: #0C5A9E; font-size: 30px;"><b>1.4. Purposeful Planning</b></p>
        <p class="text-center mt-5" style="font-size: 25px"><b>C. Strategic Planning Freamwork</b></p>
        <div class="row">
          <img src=${path.resolve('src/uploads/images/PurposefulPlanningA.png')} style="margin-top: -80px; position: relative; height:800px; width: 100%; align-items: center;">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-md-5"></div>
              <div class="col-md-4" style="z-index: 2; position: relative;">
                <p style="position: relative; width: 80px; height: 63px; margin-top:-515px; color: white; z-index: 1; font-size:10px;">${answersOfPurposefulPlanning.answer_value.strategicPlanningFramework.strategicPlanning1}</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-8" style="z-index: 2; position: relative;">
                <p style="position: relative; width: 135px; height: 63px; margin-top:-460px; color: white; font-size:10px;">${answersOfPurposefulPlanning.answer_value.strategicPlanningFramework.strategicPlanning2}</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-8" style="z-index:2; position: relative;">
                <p style="z-index: 1; position: relative; width: 204px; height: 63px; margin-top:-410px; color: white; font-size:10px;">${answersOfPurposefulPlanning.answer_value.strategicPlanningFramework.strategicPlanning3}</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-8" style="z-index: 2; position: relative;">
                <p style="z-index: 1; position: relative; width: 271px; height: 63px; margin-top:-355px; color: white; font-size:10px;">${answersOfPurposefulPlanning.answer_value.strategicPlanningFramework.strategicPlanning4}</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-8" style="z-index: 2; position: relative;">
                <p style="z-index: 1; position: relative; width: 341px; height: 63px; margin-top:-300px; color: white; font-size:10px;">${answersOfPurposefulPlanning.answer_value.strategicPlanningFramework.strategicPlanning5}</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-8" style="z-index: 2; position: relative;">
                <p style="z-index: 1; position: relative; width: 341px; height: 63px; margin-top:-240px; color: white; font-size:10px;">${answersOfPurposefulPlanning.answer_value.strategicPlanningFramework.strategicPlanning6}</p>
              </div>
            </div>
          </div>
        </div>
        <hr class="mt-5" style="width: 100%"/>
        <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
        <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfPurposefulPlanning.consultant_review !== null ? answersOfPurposefulPlanning.consultant_review.C : ''}</textarea>
      </div>
      <div style="page-break-before: always; margin-top: -120%"></div>`
     }

     // Growth Frame
     let answersOfGrowthFrame = await planOfActionDetails.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('vga_audit_number',auditNo).andWhere('question_id', questionsOfPlanOfActions.Growth_Frame).first();
     if(answersOfGrowthFrame.question_id === questionsOfPlanOfActions.Growth_Frame && answersOfGrowthFrame.sub_section_id === apiData.sectionsProcess.sections[0].subSection[1].id){
       commonhtml += `
        <div class="container" style="height: 120px;">
          <p style="color: #0C5A9E; font-size: 30px;"><b>2. ${apiData.sectionsProcess.sections[0].subSection[1].sub_section_name}</b></p>
          <p style="color: #0C5A9E; font-size: 25px;"><b>2.1. Growth Frame</b></p>
          <div class="row ml-n5 mt-5">
            <table class="mt-4" style="border: 1px solid #0C5A9E; width: 100%;">
              <div class="col-md-11">
                <tr style="background-color:#0f5ca1;">
                  <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                    <span style="color: white;">PROFIT / GROWTH MAXIMIZATION</span>
                  </th>
                </tr>
                <tr style="background-color:#0f5ca1; text-align: center">
                  <td class="p-2" class="p-2" colspan="2" rowspan="4" style="text-align: center; border: 1px solid #0C5A9E; color: white;"> VALUE /<br> WEALTH MAXIMIZATION </td>
                  <td class="p-2" style="width: 128px; border: 1px solid #0C5A9E;">&nbsp;</td>
                  <td class="p-2" style="text-align: center ! important; border: 1px solid #0C5A9E; color: white;">Strategic</td>
                  <td class="p-2" style="text-align: center ! important; border: 1px solid #0C5A9E; color: white;">Managerial</td>
                  <td class="p-2" style="text-align: center ! important; border: 1px solid #0C5A9E; color: white;">Operational</td>
                </tr>
                <tr>
                  <td class="p-2" style="text-align: center ! important; border: 1px solid #0C5A9E;"><b>Strategic</b></td>
                  <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;"><b>Strategy</b><br>
                    <table>
                      <tr>
                        <td class="p-2">Status</td>
                        <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.strategic.strategy.status}>
                      </tr>
                      <tr>
                        <td class="p-2">Score</td>
                        <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.strategic.strategy.score}></td>
                      </tr>
                      <tr>
                        <td class="p-2">Synergy</td>
                        <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.strategic.strategy.synergy}></td>
                      </tr>
                    </table>
                  </td>
                  <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;"><b>Sustainability</b><br>
                    <table>
                      <tr>
                        <td class="p-2">Status</td>
                        <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.strategic.sustainability.status}></td>
                      </tr>
                      <tr>
                        <td class="p-2">Score</td>
                        <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.strategic.sustainability.score}></td>
                      </tr>
                      <tr>
                        <td class="p-2">Synergy</td>
                        <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.strategic.sustainability.synergy}></td>
                      </tr>
                    </table>
                  </td>
                  <td class="p-2" colspan="2" style="text-align: center; border: 1px solid #0C5A9E;"><b>Solutions</b><br>
                    <table>
                      <tbody>
                        <tr>
                          <td class="p-2">Status</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.strategic.solutions.status}></td>
                        </tr>
                        <tr>
                          <td class="p-2">Score</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.strategic.solutions.score} type="text"></td>
                        </tr>
                        <tr>
                          <td class="p-2">Synergy</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.strategic.solutions.synergy} type="text"></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td class="p-2" style="text-align: center ! important; border: 1px solid #0C5A9E;"><b>Managerial</b></td>
                  <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;"><b>System</b><br>
                    <table>
                      <tbody>
                        <tr>
                          <td class="p-2">Status</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.managerial.system.status} type="text"/td>
                        </tr>
                        <tr>
                          <td class="p-2">Score</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.managerial.system.score} type="text"></td>
                        </tr>
                        <tr>
                          <td class="p-2">Synergy</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.managerial.system.synergy} type="text"/td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;"><b>Style of Leadership</b><br>
                    <table>
                      <tbody>
                        <tr>
                          <td class="p-2">Status</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.managerial.styleOfLeadership.status} type="text"></td>
                        </tr>
                        <tr>
                          <td class="p-2">Score</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.managerial.styleOfLeadership.score} type="text"></td>
                        </tr>
                        <tr>
                          <td class="p-2">Synergy</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.managerial.styleOfLeadership.synergy} type="text"></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;"><b>Skills</b><br>
                    <table>
                      <tbody>
                        <tr>
                          <td class="p-2">Status</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.managerial.skills.status} type="text"></td>
                        </tr>
                        <tr>
                          <td class="p-2">Score</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.managerial.skills.score} type="text"></td>
                        </tr>
                        <tr>
                          <td class="p-2">Synergy</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" value=${answersOfGrowthFrame.answer_value.managerial.skills.synergy} type="text"></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td class="p-2" style="text-align: center ! important; border: 1px solid #0C5A9E;"><b>Operational</b></td>
                  <td class="p-2" style="text-align: center;"><b>Structure</b><br>
                    <table>
                      <tbody>
                        <tr>
                          <td class="p-2">Status</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.oprational.structure.status}></td>
                        </tr>
                        <tr>
                          <td class="p-2">Score</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.oprational.structure.score}></td>
                        </tr>
                        <tr>
                          <td class="p-2">Synergy</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.oprational.structure.synergy}></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;"><b>Shared Values</b><br>
                    <table>
                      <tbody>
                        <tr>
                          <td class="p-2">Status</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.oprational.sharedValues.status}></td>
                        </tr>
                        <tr>
                          <td class="p-2">Score</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.oprational.sharedValues.score}></td>
                        </tr>
                        <tr>
                          <td class="p-2">Synergy</td>
                          <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.oprational.sharedValues.synergy}></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;"><b>Standard Operating Procedures</b><br>
                    <table>
                      <tr>
                        <td class="p-2">Status</td>
                        <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.oprational.standardOperationProducers.status}></td>
                      </tr>
                      <tr>
                        <td class="p-2">Score</td>
                        <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.oprational.standardOperationProducers.score}></td>
                      </tr>
                      <tr>
                        <td class="p-2">Synergy</td>
                        <td class="p-2"><input class="p-2" style="font-size:10px; width: 150px;" type="text" value=${answersOfGrowthFrame.answer_value.oprational.standardOperationProducers.synergy}></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </div>
              <div class="col-md-1"></div>
            </table>
          </div>
          <hr class="mt-5" style="width: 100%"/>
          <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
          <textarea class="mt-3" rows="7" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfDefiniteDirection.consultant_review !== null ? answersOfDefiniteDirection.consultant_review.A : ''}</textarea>
        </div>
        <div style="page-break-before: always; margin-top: -120%"></div>`
     }

     // Sales Action Plan
     let answersOfSalesActionPlan = await planOfActionDetails.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('vga_audit_number', auditNo).andWhere('question_id', questionsOfPlanOfActions.Sales_Action_Plan).first();
     if(answersOfSalesActionPlan.question_id === questionsOfPlanOfActions.Sales_Action_Plan && answersOfSalesActionPlan.sub_section_id === apiData.sectionsProcess.sections[0].subSection[2].id){
       commonhtml += `
        <div class="container mt-5" style="height: 120%;">
          <p style="color: #0C5A9E; font-size: 30px; margin-top:500%;"><b>3. ${apiData.sectionsProcess.sections[0].subSection[2].sub_section_name}</b></p>
          <p style="color: #0C5A9E; font-size: 25px;"><b>3.1. Sales Action Plan</b></p>
          <div class="row">
            <table class="mt-4" style="border: 1px solid #0C5A9E; width: 100%;">
            <div class="col-md-11">
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">GOAL</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.goal}</td></tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">CUSTOMER SEGMENT TARGETS</span>
                </th>
                <th class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">LEADING PROSPECTS</span>
                </th>
                <th class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">KEY TEAM MEMBERS</span>
                </th>
              </tr>
              <tr>
                <td class="p-2" colspan="2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.customerSegmentTargets}</td>
                <td class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.leadingProspects}</td>
                <td class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.keyTeamMembers}</td>
              </tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">ESTABLISHED PLAN OF ATTACK</span>
                </th>
                <th class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">OUTREACH & AWARENESS</span>
                </th>
                <th class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">BRAND ESTABLISHMENT</span>
                </th>
              </tr>
              <tr>
                <td class="p-2" colspan="2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.establishedPlanOfAttack}</td>
                <td class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.outreachAwareness}</td>
                <td class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.brandEstablishment}</td>
              </tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">NETWORKING</span>
                </th>
                <th class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">SUPPORTING RESEARCH</span>
                </th>
                <th class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">EVALUATION PLAN</span>
                </th>
              </tr>
              <tr>
                <td class="p-2" colspan="2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.networking}</td>
                <td class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.supportingReaserch}</td>
                <td class="p-2" colspan="3" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.evaluationPlan}</td>
              </tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">STRATEGIC ACTION DESCRIPTIONS</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">PARTY / DEPT RESPONSIBLE</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">DATE TO BEGIN</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">DATE DUE</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">RESOURCES REQUIRED</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">DESIRED OUTCOME</span>
                </th>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.strategicActionDescriptions.description1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.deptResponsible.dept1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.dateTobegin.beginDate1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.dateTodue.dueDate1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.resourcesRequired.resource1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.desiredOutcome.outcome1}</td>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.strategicActionDescriptions.description2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.deptResponsible.dept2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.dateTobegin.beginDate2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.dateTodue.dueDate2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.resourcesRequired.resource2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.desiredOutcome.outcome2}</td>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.strategicActionDescriptions.description3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.deptResponsible.dept3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.dateTobegin.beginDate3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.dateTodue.dueDate3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.resourcesRequired.resource3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.desiredOutcome.outcome3}</td>
              </tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">ADDITIONAL NOTES</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfSalesActionPlan.answer_value.additionalNotes}</td></tr>
            </table>
          </div>
          <hr class="mt-5" style="width: 100%"/>
          <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
          <textarea class="mt-3" rows="5" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfDefiniteDirection.consultant_review !== null ? answersOfDefiniteDirection.consultant_review.A : ''}</textarea>
        </div>
        <div style="page-break-before: always; margin-top: -120%"></div>`
     }

    // Work Action Plan
    let answersOfWorkActionPlan = await planOfActionDetails.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('vga_audit_number', auditNo).andWhere('question_id', questionsOfPlanOfActions.Work_Action_Plan).first();
    if(answersOfWorkActionPlan.question_id === questionsOfPlanOfActions.Work_Action_Plan && answersOfWorkActionPlan.sub_section_id === apiData.sectionsProcess.sections[0].subSection[2].id){
      commonhtml += `
      <div class="container mt-3" style="height: 120%;">
        <div class="row">
          <p style="color: #0C5A9E; font-size: 25px;"><b>3.2. Work Action Plan</b></p>
          <table style="border: 1px solid #0C5A9E; width: 100%;">
            <div class="col-md-11">
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">GOAL</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.goal}</td></tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">BENCHMARKS FOR SUCCESS</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.benchmarkForSuccess}</td></tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">EVALUATION PLAN</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.evaluationPlan}</td></tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">STRATEGIC ACTION DESCRIPTIONS</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">PARTY / DEPT RESPONSIBLE</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">DATE TO BEGIN</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">DATE DUE</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">RESOURCES REQUIRED</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">POTENTIAL HAZARDS</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">DESIRED OUTCOME</span>
                </th>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.strategicActionDescriptions.description1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.deptResponsible.dept1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTobegin.beginDate1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTodue.dueDate1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.resourcesRequired.resource1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.potentialHazards.hazards1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.desiredOutcome.outcome1}</td>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.strategicActionDescriptions.description2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.deptResponsible.dept2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTobegin.beginDate2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTodue.dueDate2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.resourcesRequired.resource2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.potentialHazards.hazards2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.desiredOutcome.outcome2}</td>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.strategicActionDescriptions.description3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.deptResponsible.dept3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTobegin.beginDate3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTodue.dueDate3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.resourcesRequired.resource3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.potentialHazards.hazards3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.desiredOutcome.outcome3}</td>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.strategicActionDescriptions.description4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.deptResponsible.dept4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTobegin.beginDate4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTodue.dueDate4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.resourcesRequired.resource4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.potentialHazards.hazards4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.desiredOutcome.outcome4}</td>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.strategicActionDescriptions.description5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.deptResponsible.dept5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTobegin.beginDate5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.dateTodue.dueDate5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.resourcesRequired.resource5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.potentialHazards.hazards5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.desiredOutcome.outcome5}</td>
              </tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">ADDITIONAL NOTES</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfWorkActionPlan.answer_value.additionalNotes}</td></tr>
            </table>
          </div>
          <hr class="mt-5" style="width: 100%"/>
          <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
          <textarea class="mt-3" rows="5" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfWorkActionPlan.consultant_review !== null ? answersOfWorkActionPlan.consultant_review.B : ''}</textarea>
        </div>
      <div style="page-break-before: always; margin-top: -120%"></div>`
    }

    // Corrective Action Plan
    let answersOfCorrectiveActionPlan = await planOfActionDetails.query().where('is_deleted', false).andWhere('client_id', clientId).andWhere('vga_audit_number', auditNo).andWhere('question_id', questionsOfPlanOfActions.Corrective_Action_Plan).first();
    if(answersOfCorrectiveActionPlan.question_id === questionsOfPlanOfActions.Corrective_Action_Plan && answersOfCorrectiveActionPlan.sub_section_id === apiData.sectionsProcess.sections[0].subSection[2].id){
      commonhtml += `
      <div class="container mt-3" style="height: 120%;">
        <div class="row">
          <p style="color: #0C5A9E; font-size: 25px;"><b>3.3. Corrective Action Plan</b></p>
          <table style="border: 1px solid #0C5A9E; width: 100%;">
            <div class="col-md-11">
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">ISSUE DESCRIPTION</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.issueDescription}</td></tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">DESIRED OUTCOME</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.desiredOutcome}</td></tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">ACTION PLAN SPONSOR</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.actionPlanSponser}</td></tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">STRATEGIC ACTION</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">PARTY / DEPT RESPONSIBLE</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">RESOURCES REQUIRED</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">STAKEHOLDERS</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">CONSTRAINTS</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">METRIC</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">DATE DUE</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">PRIORITY</span>
                </th>
                <th class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">EXECUTION TIMELINE</span>
                </th>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.strategicAction.action1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.deptResponsible.dept1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.resourcesRequired.resource1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.stackHolders.stackHolder1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.constraints.constraints1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.metric.metric1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.dateTodue.dueDate1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.priority.priority1}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.executionTimeline.executionTimeline1}</td>
              </tr>
              <tr>
              <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.strategicAction.action2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.deptResponsible.dept2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.resourcesRequired.resource2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.stackHolders.stackHolder2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.constraints.constraints2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.metric.metric2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.dateTodue.dueDate2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.priority.priority2}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.executionTimeline.executionTimeline2}</td>
              </tr>
              <tr>
              <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.strategicAction.action3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.deptResponsible.dept3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.resourcesRequired.resource3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.stackHolders.stackHolder3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.constraints.constraints3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.metric.metric3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.dateTodue.dueDate3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.priority.priority3}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.executionTimeline.executionTimeline3}</td>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.strategicAction.action4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.deptResponsible.dept4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.resourcesRequired.resource4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.stackHolders.stackHolder4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.constraints.constraints4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.metric.metric4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.dateTodue.dueDate4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.priority.priority4}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.executionTimeline.executionTimeline4}</td>
              </tr>
              <tr>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.strategicAction.action5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.deptResponsible.dept5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.resourcesRequired.resource5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.stackHolders.stackHolder5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.constraints.constraints5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.metric.metric5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.dateTodue.dueDate5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.priority.priority5}</td>
                <td class="p-2" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.executionTimeline.executionTimeline5}</td>
              </tr>
              <tr style="background-color:#0f5ca1;">
                <th class="p-2" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">
                  <span style="color: white;">ADDITIONAL NOTES</span>
                </th>
              </tr>
              <tr><td class="p-3" colspan="9" style="text-align: center; border: 1px solid #0C5A9E;">${answersOfCorrectiveActionPlan.answer_value.additionalNotes}</td></tr>
            </table>
          </div>
          <hr class="mt-5" style="width: 100%"/>
          <h4 class="mt-5" style="text-align: center;"><b>:: Consultants View ::</b></h4>
          <textarea class="mt-3" rows="5" cols="110" style="resize:none; border: 1px solid #0C5A9E; border-radius: 25px; padding: 3%;">${answersOfCorrectiveActionPlan.consultant_review !== null ? answersOfCorrectiveActionPlan.consultant_review.C : '' }</textarea>
        </div>
      <div style="page-break-before: always; margin-top: -120%"></div>`
    }
    // }                               
  // }
  commonhtml += `
  </body>
  </html>`;
  fs.writeFileSync('src/uploads/print.html', commonhtml);
  return path.resolve('src/uploads/print.html');
}

pdfRoute.post('/genrate-pdf', tokenMiddleware, async (req, res, next) => {
  
  let auditNo = req.body.vgAuditNo;
  let sectionId = req.body.sectionId;
  let clientId = req.body.clientId;
  let response = {};
   const sectionsDetails = await Audit.query()
     .innerJoin('vga_audit_details','vga_audit_details.vga_audit_id','vga_audits.id')
     .innerJoin('section_master', 'section_master.id', 'vga_audit_details.section_id')
     .select(
       'vga_audit_details.id',
       'vga_audits.id',
       'vga_audits.user_id',
       'vga_audits.audit_from_date',
       'vga_audits.audit_to_date',
       'vga_audit_details.status',
      'vga_audits.vga_audit_number as auditNo',
      'vga_audits.client_id as clientId',
      'section_master.section_name as section_name',
      'vga_audit_details.section_id as sectionId',
      'section_master.sequence_number as sequence_number',
     )
     .where('vga_audits.is_deleted',false)
     .andWhere('vga_audit_details.is_deleted',false)
     .andWhere('section_master.is_deleted', false)
     .andWhere('vga_audits.vga_audit_number',auditNo)
     .andWhere('vga_audits.client_id',clientId)
     .andWhere('vga_audit_details.section_id',sectionId).withGraphFetched('[client]');

 const consultant = await Consultant.query().where('id',sectionsDetails[0].client.consultant).first();
 response.sectionsDetails = sectionsDetails;
 response.sectionsDetails[0].consultantName = consultant.name;
  try {
    if (response) {
      let printPdf = null;
      const reportStatus = await checkAuditReportStatus(auditNo, sectionId);
      if(reportStatus.success !== false){
        let sectionsProcess, sectionsProcess2;
        if(response.sectionsDetails.length > 0){
          // if(response.sectionsDetails[0].sectionId  !== sectionEnum.CompetencyMapping){
            sectionsProcess = await sectionProcessing(response.sectionsDetails[0].sectionId, response.sectionsDetails[0].auditNo);
            response.sectionsProcess = sectionsProcess;
          // } else{
            sectionsProcess2 = await sectionProcessing2(response.sectionsDetails[0].sectionId, response.sectionsDetails[0].auditNo, clientId);
            response.sectionsProcess2 = sectionsProcess2;
          // }
          if(response.sectionsDetails[0].sectionId === sectionEnum.OrganizationDiagnosis){
            const coverPage = await gencommonhtml(response.sectionsDetails);
            console.log("html", coverPage);
            const indexPage = await indexpageHtml(response);
            console.log('indexhtml', indexPage);
            const contentOfOrganizationDiagnosis = await contentOfOrganizationDiagnosisHtml(response);
            console.log('datahtml', contentOfOrganizationDiagnosis);
            const thankyouPage = await ThankYouHtml();
            console.log('thank you tml', thankyouPage);
            printPdf = await commonHeaderFooterHtml(response.sectionsDetails);
            console.log("pdf", printPdf);
            if (printPdf) {
              res.set('Content-Type', 'application/pdf');
              res.send(printPdf);
              commonhtml = ``;
              // const fileObject = {
              //       buffer : createpdf,
              //       type: 'application/pdf',
              //       bucketName: process.env.AWS_S3_TEST_BUCKET,
              //       fileName: 'VGAuditReport_1_OrganizationDiagnosis'
              // }
              //   const s3Url = (await UploadFilesToS3(fileObject)).Location;
              //   console.log(s3Url);
            } else {
              return { meassge: 'Please try again!' }
            }
          } else if(response.sectionsDetails[0].sectionId  === sectionEnum.CompetencyMapping){
            const coverPage = await gencommonhtml(response.sectionsDetails);
            console.log("html", coverPage);
            const indexPage = await indexpageHtml(response);
            console.log('indexhtml', indexPage);
            const contentOfCompetencyMapping = await contentOfCompetencyMappingHtml(response, auditNo);
            console.log("pdf", contentOfCompetencyMapping);
            const thankyouPage = await ThankYouHtml();
            console.log('thank you tml', thankyouPage);
            printPdf = await commonHeaderFooterHtml(response.sectionsDetails);
            if (printPdf) {
              res.set('Content-Type', 'application/pdf')
              res.send(printPdf);
              commonhtml = ``;
            } else {
              return { meassge: 'Please try again!' }
            }
          } else if(response.sectionsDetails[0].sectionId  === sectionEnum.ValuegrowthMatrix){
            const coverPage = await gencommonhtml(response.sectionsDetails);
            console.log("html", coverPage);
            const indexPage = await indexpageHtml(response);
            console.log('indexhtml', indexPage);
            const contentOfValueGrowthMatrix = await contentOfValueGrowthMatrixHtml(response);
            console.log("pdf", contentOfValueGrowthMatrix);
            const thankyouPage = await ThankYouHtml();
            console.log('thank you tml', thankyouPage);
            printPdf = await commonHeaderFooterHtml(response.sectionsDetails);
            if (printPdf) {
              res.set('Content-Type', 'application/pdf')
              res.send(printPdf);
              commonhtml = ``;
            } else {
              return { meassge: 'Please try again!' }
            }
          } else if(response.sectionsDetails[0].sectionId  === sectionEnum.PerformanceEvaluation){
            const coverPage = await gencommonhtml(response.sectionsDetails);
            console.log("html", coverPage);
            const indexPage = await indexpageHtml(response);
            console.log('indexhtml', indexPage);
            const contentOfPerformanceEvaluation = await contentOfPerformanceEvaluationHtml(response, auditNo);
            console.log("pdf", contentOfPerformanceEvaluation);
            const thankyouPage = await ThankYouHtml();
            console.log('thank you tml', thankyouPage);
            printPdf = await commonHeaderFooterHtml(response.sectionsDetails);
            if (printPdf) {
              res.set('Content-Type', 'application/pdf')
              res.send(printPdf);
              commonhtml = ``;
            } else {
              return { meassge: 'Please try again!' }
            }
            commonhtml = ``;
          } else if(response.sectionsDetails[0].sectionId  === sectionEnum.PlanOfActionAndRecommendations){
            const coverPage = await gencommonhtml(response.sectionsDetails);
            console.log("html", coverPage);
            const indexPage = await indexpageHtml(response);
            console.log('indexhtml', indexPage);
            const contentOfPlanOfActions = await contentOfPlanOfActionsAndRecommendations(response, auditNo, clientId);
            console.log("pdf", contentOfPlanOfActions);
            const thankyouPage = await ThankYouHtml();
            console.log('thank you tml', thankyouPage);
            printPdf = await commonHeaderFooterHtml(response.sectionsDetails);
            if (printPdf) {
              res.set('Content-Type', 'application/pdf')
              res.send(printPdf);
              commonhtml = ``;
            } else {
              return { meassge: 'Please try again!' }
            }
            commonhtml = ``;
          } else{
            console.log("something went wrong");
          }
        } else{
          res.status(404).send({
            success: false,
            meassge: 'Report is not submitted yet! So please fill Report first!'
          })
        }
      } else{
        res.status(404).send({
          success: false,
          meassge: 'Report is not submitted yet! So please fill Report first!'
        })
      }
    } else {
      res.status(500).send({
        success: false,
        meassge: 'please check the passed arguments'
      })
    }
  } catch (error) {
    console.log("er", error);
    res.status(500).send({
      error: error.message,
      stack: error.stack
    })
    // return error;
  }
});

// const UploadFilesToS3 = async (fileObject) => {
//   const s3 = new AWS.S3({
//       accessKeyId: process.env.AWS_ACCESS_KEY_ID,
//       secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
//       region: process.env.AWS_RIGION
//   });
//   return await s3.upload({
//       Bucket: fileObject.bucketName,
//       Key: uuidv4(),
//       Body: fileObject.buffer,
//       ContentType: fileObject.type,
//       ContentEncoding: 'base64',
//       ACL: 'public-read',
//   }).promise();
// };

export default pdfRoute;