const express = require("express");
const CityRoute = express.Router();
const cities = require("./../models/cities.Model").default;
const { validationResult } = require('express-validator');
import {citybodyvalidate} from '../common/validator'

CityRoute.post("/", async (req, res, next) => {

    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 10);
    const sortColumn = req.body.SortColumn;
    const sortOrder = req.body.SortOrder;
    const SearchText = req.body.SearchText;

    try {
        let cityObj = await cities.query()
            .where("is_deleted", false)
            .andWhere('cities.city_name', 'like', '%' + SearchText + '%')
            .orderBy(sortColumn || 'cities.city_name', sortOrder || 'ASC')
            .page(pageNo, PageSiZe);

        if (cityObj) {
            res.status(200).send({
                success: true,
                meassge: "Get all cities Successfully.",
                res: cityObj
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

CityRoute.get("/:id", async (req, res, next) => {
    let states_id = req.params.id;
    
    try {
        if (states_id) {
            let city = await cities.query().where("states_id", states_id).andWhere("is_deleted",false).orderBy("city_name");

            if (city) {
                res.status(200).send({
                    success: true,
                    message:"get city by stateId.",
                    res: city
                })
            }else{
                res.status(500).send({
                    success: false,
                    message: 'No Data Found.'
                })
            }    
        } else {
            res.status(500).send({
                success: false,
                message: 'Id Not Found.'
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            error: error.stack
        })
    }
});

CityRoute.post("/add-cities",citybodyvalidate, async (req, res, next) => {
        
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }

    const citiesDetailsobj = {
        city_name: req.body.cityName,
        states_id: req.body.stateId
    }

    try {
        let city = await cities.query().insertGraphAndFetch(citiesDetailsobj);

        if (city) {
            res.status(200).send({
                state: true,
                message: 'City added Successfully.',
                res: city
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        })
    }
});

CityRoute.delete("/delete-cities/:id", async (req, res, next) => {
    let cities_id = req.params.id;

    let deletedRecord = await cities.query().where("id", cities_id).first();

    try {
        if (deletedRecord) {
            const cityById = await cities.query().where("id", cities_id).patch({ is_deleted: true, deleted_on: new Date() });

            if (cityById) {
                res.status(200).send({
                    success: true,
                    meassge: "city Deleted Successfully"
                })
            }
        } else {
            res.status(500).send({
                success: false,
                meassge: "Id Not Found."
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

CityRoute.put("/update-cities/:id", async (req, res, next) => {
    let cities_id = req.params.id;
    const citiesDetailsobj = {
        city_name: req.body.cityName,
        states_id: req.body.stateId
    }
    try {
        if (cities_id) {
            let city = await cities.query().patchAndFetchById(cities_id, citiesDetailsobj);

            if (city) {
                res.status(200).send({
                    success: true,
                    message: "update city successfully.",
                    res: city
                })
            }
        } else {
            res.status(500).send({
                success: false,
                meassge: "Id Not Found."
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

export default CityRoute;