const express = require("express");
const AccountRoute = express.Router();
const tokenMiddleware = require('./../middleware').default;
const Account = require("./../models/Account.Model").default;
const { validationResult } = require('express-validator');
import { accountbodyvalidate, updateaccountvalidate } from '../common/validator';

AccountRoute.use(async (req, res, next) => {
  next();
});

AccountRoute.get("/get-accounts", tokenMiddleware, async (req, res, next) => {
  try {
    const accountDetailObj = await Account.query().where("is_deleted", false);
    if (accountDetailObj) {
      res.status(200).send({
        success: true,
        meassge: "Get all Successfully.",
        res: accountDetailObj
      });
    }
  } catch (error) {
    res.status(500).send({
      error: error.message,
      error: error.stack,
    });
  }
});

AccountRoute.post("/", tokenMiddleware, async (req, res, next) => {
  try {
    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 5);
    const sortColumn = req.body.SortColumn;
    const sortOrder = req.body.SortOrder;
    const SearchText = req.body.SearchText;
    const SearchEmail = req.body.SearchEmail;

    const accountDetailObj = await Account.query()
      .where("is_deleted", false)
      .andWhere('accounts.name', 'like', '%' + SearchText + '%')
      .andWhere('accounts.email', 'like', '%' + SearchEmail + '%')
      .orderBy(sortColumn || 'accounts.name', sortOrder || 'ASC')
      .page(pageNo, PageSiZe);

    if (accountDetailObj) {
      res.status(200).send({
        success: true,
        meassge: "Get all Successfully.",
        res: accountDetailObj
      });
    } else{
      res.status(500).send({
        success: false,
        meassge: "Account not found.",
        error: error.stack
      });
    }
  } catch (error) {
    res.status(500).send({
      error: error.message,
      error: error.stack,
    });
  }
});

AccountRoute.post("/add_new_account", accountbodyvalidate, tokenMiddleware, async (req, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    const parent = await Account.query().findById(req.body.parent_account_id);
    const accountObj = {
      name: req.body.name,
      description: req.body.description,
      email: req.body.email,
      phone: req.body.phone,
      parent: {
        "#dbRef": req.body.parent_account_id
      },
    };
  
    let postAccountObj = await Account.query().insertGraphAndFetch(accountObj);
    const b = Object.assign({}, postAccountObj, { account_lineage: parent.account_lineage + '.' + postAccountObj.id.split('-').join('') });
    let c = await Account.query().upsertGraphAndFetch(b);

    if (c) {
      res.status(200).send({
        success: true,
        message: "Account created successfully.!",
        res: c,
      });
    }
  } catch (error) {
    res.status(500).send({
      success: false,
      error: error.message,
      stack: error.stack,
    });
  }
});

AccountRoute.delete("/delete-account/:id", tokenMiddleware, async (req, res, next) => {
  try {
    const is_deleted_id = req.params.id;
    let delted_record = await Account.query().where('id', is_deleted_id).first();
    if (delted_record) {
        let a = await Account.query().where('id', is_deleted_id).patch({ is_deleted: true, deleted_on: new Date() });
        res.status(200).send({
          success: true,
          message: "deleted Successfully."
        });
    } else {
      res.status(500).send({
        success: false,
        message: "Account Not Found."
      });
    }
  } catch (error) {
    res.push({
      message: error.message,
      stack: error.stack
    });
  }
});

AccountRoute.put("/update-account/:id", updateaccountvalidate ,tokenMiddleware, async (req, res, next) => {
  try{
    const is_account_id = req.params.id;
    let account_record = await Account.query().where('id', is_account_id).first();
    if (account_record) {
      const accountObj = {
        name: req.body.name,
        description: req.body.description,
        email: req.body.email,
        phone: req.body.phone,
        parent_account_id: req.body.parent_account_id,
      };
  
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
  
      let patchAccountObj = await Account.query().patchAndFetchById(is_account_id, accountObj);
      if (patchAccountObj) {
        res.status(200).send({
          success: true,
          message: "Account Updated successfully.!",
          res: patchAccountObj,
        });
      }
      
    } else {
      res.status(500).send({
        success: false,
        message: "Account Not Found."
      })
    }
  } catch(error){
    res.push({
      success: false,
      error: error.message,
      stack: error.stack,
    });
  }
});

export default AccountRoute;
