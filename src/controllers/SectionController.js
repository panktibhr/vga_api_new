const express = require("express");
const SectionRoute = express.Router();
const Section = require('../models/Section.Model').default;
const tokenMiddleware = require('./../middleware').default;
const { validationResult } = require('express-validator');
import {sectionbodyvalidate} from '../common/validator' 

SectionRoute.get("/getAll-section", tokenMiddleware, async(req,res,next) => {
  try{
    let sectionDetailsObj = await Section.query().where("is_deleted",false).orderBy('sequence_number');
      
    if(sectionDetailsObj){
      res.status(200).send({
        success:true,
        message:'get all sections.',
        res: sectionDetailsObj
      });
    }

  }catch(error){
    res.status(500).send({
      error:error.meassge,
      stack:error.stack
    });
  }
});

SectionRoute.post("/", tokenMiddleware, async (req, res, next) => {
  
  const pageNo = parseInt(req.body.PageNo || 0);
  const PageSiZe = parseInt(req.body.PageSize || 5);
  const sortColumn = req.body.SortColumn;
  const sortOrder = req.body.SortOrder;
  const SearchText = req.body.SearchText;

  const sectionDetailsObj = await Section.query()
    .where("is_deleted", false)
    .andWhere('section_master.section_name', 'like', '%' + SearchText + '%')
    .orderBy(sortColumn || 'section_master.sequence_number', sortOrder || 'ASC')
    .page(pageNo, PageSiZe);

  try {
    if (sectionDetailsObj) {
      res.status(200).send({
        success: true,
        meassge: "Get all Successfully.",
        res: sectionDetailsObj
      });
    }
  } catch (error) {
    res.status(500).send({
      error: error.message,
      error: error.stack,
    });
  }
});

SectionRoute.get("/:id", tokenMiddleware, async (req, res, next) => {
  let is_id = req.params.id;

  try{
    if (is_id) {
      const sectionById = await Section.query().where("id", is_id).andWhere("is_deleted", false).first();

      if (sectionById) {
        res.status(200).send({
          success: true,
          message: "section get Successfully",
          res: sectionById
        })
      } else  {
        res.status(500).send({
          error: error.meassge,
          stack: error.stck
        });
      } 
    } else {
      res.status(400).send({
        error: error.meassge,
        stack: error.stck,
        message: "Section Id Not Found."
      });
    }
  } catch(error){
    res.status(500).send({
      error: error.meassge,
      stack: error.stck
    });
  }
});

SectionRoute.post("/add-section",sectionbodyvalidate, tokenMiddleware, async (req, res, next) => {
  const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() })
        }
  
  const sectionDetailsobj = {
    section_name: req.body.sectionName,
    section_description: req.body.sectionDescription,
    sequence_number: req.body.seqNo,
    isdefault: req.body.isDefault
  }
  try {
    const sectionObj = await Section.query().insertGraphAndFetch(sectionDetailsobj);
    if (sectionObj) {
      res.status(201).send({
        success: true,
        message: "Section Created Successfully!",
        section: sectionObj
      })
    } else {
      res.status(500).send({
        error: error.message,
        stack: error.stack
      })
    }
  } catch (error) {
    res.status(400).send({
      error: error.meassge,
      stack: error.stack
    })
  }
});

SectionRoute.delete("/delete-section/:id", tokenMiddleware, async (req, res, next) => {
  const is_section_id = req.params.id;

  let deleted_record = await Section.query().where("id", is_section_id).first();
  try {
    if (deleted_record) {
      let a = await Section.query().where('id', is_section_id).patch({ is_deleted: true, deleted_on: new Date() });
      res.status(200).send({
        success: true,
        message: "Section Deleted Successfully!"
      });
    } else {
      res.status(500).send({
        success: false,
        message: "Section Not Found!"
      });
    }
  } catch (error) {
    res.status(500).send({
      error: error.message,
      stack: error.stack
    });
  }
});

SectionRoute.put("/update-section/:id", tokenMiddleware, async (req, res, next) => {
  const is_section_id = req.params.id;

  const sectionDetailsobj = {
    section_name: req.body.sectionName,
    section_description: req.body.sectionDescription,
    sequence_number: req.body.seqNo,
    isdefault: req.body.isdefault,
  }

  try {
    const sectionById = await Section.query().where("id", is_section_id).first();
    if (sectionById) {
      const sectionObj = await Section.query().patchAndFetchById(is_section_id, sectionDetailsobj);

      if (sectionObj) {
        res.status(200).send({
          success: true,
          message: "Section Updated Successfully!",
          section: sectionObj
        })
      } else {
        res.status(500).send({
          success: false,
          message: "Section Not Found!"
        });
      }
    }
  } catch (error) {
    res.status(500).send({
      error: error.meassge,
      stack: error.stack
    })
  }
});


export default SectionRoute;