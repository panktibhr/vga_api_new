const express = require("express");
const subSectionRoute = express.Router();
const SubSection = require('../models/SubSection.Model').default;
const Question = require('../models/question.Model').default;
const AnswerValues = require('../models/ReportAnswerValue.Model').default;
const tokenMiddleware = require('./../middleware').default;
const { validationResult } = require('express-validator');
import {subsectionbodyvalidate} from '../common/validator';

subSectionRoute.get("/getAll-sub-section", tokenMiddleware, async(req,res,next) => {
    try{
      let subSectionDetailsObj = await SubSection.query().where("is_deleted",false).orderBy('sequence_number');
        
      if(subSectionDetailsObj){
        res.status(200).send({
          success:true,
          message:'get all subSections.',
          res: subSectionDetailsObj
        });
      }
    
    }catch(error){
      res.status(500).send({
        error:error.meassge,
        stack:error.stack
      });
    }
    });

subSectionRoute.post("/", tokenMiddleware, async (req, res, next) => {

    const pageNo = parseInt(req.body.PageNo || 0);
    const PageSiZe = parseInt(req.body.PageSize || 5);
    const sortColumn = req.body.SortColumn;
    const sortOrder = req.body.SortOrder;
    const SearchText = req.body.SearchText;

    const subsectionObj = await SubSection.query()
        .where("is_deleted", false)
        .andWhere('sub_section_master.sub_section_name', 'like', '%' + SearchText + '%')
        .orderBy(sortColumn || 'sub_section_master.sub_section_name', sortOrder || 'ASC')
        .page(pageNo, PageSiZe)
        .withGraphFetched("[section]");

    try {
        if (subsectionObj) {
            res.status(200).send({
                success: true,
                meassge: "Get all Successfully.",
                res: subsectionObj
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

subSectionRoute.get("/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;
    try {
        if (is_id) {
            const subSectionById = await SubSection.query().where("id", is_id).andWhere("is_deleted", false).first().withGraphFetched("[section]");

            if (subSectionById) {
                res.status(200).send({
                    success: true,
                    message: "subSectionById get Successfully",
                    subsection: subSectionById
                })
            }
        } else {
            res.status(400).send({
                error: error.meassge,
                stack: error.stck,
                message: "Id Not Found."
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stck
        });
    }
});

subSectionRoute.post("/add-new-subsection",subsectionbodyvalidate,tokenMiddleware, async(req, res, next) => {
    const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() })
        }
    const subsectionDetailsObj = {
        sub_section_name: req.body.subSectionName,
        sub_section_description: req.body.description,
        sequence_number: req.body.seqNo,
        self_sub_section_id: req.body.selfSubSectionId,
        section_id: req.body.sectionId
    }

    try {

        const subsectionObj = await SubSection.query().insertGraphAndFetch(subsectionDetailsObj);

        if (subsectionObj) {
            res.status(200).send({
                success: true,
                meassge: "Sub Section Created Successfully",
                subsection: subsectionObj
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

subSectionRoute.delete("/delete-subsection/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;

    let deletedRecord = await SubSection.query().where("id", is_id).first();
    try {
        if (deletedRecord) {
            const subsectionById = await SubSection.query().where("id", is_id).patch({ is_deleted: true, deleted_on: new Date() });
            if (subsectionById) {
                res.status(200).send({
                    success: true,
                    meassge: " Deleted Successfully"
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

subSectionRoute.put("/update-subsection/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;
    const subsectionDetailsObj = {
        sub_section_name: req.body.subSectionName,
        sub_section_description: req.body.description,
        sequence_number: req.body.seqNo,
        self_sub_section_id: req.body.selfSubSectionId,
        section_id: req.body.sectionId
    }

    const sectionObjById = await SubSection.query().where("id", is_id).first();

    try {
        if (sectionObjById) {
            const subSectionObj = await SubSection.query().patchAndFetchById(is_id, subsectionDetailsObj);
            if (subSectionObj) {
                res.status(200).send({
                    success: true,
                    meassge: "subsection Updated successfully",
                    subsection: subSectionObj
                })
            }
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

subSectionRoute.post("/color", tokenMiddleware, async (req, res, next) => {
    try {
        let sectionId = req.body.sectionId;
        let auditNo = req.body.vgaAuditNumber;
        let statusColorOfSubSection;
        let statusColorOfSection;
        const subSection = await SubSection.query().where("section_id", sectionId).andWhere('sequence_number', 2).andWhere('is_deleted', false).first()
        const questions = await Question.query().where('sub_section_id', subSection.id).andWhere('is_deleted', false);
        for(let q = 0; q < questions.length; q++){
            const answers = await AnswerValues.query().where('question_id', questions[q].id).andWhere('vga_audit_number', auditNo).andWhere('is_deleted', false);
            console.log(answers);
            if(answers && answers.length > 0){
                statusColorOfSubSection = 'green';
                if(statusColorOfSubSection === 'green'){
                    statusColorOfSection = 'green';
                } else{
                    statusColorOfSection = 'red';
                }
            } else{
                statusColorOfSubSection = 'red';
            }
        }
        let statusColor = {
            'statusColorOfSubSection': statusColorOfSubSection,
            'statusColorOfSection': statusColorOfSection
        }
        if(statusColor){
            res.status(200).send({
                success: true,
                meassge: "subsection Color Updated successfully",
                status: statusColor
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
})

export default subSectionRoute;