const express = require("express");
const ConsultantRoute = express.Router();
const tokenMiddleware = require('./../middleware').default;
const account = require('../models/Account.Model').default;
const users = require("./../models/users.Model").default;
const consultant = require('./../models/consultant.Model').default;
const client = require('./../models/client.Model').default;
const CryptoJS = require("crypto-js");
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
let jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
import {consulatantbodyvalidate} from '../common/validator';
import { updateconsultantvalidate } from '../common/validator';
import roleEnum from './../enums/roleEnum';
import { getUserByEmail } from './CommonController';
import {getaccountliangeBytoken} from './CommonController';
import { raw, ref } from 'objection';

ConsultantRoute.post("/", tokenMiddleware, async (req, res, next) => {
    try {
        let response;
        const result = await getaccountliangeBytoken(req.headers.authorization);

        const pageNo = parseInt(req.body.PageNo || 0);
        const PageSiZe = parseInt(req.body.PageSize || 5);
        const sortColumn = req.body.SortColumn;
        const sortOrder = req.body.SortOrder;
        const SearchText = req.body.SearchText;

        if (result.users.role_id === roleEnum.SUPERADMINROLE) {
            response = await consultant.query().select([
                'consultant.*', client.query()
                .select(raw("array_to_string(array_agg(clients.name), ', ')"))
                .where('clients.consultant', ref('consultant.id'))
                .andWhere("clients.is_deleted", false)
                .as('clientDetails')
            ])
            .where("consultant.is_deleted", false)
            .andWhere("consultant.parent_account_id", "in", result.accounts.map((x) => x.id))
            .andWhere('consultant.name', 'like', '%' + SearchText + '%')
            .orderBy(sortColumn || 'consultant.name', sortOrder || 'ASC')
            .page(pageNo, PageSiZe);
        } else {
            response = await consultant.query().select([
                'consultant.*',
                client.query()
                .select(raw("array_to_string(array_agg(clients.name), ', ')"))
                .where('clients.consultant', ref('consultant.id'))
                .andWhere("clients.is_deleted", false)
                .as('clientDetails')
            ])
            .where("consultant.is_deleted", false)
            .andWhere("consultant.parent_account_id", "in", result.accounts.map((x) => x.id))
            .andWhere('consultant.name', 'like', '%' + SearchText + '%')
            .orderBy(sortColumn || 'consultant.name', sortOrder || 'ASC')
            .page(pageNo, PageSiZe);
        }

        if (response) {
            res.status(200).send({
                success: true,
                meassge: "Get All Consultant Successfully",
                res: response
            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

ConsultantRoute.get("/get-consultant", tokenMiddleware, async (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    
    try {
        if (token) {
            const consultantByEmail = await consultant.query()
            .innerJoin('clients', 'clients.consultant', 'consultant.id')
            .select('consultant.*', 'clients.id as clientId')
            .where('consultant.email', decodedToken.email).first();

            if (consultantByEmail) {
                res.status(200).send({
                    success: true,
                    meassge: 'get consultant successfully.',
                    res: consultantByEmail
                });

            } else {
                res.status(500).send({
                    error: error.meassge,
                    stack: error.stack
                });
            }
        } else {
            res.send({
                message: 'token invalid..'
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }

});

ConsultantRoute.get("/:id", tokenMiddleware, async (req, res, next) => {
    let is_id = req.params.id;
    try {
        if (is_id) {
            const consultantById = await consultant.query().where("id", is_id).andWhere("is_deleted", false).first();

            if (consultantById) {
                res.status(200).send({
                    success: true,
                    message: "ConsultantById Got Successfully",
                    res: consultantById
                })
            }
        } else {
            res.status(400).send({
                error: error.meassge,
                stack: error.stck,
                message: "Id Not Found."
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stck
        });

    }
});

ConsultantRoute.post("/add-consultant",consulatantbodyvalidate, tokenMiddleware, async (req, res, next) => {
    let email = req.body.email;
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    const pass = CryptoJS.AES.encrypt(req.body.password, process.env.SECRET).toString();
    const consultantDetailsObj = {
        name: req.body.consultantName,
        email: req.body.email,
        address: req.body.address,
        countries_id: req.body.country,
        states_id: req.body.state,
        city_id: req.body.city,
        zip: req.body.zipCode,
        mobile: req.body.mobile,
        company_name: req.body.companyName,
        password: pass,
        username: req.body.username,
        parent_account_id: req.body.parent_account_id
    }

    const userdata = {
        password: pass,
        role_id: roleEnum.CONSULTANTROLE,
        username: consultantDetailsObj.username,
        email: consultantDetailsObj.email,
        parent_account_id: consultantDetailsObj.parent_account_id
    }
    
    try {
        if (consultantDetailsObj) {

            const consultantObj = await consultant.query().insertGraphAndFetch(consultantDetailsObj);
            const userobjEntry = await users.query().insertGraphAndFetch(userdata);

            if (consultantObj && userobjEntry) {
                const passDecrepted = CryptoJS.AES.decrypt(userobjEntry.password, process.env.SECRET).toString(CryptoJS.enc.Utf8);
                let smtpTransport = nodemailer.createTransport({
                    type: 'OAuth2',
                    host: "smtp.gmail.com",
                    port: 587,
                    secure: false,
                    service: 'gmail', // true for 465, false for other ports
                    auth: {
                        user: process.env.AUTH_EMAIL,
                        pass: process.env.AUTH_PASSWORD,
                    },
                    tls: true,
                });
                let info = await smtpTransport.sendMail({
                    from: process.env.AUTH_EMAIL, // sender address
                    to: userobjEntry.email, // list of receivers
                    subject: "VGA - Temporary credentials", // Subject line
                    text: "Temporary Password With Login.",
                    html: generateHTMLInquiryEmail(userobjEntry.email, passDecrepted)
                });
                res.status(200).send({
                    success: true,
                    message: "Consultant created successfully & Please! check your mail.",
                    res: consultantObj
                })
            } else {
                res.status(500).send({
                    success: false,
                    message: "Something MissMatch With Database Fields"
                })
            }
        } else {
            res.send(400).send({

            })
        }
    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
})

ConsultantRoute.delete("/delete-consultant/:id", tokenMiddleware, async (req, res, next) => {
    const is_consultant_id = req.params.id;

    let deleted_record = await consultant.query().where("id", is_consultant_id).first();
    try {
        if (deleted_record) {
            let _consultant = await consultant.query().where('id', is_consultant_id).patch({ is_deleted: true, deleted_on: new Date() });
            const _getUserByEmail = await getUserByEmail(deleted_record.email);
            if (_getUserByEmail !== null) {
                let _user = await users.query().where('id', _getUserByEmail.id).patch({ is_deleted: true, deleted_on: new Date() });
            }
            if (_consultant) {
                res.status(200).send({
                    success: true,
                    message: "deleted Successfully."
                });
            }
        } else {
            res.status(500).send({
                success: false,
                message: "consultant Not Found."
            });
        }
    } catch (error) {
        res.status(500).send({
            error: error.message,
            stack: error.stack
        });
    }
})

ConsultantRoute.put("/update-consultant/:id", updateconsultantvalidate,tokenMiddleware, async (req, res, next) => {
    const is_consultant_id = req.params.id;

    const consultantDetailsObj = {
        name: req.body.consultantName,
        email: req.body.email,
        address: req.body.address,
        countries_id: req.body.country,
        states_id: req.body.state,
        city_id: req.body.city,
        zip: req.body.zipCode,
        mobile: req.body.mobile,
        company_name: req.body.companyName,
        username: req.body.username,
        parent_account_id: req.body.parent_account_id
    }

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }

    let consultantById = await consultant.query().where("id", is_consultant_id).first();

    try {
        if (consultantById) {
            const consulatntobj = await consultant.query().patchAndFetchById(is_consultant_id, consultantDetailsObj);
            const _getUserByEmail = await getUserByEmail(consultantDetailsObj.email);
            if (_getUserByEmail !== null) {
                let _user = {
                    role_id: roleEnum.CONSULTANTROLE,
                    username: consultantDetailsObj.username,
                    email: consultantDetailsObj.email,
                    parent_account_id: req.body.parent_account_id,
                    name: consultantDetailsObj.name
                }
                await users.query().patchAndFetchById(_getUserByEmail.id, _user);
            }
            if (consulatntobj) {
                res.status(200).send({
                    success: true,
                    message: "Update Consultant Successfully",
                    res: consulatntobj
                })
            }
        }

    } catch (error) {
        res.status(500).send({
            error: error.meassge,
            stack: error.stack
        })
    }
});

function generateHTMLInquiryEmail(email, password) {
    return ` 
    <!doctype html>
  <html>
  <head>
  <meta charset="utf-8">
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
  <title>Welcome to VGA</title>
  <style type="text/css"></style>
  </head>
  
  <body bgcolor="#FFF" style="margin: 0;min-width: 100%;height: 100%;padding:0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
  <table align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
  <tr>
    <td bgcolor="#FFF" align="center" height="100%" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if gte mso 9]>
    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
    <tr>
    <td align="center" valign="top">
    <![endif]-->
      
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="max-width: 600px;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tr>
          <td align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <tr>
                <td id="pre-header" valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:20px;padding-bottom:20px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tr>
                      <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; line-height: 25%;">
                <!-- Partner Branding: Start -->
                  <!-- Partner Branding: End -->
                </td>
                          </tr>
                        </table></td>
                      <td align="left" valign="top" width="255" style="max-width:255px; padding-right: 0;padding-left: 0;padding-top:0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right:15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: right;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Message: Start -->
                
                  <!-- Message: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
              <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              <table style="width:100%;">
                     <tr>
                       <td style="width:15%;">
                        
                      </td>
                       <td style="width:1%;"><img src="https://image.freepik.com/free-vector/welcome-word-flat-cartoon-people-characters_81522-4207.jpg" alt="Welcome" style="width:200px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"></td>
                       <td style="width:15%;"></td>
                     </tr>
                   </table>
              </td>
             </tr>
        <tr>
          <td id="content" bgcolor="#FFFFFF" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
      <table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom:15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Main Headline: Start -->
                
                <!-- <h1 style="font-family: Helvetica, Arial, sans-serif;font-size:36px;line-height: 40px; font-weight: normal; text-align: center; color:#2F5597; margin:0;">Welcome!</h1> -->
                
                  <!-- Main Headline: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
          <tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 15px;padding-left: 15px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:15px;padding-bottom:30px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Content Area: Start -->
                
                <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;">
                  Welcome to VGA, Please use these temporary credentials to sign in and update your password.
                  <br/>
                  <br/>
                  <table>
                    <tr>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Email Id:</td>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${email}</td>
                    </tr>
                    <tr>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">Password:</td>
                      <td style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;" align="left">${password}</td>
                    </tr>
                  </table>
                </p>
                
        <br/>                 
                  <!-- Content Area: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
          
  <!-- 					<tr>
                <td align="center" valign="top" width="100%" style="padding-top:0;padding-right: 0;padding-bottom: 0;padding-left: 10;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" style="padding-right: 0;padding-left: 0; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                           <tr>
                      <td align="center" valign="top" style="padding-top: 0;padding-bottom: 0;padding-right: 0px;padding-left: 0px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <img src="https://dev.localbox.net/static/email/footer-ilocalbox-gradient-bar.jpg" alt="" width="600" style="max-width:600px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0">
            
            </td>
                    </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr> -->
            </table></td>
        </tr>
       <tr>
          <td id="footer" align="center" valign="top" width="100%" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td align="center" valign="top" width="100%" style="padding-top:15px;padding-right: 15px;padding-bottom: 30px;padding-left: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="left" valign="top" width="350" style="max-width: 350px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top:0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Footer iLocal Box Copyright: Start -->
                <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0; margin-bottom: 18px;">&copy; iLocal Box, All Rights Reserved</p> -->
                <!-- Footer iLocal Box Copyright: End -->
                <!-- Footer Partner Info: Start -->
                              
                <!-- Footer Partner Info: End -->
                          </tr>
                        </table></td>
                      <td align="left" valign="top" width="160" style="max-width: 160px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tr>
                            <td valign="top" style="padding-right: 15px;padding-left: 15px;padding-top: 0;padding-bottom: 0;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!-- Footer Powered by iLocal Box: Start -->
                <!-- <p style="font-family: Helvetica, Arial, sans-serif;font-size: 11px;line-height: 14px; font-weight: normal; text-align: left; color:#819695; margin:0;margin-bottom: 5px;"><em>Powered by:</em></p>
                              <img src="https://dev.localbox.net/static/email/logo-ilocalbox.png" alt="iLocal Box" width="110" height="19" style="max-width: 110px; margin:0; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" align="center" border="0"> -->
                <!-- Footer Powered by iLocal Box: End -->
                </td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>  
            </table></td>
        </tr>
      </table>
      
      <!--[if gte mso 9]>
    </td>
    </tr>
    </table>
    <![endif]--></td>
  </tr>
  </table>
  </body>
  </html>
     `;
}

export default ConsultantRoute;