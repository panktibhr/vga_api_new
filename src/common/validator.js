const {body} = require('express-validator');
const users = require("./../models/users.Model").default;

// const validate = (columanValue) => {
//     let errors = [];
//     if (columanValue === null || columanValue === 'null' || columanValue === undefined || columanValue === 'undefined' || columanValue === '') {
//         let errorObject = {
//             status: 400,
//             message: `${columanValue} is required!`
//         }
//         errors.push(errorObject)
//     }
//     return errors;
// }


export const accountbodyvalidate = [
    body('name').notEmpty().withMessage('name is required.').isString().isLength({ min: 3 }).withMessage('must be at last 3 chars long.'),
    body('description').optional().isString(),
    body('email').isEmail().normalizeEmail().custom(value =>{
        return users.query().where('email',value).andWhere('is_deleted', false).first().then(res => {
            if(res){
                throw new Error('Email is already exist.');
            }
        return value; 
        })
    }),
    body('phone').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body('parent_account_id').notEmpty().withMessage('parent_account_id  is required.').isUUID().withMessage('parent_account_id must be uuid.')
];

export const updateaccountvalidate = [
    body('name').notEmpty().withMessage('name is required.').isString().isLength({ min: 3 }).withMessage('must be at last 3 chars long.'),
    body('description').optional().isString(),
    body('email').optional(),
    body('phone').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body('parent_account_id').optional(),
];

 export const  auditbodyvalidate =[
    body('clientId').notEmpty().withMessage('clientid is required.').isUUID().withMessage('clientId must be uuid.'),
    body('userId').notEmpty().withMessage('userid is required.').isUUID().withMessage('userid must be uuid.'),
    body('fromDate').notEmpty().withMessage('from date is required.'),
    body('toDate').notEmpty().withMessage('to date is required.')
];

export const citybodyvalidate =[
    body('cityName').notEmpty().withMessage('cityname is required.').isString(),
    body('stateId').notEmpty().withMessage('state id is required.').isUUID().withMessage('state id must be uuid.')
];

export const countrybodyvalidate = [
    body('countryName').notEmpty().withMessage('country name is required.').isString()
];

export const statebodyvalidate = [
    body('stateName').notEmpty().withMessage('state name is required.').isString(),
    body('countryId').notEmpty().withMessage('country id is required.').isUUID().withMessage('country id must be uuid.')
];

export const clientbodyvalidate =[
    body('clientName').notEmpty().withMessage('client name is required.').isString().isLength({ min: 3 }).withMessage('client name must be at last 3 chars long.'),
    body('address').notEmpty().withMessage('address is required.'),
    body('email').isEmail().normalizeEmail().custom(value =>{
        return users.query().where('email',value).andWhere('is_deleted', false).first().then(res => {
            if(res){
                throw new Error('Email is already exist.');
            }
           return value; 
        })
    }),
    body('mobileNumber').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body ('zip').notEmpty().withMessage('zip is required.').isLength(6).withMessage('must be 6 digits.'),
    body('username').notEmpty().withMessage('username is required.').isLength({ min: 3 }).withMessage('must be at last 3 chars long.').custom(username =>{
        return users.query().where('username',username).andWhere('is_deleted',false).first().then(res =>{
            if(res){
                throw new Error('username is already exist.');
            }
            return username;
        })
    }),
    body('clientLogo').optional().isBase64().withMessage('logo must be sent in base64 formate.'),
    body('companyName').notEmpty().withMessage('companyName is required.'),
    body('parent_account_id').notEmpty().withMessage('parent_account_id  is required.').isUUID().withMessage('parent_account_id must be uuid.'),
    body('state').notEmpty().withMessage('state id is required.').isUUID().withMessage('state id must be uuid.'),
    body('city').notEmpty().withMessage('city id is required.').isUUID().withMessage('city id must be uuid.'),
    body('country').notEmpty().withMessage('country id is required.').isUUID().withMessage('country id must be uuid.'),
    
    body('contactPerson').notEmpty().withMessage('contact Person name is required.').isString(),
    body('contactPersonDesignation').notEmpty().withMessage('contact Person Designation is required.'),
    body('contactPersonPhone').notEmpty().withMessage('contact Person Phone is required.').isMobilePhone(),
    body('contactPersonEmail').notEmpty().withMessage('contact Person email is required.').isEmail().normalizeEmail(),

    body('chiefPerson').notEmpty().withMessage('chief Person name is required.').isString(),
    body('chiefPersonDesignation').notEmpty().withMessage('chief Person Designation is required.'),
    body('chiefPersonPhone').notEmpty().withMessage('chief Person Phone is required.').isMobilePhone(),
    body('chiefPersonEmail').notEmpty().withMessage('chief Person Email is required.').isEmail().normalizeEmail(),

    body('industryType').notEmpty().withMessage('industryType is required.').isString(),
    body('marketType').notEmpty().withMessage('marketType is required.').isString(),
    body('customerType').notEmpty().withMessage('customerType is required.').isString(),
    body('ownershipType').notEmpty().withMessage('ownershipType is required.').isString(),
    body('processType').notEmpty().withMessage('processType is required.').isString(),

    body('businessNature').notEmpty().withMessage('businessNature is required.').isString(),
    body('businessScale').notEmpty().withMessage('businessScale is required.').isString(),

    body('clientCreationDate').notEmpty().withMessage('client Creation Date is required.').isDate(),
    body('renewalDate').notEmpty().withMessage('renewal Date is required.').isDate(),
    body('clientDuration').notEmpty().withMessage('clientDuration is required.'),
    body('clientType').notEmpty().withMessage('client Type is required.'),
    body('companyName').notEmpty().withMessage('company Name is required.'),

    body('coreCompetence').notEmpty().withMessage('coreCompetence is required.').isString(),
    body('coreDrivingForce').notEmpty().withMessage('coreDrivingForce is required.').isString(),

    body('intensity').notEmpty().withMessage('intensity is required.').isString(),
    body('minorCompetence').notEmpty().withMessage('minorCompetence is required.').isString(),
    body('minorDrivingForce').notEmpty().withMessage('minorDrivingForce is required.').isString(),
    body('moderateCompetence').notEmpty().withMessage('moderateCompetence is required.').isString(),
    body('moderateDrivingForce').notEmpty().withMessage('moderateDrivingForce is required.').isString(),

    body('profitabilityStatus').notEmpty().withMessage('profitabilityStatus is required.').isString(),
    body('website').notEmpty().withMessage('website is required.').isString(),

    body('employees').optional().isUUID().withMessage('employee id must be uuid.'),
    body('consultant').optional().isUUID().withMessage('consultant id must be uuid.'),
    body('vgaAuditor').optional().isUUID().withMessage('auditor id must be uuid.'),
    
];

export const updateclientvalidate = [
    body('clientName').notEmpty().withMessage('client name is required.').isString().isLength({ min: 3 }).withMessage('client name must be at last 3 chars long.'),
    body('email').optional(),
    body('mobileNumber').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body('username').optional(),
    body('parent_account_id').optional(),
];

export const employeebodyvalidate =[
    body('empName').notEmpty().withMessage('employee name is required.').isString().isLength({ min: 3 }).withMessage('employee name must be at last 3 chars long.'),
    body('address').notEmpty().withMessage('address is required.'),
    body('email').isEmail().normalizeEmail().custom(value =>{
        return users.query().where('email',value).andWhere('is_deleted', false).first().then(res => {
            if(res){
                throw new Error('Email is already exist.');
            }
           return value; 
        })
    }),
    body('mobileNumber').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body ('zipCode').notEmpty().withMessage('zip is required.').isLength(6).withMessage('must be 6 digits.'),
    body('username').notEmpty().withMessage('username is required.').isLength({ min: 3 }).withMessage('must be at last 3 chars long.').custom(username =>{
        return users.query().where('username',username).andWhere('is_deleted',false).first().then(res =>{
            if(res){
                throw new Error('username is already exist.');
            }
            return username;
        })
    }),
    body('reportingTo').optional(),
    body('empTitle').notEmpty().withMessage('employee title is required.'),
    body('empLevel').notEmpty().withMessage('employee level is required.'),
    body('designation').notEmpty().withMessage('employee designation is required.'),
    body('deptDiv').notEmpty().withMessage('employee department/division is required.'),
    body('parent_account_id').notEmpty().withMessage('parent_account_id  is required.').isUUID().withMessage('parent_account_id must be uuid.'),
    body('state').notEmpty().withMessage('state id is required.').isUUID().withMessage('state id must be uuid.'),
    body('city').notEmpty().withMessage('city id is required.').isUUID().withMessage('city id must be uuid.'),
    body('country').notEmpty().withMessage('country id is required.').isUUID().withMessage('country id must be uuid.'),
];

export const updateemployeevalidate = [
    body('empName').notEmpty().withMessage('employee name is required.').isString().isLength({ min: 3 }).withMessage('employee name must be at last 3 chars long.'),
    body('address').notEmpty().withMessage('address is required.'),
    body('email').optional(),
    body('mobileNumber').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body ('zipCode').notEmpty().withMessage('zip is required.').isLength(6).withMessage('must be 6 digits.'),
    body('username').optional(),
    body('reportingTo').optional(),
    body('empTitle').notEmpty().withMessage('employee title is required.'),
    body('empLevel').notEmpty().withMessage('employee level is required.'),
    body('designation').notEmpty().withMessage('employee designation is required.'),
    body('deptDiv').notEmpty().withMessage('employee department/division is required.'),
    body('parent_account_id').notEmpty().withMessage('parent_account_id  is required.').isUUID().withMessage('parent_account_id must be uuid.'),
    body('state').notEmpty().withMessage('state id is required.').isUUID().withMessage('state id must be uuid.'),
    body('city').notEmpty().withMessage('city id is required.').isUUID().withMessage('city id must be uuid.'),
    body('country').notEmpty().withMessage('country id is required.').isUUID().withMessage('country id must be uuid.'),
];

export const consulatantbodyvalidate = [
    body('consultantName').notEmpty().withMessage('consultant name is required.').isString().isLength({ min: 3 }).withMessage('consultant Name must be at last 3 chars long.'),
    body('address').notEmpty().withMessage('address is required.'),
    body('email').isEmail().normalizeEmail().custom(value =>{
        return users.query().where('email',value).andWhere('is_deleted', false).first().then(res => {
            if(res){
                throw new Error('Email is already exist.');
            }
           return value; 
        })
    }),
    body('mobile').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body ('zipCode').notEmpty().withMessage('zip is required.').isLength(6).withMessage('must be 6 digits.'),
    body('username').notEmpty().withMessage('username is required.').isLength({ min: 3 }).withMessage('must be at last 3 chars long.').custom(username =>{
        return users.query().where('username',username).andWhere('is_deleted',false).first().then(res =>{
            if(res){
                throw new Error('username is already exist.');
            }
            return username;
        })
    }),
    body('companyName').notEmpty().withMessage('company Name is required.'),
    body('parent_account_id').notEmpty().withMessage('parent_account_id  is required.').isUUID().withMessage('parent_account_id must be uuid.'),
    body('state').notEmpty().withMessage('state id is required.').isUUID().withMessage('state id must be uuid.'),
    body('city').notEmpty().withMessage('city id is required.').isUUID().withMessage('city id must be uuid.'),
    body('country').notEmpty().withMessage('country id is required.').isUUID().withMessage('country id must be uuid.'),
];

export const updateconsultantvalidate = [
    body('consultantName').notEmpty().withMessage('consultant name is required.').isString().isLength({ min: 3 }).withMessage('consultant Name must be at last 3 chars long.'),
    body('mobile').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body('email').optional(),
    body('username').optional()
];

export const clientEmployeebodyvalidate = [
    body('clientEmpName').notEmpty().withMessage('clientemployee name is required.').isString().isLength({ min: 3 }).withMessage('employee name must be at last 3 chars long.'),
    body('address').notEmpty().withMessage('address is required.'),
    body('email').isEmail().normalizeEmail().custom(value =>{
        return users.query().where('email',value).andWhere('is_deleted', false).first().then(res => {
            if(res){
                throw new Error('Email is already exist.');
            }
           return value; 
        })
    }),
    body('mobileNumber').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body ('zipCode').notEmpty().withMessage('zip is required.').isLength(6).withMessage('must be 6 digits.'),
    body('username').notEmpty().withMessage('username is required.').isLength({ min: 3 }).withMessage('must be at last 3 chars long.').custom(username =>{
        return users.query().where('username',username).andWhere('is_deleted',false).first().then(res =>{
            if(res){
                throw new Error('username is already exist.');
            }
            return username;
        })
    }),
    body('clientEmpTitle').notEmpty().withMessage('client employee title is required.'),
    body('clientEmpLevel').notEmpty().withMessage('client employee level is required.'),
    body('designation').notEmpty().withMessage('clientemployee designation is required.'),
    body('deptdiv').notEmpty().withMessage('clientemployee department/division is required.'),
    body('client_id').notEmpty().withMessage('client id is required.'),
    body('companyName').notEmpty().withMessage('company Name is required.'),
    body('parent_account_id').notEmpty().withMessage('parent_account_id  is required.').isUUID().withMessage('parent_account_id must be uuid.'),
    body('state').notEmpty().withMessage('state id is required.').isUUID().withMessage('state id must be uuid.'),
    body('city').notEmpty().withMessage('city id is required.').isUUID().withMessage('city id must be uuid.'),
    body('country').notEmpty().withMessage('country id is required.').isUUID().withMessage('country id must be uuid.'),
];

export const updateclientemployeevalidate = [
    body('clientEmpName').notEmpty().withMessage('client name is required.').isString().isLength({ min: 3 }).withMessage('client name must be at last 3 chars long.'),
    body('email').optional(),
    body('mobileNumber').isMobilePhone().isLength(10).withMessage('must be 10 digits.'),
    body('username').optional(),
    body('parent_account_id').optional(),
];

export const userbodyvalidate = [
    body('name').notEmpty().withMessage('name is required.').isString().isLength({ min: 3 }).withMessage('must be at last 3 chars long.'),
    body('email').isEmail().normalizeEmail().custom(value =>{
        return users.query().where('email',value).andWhere('is_deleted',false).first().then(res => {
            if(res){
                throw new Error('Email is already exist.');
            }
           return value; 
        })
    }),
    body('username').notEmpty().withMessage('username is required.').isLength({ min: 3 }).withMessage('must be at last 3 chars long.').custom(username =>{
        return users.query().where('username',username).andWhere('is_deleted',false).first().then(res =>{
            if(res){
                throw new Error('username is already exist.');
            }
            return username;
        })
    }),
    body('role').notEmpty().withMessage('role id is required.').isUUID().withMessage('role id must be uuid.'),
    body('parent_account_id').notEmpty().withMessage('parent_account_id  is required.').isUUID().withMessage('parent_account_id must be uuid.')
];

export const updateuservalidate = [
    body('name').notEmpty().withMessage('name is required.').isString().isLength({ min: 3 }).withMessage('must be at last 3 chars long.'),
    body('username').optional(),
    body('parent_account_id').notEmpty().withMessage('parent_account_id  is required.').isUUID().withMessage('parent_account_id must be uuid.')
];

export const sectionbodyvalidate = [
    body('sectionName').notEmpty().withMessage('section Name is required.'),
    body('sectionDescription').notEmpty().withMessage('Description is required.'),
    body('seqNo').notEmpty().withMessage('sequence number is required.'),
    body('isDefault').optional()
];

export const subsectionbodyvalidate = [
    body('subSectionName').notEmpty().withMessage('sub section Name is required.'),
    body('description').notEmpty().withMessage('Description is required.'),
    body('seqNo').notEmpty().withMessage('sequence number is required.'),
    body('selfSubSectionId').optional(),
    body('sectionId').notEmpty().withMessage('section id is required.').isUUID().withMessage('section id must be uuid.')
];
